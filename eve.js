/*globals define, module*/

(function(context, eve) {
	"use strict";
	if (typeof exports === "object") {
		// Node. Does not work with strict CommonJS, but only CommonJS-like
		// environments that support module.exports, like Node.
		module.exports = eve();
	} else {
		if (typeof define === "function" && define.amd) {
			// RequireJS | AMD
			define([], function() {
				// publish eve to the global namespace for backwards compatibility and define it as an AMD module
				context.eve = eve();
				return context.eve;
			});
		} else {
			// No AMD, publish eve to the global namespace
			if (context === undefined && typeof window !== "undefined"){
				context = window;
			}
			context.eve = eve();
		}
	}

}(this, function() {
	"use strict";
	var eve = {
		version: "0.0.1"
	};

	eve.__elms = [];

	//global vars
	var resizeElms = [];
//	var eveGrads = [];
	var gradNum = 0;
	var hatchNum = 0;
	eve.lGrads = [];
	eve.rGrads = [];
	eve.hatches = [];

	//CONSTs
	var HANDLE_FACTOR = 0.552284749831; //for approximating arcs with beziers
	//SVG Transform types:
	var SVG_TRANSFORM_UNKNOWN = 0; //The unit type is not one of predefined unit types. It is invalid to attempt to define a new value of this type or to attempt to switch an existing value to this type.
	var SVG_TRANSFORM_MATRIX = 1; //A matrix(…) transformation
	var SVG_TRANSFORM_TRANSLATE = 2; //A translate(…) transformation
	var SVG_TRANSFORM_SCALE = 3; //A scale(…) transformation
	var SVG_TRANSFORM_ROTATE = 4; //A rotate(…) transformation
	var SVG_TRANSFORM_SKEWX = 5; //A skewx(…) transformation
	var SVG_TRANSFORM_SKEWY = 6; //A skewy(…) transformation

	var icoShapes = [
		{id: "gift", path: "M25,8.6 C25,7.2,23.9,6,22.5,6 c0,0-2.5,0-5.8,0 c1.7,0,3.1-1.3,3.1-2.9 c0-1.6-1.4-2.9-3.1-2.9 c-1.2,0-2.3,0.6-2.8,1.6 L12.5,4 l-1.4-2.2 c-0.5-1-1.6-1.6-2.8-1.6 c-1.7,0-3.1,1.3-3.1,2.9 C5.2,4.7,6.6,6,8.3,6 C5,6,2.5,6,2.5,6 C1.1,6,0,7.2,0,8.6 c0,0,0,13.8,0,13.8 C0,23.8,1.1,25,2.5,25 c0,0,20,0,20,0 c1.4,0,2.5-1.2,2.5-2.6 C25,22.4,25,8.6,25,8.6z M15.6,2.8 c0.2-0.3,0.6-0.6,1.1-0.6 c0.6,0,1.1,0.4,1.1,0.9 c0,0.5-0.5,0.9-1.1,0.9 h-1.8L15.6,2.8z M7.2,3.1 c0-0.5,0.5-0.9,1.1-0.9 c0.5,0,0.9,0.2,1.1,0.6 L10.2,4 H8.3 C7.7,4,7.2,3.6,7.2,3.1z M2.5,8.5 h7 v14 h-7 V8.5z M22.5,22.5 h-7 v-14 h7 V22.5z", height:25, width: 25},
		{id: "cart", path: "M7.5,20C6.1,20,5,21.1,5,22.5S6.1,25,7.5,25s2.5-1.1,2.5-2.5S8.9,20,7.5,20z M0,0v2.5h2.5L7,12L5.3,15C5.1,15.4,5,15.8,5,16.3c0,1.4,1.1,2.5,2.5,2.5h15v-2.5H8c-0.2,0-0.3-0.1-0.3-0.3c0,0,0,0,0,0l0-0.1l1.1-2 h9.3c0.9,0,1.7-0.5,2.2-1.3l4.5-8.1C25,4.2,25,4,25,3.8c0-0.7-0.6-1.2-1.3-1.3H5L4,0H0z M20,20c-1.4,0-2.5,1.1-2.5,2.5 S18.6,25,20,25s2.5-1.1,2.5-2.5S21.4,20,20,20z", width: 25, height: 25},
		{id: "spending", path: "M12,18.6l5.2,1.6v0.1c-0.1,0.4-0.4,0.6-0.9,0.7c-1.7,0.3-4.6,0.1-4.6,0.1c-0.4,0-0.8,0.3-0.8,0.7c0,0.4,0.3,0.8,0.7,0.8 c0.1,0,3,0.2,4.9-0.1c1.9-0.3,2.4-2,2.3-3c0-0.3-0.3-0.6-0.6-0.7L12.4,17c-2.9-0.9-6.1,0.7-6.2,0.8l0,0l-5.8,3.5 C0,21.5-0.1,22,0.1,22.4c0.2,0.2,0.4,0.4,0.7,0.4c0.1,0,0.3,0,0.4-0.1L7,19.3C7.3,19.2,9.8,17.9,12,18.6z M31.9,17.3c-0.5-0.8-1.9-2.2-4.1-1.2c-2,0.9-6.4,3-6.4,3c-0.4,0.2-0.6,0.7-0.4,1.1s0.7,0.6,1.1,0.4c0,0,4.3-2.1,6.4-3 c0.6-0.3,1.1-0.3,1.5,0l0,0l-10.8,7.9c-0.8,0.6-1.7,0.8-2.7,0.7l-9.7-1.5c-0.2,0-0.5,0-0.6,0.2l-2.4,2c-0.3,0.3-0.4,0.8-0.1,1.1 c0.2,0.2,0.4,0.3,0.6,0.3s0.4-0.1,0.5-0.2L7,26.3l9.3,1.5c1.4,0.2,2.8-0.1,3.9-0.9l11.6-8.5C32,18.1,32.1,17.6,31.9,17.3z M17,17c1.3,0,2.6-0.5,3.5-1.5c2-2,2-5.1,0-7.1C19.6,7.5,18.3,7,17,7s-2.6,0.5-3.5,1.5c-2,2-2,5.1,0,7.1 C14.4,16.5,15.7,17,17,17z M14.7,9.7c0.6-0.6,1.4-1,2.3-1s1.7,0.3,2.3,1c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1s-1.7-0.3-2.3-1 C13.4,13,13.4,11,14.7,9.7z M22,8c1.1,0,2.1-0.4,2.8-1.2c1.6-1.6,1.6-4.1,0-5.7C24.1,0.4,23.1,0,22,0s-2.1,0.4-2.8,1.2c-1.6,1.6-1.6,4.1,0,5.7 C19.9,7.6,20.9,8,22,8z M20.4,2.4c0.4-0.4,1-0.7,1.6-0.7s1.2,0.2,1.6,0.7c0.9,0.9,0.9,2.3,0,3.2c-0.4,0.4-1,0.7-1.6,0.7 S20.8,6,20.4,5.6C19.5,4.7,19.5,3.3,20.4,2.4z", width: 32, height: 29},
		{id:"investments", path: "M14.1,6.6c2.3-1.9,3.8-6.2,2.9-6.4c-1.2-0.3-4,0.8-5.3,1c-1.9,0.2-3.9-2-5-0.8c-0.9,1,0.7,4.7,3.1,6.3 C2.4,10.3-8,28.6,10.1,29.9C35.2,31.8,22.7,10.2,14.1,6.6z M15.4,23.7H7.8v-1.4c0.5-0.3,1-0.6,1.3-1.1s0.5-1,0.5-1.5 c0-0.3,0-0.5,0-0.7H8v-1.8h1.3c-0.1-0.5-0.1-0.9-0.1-1.3c0-1.1,0.3-2,1-2.6c0.7-0.7,1.6-1,2.7-1c0.8,0,1.5,0.1,2,0.4l-0.4,2 c-0.4-0.2-0.9-0.3-1.4-0.3s-0.9,0.1-1.2,0.4c-0.3,0.3-0.4,0.7-0.4,1.2c0,0.3,0,0.7,0.1,1.2h2.3v1.9h-2.1c0,0.5-0.1,0.9-0.2,1.3 s-0.4,0.7-0.8,1.1l0,0h4.4v2.2H15.4z", width:25, height:30},
		{id: "emergency", path: "M15,0L0,26h30L15,0z M17,23h-4v-4h4V23z M13,17v-7h4v7H13z", width: 30, height: 26},
		{id: "drawdown", path: "M24,0H2C0.9,0,0,0.9,0,2v22c0,1.1,0.9,2,2,2h22c1.1,0,2-0.9,2-2V2C26,0.9,25.1,0,24,0z M7,23H4V3h3V23z M14,23h-3V9h3V23z M21,23h-3v-8h3V23z", width: 26, height: 26},
		{id: "debt", path: "M30,0H2C0.9,0,0,0.9,0,2v22c0,1.1,0.9,2,2,2h28c1.1,0,2-0.9,2-2V2C32,0.9,31.1,0,30,0z M30,2v4H2V2H30z M2,24V12h28v12H2z M4,20h4v2H4V20z M10,20h6v2h-6V20z", width: 32, height: 26},
		{id: "cash", path: "M24.5,12C24.5,12,24.5,12,24.5,12c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0 c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0-0.1-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0-0.1,0-0.1-0.1L13.4,5.4l6.1-3.9 l10.3,5.9l-4.3,2.7c-0.3,0.2-0.4,0.7-0.2,1c0.2,0.3,0.7,0.4,1,0.2L31.7,8C31.9,7.9,32,7.6,32,7.4c0-0.3-0.1-0.5-0.4-0.6L19.9,0.1 c-0.2-0.1-0.5-0.1-0.7,0l-7.5,4.7c0,0,0,0,0,0L7.9,7.2c0,0,0,0,0,0L0.3,12C0.1,12.1,0,12.3,0,12.6c0,0.3,0.1,0.5,0.4,0.6l11.7,6.7 c0.1,0.1,0.2,0.1,0.4,0.1c0.1,0,0.3,0,0.4-0.1l5.4-3.4c0.3-0.2,0.4-0.7,0.2-1c-0.2-0.3-0.7-0.4-1-0.2l-5,3.1L2.1,12.5l6.1-3.9 l11,6.2v11.3c0,0.3,0.1,0.5,0.4,0.6c0.1,0.1,0.2,0.1,0.3,0.1c0.1,0,0.3,0,0.4-0.1l3.8-2.4c0.2-0.1,0.3-0.4,0.3-0.6L24.5,12 C24.5,12.1,24.5,12.1,24.5,12C24.5,12.1,24.5,12.1,24.5,12z M20.7,24.9V14.5c0-0.3-0.1-0.5-0.4-0.6L9.7,7.8L12,6.3l10.3,5.9 l-0.9,0.6c-0.3,0.2-0.4,0.7-0.2,1c0.1,0.2,0.4,0.3,0.6,0.3c0.1,0,0.3,0,0.4-0.1l0.8-0.5v10L20.7,24.9z M30.9,10.7L25.5,14c-0.3,0.2-0.4,0.7-0.2,1c0.1,0.2,0.4,0.3,0.6,0.3c0.1,0,0.3,0,0.4-0.1l5.4-3.4c0.3-0.2,0.4-0.7,0.2-1 C31.7,10.6,31.2,10.5,30.9,10.7z M17.4,19.2l-5,3.1L1.1,15.8c-0.3-0.2-0.8-0.1-1,0.3c-0.2,0.3-0.1,0.8,0.3,1l11.7,6.7c0.1,0.1,0.2,0.1,0.4,0.1 c0.1,0,0.3,0,0.4-0.1l5.4-3.4c0.3-0.2,0.4-0.7,0.2-1C18.2,19,17.8,18.9,17.4,19.2z M30.9,14.6l-5.4,3.4c-0.3,0.2-0.4,0.7-0.2,1c0.1,0.2,0.4,0.3,0.6,0.3c0.1,0,0.3,0,0.4-0.1l5.4-3.4c0.3-0.2,0.4-0.7,0.2-1 C31.7,14.5,31.2,14.4,30.9,14.6z M17.4,23l-5,3.1L1.1,19.7c-0.3-0.2-0.8-0.1-1,0.3c-0.2,0.3-0.1,0.8,0.3,1l11.7,6.7c0.1,0.1,0.2,0.1,0.4,0.1 c0.1,0,0.3,0,0.4-0.1l5.4-3.4c0.3-0.2,0.4-0.7,0.2-1C18.2,22.9,17.8,22.8,17.4,23z M30.9,18.5l-5.4,3.4c-0.3,0.2-0.4,0.7-0.2,1c0.1,0.2,0.4,0.3,0.6,0.3c0.1,0,0.3,0,0.4-0.1l5.4-3.4c0.3-0.2,0.4-0.7,0.2-1 C31.7,18.4,31.2,18.2,30.9,18.5z M17.4,26.9l-5,3.1L1.1,23.6c-0.3-0.2-0.8-0.1-1,0.3c-0.2,0.3-0.1,0.8,0.3,1l11.7,6.7c0.1,0.1,0.2,0.1,0.4,0.1 c0.1,0,0.3,0,0.4-0.1l5.4-3.4c0.3-0.2,0.4-0.7,0.2-1C18.2,26.8,17.8,26.7,17.4,26.9z", width: 32, height: 32},
		{id: "tax-free cash", path: "M30,0v12h-2c-0.1-0.7-0.2-1.4-0.4-2H28V2h-9v5.1c-0.3,0.1-0.7,0.3-1,0.4V2h-6v5.5c-0.3-0.2-0.7-0.3-1-0.4V2H2v8h0.4 c-0.2,0.6-0.3,1.3-0.4,2H0V0H30z M30,14v3h-3.1c0.2-0.5,0.5-1.2,0.7-2H28v-1H30z M3.1,17H0v-3h2v1h0.4C2.6,15.8,2.8,16.5,3.1,17z M30,19v3H19.1c0.4,0,0.8-0.1,1.1-0.3c0.2-0.1,1.2-0.7,2-1.7H28v-1H30z M9.8,21.7c0.3,0.2,0.7,0.3,1.1,0.3H0v-3h2v1h5.7C8.5,20.9,9.4,21.5,9.8,21.7z M30,24v3H0v-3h2v1h9v-3h0.3l0.3-0.1c0.2,0,0.3-0.1,0.5-0.2V25l0,0v1h6v-0.3l0,0v-3.9c0.2,0.1,0.3,0.1,0.5,0.2l0.4,0.1l0,0v3 h9v-1h2V24z M10.9,20.5c-0.1,0-0.3,0-0.4-0.1c-0.4-0.2-2-1.2-2.5-2.9c-0.8,0.2-1.6,0.2-2.4,0.2c-0.3,0-0.6-0.2-0.8-0.4 c-0.1-0.1-1.4-2.4-1.4-4.6c0-2.1,1.1-4.2,1.4-4.6C5,7.7,5.3,7.5,5.6,7.5c2.6,0.1,5.2,0.9,7.4,2.3c0.6-0.4,1.3-0.7,2-0.7 c0.7,0,1.4,0.2,2,0.7c2.2-1.5,4.7-2.3,7.4-2.3c0.3,0,0.6,0.2,0.8,0.4c0.1,0.1,1.4,2.4,1.4,4.7c0,2.1-1.1,4.2-1.4,4.6 c-0.2,0.3-0.5,0.5-0.8,0.5c-0.8,0-1.6-0.1-2.4-0.2c-0.5,1.9-2.4,2.9-2.5,2.9c-0.1,0.1-0.3,0.1-0.4,0.1H19h-0.2c-2-0.5-3.2-2-3.9-3.1 C14.2,18.6,13,20,11,20.5L10.9,20.5L10.9,20.5z M16.2,15.8c0.4,0.9,1.3,2.3,2.8,2.9c0.4-0.3,1.1-0.9,1.3-1.7c-1.2-0.4-2.3-1-3.3-1.6 C16.7,15.5,16.5,15.7,16.2,15.8z M9.7,17c0.2,0.8,0.9,1.4,1.3,1.7c1.5-0.6,2.4-1.9,2.8-2.9c-0.3-0.1-0.6-0.3-0.8-0.4 C12,16,10.9,16.6,9.7,17z M18.1,14c1.7,1.1,3.7,1.8,5.8,1.9c0.4-0.8,0.9-2.1,0.9-3.3s-0.6-2.6-0.9-3.3c-2.1,0.1-4,0.8-5.8,1.9 C18.5,12.1,18.5,13.1,18.1,14z M6.1,9.3c-0.3,0.7-0.9,2.1-0.9,3.3c0,1.3,0.6,2.6,0.9,3.3c2.1-0.1,4-0.8,5.8-2 c-0.4-0.9-0.4-1.9,0-2.8C10.2,10.1,8.2,9.4,6.1,9.3z M15,10.9c-0.9,0-1.6,0.7-1.6,1.7c0,0.9,0.7,1.7,1.6,1.7c0.9,0,1.6-0.7,1.6-1.7 C16.6,11.7,15.9,10.9,15,10.9v-0.5V10.9z", width: 30, height: 27},
		{id: "envelope", path: "M15,15.4l-3.7-3.3L0.7,21.4C1.1,21.8,1.6,22,2.2,22h25.7c0.6,0,1.1-0.2,1.5-0.6l-10.6-9.3L15,15.4z M29.3,0.6C28.9,0.2,28.4,0,27.8,0H2.2C1.6,0,1.1,0.2,0.7,0.6L15,13.2L29.3,0.6z M0,1.9v18.3l10.4-9L0,1.9z M19.6,11.2l10.4,9V1.9L19.6,11.2z", width: 30, height: 22},
		{id: "house", path: "M15,4.8L4,14v12h8v-7h6v7h8V14L15,4.8z M29.8,11.6l-4.1-3.4V0.7c0-0.2-0.1-0.3-0.2-0.4s-0.3-0.2-0.4-0.2h-3.6c-0.2,0-0.3,0.1-0.4,0.2C21,0.3,21,0.5,21,0.7v3.6 l-4.5-3.8C16,0.2,15.5,0,15,0s-1,0.2-1.4,0.5L0.2,11.6C0.1,11.7,0,11.9,0,12c0,0.2,0,0.3,0.1,0.4l1.2,1.4C1.4,14,1.5,14,1.7,14 c0.1,0,0.3,0,0.4-0.1L15,3.2l12.9,10.7C28,14,28.1,14,28.3,14h0.1c0.2,0,0.3-0.1,0.4-0.2l1.2-1.4c0-0.1,0-0.2,0-0.4 C30,11.9,29.9,11.7,29.8,11.6z", width: 30, height: 26},
		{id: "coins", path: "M20,5.9c-4.4,0-8,3.6-8,8s3.6,8,8,8s8-3.6,8-8S24.4,5.9,20,5.9z M20,19.9c-3.3,0-6-2.7-6-6s2.7-6,6-6s6,2.7,6,6 S23.3,19.9,20,19.9z M22.5,17.6h-5.6v-1c0.4-0.2,0.7-0.4,1-0.7c0.3-0.3,0.4-0.7,0.4-1c0-0.2,0-0.4,0-0.5H17v-1.3h1c-0.1-0.3-0.1-0.6-0.1-0.9 c0-0.7,0.3-1.4,0.8-1.8c0.5-0.5,1.2-0.7,2-0.7c0.6,0,1.1,0.1,1.5,0.3l-0.3,1.4c-0.3-0.2-0.6-0.2-1-0.2s-0.7,0.1-0.9,0.3 c-0.2,0.2-0.3,0.5-0.3,0.8c0,0.2,0,0.5,0.1,0.8h1.7v1.3H20c0,0.3,0,0.6-0.1,0.9s-0.3,0.5-0.6,0.8l0,0h3.3v1.5H22.5z M11.4,18.9C10.4,19,9.2,19,8,19c-4.4,0-8-0.7-8-1.5c0,0.8,0,2.2,0,3S3.6,22,8,22c2.2,0,4.2-0.2,5.6-0.4 C12.7,20.9,11.9,19.9,11.4,18.9z M10.1,15c-0.7,0-1.3,0.1-2.1,0.1c-4.4,0-8-0.7-8-1.5c0,0.8,0,2.2,0,3s3.6,1.5,8,1.5c1,0,2,0,2.9-0.1 C10.5,17,10.2,16,10.1,15z M10,13.9c0-1,0.2-2,0.4-2.9c-0.8,0-1.6,0.1-2.4,0.1c-4.4,0-8-0.7-8-1.5c0,0.8,0,2.2,0,3s3.6,1.5,8,1.5 C8.7,14,9.4,14,10,13.9C10,14,10,13.9,10,13.9z M16,1.5C16,0.7,12.4,0,8,0S0,0.7,0,1.5s0,2.2,0,3S3.6,6,8,6s8-0.7,8-1.5C16,3.8,16,2.3,16,1.5z M2,1.5c0-0.6,2.7-1.1,6-1.1 s6,0.5,6,1.1s-2.7,1.1-6,1.1S2,2.2,2,1.5z M13.1,6.7C11.7,6.9,10,7,8,7C3.6,7,0,6.3,0,5.5c0,0.8,0,2.2,0,3C0,9.3,3.6,10,8,10c1,0,1.9,0,2.8-0.1 C11.3,8.7,12.1,7.6,13.1,6.7z", width: 28, height: 22},
		{id: "piggy", path: "M26.4,16.8h-0.9l0,0c-0.6-1.9-1.8-3.6-3.5-4.9c0-0.1,0-0.1,0-0.2c-0.3-1.2,0.1-2.3,0.6-3.1c0.3-0.5-0.1-1.2-0.7-1.2 c-1.4,0.2-2.4,0.7-3.1,1.3c-0.5,0.4-0.8,1-0.9,1.6c-1,4.3-4.9,7.4-9.4,7.4c-2.2,0-4.3-0.7-5.9-2c-0.6,1.2-0.9,2.6-0.9,4 c0,0.8,0.1,1.5,0.3,2.2l0,0l0,0c0.2,0.7,0.5,1.5,0.8,2.1c0.5,1,1.1,2.2,2,3.1C6,28.6,6.3,30.3,6.3,31v0.3l0,0l0,0l0,0 C6.3,31.7,6.6,32,7,32h4.2c0.4,0,0.7-0.3,0.7-0.7v-1c0.6,0.1,1.2,0.1,1.9,0.1c0.5,0,1,0,1.5-0.1v1c0,0.4,0.3,0.7,0.7,0.7h4.2 c0.4,0,0.7-0.3,0.7-0.7v-1c0-0.3,0.1-1.7,1.4-2.9l0.1-0.1l0,0l0,0c1.5-1.3,2.5-2.9,3.1-4.7h0.9c0.7,0,1.3-0.6,1.3-1.3v-3.2 C27.7,17.4,27.1,16.8,26.4,16.8z M21.1,17c-0.6,0-1-0.5-1-1c0-0.6,0.5-1,1-1c0.6,0,1,0.5,1,1C22.2,16.5,21.7,17,21.1,17z M8.3,0c-4.4,0-8,3.6-8,8.1c0,4.4,3.6,8.1,8.1,8.1s8.1-3.6,8.1-8.1S12.8,0,8.3,0z M11.1,11.1H5.5v-1c0.4-0.2,0.7-0.4,1-0.8 c0.3-0.3,0.4-0.7,0.4-1c0-0.2,0-0.4,0-0.5H5.6V6.6h1C6.5,6.3,6.5,6,6.5,5.7c0-0.8,0.3-1.4,0.8-1.8c0.5-0.5,1.2-0.7,2-0.7 c0.6,0,1.1,0.1,1.5,0.3l-0.3,1.4c-0.3-0.2-0.6-0.2-1.1-0.2C9,4.7,8.7,4.8,8.5,5C8.3,5.2,8.2,5.5,8.2,5.8c0,0.2,0,0.5,0.1,0.9H10V8 H8.5c0,0.3,0,0.6-0.1,0.9S8.1,9.4,7.8,9.7l0,0h3.3C11.1,9.7,11.1,11.1,11.1,11.1z", width: 28, height: 32},
		{id: "houses", path: "M19,6L7,16.1h3v11h7v-7h4v7h7v-11h3L19,6z M23,7.1l5,4.2V7.1H23z M3,10.1H0L12,0l5.5,4.6L3,16.9V10.1z M8,21.1H3v-3h5V21.1z M16,1.1l5,4.2V1.1H16z", width: 31, height: 28},
		{id: "debt weight", path: "M31.9,23.8l-5-15.5c-0.2-0.7-0.9-1.2-1.6-1.2H20c0.4-0.7,0.7-1.5,0.7-2.4C20.7,2.1,18.6,0,16,0c-2.6,0-4.7,2.1-4.7,4.7 c0,0.9,0.3,1.7,0.7,2.4H6.7C6,7.1,5.3,7.6,5.1,8.3l-5,15.5C-0.3,24.9,0.5,26,1.7,26h28.7C31.5,26,32.3,24.9,31.9,23.8z M16,2.2 c1.4,0,2.5,1.1,2.5,2.5c0,1.2-0.8,2.2-2,2.4h-1.1c-1.1-0.2-2-1.2-2-2.4C13.5,3.3,14.6,2.2,16,2.2z M19.6,23.4H12v-1.6 c0.6-0.3,1-0.7,1.4-1.2s0.5-1.1,0.5-1.7c0-0.3,0-0.6,0-0.7h-1.6v-2.1h1.3c-0.1-0.5-0.1-1.1-0.1-1.5c0-1.2,0.3-2.2,1-3 c0.7-0.8,1.6-1.1,2.7-1.1c0.8,0,1.5,0.2,2,0.5l-0.4,2.3c-0.4-0.2-0.9-0.4-1.4-0.4c-0.5,0-0.9,0.2-1.2,0.5c-0.3,0.3-0.4,0.8-0.4,1.4 c0,0.4,0,0.8,0.1,1.4h2.3v2.1h-2.1c0,0.6-0.1,1-0.2,1.5s-0.4,0.8-0.8,1.3v0h4.5V23.4z", width: 32, height: 26},
		{id: "bank", path: "M0,7h32L16,0L0,7z M4,12h3v12H4V12z M11,12h3v12h-3V12z M18,12h3v12h-3V12z M25,12h3v12h-3V12z M29.4,10.9V8.5H2.6V11h1.3h2.4h4.8h2.4h4.8h2.4h4.8h2.4h1.5V10.9z M28.9,26.9H28v-1.7H4v1.7H3.1c-0.8,0-1.1,0.6-1.1,1.4V30h28v-1.7C30,27.6,29.7,26.9,28.9,26.9z", width: 32, height: 30}
	];

	eve.getItemByID = function(arr, id){
		for (var i = 0; i < arr.length; i++){
			if (arr[i].id === id){
				return arr[i];
			}
		}
		return null;
	};

	//checks if property is null or undefined
	var isNil = function(p) {
		if (p === null || typeof p === "undefined") {
			return true;
		} else {
			return false;
		}
	};

	//Polyfill for IE - does not have 'getComputedStyle'
	if (typeof window !== "undefined"){
		window.getComputedStyle = window.getComputedStyle || function(element) {
			return element.currentStyle;
		};
	}

	function getShadow(elm){
		var obj = elm.parentNode;
		while (!strIs(obj.nodeName, "html")) {
			if (strIs(obj.nodeName, "#document-fragment")) {
				return obj;
			} else {
				obj = obj.parentNode;
			}
		}
	}

	function get(id, elm) {
		var ret = document.getElementById(id);
		if (!ret && !isNil(elm)){
			if (typeof elm === "string"){
				elm = document.getElementById(id);
			}
			if (!isNil(elm) && !isNil(elm.querySelector)){
				ret = elm.querySelector('#' + id);
				if (!ret){
					var shadowParent = getShadow(elm);
					if (shadowParent){
						return shadowParent.querySelector('#' + id);
					}
				}
			}
		}
		return ret;
	}

	var textObj = function(bold, italic, newLine, colour, family, size, link, text){
		this.bold = bold || false;
		this.italic = italic || false;
		this.newLine = newLine || false;
		this.colour = colour || "auto";
		this.face = family || "inherit";
		this.size = size || "auto";
		this.link = link || null;
		this.text = text || "";
		this.leading = "auto";
		this.clone = function(content){
			var txt = content;
			if (txt === null || typeof content === "undefined"){
				txt = this.text;
			}
			var clone = new textObj(this.bold, this.italic, this.newLine, this.colour, this.face, this.size, this.link, txt);
			return clone;
		}
	};

	function getTagProps(tag){
		var attrs = tag.substr(tag.indexOf(" ")).replace(">", "").trim().replace(/\s/g, " ").split(" ");
		var f = {};
		for (var a = 0; a < attrs.length; a++){
			var prop = attrs[a].split("=");
			var aName = prop[0].trim().replace("color", "colour").replace("href", "link");
			var aVal = prop[1].trim().replace(/['"]+/g, '');
			f[aName] = aVal;
		}
		return f;
	}

	function splitParse(str){
		str = str.split("<BR>").join("<br>").split("<br/>").join("<br>").split("<BR/>").join("<br>").split("<br />").join("<br>").split("<BR />").join("<br>");
		str = str.split("<B>").join("<b>").split("</B>").join("</b>").split("<strong>").join("<b>").split("</strong>").join("</b>").split("<STRONG>").join("<b>").split("</STRONG>").join("</b>");
		str = str.split("<I>").join("<i>").split("</I>").join("</i>").split("<em>").join("<i>").split("</em>").join("</i>").split("<EM>").join("<i>").split("</EM>").join("</i>");
		str = str.split("<FONT>").join("<font>").split("</FONT>").join("</font>");
		var paras = str.split("<br>");
		var ret = [];
		var c = 0, p = 0;
		for (p = 0; p < paras.length; p++){
			if (p === 0){
				ret.push(new textObj());
			}
			else{
				ret.push(ret[ret.length - 1].clone(""));
			}
			for (c = 0; c < paras[p].length; c++){
				if (c === 0){
					ret[ret.length - 1].newLine = true;
				}
				if (paras[p].substr(c).indexOf("<b>") === 0){
					c += 2;
					if (ret[ret.length - 1].text !== ""){
						ret.push(ret[ret.length - 1].clone(""));
						ret[ret.length - 1].newLine = false;
					}
					ret[ret.length - 1].bold = true;
				}
				else if (paras[p].substr(c).indexOf("</b>") === 0){
					if (paras[p].substr(c).length > 4){
						ret.push(ret[ret.length - 1].clone(""));
						ret[ret.length - 1].bold = false;
						ret[ret.length - 1].newLine = false;
					}
					c += 3;
				}
				else if (paras[p].substr(c).indexOf("<i>") === 0){
					c += 2;
					if (ret[ret.length - 1].text !== ""){
						ret.push(ret[ret.length - 1].clone(""));
						ret[ret.length - 1].newLine = false;
					}
					ret[ret.length - 1].italic = true;
				}
				else if (paras[p].substr(c).indexOf("</i>") === 0){
					if (paras[p].substr(c).length > 4){
						ret.push(ret[ret.length - 1].clone(""));
						ret[ret.length - 1].newLine = false;
						ret[ret.length - 1].italic = false;
					}
					c += 3;
				}
				else if (paras[p].substr(c).indexOf("<font") === 0){
					var fOpenerLength = paras[p].substr(c).indexOf(">") + 1;
					var fOpener = paras[p].substr(c, fOpenerLength);
					var fProps = getTagProps(fOpener);
					c += fOpenerLength - 1;
					if (ret[ret.length - 1].text !== ""){
						ret.push(ret[ret.length - 1].clone(""));
						ret[ret.length - 1].newLine = false;
					}
					for (var fprop in fProps){
						ret[ret.length - 1][fprop] = fProps[fprop];
					}
				}
				else if (paras[p].substr(c).indexOf("</font>") === 0){
					if (paras[p].substr(c).length > 7){
						ret.push(ret[ret.length - 1].clone(""));
						ret[ret.length - 1].size = ret[ret.length - 1].colour = ret[ret.length - 1].face = "auto";
						ret[ret.length - 1].newLine = false;
					}
					c += 6;
				}
				else if (paras[p].substr(c).indexOf("<a ") === 0){
					var aOpenerLength = paras[p].substr(c).indexOf(">") + 1;
					var aOpener = paras[p].substr(c, aOpenerLength);
					var aProps = getTagProps(aOpener);
					c += aOpenerLength - 1;
					if (ret[ret.length - 1].text !== ""){
						ret.push(ret[ret.length - 1].clone(""));
						ret[ret.length - 1].newLine = false;
					}
					for (var aprop in aProps){
						ret[ret.length - 1][aprop] = aProps[aprop];
					}
				}
				else if (paras[p].substr(c).indexOf("</a>") === 0){
					if (paras[p].substr(c).length > 4){
						ret.push(ret[ret.length - 1].clone(""));
						ret[ret.length - 1].link = null;
						ret[ret.length - 1].newLine = false;
					}
					c += 3;
				}
				else{
					ret[ret.length - 1].text += paras[p].substr(c, 1);
				}
			}
		}
		return ret;
	}

	eve.fakeGradientAlphas = function(startAlpha, endAlpha, steps) {
		var alphas = [startAlpha];
		var stepSize = (endAlpha - startAlpha) / steps;
		for (var i = 0; i < steps; i++) {
			var currentAlpha = startAlpha + i * stepSize;
			alphas.push(stepSize / (1 - currentAlpha));
		}
		return alphas.reverse();
	};

	eve.camelToDashed = function(str) {
		return str.replace(/([A-Z])/g, '-$1').toLowerCase();
	};
	eve.dashedToCamel = function(str) {
		return str.replace(/-([a-z])/g, function(g) {
			return g[1].toUpperCase();
		});
	};

	eve.isArray = function(o) {
		return Object.prototype.toString.call(o) === "[object Array]";
	};
	eve.isObject = function(o) {
		return typeof o === "object";
	};

	eve.getMidVal = function(arr){
		var sArr = arr.slice(0).sort(eve.sortNumber);
		return sArr[Math.floor(sArr.length / 2)];
	};

	eve.replaceSpecialChars = function(str){
		//return str.replace(/[^\w]/gi, '_'); //.split(",").join("_").split(".").join("_");
		//characters in IDs that break querySelector:
		var illegalChars = [",", ":", ".", " "];
		for (var c = 0; c < illegalChars.length; c++){
			str = str.split(illegalChars[c]).join("_");
		}
		return str;
	};

	function getType(str) {
		if (isNil(str)){
			return null;
		}
		str = str.toUpperCase();
		if (str === "PIE" || str === "SECTOR" || str === "DONUT" || str === "DOUGHNUT"){ // || str === "RING") {
			return "sector";
		}
		else if (str.toUpperCase()==="XY"){
			return "XY";
		}
		return str.toLowerCase();
	}

	//reasons for running Internet Explorer
	var dataDefault = [
		{reason: "browsing the internet", likelihood: 2},
		{reason: "I clicked on it by mistake", likelihood: 50},
		{reason: "to download Chrome", likelihood: 80},
		{reason: "to check if a website is HTML5", likelihood: 60}
	];

	eve.getUnits = function(str) {
		str = str.toString().toUpperCase();
		if (str.indexOf("PX") !== -1) {return "px";}
		if (str.indexOf("EM") !== -1) {return "em";}
		if (str.indexOf("EX") !== -1) {return "ex";}
		if (str.indexOf("%") !== -1) {return "%";}
		if (str.indexOf("CM") !== -1) {return "cm";}
		if (str.indexOf("MM") !== -1) {return "mm";}
		if (str.indexOf("IN") !== -1) {return "in";}
		if (str.indexOf("PT") !== -1) {return "pt";}
		if (str.indexOf("PC") !== -1) {return "pc";}
		if (str.indexOf("CH") !== -1) {return "ch";}
		if (str.indexOf("REM") !== -1) {return "rem";}
		if (str.indexOf("VH") !== -1) {return "vh";}
		if (str.indexOf("VW") !== -1) {return "vw";}
		if (str.indexOf("VMIN") !== -1) {return "vmin";}
		if (str.indexOf("VMAX") !== -1) {return "vmax";}
		return null;
	};

	eve.convertUnits = function(val, uFrom, uTo) {
		val = parseFloat(val);
		if (uFrom === "px") {
			if (uTo === "vw") {
				return (val * (100 / document.documentElement.clientWidth));
			}
		}
		return val;
	};

	eve.elmAnim = function(mode, duration, delay, ease, endOpacity) {
		this.mode = mode || null;
		this.duration = duration || null;
		this.delay = delay || null;
		this.ease = ease || null;
		this.endOpacity = endOpacity || null;
	};

	var objProps = 	{
		chart: {
			type: "XY",
			width: "auto",
			height: "auto",
			x: 0,
			y: 0,
			drawMode: "SVG",
			id: "auto",
			idPrefix: "eve_chart",
			container: null,
			backgroundColour: "transparent",
			plotBackgroundColour: "transparent",
			blockHighlight: "transparent",
			fontFamily: null,
			showExport: false,
			showLegend: true,
			exportDashes: false,
			units: "px",
			title: "Eve Chart",
			titleFontSize: 14,
			titleFontColour: "#3c3c3c",
			titleFontWeight: "inherit",
			titleOffsetX: 0,
			titleOffsetY: 0,
			titleChangeAnim: new eve.elmAnim("fadeDownChangeFadeUp", 0.5, 0, "easeInOut"),
			marginTop: 5,
			marginRight: 5,
			marginBottom: 5,
			marginLeft: 5,
			margin: "5 5 5 5",
			paddingTop: 50,
			paddingRight: 50,
			paddingBottom: 50,
			paddingLeft: 50,
			padding: "50 50 50 50",
			cushion: "auto",
			palette: "auto",
			plotOrigin: {x: 0, y: 0},
			legendBackgroundColour: "auto",
			legendBackgroundOpacity: 1,
			legendOffsetX: 0,
			legendOffsetY: 0,
			legendPos: "auto", //topLeft, topCentre, topRight, middleLeft, middleCentre, middleRight, bottomLeft, bottomCentre, bottomRight
			legendStacked: true,
			seriesLayout: "grouped",
			lblCount: 0,
			ready: true,
			drawCall: 0,
			blueprint: null,
			artboard: null,
			axes: [],
			labels: [],
			xAxis: null,
			yAxis: null,
			series: [],
			barGap: 0,
			animInRatios: {axes: 0.25, series: 0.5, legend: 0.1, overlays: 0.15},
			oldProps: [],
			mediaProps: []
		},
		series: {
			age: 0,
			id: "auto",
			idPrefix: "eve_series",
			chart: null,
			type: "",
			axisX: null,
			axisY: null,
			data: null,
			fdata: null,
			baseAxis: null,
			fringe: null,
			curved: false,
			aboveAxes: false,
			domains: [],
			offsetX: 0,
			offsetY: 0,
			clockwise: true,
			rotation: 0,
			innerRadius: 0,
			shadow: false,
			shadowOpacity: 0.25,
			shadowSteps: 50,
			shadowOffsetX: 0,
			shadowOffsetY: 0,
			snapTo: false,
			value: null,
			editable: false,
			dragMinValue: 0,
			tickColour: "#3c3c3c",
			tickWidth: 1,
			icon: null,
			iconColour: "#3c3c3c",
			iconContainerStrokeColour: "white",
			iconContainerStrokeWidth: 2,
			showTicks: true,
			pointOriginX: null,
			pointOriginY: null,
			pointEndX: null,
			pointEndY: null,
			handleOriginX: null,
			handleOriginY: null,
			handleEndX: null,
			handleEndY: null,
			barPadding: 6,
			colour: "auto",
			hoverColour: "auto",
			fillOpacity: "auto",
			fillHoverOpacity: "auto",
			fringeOpacity: 0.1,
			strokeColour: "auto",
			strokeHoverColour: "auto",
			strokeHoverOpacity: "auto",
			strokeOpacity: 1,
			strokeWidth: 0,
			lineColour: "auto",
			lineHoverColour: "auto",
			lineOpacity: 1,
			lineWidth: 10,
			lineDash: "0",
			lineJoin: "round",
			lineCap: "butt",
			reflect: false,
			reflectDepth: 0,
			reflectSteps: 0,
			reflectStartOpacity: 0.5,
			nodes: [],
			nodeLabels: false,
			nodeLabelPosn: null,
			nodeLabelOffsetX: 0,
			nodeLabelOffsetY: 0,
			nodeLabelShowNumber: true,
			nodeLabelShowLabel: true,
			nodeLabelNumberFirst: true,
			nodeLabelNumberFontSize: 24,
			nodeLabelNumberFormat: "",
			nodeLabelNumberTextGap: "auto",
			nodeLabelTextFontSize: 12,
			nodeLabelFontFamily: "inherit",
			nodeLabelFontColour: "auto",
			nodeLabelLeaders: false,
			nodeLabelLeaderDotSize: 6,
			nodeLabelLeaderLength: null,
			nodeLabelLeaderStrokeWeight: 4,
			nodeLabelLeaderColour: "auto",
			nodeLabelLeaderDoubleStrokeWeight: 1,
			nodeLabelLeaderDoubleStroke: true,
			nodeLabelLeaderDoubleStrokeColour: null,
			nodeLabelLeaderSpoke: null,
			nodeLabelLeaderAlign: "split", //top, bottom, split
			nodeLabelLeaderShoulderOffset: null,
			nodeLabelTextLeading: null,
			nodeColour: "auto",
			nodeFillOpacity: "auto",
			nodeHoverColour: "auto",
			nodeFillHoverOpacity: "auto",
			nodeStrokeColour: "auto",
			nodeStrokeOpacity: "auto",
			nodeStrokeWidth: "auto",
			nodeStrokeHoverColour: "auto",
			nodeStrokeHoverOpacity: "auto",
			nodeIconFillColour: null,
			nodeIconScale: 1,
			nodeGlow: null,
			nodeGlowOpacity: 0.15,
			nodeGlowColour: "black",
			nodeGlowOffsetX: 2,
			nodeGlowOffsetY: 2,
			nodeGlowSteps: 3,
			nodeGlowSize: 1,
			hoverOffset: 0,
			nodeWidth: 16,
			nodeHeight: 16,
			nodeType: "circle",
			legend: "auto",
			legendFontSize: 12,
			legendFontColour: "#3c3c3c",
			legendFontFamily: "inherit",
			legendOpacity: 1,
			legendSwatchType: "rectangle",
			legendIgnoreBreaks: false,
			legendOffsetX: 0,
			legendOffsetY: 0,
			legendSwatchWidth: 16,
			legendSwatchHeight: 16,
			legendSwatchColour: "auto",
			legendChangeLegendAnim: "fadeDownChangeFadeUp",
			tooltipFontSize: 12,
			tooltipFontColour: "#3c3c3c",
			tooltipPadding: 10,
			tooltipFontFamily: "inherit",
			tooltipBackgroundColour: "white",
			tooltipBackgroundOpacity: 1,
			tooltipBorderColour: "#3c3c3c",
			tooltipBorderWidth: 1,
			showLegend: true,
			showTooltips: true,
			showMeasureLines: true,
			measureLineColour: "auto",
			measureLineWidth: 2,
			measureLineOpacity: 0.75,
			visible: true,
			animBirth: new eve.elmAnim(),
			animAdd: new eve.elmAnim(),
			animChange: new eve.elmAnim(),
			animDeath: new eve.elmAnim(),
			animCurrent: new eve.elmAnim(),
			colourEnd: null,
			knots: [],
			targets: [],
			maxRange: null,
			redrawRequired: true,
			redrawQueued: false,
			segments: [],
			highlights: [],
			shapeNodes: false,
			width: "50%",
			stepped: false,
			dialFillColour: "white",
			dialShadowFillColour: "black",
			dialShadowOpacity: 0.2,
			oldProps: [],
			mediaProps: []
		},
		axis: {
			age: 0,
			id: "auto",
			idPrefix: "eve_axis",
			chart: null,
			position: null,
			type: null,
			domain: null,
			horizontal: true,
			fontFamily: "inherit",
			fontWeight: "normal",
			overhangOrigin: 6,
			overhangEnd: 10,
			label: "auto|",
			labelFontSize: 14,
			labelFontWeight: "normal",
			labelFontColour: "#3c3c3c",
			labelOpacity: 1,
			labelOffsetX: 8,
			labelOffsetY: 0,
			axisColour: "#3c3c3c",
			axisOpacity: 1,
			gaugeColour: "black",
			gaugeColourEnd: "black",
			gaugeOpacity: 0.15,
			showBandLabels: true,
			bandLabelRotation: 0,
			autoRotateLabels: true,
			tickFontSize: 12,
			tickTextMargin: 3,
			tickTextOpacity: 1,
			tickLength: 6,
			tickWidth: 1,
			tickOpacity: 1,
			tickColour: "#3c3c3c",
			tickTextFormat: "auto",
			tickFontColour: "#3c3c3c",
			tickTextSize: {width: 0, height: 0}, //calculated value
			autoSubTicks: null,
			useSubTicks: null,
			subTicks: "auto|",
			showTicks: true,
			showSubTicks: false,
			subTickLength: 5,
			subTickWidth: 1,
			subTickColour: "#3c3c3c",
			subTickOpacity: 1,
			gridLineWidth: 1,
			gridLineColour: "#3c3c3c",
			gridLineOpacity: 0.25,
			subGridLineWidth: 1,
			subGridLineColour: "#3c3c3c",
			subGridLineOpacity: 0.1,
			origin: "left",
			autoRangeMin: null,
			autoRangeMax: null,
			rangeMin: null,
			rangeMax: null,
			range: null,
			rangeLabelStart: null,
			rangeLabelEnd: null,
			showGridLines: true,
			showSubGridLines: false,
			visible: true,
			redrawRequired: true,
			redrawQueued: false,
			bandLabels: null,
			bandSize: 0,
			animBirth: new eve.elmAnim(),
			animAdd: new eve.elmAnim(),
			animChange: new eve.elmAnim(),
			animDeath: new eve.elmAnim("fade", 0, 0, "easeOut"),
			animCurrent: new eve.elmAnim(),
			bands: [{_label: "", _value: "", _labelPos: 0, _labelOffsetX: 0, _labelOffsetY: 0, _tickPos: 0, _start: 0, _end: 0, _startTick: 0, _startLabel: "", _startValue: 0}],
			numBands: "auto|",
			oldProps: [],
			mediaProps: []
		},
		seriesNode: {
			series: null,
			idPrefix: "eve_node",
			id: null,
			data: null,
			type: null,
			criteria: null,
			criteriaDomain: null,
			visible: true,
			colour: "auto",
			hoverColour: "auto",
			fillOpacity: 1,
			fillHoverOpacity: "auto",
			strokeColour: "auto",
			strokeHoverColour: "auto",
			strokeHoverOpacity: "auto",
			strokeOpacity: 1,
			strokeWidth: 2,
			width: 16,
			height: 16,
			labels: null,
			labelPosn: null,
			labelOffsetX: 0,
			labelOffsetY: 0,
			labelShowLabel: true,
			labelShowNumber: true,
			labelNumberFirst: true,
			labelNumberFontSize: 24,
			labelNumberFormat: null,
			labelNumberTextGap: null,
			labelLeaderAlign: null,
			labelTextFontSize: 12,
			labelFontFamily: "inherit",
			labelFontColour: "auto",
			labelLeaderDotSize: 6,
			labelLeaderLength: null,
			labelLeaderStrokeWeight: 1,
			labelLeaderColour: "auto",
			labelLeaderDoubleStrokeWeight: 1,
			labelLeaderDoubleStroke: true,
			labelLeaderDoubleStrokeColour: "white",
			labelLeaderSpoke: false,
			labelLeaderShoulderOffset: null,
			labelTextLeading: null,
			glow: null,
			glowOpacity: 0.15,
			glowColour: "black",
			glowOffsetX: 2,
			glowOffsetY: 2,
			glowSteps: 3,
			glowSize: 1,
			icon: null,
			iconFillColour: null,
			iconOffsetX: 0,
			iconOffsetY: 0,
			iconScale: 1,
			tooltipTitle: "",
			mediaProps: []
		},
		seriesLineSegment: {
			series: null,
			id: null,
			idPrefix: "eve_segment",
			lineColour: "auto",
			lineHoverColour: "auto",
			lineOpacity: 1,
			strokeDash: "0",
			criteria: null,
			criteriaDomain: null,
			dataStart: null,
			dataEnd: null,
			mediaProps: []
		},
		highlight: {
			id: "auto",
			idPrefix: "eve_highlight",
			series: null,
			criteria: null,
			criteriaDomain: null,
			elmRef: null,
			type: null,
			width: null,
			height: null,
			colour: "auto",
			hoverColour: "auto",
			fillOpacity: 1,
			strokeColour: "auto",
			strokeHoverColour: "auto",
			strokeOpacity: 1,
			strokeWidth: "auto",
			lineColour: "auto",
			lineHoverColour: "auto",
			lineOpacity: 1,
			lineWidth: 10,
			strokeDash: "0",
			mediaProps: []
		},
		leader: {
			id: "auto",
			idPrefix: "eve_leader",
			label: null,
			strokeWidth: 2,
			strokeColour: "black",
			strokeDash: "0",
			strokeOpacity: 0.4,
			horizontal: false,
			vertical: false,
			arrowhead: false,
			arrowheadWidth: 8,
			arrowheadHeight: 14,
			arrowheadType: "arrow",
			startX: null,
			startY: null,
			endX: null,
			endY: null,
			offset: {start: null, end: null},
			mediaProps: []
		},
		intNode: {
			//defaults
			id: "auto",
			idPrefix: "eve_intNode",
			label: null,
			series: null,
			bottom: false,
			data: null,
			visible: true,
			colour: "#9b9b9b",
			hoverColour: "#c2c2c2",
			fillOpacity: 0.75,
			strokeColour: "#292929",
			strokeHoverColour: "#1a1a1a",
			strokeOpacity: 1,
			strokeWidth: 2,
			width: 16,
			height: 16,
			type: null,
			mediaProps: []
		},
		label: {
			age: 0,
			chart: null,
			id: "auto",
			idPrefix: "eve_label",
			link: null,
			text: "Eve label",
			dragSnap: null,
			axisVal: 0,
			draggable: false,
			onDrag: null,
			onDragParams: null,
			rangeStart: false,
			rangeEnd: false,
			dragRect: {xMin: null, xMax:null, yMin: null, yMax: null, xMinVal: null, xMaxVal: null, yMinVal: null, yMaxVal: null},
			boxStyle: "none",
			cornerRadius: 0,
			pointerBottom: false,
			pointerTop: false,
			pointerLeft: false,
			pointerRight: false,
			pointerOffsetBottom: 0,
			pointerOffsetTop: 0,
			pointerOffsetLeft: 0,
			pointerOffsetRight: 0,
			pointerLength: 7,
			pointerWidth: 13,
			pointerTangential: false,
			highlightBlock: null,
			highlightBlockColour: "white",
			highlightBlockOpacity: 1,
			width: "auto",
			height: "auto",
			padding: 10,
			fontSize: 12,
			fontColour: "#3c3c3c",
			fontFamily: "inherit",
			fontWeight: "normal",
			textAlign: "centre",
			textOffsetX: 0,
			textOffsetY: 0,
			leading: "auto",
			backgroundColour: "white",
			backgroundOpacity: 1,
			borderColour: "#3c3c3c",
			borderWidth: 0,
			glow: null,
			glowOpacity: 0.15,
			glowColour: "black",
			glowOffsetX: 2,
			glowOffsetY: 2,
			glowSteps: 3,
			glowSize: 1,
			origin: "pointerEnd",
			icon: null,
			iconFillColour: "#7f7f7f",
			iconOffsetX: 0,
			iconOffsetY: 0,
			iconScale: 1,
			iconAlign: "centre",
			x: 0,
			y: 0,
			offsetX: 0,
			offsetY: 0,
			animBirth: new eve.elmAnim("fade in", null, null, "easeOut"),
			animAdd: new eve.elmAnim("fade in", null, null, "easeOut"),
			animChange: new eve.elmAnim("morph", null, null, "easeInOut"),
			animDeath: new eve.elmAnim("fade", null, null, "easeOut"),
			animCurrent: new eve.elmAnim("fade in", null, null, "easeOut"),
			redrawRequired: true,
			redrawQueued: false,
			children: [],
			leaders: [],
			intNodes: [],
			oldProps: [],
			mediaProps: []
		}
	};

	var seriesPropsRedraw = ["type", "axisX", "axisY", "data", "fdata", "fringe", "curved", "offsetX", "offsetY", "clockwise", "rotation", "innerRadius", "shadow", "shadowOpacity", "shadowSteps", "shadowOffsetX", "shadowOffsetY", "snapTo", "value", "editable", "dragMinValue", "tickColour", "tickWidth", "icon", "iconColour", "iconContainerStrokeColour", "iconContainerStrokeWidth", "showTicks", "pointOriginX", "pointEndX", "handleOriginX", "handleEndX", "pointOriginY", "pointEndY", "handleOriginY", "handleEndY", "barPadding", "colour", "fillOpacity", "fringeOpacity", "strokeColour", "strokeHoverColour", "strokeOpacity", "strokeWidth", "lineColour", "lineOpacity", "lineWidth", "lineDash", "lineJoin", "lineCap", "reflect", "reflectDepth", "reflectSteps", "reflectStartOpacity", "nodes", "nodeLabels", "nodeLabelPosn", "nodeLabelOffsetX", "nodeLabelOffsetY", "nodeLabelShowNumber", "nodeLabelShowLabel", "nodeLabelNumberFirst", "nodeLabelNumberFontSize", "nodeLabelNumberFormat", "nodeLabelNumberTextGap", "nodeLabelTextFontSize", "nodeLabelFontFamily", "nodeLabelFontColour", "nodeLabelLeaders", "nodeLabelLeaderDotSize", "nodeLabelLeaderLength", "nodeLabelLeaderStrokeWeight", "nodeLabelLeaderColour", "nodeLabelLeaderDoubleStrokeWeight", "nodeLabelLeaderDoubleStroke", "nodeLabelLeaderDoubleStrokeColour", "nodeLabelLeaderSpoke", "nodeLabelLeaderAlign", "nodeLabelLeaderShoulderOffset", "nodeLabelTextLeading", "nodeColour", "nodeFillOpacity", "nodeHoverColour", "nodeFillHoverOpacity", "nodeStrokeColour", "nodeStrokeOpacity", "nodeStrokeWidth", "nodeStrokeHoverColour", "nodeStrokeHoverOpacity", "nodeIconFillColour", "nodeIconScale", "nodeGlow", "nodeGlowOpacity", "nodeGlowColour", "nodeGlowOffsetX", "nodeGlowOffsetY", "nodeGlowSteps", "nodeGlowSize", "hoverOffset", "nodeWidth", "nodeHeight", "nodeType", "legend", "legendFontSize", "legendFontColour", "legendFontFamily", "legendOpacity", "legendSwatchType", "legendIgnoreBreaks", "legendOffsetX", "legendOffsetY", "legendSwatchWidth", "legendSwatchHeight", "legendSwatchColour", "legendChangeLegendAnim", "tooltipFontSize", "tooltipFontColour", "tooltipPadding", "tooltipFontFamily", "tooltipBackgroundColour", "tooltipBackgroundOpacity", "tooltipBorderColour", "tooltipBorderWidth", "showLegend", "showTooltips", "showMeasureLines", "measureLineColour", "measureLineWidth", "measureLineOpacity", "visible", "colourEnd", "knots", "targets", "maxRange", "segments", "highlights", "shapeNodes", "width", "offsetX", "offsetY", "stepped", "dialFillColour", "dialShadowFillColour", "dialShadowOpacity"];

	var axisPropsRedraw = ["position", "type", "domain", "horizontal", "fontFamily", "fontWeight", "overhangOrigin", "overhangEnd", "label", "labelFontSize", "labelFontWeight", "labelFontColour", "labelOpacity", "labelOffsetX", "labelOffsetY", "axisColour", "axisOpacity", "gaugeColour", "gaugeColourEnd", "gaugeOpacity", "showBandLabels", "bandLabelRotation", "autoRotateLabels", "tickFontSize", "tickTextMargin", "tickTextOpacity", "tickLength", "tickWidth", "tickOpacity", "tickColour", "tickTextFormat", "tickFontColour", "tickTextSize", "autoSubTicks", "useSubTicks", "subTicks", "showTicks", "showSubTicks", "subTickLength", "subTickWidth", "subTickColour", "subTickOpacity", "gridLineWidth", "gridLineColour", "gridLineOpacity", "subGridLineWidth", "subGridLineColour", "subGridLineOpacity", "origin", "rangeMin", "rangeMax", "rangeLabelStart", "rangeLabelEnd", "showGridLines", "showSubGridLines", "visible", "bandLabels", "bandSize", "bands", "numBands"];

	var labelPropsRedraw = ["link", "text", "dragSnap", "axisVal", "rangeStart", "rangeEnd", "dragRect", "boxStyle", "cornerRadius", "pointerBottom", "pointerTop", "pointerLeft", "pointerRight", "pointerOffsetBottom", "pointerOffsetTop", "pointerOffsetLeft", "pointerOffsetRight", "pointerLength", "pointerWidth", "pointerTangential", "highlightBlock", "highlightBlockColour", "highlightBlockOpacity", "width", "height", "padding", "fontSize", "fontColour", "fontFamily", "fontWeight", "textAlign", "textOffsetX", "textOffsetY", "leading", "backgroundColour", "backgroundOpacity", "borderColour", "borderWidth", "glow", "glowOpacity", "glowColour", "glowOffsetX", "glowOffsetY", "glowSteps", "glowSize", "origin", "icon", "iconFillColour", "iconOffsetX", "iconOffsetY", "iconScale", "iconAlign", "x", "y", "offsetX", "offsetY", "redrawRequired", "redrawQueued", "children", "leaders", "intNodes"];

	eve.setProp = function(obj, prop) {
		if (!isNil(obj[prop])) {
			obj["_" + prop] = obj[prop];
		}
	};

	eve.setObjProps = function(obj, defaults, userValsAfter) {
		// assume default properties from arrays of default values:
		var propsFrom = objProps.chart;
		if (obj instanceof eve.series) {propsFrom = objProps.series;}
		if (obj instanceof eve.axis) {propsFrom = objProps.axis;}
		if (obj instanceof eve.seriesNode) {propsFrom = objProps.seriesNode;}
		if (obj instanceof eve.highlight) {propsFrom = objProps.highlight;}
		if (obj instanceof eve.label) {propsFrom = objProps.label;}
		if (obj instanceof eve.leader) {propsFrom = objProps.leader;}
		if (obj instanceof eve.intNode) {propsFrom = objProps.intNode;}
		if (obj instanceof eve.seriesLineSegment) {propsFrom = objProps.seriesLineSegment;}
		if (!defaults){
			// user-defined properties (no underscores)
			var propsDefault = propsFrom;
			propsFrom = [];
			for (var uProp in obj){
				if (obj.hasOwnProperty(uProp) && uProp.indexOf("__") !== 0 && uProp.indexOf("_") !== 0) {
//					if (isNil(obj["_" + uProp])){
						propsFrom[uProp] = obj[uProp];
//					}
				}
			}
			// override with mediaProps:
			for (var mp = 0; mp < obj.mediaProps.length; mp++) {
				var ruleOn = true;
				var rules = obj.mediaProps[mp].case;
				if (!eve.isArray(obj.mediaProps[mp].case)) {
					rules = [obj.mediaProps[mp].case];
				}
				for (var r = 0; r < rules.length; r++) {
					if (!isNil(rules[r])){
						if (rules[r].maxWidth) {
							if (rules[r].maxWidth < document.body.clientWidth) {
								ruleOn = false;
							}
						}
						if (rules[r].minWidth) {
							if (rules[r].minWidth > document.body.clientWidth) {
								ruleOn = false;
							}
						}
					}
				}
				for (prop in obj.mediaProps[mp].props) {
					// reset:
					if (ruleOn) {
						if (isNil(propsFrom[prop])){
							propsFrom[prop] = propsDefault[prop];
						}
						if (obj.mediaProps[mp].props.hasOwnProperty(prop)) {
							propsFrom[prop] = obj.mediaProps[mp].props[prop];
							if (eve.isNumeric(obj.mediaProps[mp].props[prop]) && !eve.isArray(obj.mediaProps[mp].props[prop])) {
								propsFrom[prop] = calcDim(obj.mediaProps[mp].props[prop]);
								propsFrom[prop] = calcDim(obj.mediaProps[mp].props[prop]);
							}
						}
					}
				}
			}
		}
		for (var prop in propsFrom){
			if(!defaults && (prop === "animCurrent" || prop === "animBirth" || prop === "animAdd" || prop === "animChange" || prop === "animDeath")){
				if (!isNil(propsFrom[prop].mode)){obj["_" + prop].mode = propsFrom[prop].mode;}
				if (!isNil(propsFrom[prop].ease)){obj["_" + prop].ease = propsFrom[prop].ease;}
				if (!isNil(propsFrom[prop].duration)){obj["_" + prop].duration = propsFrom[prop].duration;}
				if (!isNil(propsFrom[prop].delay)){obj["_" + prop].delay = propsFrom[prop].delay;}
			}
			else if (obj instanceof eve.chart && obj._drawMode === "PDF" && prop === "container"){
				obj["_" + prop] = propsFrom[prop];
			}
//			else if(propsFrom[prop] instanceof eve.parallelHatch){
//				obj["_" + prop] = propsFrom[prop].clone();
//			}
//			else if(propsFrom[prop] instanceof eve.linearGradient){
//				obj["_" + prop] = propsFrom[prop].clone();
//			}
			else if (propsFrom[prop] instanceof eve.linearGradient || propsFrom[prop] instanceof eve.parallelHatch || propsFrom[prop] instanceof eve.axis || propsFrom[prop] instanceof eve.chart || propsFrom[prop] instanceof eve.series || propsFrom[prop] instanceof eve.seriesNode || propsFrom[prop] instanceof eve.highlight || propsFrom[prop] instanceof eve.label || propsFrom[prop] instanceof eve.leader || propsFrom[prop] instanceof eve.intNode || propsFrom[prop] instanceof eve.seriesLineSegment){
				obj["_" + prop] = propsFrom[prop].id;
			}
			else if (eve.isArray(propsFrom[prop])){
				// don't clone the data, keep a reference to the original for comparison later on:
				if (obj instanceof eve.series && prop === "data"){
					obj["_" + prop] = propsFrom[prop];
				}
				else{
					obj["_" + prop] = propsFrom[prop].clone(obj === "oldProps");
				}
			}
			else if (eve.isObject(propsFrom[prop]) && !eve.isElement(propsFrom[prop])){
				obj["_" + prop] = cloneObj(propsFrom[prop]);
			}
			else{
				if (!defaults && (prop === "margin" || prop === "padding") && typeof obj[prop] !== "object" && typeof obj[prop] !== "number"){
					if (!isNil(obj[prop])){
						if (obj[prop] === ""){
							obj[prop] = "0";
						}
						obj["_" + prop] = propsFrom[prop];
						var pVal = obj[prop].toString().split(/[ ,]+/).filter(Boolean);
						if (pVal.length > 3) {
							obj["_" + prop + "Top"] = calcDim(pVal[0]);
							obj["_" + prop + "Right"] = calcDim(pVal[1]);
							obj["_" + prop + "Bottom"] = calcDim(pVal[2]);
							obj["_" + prop + "Left"] = calcDim(pVal[3]);
						} else if (pVal.length === 3) {
							obj["_" + prop + "Top"] = calcDim(pVal[0]);
							obj["_" + prop + "Right"] = obj["_" + prop + "Left"] = calcDim(pVal[1]);
							obj["_" + prop + "Bottom"] = calcDim(pVal[2]);
						} else if (pVal.length === 2) {
							obj["_" + prop + "Top"] = obj["_" + prop + "Bottom"] = calcDim(pVal[0]);
							obj["_" + prop + "Right"] = obj["_" + prop + "Left"] = calcDim(pVal[1]);
						} else {
							obj["_" + prop + "Top"] = obj["_" + prop + "Bottom"] = obj["_" + prop + "Right"] = obj["_" + prop + "Left"] = calcDim(pVal[0]);
						}
						obj[prop + "Top"] = obj["_" + prop + "Top"];
						obj[prop + "Right"] = obj["_" + prop + "Right"];
						obj[prop + "Bottom"] = obj["_" + prop + "Bottom"];
						obj[prop + "Left"] = obj["_" + prop + "Left"];
					}
				}
				else{
					obj["_" + prop] = propsFrom[prop];
				}
			}
		}
		if (defaults && userValsAfter) {
			eve.setObjProps(obj, false);
		}
	};

	var getChartWidth = function(c){
		var w;
		if (c._drawMode === "PDF"){
			w = c._container.page.width;
		}
		else{
			w = parseFloat(c._container.clientWidth) || 0;
			//if display is set to "none" on the container object, clientWidth will return 0, so try the css property:
			if (w === 0) {
				w = parseInt(c.width);
			}
			//TODO: this is a horrible hack. Do it better:
			if (isNaN(w)){
				w = parseInt(c._container.style.width);
			}
			if (w <= 0) {
				w = 200;
			}
		}
		if (!isNil(c._width) && c._width.toString().indexOf("%") !== -1){
			w *= parseFloat(c._width.toString()) / 100.0;
		}
		return w;
	};
	var getChartHeight = function(c){
		var h;
		if (c._drawMode === "PDF"){
			h = get(c._container).page.height;
		}
		else{
			h = parseFloat(c._container.clientHeight) || 0;
			//if display is set to "none" on the container object, clientHeight will return 0, so try the css property:
			if (h === 0) {
				h = parseInt(c.height);
			}
			//TODO: this is a horrible hack. Do it better:
			if (isNaN(h)){
				h = parseInt(c._container.style.height);
			}
			if (h <= 0) {
				h = 150;
			}
		}
		if (!isNil(c._height) && c._height.toString().indexOf("%") !== -1){
			h *= parseFloat(c._height.toString()) / 100.0;
		}
		return h;
	}

	function setChartSize(c){
		if (isNil(c.width) || c.width === "auto" || c.width.toString().indexOf("%") !== -1) {
			c._width = getChartWidth(c);
		}
		if (isNil(c.height) || c.height === "auto" || c.height.toString().indexOf("%") !== -1) {
			c._height = getChartHeight(c);
		}
	}

	var rectanglesIntersect = function(r1, r2) {
		return !(r2.left > r1.left + r1.width || r2.left + r2.width < r1.left || r2.top > r1.top + r1.height || r2.top + r2.height < r1.top);
	};

	//split a string into words and numbers:
	var splitMixedString = function(inStr) {
		var c;
		if (isNil(inStr)){
			inStr = "";
		}
		var str = inStr.toString();
		var ret = [];
		var nums = "0123456789";
		var numChars = "-,.+";
		if (str.length === 0) {
			return null;
		}
		var inNum = false;
		var word = "";
		var prevChar = "";
		for (c = 0; c < str.length; c++) {
			var char = str.substr(c, 1);
			if (inNum) {
				if (nums.indexOf(char) !== -1 || numChars.indexOf(char) !== -1) {
					word += char;
				} else {
					if (prevChar === "," || prevChar === ".") {
						word = word.slice(0, -1);
						ret.push({num: true, val: word});
						word = prevChar + char;
					} else {
						ret.push({num: true, val: word});
						word = char;
					}
					inNum = false;
				}
			} else {
				if (nums.indexOf(char) !== -1) {
					if (word.length > 0) {
						if (prevChar === "+" || prevChar === "-") {
							ret.push({num: false, val: word.slice(0, -1)});
							word = prevChar + char;
						} else {
							ret.push({num: false, val: word});
							word = char;
						}
					} else {
						word = char;
					}
					inNum = true;
				} else {
					word += char;
				}
			}
			if (c === str.length - 1) {
				if ((char === "." || char === ",") && inNum) {
					ret.push({num: true, val: word.slice(0, -1)});
					ret.push({num: false, val: char});
				} else {
					ret.push({num: inNum, val: word});
				}
			}
			prevChar = char;
		}
		return ret;
	};

	function getRandomCharCode(uppercase) {
		if (uppercase) {
			//uppercase characters:
			return eve.getRandomInt(65, 90);
		} else {
			//lowercase characters:
			return eve.getRandomInt(97, 122);
		}
	}

	function getCharType(c) {
		var ret = "symbol";
		if (c.charCodeAt(0) > 47 && c.charCodeAt(0) < 58) {
			ret = "number";
		} else if (c.charCodeAt(0) > 64 && c.charCodeAt(0) < 91) {
			ret = "letterUC";
		} else if (c.charCodeAt(0) > 96 && c.charCodeAt(0) < 123) {
			ret = "letterLC";
		}
		return ret;
	}

	function getInnerText(elm) {
		if (strIs(elm.nodeName, "#text")) {
			return elm.nodeValue;
		} else {
			return elm.innerHTML;
		}
	}

	function setInnerText(elm, str) {
		if (strIs(elm.nodeName, "#text")) {
			elm.nodeValue = str;
		} else {
			elm.innerHTML = str;
		}
	}

	eve.flipText = function(target, endText, duration, randomise, startText) {

		duration = typeof duration !== 'undefined' ? duration : 1;
		randomise = typeof randomise !== 'undefined' ? randomise : false;
		endText = endText || target.nodeValue;

		if (!isNil(target.eveFlipper)) {
			cancelAnimationFrame(target.eveFlipper);
			target.eveFlipper = null;
		}
		if (!isNil(target.eveFlipTargetText) && endText !== target.eveFlipTargetText) {
			setInnerText(target, target.eveFlipTargetText);
		}
		startText = typeof startText !== 'undefined' ? startText : getInnerText(target);
		target.eveFlipTargetText = endText;

		var chunksFrom = splitMixedString(startText) || [{num: false, val: " "}];
		var chunksTo = splitMixedString(endText) || [{num: false, val: " "}];
		var c;

		var startTime = Date.now();
		var changeDuration = duration * 1000.0;

		function flipLoop() {
			var retStr = "";
			var timeGone = Date.now() - startTime;
			var doneSoFar = timeGone / changeDuration;
			for (c = 0; c < chunksTo.length; c++) {
				if (timeGone < changeDuration) {
					if (!chunksTo[c].num) {
						//it's letters:
						var lenFrom = chunksFrom[c].val.length;
						var lenTo = chunksTo[c].val.length;
						var len = lenTo;
						if (lenFrom !== lenTo) {
							len = Math.floor(lenFrom + (doneSoFar * (lenTo - lenFrom)));
						}
						if (chunksFrom[c] === chunksTo[c]) {
							retStr += chunksFrom[c];
						} else {
							for (var m = 0; m < len; m++) {
								var fromCharCode = chunksFrom[c].val.substr(m, 1).charCodeAt(0);
								if (isNaN(fromCharCode)) {
									fromCharCode = getRandomCharCode(getCharType(chunksTo[c].val.substr(0, 1)) === "letterUC");
								}
								var toCharCode = chunksTo[c].val.substr(m, 1).charCodeAt(0);
								if (isNaN(toCharCode)) {
									toCharCode = getRandomCharCode(getCharType(chunksFrom[c].val.substr(0, 1)) === "letterUC");
								}

								var charCode = Math.floor(fromCharCode + (doneSoFar * (toCharCode - fromCharCode)));
								if ((charCode < 32 || charCode > 126) && (toCharCode !== fromCharCode)) {
									charCode = eve.getRandomInt(48, 122);
								}
								//space:
								if (toCharCode === 32) {
									charCode = 32;
								}
								if (randomise) {
									charCode = getRandomCharCode(getCharType(chunksTo[c].val.substr(0, 1)) === "letterUC");
								}
								retStr += String.fromCharCode(charCode);
							}
						}
					} else {
						//it's a number:
						if (timeGone < changeDuration) {
							var val = 0;
							var fmtStr = ".0";
							if (chunksTo[c].val.indexOf(".") !== -1) {
								var trailing = chunksTo[c].val.substr(chunksTo[c].val.indexOf(".") + 1);
								trailing = trailing.replace(/[^0-9]/g, '');
								fmtStr = "." + trailing.length.toString();
							}
							if (chunksTo[c].val.indexOf(",") !== -1) {
								fmtStr += ",";
							}
							if (chunksTo[c].val.indexOf("+") === 0 || chunksTo[c].val.indexOf("-") === 0) {
								fmtStr += "+";
							}

							var valMultiplier = 1;
							var valNum = chunksFrom[c].val.replace(/,/g, "");
							if (valNum.indexOf("-") === 0) {
								valMultiplier = -1;
							}
							valNum = valNum.replace(/[^0-9\.]/g, '');
							var valFrom = parseFloat(valNum) * valMultiplier;
							if (isNaN(valFrom) || valFrom === "") {
								valFrom = 0;
							}

							valMultiplier = 1;
							valNum = chunksTo[c].val.replace(/,/g, "");
							if (valNum.indexOf("-") === 0) {
								valMultiplier = -1;
							}
							valNum = valNum.replace(/[^0-9\.]/g, '');
							var valTo = parseFloat(valNum) * valMultiplier;
							if (isNaN(valTo) || valTo === "") {
								valTo = 0;
							}

							var totChange = valTo - valFrom;
							if (timeGone < changeDuration) {
								val = parseFloat(valFrom) + (doneSoFar * totChange);
							} else {
								val = valTo;
							}
							if (fmtStr !== "") {
								val = eve.formatNumber(val, fmtStr);
							}
							retStr += val;
						} else {
							retStr += chunksTo[c].val;
						}
					}
				} else {
					retStr = endText;
				}
			}
			setInnerText(target, retStr);
			if (timeGone < changeDuration) {
				target.eveFlipper = requestAnimationFrame(flipLoop);
			} else {
				target.eveFlipTargetText = null;
				target.eveFlipper = null;
			}
		}

		//if the strings start with different number/letter chunks, add a new chunk to the front of the target:
		if (chunksFrom.length === 0 || chunksTo[0].num !== chunksFrom[0].num) {
			if (chunksFrom[0].num) {
				chunksTo.unshift({val: "0", num: true});
			} else {
				chunksTo.unshift({val: " ", num: false});
			}
		}

		//if the source string has more chunks than the target, merge the last source chunks into one:
		if (chunksTo.length < chunksFrom.length) {
			var strConcat = "";
			for (c = chunksTo.length - 1; c < chunksFrom.length; c++) {
				strConcat += chunksFrom[c].val;
			}
			chunksFrom[chunksTo.length - 1].val = strConcat;
			chunksFrom.splice(chunksTo.length, chunksFrom.length - chunksTo.length);
		}

		//if the target string has more chunks than the source, add new chunks to the source:
		if (chunksFrom.length < chunksTo.length) {
			for (c = chunksFrom.length; c < chunksTo.length; c++) {
				if (chunksTo[c].num) {
					chunksFrom[c] = {num: true, val: "0"};
				} else {
					chunksFrom[c] = {num: false, val: " "};
				}
			}
		}
		flipLoop();
	};

	eve.flipElmText = function(elm, duration, delay) {
		var t;
		var tElm = [];
		var tDelay = delay || 0;
		// if (strIs(elm.nodeName, "tspan")) {
		// 	for (t = 0; t < elm.childNodes.length; t++) {
		// 		if (strIs(elm.childNodes[t].nodeName, "#text")) {
		// 			tElm.push(elm.childNodes[t]);
		// 		}
		// 	}
		// } else if (strIs(elm.nodeName, "#text")) {
		// 	tElm.push(elm);
		// }
		if (strIs(elm.tagName, "text")) {
			for (t = 0; t < elm.childNodes.length; t++) {
				if (strIs(elm.childNodes[t].nodeName, "#text")) {
					tElm.push(elm.childNodes[t]);
				}
			}
		}
		else if (strIs(elm.tagName, "tspan")) {
			tElm.push(elm);
		}

		setTimeout(function() {
			for (t = 0; t < tElm.length; t++) {
				eve.flipText(tElm[t], tElm[t].newText, duration, null, tElm[t].oldText);
			}
		}, tDelay * 1000.0);
	};

	//attributes that are SOLELY SVG attrs - e.g. don't include "opacity" in this list since it is also a style property:
	var svgAttributes = ["cx", "cy", "r", "d", "rx", "ry", "dx", "dy", "x1", "x2", "y1", "y2", "height", "width"];

	var cartesianToPolar = function(centre, point, zeroIs360) {
		var x = point[0] - centre[0];
		var y = point[1] - centre[1];
		var distance = Math.sqrt(x * x + y * y);
		var radians = Math.atan2(y, x) + (Math.PI / 2);

		if (point[0] === centre[0]) {
			distance = Math.abs(point[1] - centre[1]);
			if (point[1] < centre[1]) {
				radians = 0;
			} else {
				radians = Math.PI;
			}
		} else if (point[1] === centre[1]) {
			distance = Math.abs(point[0] - centre[0]);
			if (point[0] < centre[0]) {
				radians = (Math.PI / 2) * 3;
			} else {
				radians = Math.PI / 2;
			}
		}

		if (zeroIs360 && radians === 0) {
			radians = Math.PI * 2;
		}

		if (radians < 0) {
			radians = (Math.PI * 2) + radians;
		} else if (radians > Math.PI * 2) {
			radians -= Math.PI * 2;
		}
		return {distance: distance, radians: radians, degrees: eve.toDegrees(radians)};
	};

	eve.polarToCartesian = function(angle, distance, cenX, cenY, flipY) {
		var ret = [distance * Math.sin(angle), distance * Math.cos(angle)];
		if (flipY) {ret[1] *= -1;}
		if (cenX) {ret[0] += cenX;}
		if (cenY) {ret[1] += cenY;}
		return ret;
	};

	eve.getArcY = function(x, r) {
		return Math.sqrt((r * r) - (x * x));
	};
	eve.getArcX = function(y, r) {
		return Math.sqrt(Math.abs((r * r) - (y * y)));
	};

	eve.fixAngleDegs = function(a){
		if (a < 0){
			return a + 360;
		}
		if(a > 360){
			return a - 360;
		}
		return a;
	};

	eve.findAngle = function(A, B, C, clockwise) {
		if (isNil(C)) {
			//angle between 2 points
			return Math.atan2(B[1] - A[1], B[0] - A[0]);
		}
		//angle between 3 points:
		/* A first point
		 * C second point
		 * B center point
		 */
		if (clockwise) {
			return Math.atan2(C[1] - B[1], C[0] - B[0]) - Math.atan2(A[1] - B[1], A[0] - B[0]);
		}
		return Math.atan2(A[1] - B[1], A[0] - B[0]) - Math.atan2(C[1] - B[1], C[0] - B[0]);

	};

	eve.fixAngle = function(radians, zeroIs360) {
		if (radians < 0) {
			radians += Math.PI * 2;
		}
		if (radians > Math.PI * 2) {
			radians -= Math.PI * 2;
		}
		if (zeroIs360 && radians === 0) {
			radians = Math.PI * 2;
		}
		return radians;
	};

	eve.isEven = function(n) {
		return n % 2 === 0;
	};

	eve.isOdd = function(n) {
		return Math.abs(n % 2) === 1;
	};
	eve.toDegrees = function(angle) {
		return angle * (180 / Math.PI);
	};
	eve.toRadians = function(angle) {
		return angle * (Math.PI / 180);
	};
	eve.splitTrim = function(str, splitChar) {
		var a = str.split(splitChar),
			m;
		for (m = 0; m < a.length; m++) {
			a[m].trim();
		}
		return a;
	};
	eve.sortNumber = function(a, b) {
		return a - b;
	};

	//Returns true if it is a DOM node
	eve.isElement = function(elm) {
    // works on major browsers back to IE7
		return elm instanceof Element;
	};

	//sort an array of objects by an object property
	function sortByProperty(prop) {
		return function(a, b) {
			if (a.hasOwnProperty(prop) && b.hasOwnProperty(prop)) {
				return (a[prop] < b[prop]) ? -1 : (a[prop] > b[prop]) ? 1 : 0;
			}
		};
	}

	eve.getNum = function(num) {
		var ret = 0;
		if (!isNil(parseFloat(num)) && !isNaN(parseFloat(num))) {
			ret = parseFloat(num);
		}
		return ret;
	};

	eve.makeNum = function(strNum, defaultToZero) {
		var ret = strNum;
		//convert a string to a number only if it is a number to begin with:
		if (!isNil(strNum)) {
			strNum = strNum.toString().replace("%", "").replace("px", "");
			if (!isNil(parseFloat(strNum)) && !isNaN(parseFloat(strNum))) {
				if (parseFloat(strNum).toString() === strNum) {
					ret = parseFloat(strNum);
					if (parseInt(ret) === ret) {
						ret = parseInt(ret);
					}
				}
			}
		}
		if (defaultToZero && ret === "") {
			ret = 0;
		}
		return ret;
	};

	eve.getRootSVG = function(elm) {
		var obj = elm.parentNode;
		while (!strIs(obj.tagName, "html")) {
			if (strIs(obj.nodeName, "svg")) {
				return obj;
			} else {
				obj = obj.parentNode;
			}
		}
	};

	eve.getSvgDefs = function(svgElm) {
		for (var c = 0; c < svgElm.childNodes.length; c++) {
			if (!isNil(svgElm.childNodes[c].tagName) && strIs(svgElm.childNodes[c].tagName, "defs")) {
				return svgElm.childNodes[c];
			}
		}
		return null;
	};

	eve.setSvgTransform = function(elm, transform, val) {
		var t;
		var tTransform = null;
		var tType = SVG_TRANSFORM_TRANSLATE;
		if (transform === "rotate") {
			tType = SVG_TRANSFORM_ROTATE;
		}
		else if (transform === "scale") {
			tType = SVG_TRANSFORM_SCALE;
		}

		if (!isNil(elm.transform)) {
			for (t = 0; t < elm.transform.baseVal.numberOfItems; t++) {
				if (elm.transform.baseVal.getItem(t).type === tType) {
					tTransform = elm.transform.baseVal.getItem(t);
					break;
				}
			}

			if (tTransform === null) {
				var tMatrix = document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGMatrix();
				tTransform = elm.transform.baseVal.createSVGTransformFromMatrix(tMatrix);
				elm.transform.baseVal.appendItem(tTransform);
				t = elm.transform.baseVal.numberOfItems - 1;
			}
			if (transform === "translate") {
				var elmTrans = eve.getSvgTransforms(elm);
				var elmX = elmTrans.x || 0;
				var elmY = elmTrans.y || 0;
				if (!eve.isArray(val)) {
					val = [val, elmY];
				} else if (val.length === 1) {
					val.push(elmY);
				} else if (val[0] === null) {
					val[0] = elmX;
				}
				elm.transform.baseVal.getItem(t).setTranslate(val[0] || 0, val[1] || 0);
			}
			else if (transform === "scale") {
				if (!eve.isArray(val)) {
					val = [val, val];
				}
				elm.transform.baseVal.getItem(t).setScale(val[0], val[1]);
			}
			else {
				if (!eve.isArray(val)) {
					val = [val, 0, 0];
				} else if (val.length === 1) {
					val.push(0);
					val.push(0);
				} else if (val.length === 2) {
					val.push(0);
				}
				var eSize = getSvgElmSize(elm);
				for (var c = 1; c < 3; c++) {
					if (val[c].toString().indexOf("%") !== -1) {
						val[c] = eSize[c] * (parseFloat(val[c]) / 100.0);
					} else if (val[c].toString().indexOf("px") !== -1) {
						val[c] = parseFloat(val[c]);
					}
				}
				elm.transform.baseVal.getItem(t).setRotate(val[0], val[1], val[2]);
			}
		}
//		else {
//
//		}
	};

	eve.getSvgTransforms = function(elm) {
		var t;
		var ret = {x: 0, y: 0, scale: 1, scaleY: null, rotate: 0, rotateX: null, rotateY: null, skewX: null, skewY: null};

		for (t = 0; t < elm.transform.baseVal.numberOfItems; t++) {
			var transform = elm.transform.baseVal.getItem(t);
			var m = transform.matrix;
			switch (transform.type) {
				case SVG_TRANSFORM_TRANSLATE:
					ret.x += m.e;
					ret.y += m.f;
					break;
				case SVG_TRANSFORM_SCALE:
					ret.scale *= m.a;
					ret.scaleY *= m.d;
					break;
				case SVG_TRANSFORM_ROTATE:
					ret.rotate += transform.angle;
					break;
				case SVG_TRANSFORM_SKEWX:
					break;
				case SVG_TRANSFORM_SKEWY:
					break;
				case SVG_TRANSFORM_MATRIX:
					break;
				case SVG_TRANSFORM_UNKNOWN:
					break;
			}
		}
		return ret;
	};

	var setText = function(elm) {
		for (var e = 0; e < elm.childNodes.length; e++) {
			elm.childNodes[e].firstChild.nodeValue = elm.childNodes[e].newText;
			if (!isNil(elm.childNodes[e].newX)) {
				elm.childNodes[e].setAttribute("x", elm.childNodes[e].newX);
			}
			if (!isNil(elm.childNodes[e].newY)) {
				elm.childNodes[e].setAttribute("y", elm.childNodes[e].newY);
			}
			if (!isNil(elm.childNodes[e].newRot)) {
				elm.childNodes[e].setAttribute("rotation", elm.childNodes[e].newRot);
			}
		}
	};

	var setPath = function(elm) {
		elm.setAttribute("d", elm.path.toSVG());
		if (!isNil(elm._knots)) {
			for (var k = 0; k < elm._knots.length; k++) {
				var knot = get(elm._knots[k].id, elm);
				if (!isNil(knot)) {
					knot.setAttribute("cx", elm.path.commands[k].to[0]);
					knot.setAttribute("cy", elm.path.commands[k].to[1]);
				}
			}
		}
	};

	//this function is necessary because <IE9 doesn't support addEventListener:
	eve.addEventHandler = function(elm, eventType, handler, useCapture) {
		useCapture = useCapture || false;
		if (elm.addEventListener) {
			elm.addEventListener(eventType, handler, useCapture);
		} else if (elm.attachEvent) {
			elm.attachEvent('on' + eventType, handler);
		}
	};

	//this function is necessary because <IE9 doesn't support removeEventListener:
	eve.removeEventHandler = function(elm, eventType, handler) {
		if (elm.removeEventListener) {
			elm.removeEventListener(eventType, handler, false);
		} else if (elm.detachEvent) {
			elm.detachEvent('on' + eventType, handler);
		}
	};

	function eResize() {
		var e, svgElm;
		for (e = 0; e < resizeElms.length; e++) {
			var cWidth = getChartWidth(resizeElms[e]);
			var cHeight = getChartHeight(resizeElms[e]);
			if (resizeElms[e]._width !== cWidth || resizeElms[e]._height !== cHeight){
				svgElm = resizeElms[e]._artboard;
				resizeElms[e].width = resizeElms[e].height = "auto";
				setChartSize(resizeElms[e])
				svgElm.setAttribute("viewBox", "0 0 " + resizeElms[e]._width + " " + resizeElms[e]._height);
				rejigTextElms(svgElm, resizeElms[e]);
				resizeElms[e].draw(0, null, null, null, null, null, null, true);
			}
		}
	}

	eve.afterResize = function(){
		var e, svgElm;
		for (e = 0; e < resizeElms.length; e++) {
			var cWidth = getChartWidth(resizeElms[e]);
			var cHeight = getChartHeight(resizeElms[e]);
			if (resizeElms[e]._width !== cWidth || resizeElms[e]._height !== cHeight){
				svgElm = resizeElms[e]._artboard;
				svgElm.style.visibility = "hidden";
				resizeElms[e]._width = resizeElms[e]._height = "auto";
			}
		}
		if (eve.resizeTimer){
			clearTimeout(eve.resizeTimer);
		}
		eve.resizeTimer = setTimeout(function() {
			eResize();
		}, 100);
	};

	if (typeof window !== "undefined"){
		eve.addEventHandler(window, "resize", eve.afterResize, false);
	}

	function getSvgElmSize(elm) {
		var eBox = {x: 0, y: 0, width: 0, height: 0};
		var cElm = elm;
		var dNoneElms = [];
		while (!isNil(cElm)){
			if (cElm.style && cElm.style.display === "none"){
				dNoneElms.push(cElm);
				cElm.style.display = "block";
			}
			cElm = cElm.parentNode;
		}
		if (strIs(elm.tagName, "tspan")) {
			var extent = elm.getExtentOfChar(0);
			var width = elm.getComputedTextLength();
			eBox.x = extent.x;
			eBox.y = extent.y;
			eBox.width = width;
			eBox.height = extent.height;
		}
		else if (strIs(elm.tagName, "text") && elm.textContent !== "") {
			eBox = elm.getBBox();
		}
		for (var d = 0; d < dNoneElms.length;d++){
			dNoneElms[d].style.display = "none";
		}
		return eBox;
	}

	var rejigTextElms = function(svgElm, chart) {
		var c, z;
		var tspans = Array.prototype.slice.call(svgElm.getElementsByTagName("tspan"), 0);
		var textElms = svgElm.getElementsByTagName("text");
		var rebuildElms = [];
		for (c = 0; c < textElms.length; c++) {
			if (textElms[c].childNodes.length > 1) {
				rebuildElms.push(textElms[c]);
				for (z = 0; z < textElms[c].childNodes.length; z++) {
					tspans.splice(tspans.indexOf(textElms[c].childNodes[z]), 1);
				}
			}
		}
		for (c = 0; c < rebuildElms.length; c++) {
			var bOptions = rebuildElms[c]._buildOptions;
			if (bOptions) {
				bOptions.drawCall = 1;
			}
			rebuildElms[c] = eve.makeText(chart, rebuildElms[c].parentNode, c._drawMode, bOptions);
		}

		var tBaseline = 0;
//		var lBottom = 0;
		var numLines = 0;
		var lineHeight = [];
		for (c = 0; c < tspans.length; c++) {
			if (tspans[c].dStyle) {
				var dBox = getSvgElmSize(tspans[c]);
				tspans[c].dStyle.tSize = {width: dBox.width, height: dBox.height};
				if (tspans[c].dStyle.line > numLines) {
					numLines = tspans[c].dStyle.line;
				}
				if (isNil(lineHeight[tspans[c].dStyle.line]) || lineHeight[tspans[c].dStyle.line] < tspans[c].dStyle.tSize.height) {
					lineHeight[tspans[c].dStyle.line] = tspans[c].dStyle.tSize.height;
				}
			}
		}

		for (c = 0; c < tspans.length; c++) {
//			lBottom = tBaseline;
			if (tspans[c].dStyle) {
				tspans[c].dStyle.y = tBaseline;
			}
		}

	};

	eve.sameSign = function(a, b) {
		if (a === 0 && b === 0) {
			return true;
		} else {
			if ((a > 0 && b > 0) || (a < 0 && b < 0)) {
				return true;
			} else {
				return false;
			}
		}
	};

	eve.isEqual = function(objA, objB) {
		var omitList = ["_series", "_chart"];
		var prop, ap;
		if (isNil(objA) && isNil(objB)) {
			return true;
		} else if (eve.isArray(objA)) {
			if (!eve.isArray(objB)) {
				return false;
			}
			if (objA.length !== objB.length) {
				return false;
			}
			for (var e = 0; e < objA.length; e++) {
				if (!eve.isEqual(objA[e], objB[e])) {
					return false;
				}
			}
			return true;
		} else if (typeof objA === 'object') {
			if (objA instanceof eve.axis) {
				if (objB instanceof eve.axis === false) {
					return false;
				}
				for (ap = 0; ap < axisPropsRedraw.length; ap++) {
					if (!eve.isEqual(objA[axisPropsRedraw[ap]], objB[axisPropsRedraw[ap]])) {
						return false;
					}
				}
			} else {
				if (typeof objB !== 'object') {
					return false;
				} else {
					if (Object.keys(objA).length !== Object.keys(objB).length) {
						return false;
					}
				}
				if (objA === objB) {
					return true;
				}
				for (prop in objA) {
					//don't iterate through all the series properties for each node and all the chart properties for each axis - it causes a stack error! :
					if (objA.hasOwnProperty(prop) && omitList.indexOf(prop) === -1) {
						if (objB.hasOwnProperty(prop)) {
							if (!eve.isEqual(objA[prop], objB[prop])) {
								return false;
							}
						} else {
							return false;
						}
					}
				}
			}
			return true;
		} else {
			if (objA === objB) {
				return true;
			}
		}
	};

	function getQueryString() {
		var querystring = [];
		var q = String(document.location).split('#')[0];
		q = q.split('?')[1];
		if (!q) {
			return false;
		}
		q = q.split('&');
		for (var i = 0; i < q.length; i++) {
			var o = q[i].split('=');
			querystring[o[0]] = o[1];
		}
		return querystring;
	}


	eve.getQueryValue = function(name) {
		return getQueryString()[name];
	};

	eve.percentToDecimal = function(num){
		if (typeof num === "string" && num.indexOf("%") !== -1){
			return parseFloat(num)/100.0;
		}
	};

	eve.roundHalfDown = function(num) {
		return -Math.round(-num);
	};

	eve.setDecimal = function(num, decPlaces) {
		var denominator = "1";
		var step = 0;
		while (step < decPlaces) {
			step++;
			denominator += "0";
		}
		denominator = parseInt(denominator);
		return Math.round(num * denominator) / denominator;
	};

	eve.formatNumber = function(num, format) {
		num = parseFloat(num);
		var strNum = num.toString() || "0";
		format = format || "";
		if (format === "auto") {
			format = "";
		}
		var pluralise = false;
		var comma = "";
		var multiplier = 1;
		var denominator = 1;
		var sign = "";
		var abs = false;
		var floor = false;
		var round = false;

		if (format.indexOf("&") !== -1) {
			abs = true;
			format = format.replace(/\&/g, "");
		}

		if (format.indexOf("!") !== -1) {
			floor = true;
			format = format.replace(/\!/g, "");
		}

		if (format.indexOf("^") !== -1) {
			round = true;
			format = format.replace(/\^/g, "");
		}

		if (format.indexOf("~") !== -1) {
			pluralise = true;
			format = format.replace(/\~/g, "");
		}

		if (format.indexOf("+") !== -1) {
			sign = "+";
			format = format.replace(/\+/g, "");
		}

		if (strNum.indexOf("-") === 0) {
			if (!abs) {
				sign = "-";
			}
			strNum = strNum.substr(1);
			num = Math.abs(num);
		}

		if (round) {
			num = Math.round(num);
		}
		if (floor) {
			num = Math.floor(num);
		}

		if (format.indexOf("x") !== -1) {
			multiplier = parseFloat(format.split("x")[1]);
			format = format.replace("x" + multiplier.toString(), "");
		}
		if (format.indexOf("/") !== -1) {
			denominator = parseFloat(format.split("/")[1]);
			format = format.replace("/" + denominator.toString(), "");
		}

		var decPlaces = -1; //means keep the original decimal
		if (format.indexOf(",") !== -1) {
			format = format.replace(/,/g, "");
			comma = ",";
		}
		var start = format.split(".")[0];
		var end = "";
		if (format.indexOf(".") !== -1) {
			end = format.split(".")[1];
			decPlaces = parseInt(end);
		}
		if (end !== "") {
			end = end.replace(decPlaces.toString(), "");
		}

		num = (Math.floor((num * multiplier) * 1000000)) / 1000000; //to get around floating point precision problems
		num = (Math.floor((num / denominator) * 1000000)) / 1000000; //to get around floating point precision problems
		for (var c = 0; c < decPlaces; c++) {
			num *= 10;
		}
		num = Math.round(num);
		for (var dp = 0; dp < decPlaces; dp++) {
			num /= 10;
		}
		if (decPlaces !== -1){
			num = eve.setDecimal(num, decPlaces);
		}
		if (abs) {
			num = Math.abs(num);
		}

		strNum = num.toString();

		var wholeNum = num.toString().split(".")[0];
		if (Math.abs(num) > 999 && comma !== "") {
			var endNum = "";
			var theNum = wholeNum;
			while (theNum.length > 3) {
				endNum = comma + theNum.substr(theNum.length - 3) + endNum;
				theNum = theNum.substr(0, theNum.length - 3);
			}
			wholeNum = theNum + endNum;
		}

		var dec = "";
		var sep = "";
		if (strNum.indexOf(".") !== -1) {
			dec = strNum.split(".")[1];
		}
		if (decPlaces > 0) {
			sep = ".";
			if (dec !== "") {
				if (dec.length > decPlaces) {
					dec = dec.substr(0, decPlaces - 1) + Math.round((parseInt(dec.substr(decPlaces - 1, 2))) / 10).toString();
				} else {
					while (dec.length < decPlaces) {
						dec = dec + "0";
					}
				}
			} else {
				while (dec.length < decPlaces) {
					dec = dec + "0";
				}
			}
		} else if (decPlaces === -1 && dec !== "") {
			sep = ".";
		}
		if (start.indexOf("%") !== -1 && end.indexOf("%") === -1) {
			start = start.replace(/%/g, "");
			end = end + "%";
		}

		if (parseFloat(start + wholeNum + dec) !== 1 && pluralise){
			end +="s";
		}

		return sign + start + wholeNum + sep + dec + end;
	};

	//find all elements with a particular ID prefix:
	eve.getElmsByIdPrefix = function(idPrefix, holder) {
		holder = holder || document;
		var matches = [];
		var elms = holder.getElementsByTagName("*");
		for (var i = 0; i < elms.length; i++) {
			if (elms[i].id.indexOf(idPrefix) === 0) {
				matches.push(elms[i]);
			}
		}
		return matches;
	};

	//get a random number between 2 integers:
	eve.getRandomInt = function(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	var makeDraggable = function(elm, borderWidth) {
		elm.dragBorder = borderWidth;
		elm.oldCursor = elm.style.cursor || "default";
		elm.dims = getCssDims(elm);

		if (!isNil(elm.dragBorder)) {
			eve.addEventHandler(elm, "mousemove", function(evt) {
				if (!elm.dragging) {
					elm.dims = getCssDims(elm);
					var mouseX = evt.clientX - parseFloat(elm.dims.left);
					var mouseY = evt.clientY - parseFloat(elm.dims.top);
					if (mouseX > elm.dragBorder && mouseY > elm.dragBorder && mouseX < parseFloat(elm.dims.width) - elm.dragBorder && mouseY < parseFloat(elm.dims.height) - elm.dragBorder) {
						elm.style.cursor = elm.oldCursor;
						eve.removeEventHandler(elm, "mousedown", domStartDrag);
						eve.removeEventHandler(elm, "touchstart", domStartDrag);
					} else {
						eve.addEventHandler(elm, 'mousedown', domStartDrag);
						eve.addEventHandler(elm, 'touchstart', domStartDrag);
						elm.style.cursor = "move";
					}
				}
			}, false);
		} else {
			eve.addEventHandler(elm, 'mousedown', domStartDrag);
			eve.addEventHandler(elm, 'touchstart', domStartDrag);
			elm.style.cursor = "move";
		}
	};
	var domStartDrag = function(evt) {
		var elm = evt.currentTarget;
		elm.dragging = true;
		if (evt.type === "touchstart") {
			elm.clickClientX = evt.touches[0].clientX;
			elm.clickClientY = evt.touches[0].clientY;
		} else {
			if (!buttonIsLeft(evt.button)) {
				return null;
			}
			evt.preventDefault();
			elm.clickClientX = evt.clientX;
			elm.clickClientY = evt.clientY;
		}

		elm.dims = getCssDims(elm);
		elm.cursor = "move";
		if (evt.type === "touchstart") {
			eve.addEventHandler(window, "touchmove", domDoDrag);
			eve.addEventHandler(window, "touchend", domStopDrag);
		} else {
			eve.addEventHandler(window, "mousemove", domDoDrag);
			eve.addEventHandler(window, "mouseup", domStopDrag);
		}
		window.eveDraggingElm = elm;
		elm.startLeft = parseFloat(elm.dims.left);
		elm.startTop = parseFloat(elm.dims.top);

	};
	var domDoDrag = function(evt) {
		if (evt.type !== "touchstart") {
			evt.preventDefault();
		}
		var pX = evt.clientX;
		var pY = evt.clientY;
		if (evt.type === "touchmove") {
			pX = evt.touches[0].clientX;
			pY = evt.touches[0].clientY;
		}
		var xChange = pX - window.eveDraggingElm.clickClientX;
		var yChange = pY - window.eveDraggingElm.clickClientY;
		window.eveDraggingElm.style.left = (window.eveDraggingElm.startLeft + xChange) + "px";
		window.eveDraggingElm.style.top = (window.eveDraggingElm.startTop + yChange) + "px";
	};
	var domStopDrag = function() {
		window.eveDraggingElm.dragging = false;
		window.eveDraggingElm.style.cursor = window.eveDraggingElm.oldCursor;
		eve.removeEventHandler(window, "touchmove", domDoDrag);
		eve.removeEventHandler(window, "touchend", domStopDrag);
		eve.removeEventHandler(window, "mousemove", domDoDrag);
		eve.removeEventHandler(window, "mouseup", domStopDrag);
		window.eveDraggingElm = null;
	};

	var isIE8orLower = function() {
		if (window.attachEvent && !window.addEventListener) {
			return true;
		}
		return false;
	};

	var buttonIsLeft = function(btn) {
		if (isIE8orLower() && btn === 1) {
			return true;
		} else if (btn === 0) {
			return true;
		}
		return false;
	};

	var startDrag = function(evt, elm, restrict, rangeMin, rangeMax, dragFunction, dragCompleteFunction) {
		if (evt.type !== "touchstart") {
			evt.preventDefault();
			if (!buttonIsLeft(evt.button)) {
				return null;
			}
		}
		var chartReady = true;
		var chart;
		if (!isNil(elm._labelObj) && !isNil(elm._labelObj._chart)) {
			chart = getEveElmByID(elm._labelObj._chart);
//			chart = elm._labelObj._chart;
		}
		if (!isNil(elm._eveElm) && !isNil(elm._eveElm._chart)){
			chart = getEveElmByID(elm._eveElm._chart);
//			chart = elm._eveElm._chart;
		}
		if (!isNil(chart)) {
			chartReady = chart._ready;
		}
		if (chartReady) {
			elm.dragFunction = dragFunction || null;
			elm.dragCompleteFunction = dragCompleteFunction || null;
			elm.dragging = true;
			if (elm.tagName === "circle") {
				elm._dragPosX = elm.getAttribute("cx");
				elm._dragPosY = elm.getAttribute("cy");
				elm.style.fillOpacity = 0.5;
			} else {
				var et = eve.getSvgTransforms(elm);
				elm._dragPosX = et.x;
				elm._dragPosY = et.y;
			}
			if (!isNil(restrict)) {
				elm.dragMinX = 0;
				if (!isNil(restrict.xMin)){
					if (restrict.xMin.toString().indexOf("%") !== -1 && !isNil(chart)){
						elm.dragMinX = (parseFloat(restrict.xMin) / 100.0) * chart._plotWidth;
					}
					else{
						elm.dragMinX = restrict.xMin;
					}
				}
				else if (!isNil(restrict.xMinVal) && !isNil(elm._labelObj) && !isNil(elm._labelObj._link) && elm._labelObj._link instanceof eve.axis){
					elm.dragMinX = elm._labelObj._link.getScreenPos(restrict.xMinVal);
				}
				elm.dragMaxX = 0;
				if (!isNil(restrict.xMax)){
					if (restrict.xMax.toString().indexOf("%") !== -1 && !isNil(chart)){
						elm.dragMaxX = (parseFloat(restrict.xMax) / 100.0) * chart._plotWidth;
					}
					else{
						elm.dragMaxX = restrict.xMax;
					}
				}
				else if (!isNil(restrict.xMinVal) && !isNil(elm._labelObj) && !isNil(elm._labelObj._link) && elm._labelObj._link instanceof eve.axis){
					elm.dragMaxX = elm._labelObj._link.getScreenPos(restrict.xMaxVal);
				}
				elm.dragMaxY = 0;
				if (!isNil(restrict.yMin) && !isNil(chart)){
					if (restrict.yMin.toString().indexOf("%") !== -1){
						elm.dragMaxY = (100 - (parseFloat(restrict.yMin) / 100.0)) * chart._plotHeight;
					}
					else{
						elm.dragMaxY = chart._plotHeight - restrict.yMin;
					}
				}
				else if (!isNil(restrict.yMinVal) && !isNil(elm._labelObj) && !isNil(elm._labelObj._link) && elm._labelObj._link instanceof eve.axis){
					elm.dragMaxY = elm._labelObj._link.getScreenPos(restrict.yMinVal);
				}
				elm.dragMinY = 0;
				if (!isNil(restrict.yMax) && !isNil(chart)){
					if (restrict.yMax.toString().indexOf("%") !== -1){
						elm.dragMinY = (100 - (parseFloat(restrict.yMax) / 100.0)) * chart._plotHeight;
					}
					else{
						elm.dragMinY = chart._plotHeight - restrict.yMax;
					}
				}
				else if (!isNil(restrict.yMaxVal) && !isNil(elm._labelObj) && !isNil(elm._labelObj._link) && elm._labelObj._link instanceof eve.axis){
					elm.dragMinY = elm._labelObj._link.getScreenPos(restrict.yMaxVal);
				}
			}

			if (!isNil(elm._offsetX)) {
				elm.dragMinX -= elm._offsetX;
				elm.dragMaxX -= elm._offsetX;
			}
			if (!isNil(elm._offsetY)) {
				elm.dragMinY -= elm._offsetY;
				elm.dragMaxY -= elm._offsetY;
			}

			elm.axisMin = rangeMin || 0;
			elm.axisMax = rangeMax || 1;
			elm.startX = elm._dragPosX;
			elm.startY = elm._dragPosY;
			window.eveDraggingElm = elm;
			if (evt.type === "touchstart") {
				elm.clickClientX = evt.touches[0].clientX;
				elm.clickClientY = evt.touches[0].clientY;
				eve.addEventHandler(window, "touchmove", doDrag);
				eve.addEventHandler(window, "touchend", stopDrag);
			} else {
				elm.clickClientX = evt.clientX;
				elm.clickClientY = evt.clientY;
				eve.addEventHandler(window, "mousemove", doDrag);
				eve.addEventHandler(window, "mouseup", stopDrag);
			}
		}
	};

	var updateChildren = function(elm, fromDrag) {
		var posX, posY;
		var et = eve.getSvgTransforms(elm);
		var dX = et.x;
		var dY = et.y;

		if (fromDrag) {
			if (!isNil(elm._dragPosX)) {
				dX = elm._dragPosX;
			}
			if (!isNil(elm._dragPosY)) {
				dY = elm._dragPosY;
			}
		}

		if (!isNil(elm._intNodes)) {
			for (var n = 0; n < elm._intNodes.length; n++) {
				var nod = get(getEveElmByID(elm._intNodes[n]).domID, elm);
				var sObj = get(nod._seriesObj, elm);
				var sType = getType(nod._seriesObjType);
				var sPoints = sObj.points.slice();
				if (sType === "funnel") {
					sPoints = sObj.points.slice(0, sObj.points.length / 2);
					if (elm._intNodes[n]._bottom) {
						sPoints = sObj.points.slice(sObj.points.length / 2).reverse();
					}
				}
				var tangents = lineFiniteDifferences(sPoints);
				posY = getPoint(dX + elm._offsetX, sObj.path, sObj.interpolation, tangents, sObj.direction === "up" || sObj.direction === "down");
				posX = getPoint(dY + elm._offsetY, sObj.path, sObj.interpolation, tangents, sObj.direction === "up" || sObj.direction === "down");
				if (sType === "funnel") {
					posY = getPoint(dX + elm._offsetX, sObj.pathTop, sObj.interpolation, tangents, sObj.direction === "up" || sObj.direction === "down");
					posX = getPoint(dY + elm._offsetY, sObj.pathTop, sObj.interpolation, tangents, sObj.direction === "up" || sObj.direction === "down");
					if (elm._intNodes[n]._bottom) {
						posY = getPoint(dX + elm._offsetX, sObj.pathBottom, sObj.interpolation, tangents, sObj.direction === "up" || sObj.direction === "down");
						posX = getPoint(dY + elm._offsetY, sObj.pathBottom, sObj.interpolation, tangents, sObj.direction === "up" || sObj.direction === "down");
					}
				}
				elm.posX = posX;
				elm.posY = posY;
				if (elm._linkedAxis._horizontal) {
					nod.eY = posY - dY;
					nod.seriesVal = ((elm._yAxisStart - elm._yChartOffset) - posY) * elm._yAxisRatio;
					nod.setAttribute("cy", nod.eY);
				} else {
					nod.seriesVal = (posX - elm._xChartOffset) * elm._xAxisRatio;
					nod.eX = posX - dX;
					nod.setAttribute("cx", nod.eX);
				}
				nod.axisVal = elm._labelObj.axisVal;
				var refObj = getEveElmByID(nod._referenceObj);
				refObj.axisVal = elm._labelObj.axisVal;
				getEveElmByID(elm._intNodes[n]).axisVal = elm._labelObj.axisVal;
				refObj.seriesVal = nod.seriesVal;

				var pos = getSvgElmPosn(nod);

				refObj.xValue = eve.setDecimal(elm._xAxisRatio * (pos.x - elm._xAxisStart), 3);
				refObj.yValue = eve.setDecimal((elm._yAxisStart - pos.y) * elm._yAxisRatio, 3);

			}
		}
		if (elm._labelObj._children.length > 0) {
			for (var c = 0; c < elm._labelObj._children.length; c++) {
				var child = elm._labelObj._children[c];
				var childElm = get(child._domElmID, elm);
				child.axisVal = elm._labelObj.axisVal;
				if (childElm) {
					var childBox = childElm.getBBox();
					if (elm._linkedAxis._horizontal) {
						var childX = (elm._eveElm._lblWidth / 2) - (childBox.width / 2);
						if (childX + dX > elm._labelObj._chart._plotWidth + elm._labelObj._chart._paddingRight - childBox.width) {
							eve.setSvgTransform(childElm, "translate", elm._labelObj._chart._plotWidth + elm._labelObj._chart._paddingRight - dX - childBox.width);
						} else if (childX + dX < -elm._labelObj._chart._paddingLeft) {
							eve.setSvgTransform(childElm, "translate", -elm._labelObj._chart._paddingLeft - dX);
						} else {
							eve.setSvgTransform(childElm, "translate", childX);
						}
						child.seriesVal = (posX - elm._xChartOffset) * elm._xAxisRatio;
					}
					var cTextElm = get(childElm._textElm, elm);
					var cContent = eve.makeText(childElm._labelObj._chart, childElm._labelObj._chart._blueprintGroup, childElm._labelObj._chart._drawMode, {id: cTextElm.id, parentElm: childElm._labelObj._chart._artboard, holderObj: childElm._labelObj, string: childElm._innerText, className: cTextElm.className, fontSize: childElm._labelObj._fontSize, fontFamily: childElm._labelObj._fontFamily, fontColour: childElm._labelObj._fontColour, fontWeight: childElm._labelObj._fontWeight, fontStyle: childElm._labelObj._fontStyle, textAlign: childElm._labelObj._textAlign, leading: childElm._labelObj._leading}).toSVG();

					//remove the existing nodes first:
					while (cTextElm.firstChild) {
						cTextElm.removeChild(cTextElm.firstChild);
					}
					//then add our new content:
					while (cContent.firstChild) {
						cTextElm.appendChild(cContent.firstChild);
					}
				}
			}
		}
	};

	var doDrag = function(evt) {
		if (!isNil(window.eveDraggingElm)) {

			var moved = false;

			var a, c;

			var pX = evt.clientX;
			var pY = evt.clientY;
			if (evt.type === "touchmove") {
				pX = evt.touches[0].clientX;
				pY = evt.touches[0].clientY;
			} else {
				evt.preventDefault();
			}

			var elm = window.eveDraggingElm;
			var yChange = 0;
			var xChange = 0;

			yChange = pY - elm.clickClientY;
			xChange = pX - elm.clickClientX;

			elm._dragPosX = parseFloat(elm.startX) + xChange;
			elm._dragPosY = parseFloat(elm.startY) + yChange;

			if (!isNil(elm.dragMinX) && elm._dragPosX < elm.dragMinX) {
				elm._dragPosX = elm.dragMinX;
			}
			if (!isNil(elm.dragMaxX) && elm._dragPosX > elm.dragMaxX) {
				elm._dragPosX = elm.dragMaxX;
			}
			if (!isNil(elm.dragMinY) && elm._dragPosY < elm.dragMinY) {
				elm._dragPosY = elm.dragMinY;
			}
			if (!isNil(elm.dragMaxY) && elm._dragPosY > elm.dragMaxY) {
				elm._dragPosY = elm.dragMaxY;
			}

			var rlEnd = null;
			var rlStart = null;

			if (!isNil(elm._linkedAxis) && !isNil(elm._linkedAxis._rangeLabelEnd)) {
				rlEnd = get(elm._linkedAxis._rangeLabelEnd, elm);
			}
			if (!isNil(elm._linkedAxis) && !isNil(elm._linkedAxis._rangeLabelStart)) {
				rlStart = get(elm._linkedAxis._rangeLabelStart, elm);
			}

			var rGap = 2;
			var dGap = 0;
			var startAt = 0;
			var endAt = 0;

			//if it's a range label:
			if (!isNil(elm._linkedAxis)) {

				if (!isNil(elm._snaps) && elm._snaps.length > 1) {
					rGap = Math.abs(elm._snaps[1] - elm._snaps[0]);
				}

				if (rlStart === elm && !isNil(rlEnd)) {
					//we're dragging the start label of a range
					if (elm._linkedAxis._horizontal) {
						if (elm._dragPosX >= elm.dragMaxX - rGap) {
							elm._dragPosX = elm.dragMaxX - rGap;
						}
						startAt = elm._dragPosX;
						endAt = eve.getSvgTransforms(rlEnd).x;
						dGap = endAt - startAt;
					} else {
						startAt = elm._dragPosY;
						endAt = eve.getSvgTransforms(rlEnd).y;
					}
				} else if (rlEnd === elm && !isNil(rlStart)) {
					//we're dragging the end label of a range
					if (elm._linkedAxis._horizontal) {
						startAt = eve.getSvgTransforms(rlStart).x;
						if (elm._dragPosX <= elm.dragMinX + rGap) {
							elm._dragPosX = elm.dragMinX + rGap;
						}
						endAt = elm._dragPosX;
						dGap = endAt - startAt;
					} else {
						startAt = eve.getSvgTransforms(rlStart).y;
						endAt = elm._dragPosY;
					}
				}
			}

			if (!isNil(elm._linkedAxis) && !isNil(elm._snaps)) {
				if (elm._linkedAxis._horizontal) {
					elm._dragPosX = elm._snaps.closest(elm._dragPosX + elm._offsetX) - elm._offsetX;
				} else {
					elm._dragPosY = elm._snaps.closest(elm._dragPosY + elm._offsetY) - elm._offsetY;
				}
			}

			var movedSibling = false;
			//if it's a range label:
			if (!isNil(elm._linkedAxis)) {
				var rlSibling = rlEnd;
				if (rlStart === elm && !isNil(rlEnd)) {
					//we're dragging the start label of a range
					if (elm._linkedAxis._horizontal) {
						if (dGap < rGap) {
							rlEnd._dragPosX = elm._dragPosX + (rGap + elm._offsetX - rlSibling._offsetX);
							eve.setSvgTransform(rlEnd, "translate", rlEnd._dragPosX);
							movedSibling = true;
						}
					}
				} else if (rlEnd === elm && !isNil(rlStart)) {
					rlSibling = rlStart;
					//we're dragging the end label of a range
					if (elm._linkedAxis._horizontal) {
						if (dGap < rGap) {
							rlStart._dragPosX = elm._dragPosX - (rGap - (elm._offsetX - rlSibling._offsetX));
							eve.setSvgTransform(rlStart, "translate", rlStart._dragPosX);
							movedSibling = true;
						}
					}
				}

				if (movedSibling) {
					elm.rangeSibling = rlSibling;
					if (!isNil(rlSibling._innerText) && !isNil(rlSibling._textElm)) {
						if (rlSibling._linkedAxis._position === "bottom" || rlSibling._linkedAxis._position === "top") {
							rlSibling.axisVal = rlSibling._linkedAxis.getVal(rlSibling._dragPosX + rlSibling._offsetX);
						} else {
							rlSibling.axisVal = rlSibling._linkedAxis.getVal(rlSibling._dragPosY + rlSibling._offsetY);
						}
						rlSibling._labelObj.axisVal = rlSibling.axisVal;
						var sTextElm = get(rlSibling._textElm, elm);
						var sContent = eve.makeText(rlSibling._labelObj._chart, rlSibling._labelObj._chart._blueprintGroup, rlSibling._labelObj._chart._drawMode, {id: sTextElm.id, parentElm: rlSibling._labelObj._chart._artboard, holderObj: rlSibling._labelObj, string: rlSibling._innerText, className: sTextElm.className, fontSize: rlSibling._labelObj._fontSize, fontFamily: rlSibling._labelObj._fontFamily, fontColour: rlSibling._labelObj._fontColour, fontWeight: rlSibling._labelObj._fontWeight, fontStyle: rlSibling._labelObj._fontStyle, textAlign: rlSibling._labelObj._textAlign, leading: rlSibling._labelObj._leading}).toSVG();

						//remove the existing nodes first:
						while (sTextElm.firstChild) {
							sTextElm.removeChild(sTextElm.firstChild);
						}
						//then add our new content:
						while (sContent.firstChild) {
							sTextElm.appendChild(sContent.firstChild);
						}
					}
				}
			}

			if (!isNil(elm._linkedAxis)) {
				if (elm._linkedAxis._horizontal) {
					elm.axisVal = elm._linkedAxis.getVal(elm._dragPosX + elm._offsetX);
				} else {
					elm.axisVal = elm._linkedAxis.getVal(elm._dragPosY + elm._offsetY);
				}
				elm._labelObj.axisVal = elm._labelObj._axisVal = elm.axisVal;

				if (elm._highlight !== null && elm._labelObj._highlightBlock === "col"){
					elm._highlight.eX = (elm.axisVal - 1) *  parseFloat(get(elm._highlight.id).getAttribute("width"));
					get(elm._highlight.id).eX = elm._highlight.eX;
					eve.setSvgTransform(get(elm._highlight.id), "translate", [elm._highlight.eX, 0]);
				}

			}

			var prevPos = eve.getSvgTransforms(elm);
			if (elm.tagName === "circle") {
				prevPos.x = elm.getAttribute("cx");
				prevPos.y = elm.getAttribute("cy");
			}
			if (prevPos.x !== elm._dragPosX || prevPos.y !== elm._dragPosY) {
				moved = true;
			}

			if (moved && !isNil(elm._intNodes) || (!isNil(elm._children) && elm._children.length > 0)) {
				updateChildren(elm, true);
			}

			if (elm.tagName === "circle") {
				var nodePoint = true;
				var nodeEnd = true;
				if (elm.id.indexOf("origin") !== -1) {
					nodeEnd = false;
				}
				if (elm.id.indexOf("handle") !== -1) {
					nodePoint = false;
				}

				elm.setAttribute("cx", elm._dragPosX);
				elm.setAttribute("cy", elm._dragPosY);
				if (!isNil(elm.bandline)) {
					if (nodePoint) {
						get(elm.bandline, elm).setAttribute("x1", elm._dragPosX);
						get(elm.bandline, elm).setAttribute("y1", elm._dragPosY);
					} else {
						get(elm.bandline, elm).setAttribute("x2", elm._dragPosX);
						get(elm.bandline, elm).setAttribute("y2", elm._dragPosY);
					}
				}
				if (!isNil(elm.curve)) {
					var curve = get(elm.curve, elm);
					var cPath = eve.svgPathToEvePath(curve.getAttribute("d"));
					if (nodePoint) {
						if (nodeEnd) {
							cPath.commands[1].to[0] = elm._dragPosX;
							cPath.commands[1].to[1] = elm._dragPosY;
						}
						else{
							cPath.commands[0].to[0] = elm._dragPosX;
							cPath.commands[0].to[1] = elm._dragPosY;
						}
					}
					else{
						if (nodeEnd) {
							cPath.commands[1].handleTo[0] = elm._dragPosX;
							cPath.commands[1].handleTo[1] = elm._dragPosY;
						}
						else{
							cPath.commands[1].handleFrom[0] = elm._dragPosX;
							cPath.commands[1].handleFrom[1] = elm._dragPosY;
						}
					}
					var dString = cPath.toSVG();
					curve.setAttribute("d", dString);
					var _plotHeight = parseFloat(curve._plotHeight);
					var _plotWidth = parseFloat(curve._plotWidth);
					var _plotOriginX = curve._plotOriginX;
					var _plotOriginY = curve._plotOriginY;

					var output = "curve.pointOriginX = " + eve.setDecimal(((cPath.commands[0].to[0] - _plotOriginX) / (_plotWidth - _plotOriginX)), 3) + ";<br />";
					output += "curve.pointOriginY = " + eve.setDecimal((1 - (cPath.commands[0].to[0] / (_plotHeight - _plotOriginY))), 3) + ";<br />";
					output += "curve.handleOriginX = " + eve.setDecimal(((cPath.commands[1].handleFrom[0] - _plotOriginX) / (_plotWidth - _plotOriginX)), 3) + ";<br />";
					output += "curve.handleOriginY = " + eve.setDecimal((1 - (cPath.commands[1].handleFrom[1] / (_plotHeight - _plotOriginY))), 3) + ";<br />";
					output += "curve.pointEndX = " + eve.setDecimal(((cPath.commands[1].to[0] - _plotOriginX) / (_plotWidth - _plotOriginX)), 3) + ";<br />";
					output += "curve.pointEndY = " + eve.setDecimal((1 - (cPath.commands[1].to[1] / (_plotHeight - _plotOriginY))), 3) + ";<br />";
					output += "curve.handleEndX = " + eve.setDecimal(((cPath.commands[1].handleTo[0] - _plotOriginX) / (_plotWidth - _plotOriginX)), 3) + ";<br />";
					output += "curve.handleEndY = " + eve.setDecimal((1 - (cPath.commands[1].handleTo[1] / (_plotHeight - _plotOriginY))), 3) + ";<br />";
					get(elm.output, elm).innerHTML = output;
				}
			} else {
				if (elm._rotateCentreX) {
					var rootSVG = eve.getRootSVG(elm);

					//the "draggables" group translate:
					var pe = eve.getSvgTransforms(elm.parentNode);
					var pOffsetX = pe.x;
					var pOffsetY = pe.y;
					console.log(pY);

					//pX and pY are relative to the window
					//so are clickClientX and clickClientY
					//_rotateCentreX and _rotateCentreY are relative to the top-left of the rectangle when it's at the top of the circle

					//TODO: this is broken when the page is scrolled.
					// getOffset(rootSVG) doesn't change on page scroll - do we need to include the window (or container) scroll amount or does
					// getOffset() need rethinking?
					var dCen = [elm._rotateCentreX + elm.startX + pOffsetX, elm._rotateCentreY + elm.startY + pOffsetY];
					var dPoint = [pX - getOffset(rootSVG).left + window.scrollX, pY - getOffset(rootSVG).top + window.scrollY];

					var ang = cartesianToPolar(dCen, dPoint).degrees;
					eve.setSvgTransform(elm, "rotate", [ang, elm._rotateCentreX, elm._rotateCentreY]);

					if (elm.series) {
						c = getEveElmByID(elm.series._chart);
						a = getEveElmByID(c._axes[0]);
						get(elm.series.sectorID, elm).sector.angleRange = ang - get(elm.series.sectorID, elm).sector.startAngle;
						get(elm.series.sectorID, elm).setAttribute("d", get(elm.series.sectorID, elm).sector.toSVG());
						var rMin = a._rangeMin || 0;
						elm.series.value = (a._rangeMax - rMin) * (ang / 360);
						get(elm.series._numTextElm.id, elm).textContent = eve.formatNumber(elm.series.value, elm.series._nodeLabelNumberFormat);
					}

				} else {
					eve.setSvgTransform(elm, "translate", [elm._dragPosX, elm._dragPosY]);
				}
			}

			if (!isNil(elm._innerText) && !isNil(elm._textElm)) {
				var lText = null;
				if (elm._labelObj){
					lText = elm._labelObj.text;
					if (!isNil(elm._labelObj._m_text)){
						lText = elm._labelObj._m_text;
					}
					if(lText !== elm._innerText){
						elm._innerText = lText;
					}
				}
				var textElm = get(elm._textElm, elm);
				var content = eve.makeText(getEveElmByID(elm._labelObj._chart), getEveElmByID(elm._labelObj._chart)._blueprintGroup, getEveElmByID(elm._labelObj._chart)._drawMode, {id: elm._textElm, holderObj: elm._labelObj, string: elm._innerText, className: textElm.className, fontSize: elm._labelObj._fontSize, fontFamily: elm._labelObj._fontFamily, fontColour: elm._labelObj._fontColour, fontWeight: elm._labelObj._fontWeight, fontStyle: elm._labelObj._fontStyle, textAlign: elm._labelObj._textAlign, leading: elm._labelObj._leading}).toSVG();
				//remove the existing nodes first:
				while (textElm.firstChild) {
					textElm.removeChild(textElm.firstChild);
				}
				//then add our new content:
				while (content.firstChild) {
					textElm.appendChild(content.firstChild);
				}
			}

			if (!isNil(elm.dragFunction) && moved) {
				elm.dragFunction(elm);
			}

		}
	};
	//var stopDrag = function(evt) {
	var stopDrag = function() {
		var a, c ;
		var elm = window.eveDraggingElm;
		if (!isNil(elm)) {
			if (elm.tagName === "circle") {
				elm.style.fillOpacity = "0";
			}
			//if (evt.type === "touchstart") {
				eve.removeEventHandler(window, "touchmove", doDrag);
				eve.removeEventHandler(window, "touchend", stopDrag);
			//} else {
				eve.removeEventHandler(window, "mousemove", doDrag);
				eve.removeEventHandler(window, "mouseup", stopDrag);
			//}
			if (elm.series) { //dragging a sector in a ring chart
				if (elm.series._snapTo) {
					c = getEveElmByID(elm.series._chart);
					a = getEveElmByID(c._axes[0]);
					//tween the sector:
					var rMin = a._rangeMin || 0;
					var aRange = a._rangeMax - rMin;
					var numTicks = (a.numBands || aRange) + 1;
					if (strIs(elm.series._snapTo, "subticks") && !isNil(elm.series._subTicks)){
						numTicks *= elm.series._subTicks;
					}
					var snaps = [];
					for (var at = 0; at < numTicks; at++) {
						var addSnap = true;
						if (elm.series._dragMinValue && elm.series._dragMinValue > (at / (numTicks - 1)) * aRange) {
							addSnap = false;
						}
						if (addSnap) {
							snaps.push(elm.series._rotation + (360 / (numTicks - 1)) * at);
						}
					}
					var snapTo = snaps.closest((elm.series.value / aRange) * 360);
					var sectorElm = get(elm.series.sectorID, elm);
					var nSector = cloneEveObj(sectorElm.sector);
					nSector.angleRange = snapTo - nSector.startAngle;
					sectorElm.oSector = cloneEveObj(sectorElm.sector);
					sectorElm.sector = cloneEveObj(nSector);
					eve.tweenTo(get(elm.series.sectorID, elm), 0.5, 0, "easeInOut", null, {
						"sector": nSector
					}, null);
					elm.series.value = aRange * snapTo / 360;
					get(elm.series._numTextElm.id, elm).textContent = eve.formatNumber(elm.series.value, elm.series._nodeLabelNumberFormat);

					//move the handle:
					eve.setSvgTransform(elm, "rotate", [snapTo, elm._rotateCentreX, elm._rotateCentreY]);

				}
			}
			if (!isNil(elm.dragCompleteFunction)) {
				elm.dragCompleteFunction(elm);
			}
			//if this is one of a pair of range ends, run the dragCompleteFunction function of the sibling
			if (!isNil(elm.rangeSibling) && !isNil(elm.rangeSibling._labelObj._onDragComplete)) {
				elm.rangeSibling._labelObj._onDragComplete(elm.rangeSibling);
			}
			elm.rangeSibling = null;
		}
		window.eveDraggingElm = null;
	};

	var changeColour = function(evt, elm, colour, time, attr, owner) {
		var area = attr || "fill";
		var cOwner = owner || window;
		var cColour = colour || "black";
		var cTime = time || 0;
		var twObj = {};
		twObj[area] = cColour;
		eve.tweenTo(elm, cTime, 0, null, null, twObj, cOwner);
	};

	eve.fillArray = function(input, targetLength, trimToLength) {
		trimToLength = trimToLength || false;
		var ret = null;
		if (!isNil(input) && input !== "") {
			if (eve.isArray(input)) {
				if (input.length < targetLength) {
					while (input.length < targetLength) {
						var iLength = input.length;
						for (var i = 0; i < iLength; i++) {
							input.push(input[i]);
						}
					}
					ret = input.slice(0, targetLength);
				} else if (input.length > targetLength && trimToLength) {
					ret = input.slice(0, targetLength);
				} else {
					ret = input;
				}
			} else {
				ret = [];
				for (var aCount = 0; aCount < targetLength; aCount++) {
					ret.push(input);
				}
			}
		}
		return ret;
	};

	var displayNone = function(elm) {
		elm.style.display = "none";
	};

	var setReady = function(chart) {
		chart._ready = true;
	};

	var fixNodeProps = function(elm){
		if (isDefault(elm.nodeProps.stroke)){
			elm.nodeProps.stroke = null;
		}
		if (isDefault(elm.nodeProps.fillOpacity)){
			elm.nodeProps.fillOpacity = 1;
		}
		if (isDefault(elm.nodeProps.strokeOpacity)){
			elm.nodeProps.strokeOpacity = 1;
		}
		if (isDefault(elm.nodeProps.fillHover)){
			elm.nodeProps.fillHover = elm.nodeProps.fill;
		}
		if (isDefault(elm.nodeProps.fillHoverOpacity)){
			elm.nodeProps.fillHoverOpacity = elm.nodeProps.fillOpacity;
		}
		if (isDefault(elm.nodeProps.strokeHover)){
			elm.nodeProps.strokeHover = elm.nodeProps.stroke;
		}
		if (isDefault(elm.nodeProps.strokeHoverOpacity)){
			elm.nodeProps.strokeHoverOpacity = elm.nodeProps.strokeOpacity;
		}
	};

	var nodeOver = function(evt, elm, chart, series, showMeasures, showTips, isEditable) {
		if (chart._ready) {
			if (!isNil(window.eveNodeOver)) {
				nodeLeave(null, window.eveNodeOver, chart, series, window.eveNodeOver.showMeasures, window.eveNodeOver.showTips, window.eveNodeOver.isEditable, window.eveNodeOver.fColour);
			}
			elm.showMeasures = showMeasures;
			elm.showTips = showTips;
			elm.isEditable = isEditable;
			window.eveNodeOver = elm;
			var blendDur = 0.15;
			if (!isNil(elm._hoverOffset)) {
				eve.tweenTo(elm, blendDur, 0, null, null, {"x": elm._hoverOffset[0], "y": elm._hoverOffset[1]}, window);
			}
			if ((series._type === "shape" || series._type === "funnel") && series._shapeNodes){
				eve.tweenTo(elm, blendDur, 0, null, null, {"opacity": 1}, window);
			}
			if (elm.nodeProps.fill instanceof eve.parallelHatch){
				eve.tweenFromTo(elm, blendDur, 0, null, null, {stroke: elm.nodeProps.stroke, fillOpacity: elm.nodeProps.fillOpacity, strokeOpacity: elm.nodeProps.strokeOpacity}, {stroke: elm.nodeProps.strokeHover, fillOpacity: elm.nodeProps.fillHoverOpacity, strokeOpacity: elm.nodeProps.strokeHoverOpacity}, window);
				eve.tweenTo(get(elm.nodeProps.fill.id).firstChild, blendDur, 0, null, null, {stroke: elm.nodeProps.fillHover}, window);
			}
			else{
				eve.tweenFromTo(elm, blendDur, 0, null, null, {fill: elm.nodeProps.fill, stroke: elm.nodeProps.stroke, fillOpacity: elm.nodeProps.fillOpacity, strokeOpacity: elm.nodeProps.strokeOpacity}, {fill: elm.nodeProps.fillHover, stroke: elm.nodeProps.strokeHover, fillOpacity: elm.nodeProps.fillHoverOpacity, strokeOpacity: elm.nodeProps.strokeHoverOpacity}, window);
			}
			if (showMeasures && !isEditable) {
				showMeasureLines(evt, elm, 1);
			}
			if (showTips && !isEditable) {
				showToolTip(evt, elm, 0.2, 0.5);
			}
		}
	};

	var nodeLeave = function(evt, elm, chart, series, showMeasures, showTips, isEditable) {
		if (chart._ready) {
			var blendDur = 0.15;
			if (!isNil(window.eveNodeOver) && window.eveNodeOver === elm) {
				window.eveNodeOver = null;
			}

			if (elm.nodeProps.fill instanceof eve.parallelHatch){
				eve.tweenTo(elm, blendDur, 0, null, null, {stroke: elm.nodeProps.stroke, fillOpacity: elm.nodeProps.fillOpacity, strokeOpacity: elm.nodeProps.strokeOpacity}, window);
				eve.tweenTo(get(elm.nodeProps.fill.id).firstChild, blendDur, 0, null, null, {stroke: elm.nodeProps.fill.strokeColour}, window);
			}
			else{
				eve.tweenTo(elm, blendDur, 0, null, null, {fill: elm.nodeProps.fill, stroke: elm.nodeProps.stroke, fillOpacity: elm.nodeProps.fillOpacity, strokeOpacity: elm.nodeProps.strokeOpacity}, window);
			}

			if (showMeasures && !isEditable) {
				hideMeasureLines(evt, elm);
			}
			if (showTips && !isEditable) {
				hideToolTip(evt, elm, blendDur, 0);
			}
			if ((series._type === "shape" || series._type === "funnel") && series._shapeNodes){
				eve.tweenTo(elm, blendDur, 0, null, null, {"opacity": 0}, window);
			}
			if (!isNil(elm._hoverOffset)) {
				eve.tweenTo(elm, blendDur, 0, null, null, {x: 0, y: 0}, window);
			}
		}

	};

	var showToolTip = function(evt, elm, time, delay, owner) {
		var tt = get(elm.parentNode.tooltipID, elm);
		if (isNil(tt) && !isNil(elm.sector) && !isNil(elm.sector.tooltipID)) {
			tt = get(elm.sector.tooltipID, elm);
		}
		if (!isNil(tt)) {
			tt.style.opacity = 0;
			tt.style.display = "block";
			eve.tweenTo(tt, time, delay, null, null, {opacity: 1}, owner);
		}
	};

	var hideToolTip = function(evt, elm, time, delay, owner) {
		var tt = get(elm.parentNode.tooltipID, elm);
		if (isNil(tt) && !isNil(elm.sector) && !isNil(elm.sector.tooltipID)) {
			tt = get(elm.sector.tooltipID, elm);
		}
		if (!isNil(tt)) {
			eve.tweenTo(tt, time, delay, null, null, {opacity: 0, onComplete: displayNone, onCompleteParams: [tt]}, owner);
		}
	};

	var showMeasureLines = function(evt, elm, time, owner) {
		var sOwner = owner || window;
		var hLine = get(elm.parentNode.hLine, elm);
		var vLine = get(elm.parentNode.vLine, elm);
		var target = get(elm.id.replace("knot", "target"), elm);
		if (!isNil(target)) {
			target.setAttribute("cx", elm.getAttribute("cx"));
			target.setAttribute("cy", elm.getAttribute("cy"));
			eve.tweenTo(target, time, 0, "easeInOut", null, {opacity: 1, r: target.radHover}, sOwner);
		}
		eve.tweenTo(hLine, time, 0, "easeInOut", null, {x2: hLine.endX}, sOwner);
		eve.tweenTo(vLine, time, 0, "easeInOut", null, {y2: vLine.endY}, sOwner);
	};

	var hideMeasureLines = function(evt, elm) {
		var target = get(elm.id.replace("knot", "target"), elm);
		if (!isNil(target)) {
			eve.killTweensOf(target);
			target.style.opacity = 0;
		}
		var hLine = get(elm.parentNode.hLine, elm);
		var vLine = get(elm.parentNode.vLine, elm);
		eve.killTweensOf(hLine);
		eve.killTweensOf(vLine);
		hLine.setAttribute('x2', hLine.getAttribute("x1"));
		vLine.setAttribute('y2', vLine.getAttribute("y1"));
	};

	function strIs(str, match) {
		return str.toString().toLowerCase() === match;
	}

	eve.gradStop = function(colour, opacity, offset){
		this.colour = colour || "black";
		this.offset = offset || 0;
		if (!isNil(opacity)){
			this.opacity = opacity;
		}
		else{
			this.opacity = 1;
		}
		this.clone = function(){
			return new eve.gradStop(this.colour, this.opacity, this.offset);
		};
	};

	var addEveID = function(elmID, prefix, elm){
		var iNum = -1;
		var num = 0;
		var i;
		var id = elmID;
		if (elmID){
			var duplicate = false;
			for (i = 0; i < eve.__elms.length; i++){
				if (eve.__elms[i].id === elmID){
					duplicate = true;
					break;
				}
			}
			if (duplicate){
				for (i = 0; i < eve.__elms.length; i++){
					if (eve.__elms[i].id.indexOf(elmID) === 0){
						num = parseInt(eve.__elms[i].id.substr(elmID.length + 1));
					}
				}
				num++;
				id = elmID + "_" + num;
			}
		}
		else{
			for (i = 0; i < eve.__elms.length; i++){
				if (eve.__elms[i].id.indexOf(prefix) === 0){
					num = parseInt(eve.__elms[i].id.substr(prefix.length + 1));
					if (num > iNum){
						iNum = num;
					}
				}
			}
			num++;
			id = prefix + "_" + num;
		}
		eve.__elms.push({id: id, elm: elm});
		return id;
	};

	var getEveElmByID = function(id){
		for (var e = 0; e < eve.__elms.length; e++){
			if (eve.__elms[e].id === id){
				return eve.__elms[e].elm;
			}
		}
		return null;
	};

	var getNextIdNum = function(arr, prefix){
		var iNum = -1;
		for (var i = 0; i < arr.length; i++){
			if (arr[i].id.indexOf(prefix) !== -1){
				var num = parseInt(arr[i].id.substr(prefix.length));
				if (num > iNum){
					iNum = num;
				}
			}
		}
		return iNum + 1;
	};

	eve.parallelHatch = function (strokeWidth, strokeGap, strokeColour, rotation, id){
		this.strokeWidth = strokeWidth || 1;
		this.strokeGap = strokeGap || 3;
		this.strokeColour = strokeColour || "#333333";
		this.rotation = rotation;
		if (isNil(this.rotation)){
			this.rotation = 45;
		}
		this.id = id;
		if (isNil(id) || id === ""){
			this.id = "eve_hatch_" + eve.hatches.length;
			this.id = "eve_hatch_" + getNextIdNum(eve.hatches, "eve_hatch_");
		}
		this.toSVGPattern = function(){
			var hatch = get(this.id);
			if (isNil(hatch)){
				hatch = document.createElementNS(svgns, "pattern");
				hatch.id = this.id;
				hatch.setAttribute("patternUnits", "userSpaceOnUse");
				hatch.style.stroke = this.StrokeColour;
				hatch.setAttribute("patternTransform", "rotate(" + this.rotation + " " + ((this.strokeGap + this.strokeWidth) / 2) + " " + ((this.strokeGap + this.strokeWidth) / 2) + ")");
				hatch.setAttribute("width", this.strokeGap + this.strokeWidth);
				hatch.setAttribute("height", this.strokeGap + this.strokeWidth);
				var pat = document.createElementNS(svgns, "path");
				pat.setAttribute("d", "M -" + this.strokeWidth + "," + (this.strokeWidth / 2) + " L" + ((this.strokeGap + this.strokeWidth) + 1) + "," + (this.strokeWidth / 2));
				pat.style.strokeWidth = this.strokeWidth;
				pat.style.stroke = this.strokeColour;
				hatch.appendChild(pat);
			}
			return hatch;
		}
		this.clone = function(defsElm){
			var clone = new eve.parallelHatch;
			clone.strokeWidth = this.strokeWidth;
			clone.strokeGap = this.strokeGap;
			clone.strokeColour = this.strokeColour;
			clone.rotation = this.rotation;
//			clone.id = incrementID(this.id, "_");
			clone.id = "eve_hatch_" + getNextIdNum(eve.hatches, "eve_hatch_");
			if (!isNil(defsElm)){
				defsElm.appendChild(clone.toSVGPattern());
			}
			return clone;
		}
		eve.hatches.push(this);
	};

	eve.linearGradient = function(startX, startY, endX, endY, stops, id, spreadMethod, gradientTransform){
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
		if (isNil(this.startX)){
			this.startX = "0%";
		}
		if (isNil(this.startY)){
			this.startY = "0%";
		}
		if (isNil(this.endX)){
			this.endX = "100%";
		}
		if (isNil(this.endY)){
			this.endY = "0%";
		}
		this.stops = stops || [];
		this.spreadMethod = spreadMethod || "pad"; // pad, repeat, reflect
		this.gradientTransform = gradientTransform || ""; // e.g. "rotate(45)"
		this.id = id;
		if (isNil(id) || id === ""){
			this.id = "eve_l_gradient_" + eve.lGrads.length;
			this.id = "eve_l_gradient_" + getNextIdNum(eve.lGrads, "eve_l_gradient_");
		}
		this.addStop = function(colour, opacity, offset){
			var stop = new eve.gradStop(colour, opacity, offset);
			this.stops.push(stop);
		}
		this.clone = function(defsElm){
			var clone = new eve.linearGradient;
			clone.startX = this.startX;
			clone.startY = this.startY;
			clone.endX = this.endX;
			clone.endY = this.endY;
			clone.spreadMethod = this.spreadMethod;
			clone.gradientTransform = this.gradientTransform;
//			clone.id = incrementID(this.id, "_");
			for (var s = 0; s < this.stops.length; s++){
				clone.stops.push(this.stops[s].clone());
			}
			if (!isNil(defsElm)){
				defsElm.appendChild(clone.toSVGGradient());
			}
			return clone;
		}
		this.toSVGGradient = function(){
			var grad = document.createElementNS(svgns, "linearGradient");
			grad.id = this.id;
			grad.setAttribute("x1", this.startX);
			grad.setAttribute("y1", this.startY);
			grad.setAttribute("x2", this.endX);
			grad.setAttribute("y2", this.endY);
			grad.setAttribute("spreadMethod", this.spreadMethod);
			if (this.gradientTransform !== ""){
				grad.setAttribute("gradientTransform", this.gradientTransform);
			}
			for (var s = 0; s < this.stops.length; s++) {
				var newStop = document.createElementNS(svgns, "stop");
				newStop.setAttribute("offset", this.stops[s].offset);
				newStop.style.stopColor = this.stops[s].colour;
				newStop.style.stopOpacity = this.stops[s].opacity;
				grad.appendChild(newStop);
			}
			return grad;
		}
		eve.lGrads.push(this);
	};

	eve.svgLinearGradientToEve = function(svgGrad){
		if (!isNil(svgGrad)){
			var eGrad = new eve.linearGradient(svgGrad.getAttribute("x1"), svgGrad.getAttribute("y1"), svgGrad.getAttribute("x2"), svgGrad.getAttribute("y2"));
			eGrad.id = svgGrad.id;
			for (var c = 0; c < svgGrad.childNodes.length; c++){
				if (strIs(svgGrad.childNodes[c].nodeName, "stop")){
					var ssOffset = parseFloat(svgGrad.childNodes[c].getAttribute("offset"));
					var ssColour = svgGrad.childNodes[c].style.stopColor;
					var ssOpacity = parseFloat(svgGrad.childNodes[c].style.stopOpacity);
					eGrad.addStop(ssColour, ssOpacity, ssOffset);
				}
			}
			return eGrad;
		}
		return null;
	}

	eve.radialGradient = function(centreX, centreY, radius, focalX, focalY, stops, id, spreadMethod, gradientTransform){
		this.centreX = centreX || "50%";
		this.centreY = centreY || "50%";
		this.radius = radius || "50%";
		this.focalX = focalX || "50%";
		this.focalY = focalY || "50%";
		this.stops = stops || [];
		this.spreadMethod = spreadMethod || "pad"; // pad, repeat, reflect
		this.gradientTransform = gradientTransform || null; // e.g. "rotate(45)"
		this.id = id;
		if (isNil(id) || id === ""){
			this.id = "eve_r_gradient_" + eve.rGrads.length;
			this.id = "eve_r_gradient_" + getNextIdNum(eve.rGrads, "eve_r_gradient_");
		}
		this.addStop = function(colour, opacity, offset){
			var stop = new eve.gradStop(colour, opacity, offset);
			this.stops.push(stop);
		}
		this.clone = function(){
			var clone = new eve.linearGradient;
			clone.centreX = this.centreX;
			clone.centreY = this.centreY;
			clone.radius = this.radius;
			clone.focalX = this.focalX;
			clone.focalY = this.focalY;
			clone.spreadMethod = this.spreadMethod;
			clone.gradientTransform = this.gradientTransform;
			for (var s = 0; s < this.stops.length; s++){
				clone.stops.push(this.stops[s]);
			}
			return clone;
		}
		this.toSVGGradient = function(){
			var grad = document.createElementNS(svgns, "radialGradient");
			grad.id = this.id;
			grad.setAttribute("cx", this.centreX);
			grad.setAttribute("cy", this.centreY);
			grad.setAttribute("r", this.radius);
			grad.setAttribute("fx", this.focalX);
			grad.setAttribute("fy", this.focalY);
			grad.setAttribute("spreadMethod", this.spreadMethod);
			grad.setAttribute("gradientTransform", this.gradientTransform);
			for (var s = 0; s < this.stops.length; s++) {
				var newStop = document.createElementNS(svgns, "stop");
				newStop.setAttribute("offset", this.stops[s].offset);
				newStop.style.stopColor = this.stops[s].colour;
				newStop.style.stopOpacity = this.stops[s].opacity;
				grad.appendChild(newStop);
			}
			return grad;
		}
		eve.rGrads.push(this);
	};

	var componentToHex = function(c) {
		var hex = parseInt(c).toString(16);
		return hex.length === 1 ? "0" + hex : hex;
	};

	var hexToRgb = function(hex) {
		// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
			return r + r + g + g + b + b;
		});
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		//return result ? {r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16)} : null;
		return result ? [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)] : null;
	};

	eve.colourToRGBA = function(c) {
		c = c.toString().toLowerCase();
		if (c.indexOf("rgba") === 0) {
			//this is what we want - the rgba string
		} else {
			if (c.indexOf("rgb") === 0) {
				c = c.split(")")[0] + ", 1)";
			} else if (c === "transparent") {
				c = "rgba(125, 125, 125, 0)";
			} else if (typeof colourNames[c.toLowerCase()] !== 'undefined') {
				c = colourNames[c.toLowerCase()];
			}
			if (c.indexOf("#") === 0) {
				c = "rgba(" + hexToRgb(c).toString() + ", 1)";
			}
		}
		var vals = c.split("(")[1].split(")")[0].replace(/ /g, "").split(",");
		return {r: parseInt(vals[0]), g: parseInt(vals[1]), b: parseInt(vals[2]), a: parseFloat(vals[3])};
	};

	eve.colourToHex = function(strColour) {
		if (isNil(strColour) || strColour === "") {
			return strColour;
		}
		if (strColour.indexOf("#") === 0) {
			return strColour;
		}
		else{
			if(new RegExp("\\d{6}").test(strColour)){
				return "#" + strColour;
			}
			if (typeof colourNames[strColour.toLowerCase()] !== 'undefined') {
				return colourNames[strColour.toLowerCase()];
			}
			var str = strColour.replace("rgba", "").replace("rgb", "").replace("(", "").replace(")", "").replace(/ /g, "");
			var rgb = str.split(",");
			return "#" + componentToHex(rgb[0]) + componentToHex(rgb[1]) + componentToHex(rgb[2]);
		}
	};

	//darken or lighten a colour:
	eve.shadeColour = function(colour, amount) {
		if (colour instanceof eve.linearGradient){
			var cGrad = colour.clone();
			for (var c = 0; c < colour.stops.length; c++){
				cGrad.stops[c].colour = eve.shadeColour(colour.stops[c].colour, amount);
			}
			return cGrad;
		}
		else if (colour instanceof eve.parallelHatch){
			var cHatch = colour.clone();
			cHatch.strokeColour = eve.shadeColour(colour.strokeColour, amount);
			return cHatch;
		}
		colour = eve.colourToHex(colour);
		var f = parseInt(colour.slice(1), 16),
			t = amount < 0 ? 0 : 255,
			p = amount < 0 ? amount * -1 : amount,
			R = f >> 16,
			G = f >> 8 & 0x00FF,
			B = f & 0x0000FF;
		return "#" + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + (Math.round((t - B) * p) + B)).toString(16).slice(1);
	};

	//blend between 2 colours:
	eve.blendColours = function(c0, c1, p) {
		var rgbaFormat = false;
		if (isNil(c0) || isNil(c1) || c0 === "" || c1 === "") {
			return null;
		}
		if (strIs(c0, "transparent") || c0.toString().toLowerCase().indexOf("rgba") === 0 || strIs(c1, "transparent") || c1.toString().toLowerCase().indexOf("rgba") === 0) {
			rgbaFormat = true;
		}
		if (rgbaFormat) {
			//if both colours are "transparent":
			if (strIs(c0, "transparent") && strIs(c1, "transparent")) {
				return "transparent";
			}
			var cr0 = c0;
			var cr1 = c1;
			if (strIs(cr0, "transparent")) {
				cr0 = eve.colourToRGBA(cr1);
				cr0.a = 0;
			} else {
				cr0 = eve.colourToRGBA(cr0);
			}
			if (strIs(cr1, "transparent")) {
				cr1 = eve.colourToRGBA(cr0);
				cr1.a = 0;
			} else {
				cr1 = eve.colourToRGBA(cr1);
			}
			var r = cr0.r + ((cr1.r - cr0.r) * p);
			var g = cr0.g + ((cr1.g - cr0.g) * p);
			var b = cr0.b + ((cr1.b - cr0.b) * p);
			var a = cr0.a + ((cr1.a - cr0.a) * p);
			return "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
		} else {
			c0 = eve.colourToHex(c0);
			c1 = eve.colourToHex(c1);
			var f = parseInt(c0.slice(1), 16),
				t = parseInt(c1.slice(1), 16),
				R1 = f >> 16,
				G1 = f >> 8 & 0x00FF,
				B1 = f & 0x0000FF,
				R2 = t >> 16,
				G2 = t >> 8 & 0x00FF,
				B2 = t & 0x0000FF;
			return "#" + (0x1000000 + (Math.round((R2 - R1) * p) + R1) * 0x10000 + (Math.round((G2 - G1) * p) + G1) * 0x100 + (Math.round((B2 - B1) * p) + B1)).toString(16).slice(1);
		}
	};

	var getGradientAverageColour = function(grad){
		var s, stops = [];
		// put the stops in a new array so we don't mess up the originals
		for (s = 0; s < grad.stops.length; s++){
			stops[s] = grad.stops[s].clone(0);
			stops[s].offset = eve.percentToDecimal(stops[s].offset);
		}
		//put them in order of where they appear along the gradient:
		stops.sort(sortByProperty("offset"));
		var left = 0;
		var right = 0;
		//give each a "weight" - how much of the total gradient it occupies:
		for (s = 0; s < stops.length; s++){
			if (s === stops.length - 1){
				right = 1;
			}
			else{
				right = stops[s + 1].offset;
			}
			stops[s].weight = left + ((right - stops[s].offset) / 2);
			left += stops[s].weight;
		}
		//blend each colour with the next in the gradient based on their relative weights:
		while (stops.length > 1){
			var ratio = stops[0].weight / (stops[0].weight + stops[1].weight);
			stops[1].colour = eve.blendColours(stops[0].colour, stops[1].colour, ratio);
			stops.shift();
		}
		return stops[0].colour;
	}

	var getColourLuma = function(colour) {
		if (colour instanceof eve.linearGradient){
			colour = getGradientAverageColour(colour);
		}
		if (colour instanceof eve.parallelHatch){
			colour = colour.strokeColour;
		}
		var c = eve.colourToHex(colour).substring(1); // strip #
		var rgb = parseInt(c, 16); // convert rrggbb to decimal
		var r = (rgb >> 16) & 0xff; // extract red
		var g = (rgb >> 8) & 0xff; // extract green
		var b = (rgb >> 0) & 0xff; // extract blue
		return 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
	};

	var appear = function(elm, delaySecs, endOpacity) {
		var e = elm;
		var opacity = endOpacity || 1;
		if (isNil(delaySecs) || delaySecs === 0) {
			e.style.opacity = opacity;
		} else {
			setTimeout(function() {
				e.style.opacity = opacity;
			}, delaySecs * 1000.0);
		}
	};
	var disappear = function(elm, delaySecs) {
		var e = elm;
		setTimeout(function() {
			removeNode(e);
			if (!isNil(e.bpElm)) {
				e.bpElm.parent.removeNode(e.bpElm.id);
			}
		}, delaySecs * 1000.0);
	};

	eve.svgPathToEvePath = function(strPath) {
		var c, rLength, rPoints = [], retPath = null;
		var pClosed = strPath.indexOf("z") > -1 || strPath.indexOf("Z") > -1;
		strPath = strPath.replace(/0-/g, "0 -").replace(/1-/g, "1 -").replace(/2-/g, "2 -").replace(/3-/g, "3 -").replace(/4-/g, "4 -").replace(/5-/g, "5 -").replace(/6-/g, "6 -").replace(/7-/g, "7 -").replace(/8-/g, "8 -").replace(/9-/g, "9 -");

		if (strPath.length > 0) {

			var strCommands = {m: "moveTo", l: "lineTo", c: "cubic", s: "cubicReflect", q: "quadratic", t: "quadraticReflect", h: "horizontal", v: "vertical", a: "arc", z: "close"};
			//split the string at command characters:
			var svgCmds = strPath.split(/[ACHLMQSTVZachlmqstvz]/);

			//tidy up:
			for (c = svgCmds.length - 1; c >= 0; c--) {
				svgCmds[c] = svgCmds[c].trim();
				if (svgCmds[c] === "") {
					svgCmds.splice(c, 1);
				}
			}

			//then see which type of command each of them is:
			rLength = 0;
			for (c = 0; c < svgCmds.length; c++) {
				var cmdAbbr = strPath.substr(rLength, 1);
				while ((cmdAbbr === " " || cmdAbbr === "." || cmdAbbr === "z" || cmdAbbr === "Z" || !isNaN(cmdAbbr)) && rLength < strPath.length){
					rLength += 1;
					cmdAbbr = strPath.substr(rLength, 1);
					if (cmdAbbr === "z" || cmdAbbr === "Z"){
						rPoints.push(new eve.command("close", null, null, null, null, null, null, null, null, null, true));
					}
				}
				var cmdName = strCommands[cmdAbbr.toLowerCase()];
				var cmdAbs = cmdAbbr.toUpperCase() === cmdAbbr;
				rPoints.push(new eve.command(cmdName, null, null, null, null, null, null, null, null, null, cmdAbs));

				/*
				M: end.x, end.y
				L: end.x, end.y
				C: bp1.x, bp1.y, bp2.x, bp2.y, end.x, end.y
				S: bp2.x, bp2.y, end.x, end.y
				Q: bp.x, bp.y, end.x, end.y
				T: end.x, end.y
				H: end.distanceX
				V: end.distanceY
				A: rad.x, rad.y, angle, largeArc (0 or 1), sweepFlag (0 or 1), end.X, end.Y
				*/

				var coords = svgCmds[c].split(/[,\s]/);
				var newCoord = null;
				var d;
				for (d = 0; d < coords.length; d++){
					if (coords[d].indexOf ("-") > 0){
						newCoord = coords[d].substr(coords[d].indexOf("-"));
						coords[d] = coords[d].substr(0, coords[d].indexOf("-"));
						coords.splice(d + 1, 0, newCoord);
					}
					var decimals = coords[d].match(new RegExp("\\.", "g"));
					var decimalCount = 0;
					if (decimals !== null){
						decimalCount = decimals.length;
					}
					if (decimalCount > 1){
						var pointAt = coords[d].replace(".", 0).indexOf(".");
						newCoord = coords[d].substr(pointAt);
						coords[d] = coords[d].substr(0, pointAt);
						coords.splice(d + 1, 0, newCoord);
					}
				}
				if (rPoints[c].type === "vertical" || rPoints[c].type === "horizontal") {
					rPoints[c].length = parseFloat(coords[0].trim());
				}
				if (rPoints[c].type === "moveTo" || rPoints[c].type === "lineTo" || rPoints[c].type === "quadraticReflect") {
					rPoints[c].to = [parseFloat(coords[0].trim()), parseFloat(coords[1].trim())];
				} else if (rPoints[c].type === "cubic") {
					rPoints[c].handleFrom = [parseFloat(coords[0].trim()), parseFloat(coords[1].trim())];
					rPoints[c].handleTo = [parseFloat(coords[2].trim()), parseFloat(coords[3].trim())];
					rPoints[c].to = [parseFloat(coords[4].trim()), parseFloat(coords[5].trim())];
				} else if (rPoints[c].type === "cubicReflect") {
					rPoints[c].handleTo = [parseFloat(coords[0].trim()), parseFloat(coords[1].trim())];
					rPoints[c].to = [parseFloat(coords[2].trim()), parseFloat(coords[3].trim())];
				} else if (rPoints[c].type === "quadratic") {
					rPoints[c].handleFrom = [parseFloat(coords[0].trim()), parseFloat(coords[1].trim())];
					rPoints[c].to = [parseFloat(coords[2].trim()), parseFloat(coords[3].trim())];
				} else if (rPoints[c].type === "arc") {
					rPoints[c].handleFrom = [parseFloat(coords[0].trim()), parseFloat(coords[1].trim())];
					rPoints[c].to = [parseFloat(coords[2].trim()), parseFloat(coords[3].trim())];
				}
				rLength += svgCmds[c].length + 1;
			}
			retPath = eve.addPath(rPoints, pClosed);
		}
		return retPath;

	};

	eve.command = function(type, to, handleFrom, handleTo, length, radius, angle, centre, largeArc, sweepFlag, absolute) {
		this.absolute = true;
		if(absolute === false){
			this.absolute = false;
		}
		this.length = length || null;
		this.type = type || "lineTo"; //lineTo, moveTo, cubic, cubicReflect, quadratic, quadraticReflect, horizontal, vertical, arc
		this.to = to || [null, null];
		this.handleFrom = handleFrom || [null, null];
		this.handleTo = handleTo || [null, null];
		this.radius = radius || [null, null];
		this.centre = centre || [null, null];
		this.angle = angle || null;
		this.largeArc = largeArc || 0;
		this.sweepFlag = sweepFlag || 0;
	};

	var pathsAreMorphable = function(srcPath, tgtPath) {
		var ret = true;
		if (srcPath.commands.length !== tgtPath.commands.length) {
			ret = false;
		} else {
			for (var c = 0; c < srcPath.commands.length; c++) {
				if ((srcPath.commands[c].type !== tgtPath.commands[c].type)) { // || (srcPath.command[c].absolute!==tgtPath.command[c].absolute)){
					ret = false;
					break;
				}
			}
		}
		return ret;
	};

	//to morph one path into another, they must both have the same number (and same type) of points
	//if the source path has too many points, we need to make a "pre-target" path that resembles the target path but has more points (some of which will be coincident)
	//if the source path doesn't have enough points, then we must add more points but hide them by putting then on top of existing points
	//we also need to hide curved points by setting their handles on top of their origin points if they're morphing to/from a lineTo point:
	var makeMorphPath = function(pathFrom, pathTo) { // , interpolation) {

		var newCommands = [];
		var knotPoint = [];
		var m, n;

		var pathLong = pathFrom;
		var pathShort = pathTo;

		if (pathShort.commands.length > pathLong.commands.length) {
			pathShort = pathFrom;
			pathLong = pathTo;
		}
		var step = (pathLong.commands.length - 1) / (pathShort.commands.length - 1);

		//if the target path has more points than the original, we need to add more add points:
		var pPoints = [];
		for (m = 0; m < pathShort.commands.length; m++) {
			var comd = cloneEveObj(pathShort.commands[m]);
			newCommands[Math.round(m * step)] = comd;
			pPoints.push([comd.to[0], comd.to[1]]);
		}
		//keep first and last commands the same:
		newCommands[0] = cloneEveObj(pathShort.commands[0]);
		newCommands[pathLong.commands.length - 1] = cloneEveObj(pathShort.commands[pathShort.commands.length - 1]);

		for (m = 0; m < newCommands.length; m++) {
			if (!isNil(newCommands[m])) {
				knotPoint.push(m);
			}
		}

		for (n = 0; n < pathLong.commands.length; n++) {
			var nextVal = null;
			var lookStep = 0;
			if (isNil(newCommands[n])) {
				while (isNil(nextVal)) {
					lookStep++;
					nextVal = newCommands[n + lookStep];
				}
				if (nextVal !== null) {
					var nextComd = newCommands[n + 1];
					var lookIt = 1;
					while (isNil(nextComd)){
						lookIt++;
						nextComd = newCommands[n + lookIt];
					}
					newCommands[n] = cloneEveObj(newCommands[n - 1]);
					newCommands[n].type = pathLong.commands[n].type;
					newCommands[n].to[0] = newCommands[n].to[0] + ((nextVal.to[0] - newCommands[n - 1].to[0]) / (lookStep + 1));
					newCommands[n].to[1] = newCommands[n].to[1] + ((nextVal.to[1] - newCommands[n - 1].to[1]) / (lookStep + 1));
					newCommands[n].to[1] = pathShort.getClosestPoint([newCommands[n].to[0], newCommands[n].to[1]])[1];

					newCommands[n].handleFrom[0] = nextComd.handleFrom[0];
					newCommands[n].handleFrom[1] = nextComd.handleFrom[1];
					newCommands[n].handleTo[0] = newCommands[n].to[0];
					newCommands[n].handleTo[1] = newCommands[n].to[1];
					if (!isNil(newCommands[n + 1])) {
						newCommands[n + 1].handleFrom[0] = newCommands[n].to[0];
						newCommands[n + 1].handleFrom[1] = newCommands[n].to[1];
					}
					pPoints[n] = [newCommands[n].to[0], newCommands[n].to[1]];
				}
			}
		}
		var morphPath = cloneEveObj(pathLong);
		morphPath.knotPoint = knotPoint;
		morphPath.commands = newCommands;
		return morphPath;
	};

	eve.sector = function(centre, startAngle, angleRange, radOuter, radInner) {

		this.centre = centre || [0, 0];
		this.startAngle = startAngle || 0;
		this.angleRange = angleRange || 0;
		this.radOuter = radOuter || null;
		this.radInner = radInner || null;

		this.getCentroid = function() {
			var iRad = this.radInner || 0;
			return eve.polarToCartesian(eve.toRadians(this.startAngle + (this.angleRange / 2)), iRad + ((this.radOuter - iRad) / 2));
		};

		this.toSVG = function(restrict) {
			var svgPath = "";

			var endAngle = this.startAngle + this.angleRange;
			if (restrict){
				if (endAngle > 360) {
					endAngle = 359.99;
				}
				else if(endAngle < 0){
					endAngle = 0;
				}
			}
			else{
				if (endAngle > 360) {
					endAngle -= 360;
				}
			}

			var p0 = eve.polarToCartesian(eve.toRadians(this.startAngle), this.radOuter);
			p0[0] += this.centre[0];
			p0[1] = this.centre[1] - p0[1];
			var p1 = eve.polarToCartesian(eve.toRadians(endAngle), this.radOuter);
			p1[0] += this.centre[0];
			p1[1] = this.centre[1] - p1[1];
			var p2 = eve.polarToCartesian(eve.toRadians(endAngle), this.radInner);
			p2[0] += this.centre[0];
			p2[1] = this.centre[1] - p2[1];
			var p3 = eve.polarToCartesian(eve.toRadians(this.startAngle), this.radInner);
			p3[0] += this.centre[0];
			p3[1] = this.centre[1] - p3[1];

			p0[0] = eve.setDecimal(p0[0], 5);
			p0[1] = eve.setDecimal(p0[1], 5);
			p1[0] = eve.setDecimal(p1[0], 5);
			p1[1] = eve.setDecimal(p1[1], 5);
			p2[0] = eve.setDecimal(p2[0], 5);
			p2[1] = eve.setDecimal(p2[1], 5);
			p3[0] = eve.setDecimal(p3[0], 5);
			p3[1] = eve.setDecimal(p3[1], 5);

			var axisRotation = 0;

			var sweepOuter = 1; //clockwise
			var sweepInner = 0; //anti-clockwise

			var largeArc = this.angleRange > 180 ? 1 : 0;

			if (this.angleRange === 360) {
				svgPath = "M" + this.centre[0] + " " + (this.centre[1] - this.radOuter) + " ";
				svgPath += "A" + this.radOuter + " " + this.radOuter + " " + axisRotation + " " + largeArc + " " + sweepOuter + " " + this.centre[0] + " " + (this.centre[1] + this.radOuter) + " ";
				svgPath += "A" + this.radOuter + " " + this.radOuter + " " + axisRotation + " " + largeArc + " " + sweepOuter + " " + this.centre[0] + " " + (this.centre[1] - this.radOuter) + " ";
				svgPath += "Z";
				if (this.radInner > 0) {
					svgPath += "M" + this.centre[0] + " " + (this.centre[1] - this.radInner) + " ";
					svgPath += "A" + this.radInner + " " + this.radInner + " " + axisRotation + " " + largeArc + " " + sweepOuter + " " + this.centre[0] + " " + (this.centre[1] + this.radInner) + " ";
					svgPath += "A" + this.radInner + " " + this.radInner + " " + axisRotation + " " + largeArc + " " + sweepOuter + " " + this.centre[0] + " " + (this.centre[1] - this.radInner) + " ";
					svgPath += "Z";
				}
			} else {
				svgPath = "M" + p0[0] + " " + p0[1] + " ";
				svgPath += "A" + this.radOuter + " " + this.radOuter + " " + axisRotation + " " + largeArc + " " + sweepOuter + " " + p1[0] + " " + p1[1] + " ";
				svgPath += "L" + p2[0] + " " + p2[1] + " ";
				if (this.radInner > 0) {
					svgPath += "A" + this.radInner + " " + this.radInner + " " + axisRotation + " " + largeArc + " " + sweepInner + " " + p3[0] + " " + p3[1] + " ";
				}
				svgPath += "Z";
			}

			return svgPath;
		};

	};

	eve.addSector = function(centre, startAngle, angleRange, radOuter, radInner) {
		var newSector = new eve.sector(centre, startAngle, angleRange, radOuter, radInner);
		return newSector;
	};

	var proportionalProps = function(arr, ratio, props){
		var i, arrMem = {}, prev, next, amount;
		var index = arr.length * ratio;
		if (!Number.isInteger(index)){
			prev = Math.floor(index);
			next = Math.floor(index) + 1;
			amount = index - prev;
			for (i = 0; i < props.length; i++){
				arrMem[props[i]] = arr[prev][props[i]] + ((arr[next][props[i]] - arr[prev][props[i]]) * amount);
			}
		}
		else{
			arrMem = arr[index];
		}
		return arrMem;
	};

	eve.getPointBetweenPoints = function(p1, p2, d){
		return [
			p1[0] + ((p2[0] - p1[0]) * d),
			p1[1] + ((p2[1] - p1[1]) * d)
		];
	};

	var projectionratio = function(t, n) {
		// see u(t) note on http://pomax.github.io/bezierinfo/#abc
		if (n !== 2 && n !== 3) {
			return false;
		}
		if (typeof t === "undefined") {
			t = 0.5;
		} else if (t === 0 || t === 1) {
			return t;
		}
		var top = Math.pow(1 - t, n),
		bottom = Math.pow(t, n) + top;
		return top / bottom;
	};

	var abcratio = function(t, n) {
		// see ratio(t) note on http://pomax.github.io/bezierinfo/#abc
		if (n !== 2 && n !== 3) {
			return false;
		}
		if (typeof t === "undefined") {
			t = 0.5;
		} else if (t === 0 || t === 1) {
			return t;
		}
		var bottom = Math.pow(t, n) + Math.pow(1 - t, n),
		top = bottom - 1;
		return Math.abs(top / bottom);
	};

	function getABC(n, S, B, E, t) {
    if (typeof t === "undefined") {
      t = 0.5;
    }
    var u = projectionratio(t, n),
      um = 1 - u,
      C = [u * S[0] + um * E[0], u * S[1] + um * E[1]],
      s = abcratio(t, n),
      A = [B[0] + (B[0] - C[0]) / s, B[1] + (B[1] - C[1]) / s];
    return { A: A, B: B, C: C };
  }

	eve.cubicFromPoints = function(S, B, E, t, d1) {
    if (typeof t === "undefined") {
      t = 0.5;
    }
    var abc = getABC(3, S, B, E, t);
    if (typeof d1 === "undefined") {
			d1 = lineLength(B, abc.C);
    }
    var d2 = d1 * (1 - t) / t;

		var selen = Math.sqrt((S[0] - E[0]) * (S[0] - E[0]) + (S[1] - E[1]) * (S[1] - E[1]));
		var lx = (E[0] - S[0]) / selen,
      ly = (E[1] - S[1]) / selen,
      bx1 = d1 * lx,
      by1 = d1 * ly,
      bx2 = d2 * lx,
      by2 = d2 * ly;
    // derivation of new hull coordinates
    var e1 = [B[0] - bx1, B[1] - by1],
      e2 = [B[0] + bx2, B[1] + by2],
      A = abc.A,
      v1 = [A[0] + (e1[0] - A[0]) / (1 - t), A[1] + (e1[1] - A[1]) / (1 - t)],
      v2 = [A[0] + (e2[0] - A[0]) / t, A[1] + (e2[1] - A[1]) / t],
      nc1 = [S[0] + (v1[0] - S[0]) / t, S[1] + (v1[1] - S[1]) / t],
      nc2 = [E[0] + (v2[0] - E[0]) / (1 - t), E[1] + (v2[1] - E[1]) / (1 - t)];
		return {start: S, ctrl1: nc1, ctrl2: nc2, end: E};
  };

	eve.path = function(commands, closed) {

		this.commands = commands || [];
		this.closed = closed || false;

		this.addCommand = function(type, to, handleFrom, handleTo, length, radius, angle, centre, largeArc, sweepFlag, absolute, addToFront) {
			var newCommand = new eve.command(type, to, handleFrom, handleTo, length, radius, angle, centre, largeArc, sweepFlag, absolute);
			if (addToFront) {
				this.commands.unshift(newCommand);
			} else {
				this.commands.push(newCommand);
			}
			return newCommand;
		};

		this.rationalise = function(){
			for (var c = 1; c < this.commands.length; c++){
				if (this.commands[c].type === "cubicReflect" && this.commands[c - 1].type === "cubic"){
					this.commands[c].handleFrom = [
						this.commands[c - 1].to[0] + (this.commands[c - 1].to[0] - this.commands[c - 1].handleTo[0]),
						this.commands[c - 1].to[1] + (this.commands[c - 1].to[1] - this.commands[c - 1].handleTo[1])
					];
				}
			}
		};

		this.getSize = function(){
			var top = null, right = null, bottom = null, left = null;
			for (var c = 0; c < this.commands.length; c++){
				if (top === null || top > this.commands[c].to[1]){
					top = this.commands[c].to[1];
				}
				if (left === null || left > this.commands[c].to[0]){
					left = this.commands[c].to[0];
				}
				if (bottom < this.commands[c].to[1]){
					bottom = this.commands[c].to[1];
				}
				if (right < this.commands[c].to[0]){
					right = this.commands[c].to[0];
				}
			}
			return {top: top, right: right, bottom: bottom, left: left, width: right - left, height: bottom - top};
		};

		this.unitise = function(){
			//make the first point 0,0, the last point 1,1 and scale other commands accordingly
			var startX = this.commands[0].to[0];
			var startY = this.commands[0].to[1];
			var width = this.commands[this.commands.length - 1].to[0] - startX;
			var height = this.commands[this.commands.length - 1].to[1] - startY;
			var scaleX = 1 / width;
			var scaleY = 1 / height;

			for (var c = 0; c < this.commands.length; c++){
				this.commands[c].to[0] = (this.commands[c].to[0] - startX) * scaleX;
				this.commands[c].to[1] = (this.commands[c].to[1] - startY) * scaleY;
				if (!isNil(this.commands[c].handleTo[0])){
					this.commands[c].handleTo[0] = (this.commands[c].handleTo[0] - startX) * scaleX;
				}
				if (!isNil(this.commands[c].handleTo[1])){
					this.commands[c].handleTo[1] = (this.commands[c].handleTo[1] - startY) * scaleY;
				}
				if (!isNil(this.commands[c].handleFrom[0])){
					this.commands[c].handleFrom[0] = (this.commands[c].handleFrom[0] - startX) * scaleX;
				}
				if (!isNil(this.commands[c].handleFrom[1])){
					this.commands[c].handleFrom[1] = (this.commands[c].handleFrom[1] - startY) * scaleY;
				}
			}
		};

		this.getSegmentLength = function(s){
			if (this.commands[s].type === "lineTo"){
				return lineLength(this.commands[s - 1].to, this.commands[s].to);
			}
			if (this.commands[s].type.indexOf("cubic") === 0){
				this.rationalise();
				var xs = [this.commands[s - 1].to[0], this.commands[s].handleFrom[0], this.commands[s].handleTo[0], this.commands[s].to[0]];
				var ys = [this.commands[s - 1].to[1], this.commands[s].handleFrom[1], this.commands[s].handleTo[1], this.commands[s].to[1]];
				return eve.getArcLength(xs, ys, 1, 24);
			}
			return 0;
		};

		this.getTotalLength = function(){
			var l = 0;
			for (var c = 0; c < this.commands.length; c++){
				l += this.getSegmentLength(c);
			}
			return l;
		};

		this.getPointAtLength = function(d){
			if (d <= 0){
				return this.commands[0].to;
			}
			var totalLength = this.getTotalLength();
			if (d >= totalLength){
				return this.commands[this.commands.length - 1].to.clone();
			}
			var s, runningLength = 0, bLength = 0, comd;
			for (s = 0; s < this.commands.length - 1; s++){
				runningLength += this.getSegmentLength(s);
				if (runningLength > d){
					break;
				}
				bLength = runningLength;
			}
			comd = this.commands[s];
			d -= bLength;
			// make d a factor of the segment length (0 - 1):
			d /= this.getSegmentLength(s);

			if (comd.type === "lineTo"){
				return eve.getPointBetweenPoints(this.commands[s - 1].to, comd.to, d);
			}
			else if (comd.type.indexOf("cubic") === 0){
				this.rationalise();
				var points = [
					eve.getPointBetweenPoints(this.commands[s - 1].to, comd.handleFrom, d),
					eve.getPointBetweenPoints(comd.handleTo, comd.to, d),
					eve.getPointBetweenPoints(comd.handleFrom, comd.handleTo, d)
				];
				points.push(
					eve.getPointBetweenPoints(points[0], points[2], d)
				);
				points.push(
					eve.getPointBetweenPoints(points[2], points[1], d)
				);
				points.push(
					eve.getPointBetweenPoints(points[3], points[4], d)
				);
				return points[5];
			}

		};

		this.getDistAlongSegmentAtPoint = function(point, precision){
			precision = precision || 1;
			var tLength = this.getTotalLength();
			var step = 1 / tLength;
			var p;
			var s;
			var d = 0;
			while (d < 1){
				p = this.getPointAtLength(d * tLength);
				if ((p[0] < point[0] + precision && p[0] > point[0] - precision) && (p[1] > point[1] - precision && p[1] < point[1] + precision)){
					var pLength = 0;
					for (s = 0; s < this.commands.length; s++){
						var sLength = this.getSegmentLength(s);
						if (pLength + sLength > d * tLength){
							break;
						}
						pLength += sLength;
					}
					return [s, ((d * tLength) - pLength) / this.getSegmentLength(s)];
				}
				d += step;
			}
			return [0, 0];
		};

		this.getLengthAtPoint = function(point, precision){
			precision = precision || 1;
			var tLength = this.getTotalLength();
			var step = 1 / tLength;
			var p;
			var d = 0;
			while (d < 1){
				p = this.getPointAtLength(d * tLength);
				if ((p[0] < point[0] + precision && p[0] > point[0] - precision) && (p[1] > point[1] - precision && p[1] < point[1] + precision)){
					return d * tLength;
				}
				d += step;
			}
			return 0;
		};

		this.getSegmentAtPoint = function(point, precision){
			var pLength = this.getLengthAtPoint(point, precision);
			var tLength = 0;
			for (var s = 0; s< this.commands.length; s++){
				tLength += this.getSegmentLength(s);
				if (tLength > pLength){
					return s;
				}
			}
			return this.commands.length - 1;
		};

		this.getClosestPoint = function(point) {
			var pathLength = this.getTotalLength(), precision = 8, best, bestLength, bestDistance = Infinity;

			function distance2(p) {
				var dx = p[0] - point[0];
				var dy = p[1] - point[1];
				return dx * dx + dy * dy;
			}

			// linear scan for coarse approximation
			for (var scanLength = 0; scanLength <= pathLength; scanLength += precision) {
				var scan = this.getPointAtLength(scanLength);
				var scanDistance = distance2(scan);
				if (scanDistance < bestDistance) {
					best = scan;
					bestLength = scanLength;
					bestDistance = scanDistance;
				}
			}

			// binary search for precise estimate
			precision /= 2;
			while (precision > 0.5) {
				var before, after, beforeLength, afterLength, beforeDistance, afterDistance;
				if ((beforeLength = bestLength - precision) >= 0 && (beforeDistance = distance2(before = this.getPointAtLength(beforeLength))) < bestDistance) {
					best = before;
					bestLength = beforeLength;
					bestDistance = beforeDistance;
				} else if ((afterLength = bestLength + precision) <= pathLength && (afterDistance = distance2(after = this.getPointAtLength(afterLength))) < bestDistance) {
					best = after;
					bestLength = afterLength;
					bestDistance = afterDistance;
				} else {
					precision /= 2;
				}
			}

			best = [best[0], best[1]];
			best.distance = Math.sqrt(bestDistance);
			return best;
		};

		var strComds = {moveTo: "M", lineTo: "L", cubic: "C", cubicReflect: "S", quadratic: "Q", quadraticReflect: "T", horizontal: "H", vertical: "V", arc: "A", close: "Z"};
		//commands: M, m, L, l, H, h, V, v, C, c, S, s, Q, q, T, t, A, a, Z, z
		/*
		M: end.x, end.y
		L: end.x, end.y
		C: bp1.x, bp1.y, bp2.x, bp2.y, end.x, end.y
		S: bp2.x, bp2.y, end.x, end.y
		Q: bp.x, bp.y, end.x, end.y
		T: end.x, end.y
		H: end.distanceX
		V: end.diatanceY
		A: rad.x, rad.y, angle, largeArc (0 or 1), sweepFlag (0 or 1), end.X, end.Y
		*/

		this.toSVG = function() {
			var svgPath = "";
			for (var p = 0; p < this.commands.length; p++) {
				var cmd = cloneEveObj(this.commands[p]);
				if (p === 0) {
					//according to the spec: https://www.w3.org/TR/SVG/paths.html#PathDataMovetoCommands
					cmd.type = "moveTo";
				}
				var abbr = strComds[cmd.type];
				if (!cmd.absolute) {
					abbr = abbr.toLowerCase();
				}

				if (cmd.type === "horizontal" || cmd.type === "vertical") {
					svgPath += abbr + cmd.length + " ";
				} else if (cmd.type === "lineTo" || cmd.type === "moveTo" || cmd.type === "quadraticReflect") {
					svgPath += abbr + eve.setDecimal(cmd.to[0], 3) + "," + eve.setDecimal(cmd.to[1], 3) + " ";
				} else if (cmd.type === "cubic") {
					svgPath += abbr + eve.setDecimal(cmd.handleFrom[0], 3) + "," + eve.setDecimal(cmd.handleFrom[1], 3) + " " + eve.setDecimal(cmd.handleTo[0], 3) + "," + eve.setDecimal(cmd.handleTo[1], 3) + " " + eve.setDecimal(cmd.to[0], 3) + "," + eve.setDecimal(cmd.to[1], 3) + " ";
				} else if (cmd.type === "cubicReflect") {
					svgPath += abbr + eve.setDecimal(cmd.handleTo[0], 3) + "," + eve.setDecimal(cmd.handleTo[1], 3) + " " + eve.setDecimal(cmd.to[0], 3) + "," + eve.setDecimal(cmd.to[1], 3) + " ";
				} else if (cmd.type === "quadratic") {
					svgPath += abbr + eve.setDecimal(cmd.handleFrom[0], 3) + "," + eve.setDecimal(cmd.handleFrom[1], 3) + " " + eve.setDecimal(cmd.to[0], 3) + "," + eve.setDecimal(cmd.to[1], 3) + " ";
				} else if (cmd.type === "arc") {
					svgPath += abbr + eve.setDecimal(cmd.radius[0], 3) + "," + eve.setDecimal(cmd.radius[1], 3) + " " + eve.setDecimal(cmd.angle, 3) + " " + cmd.largeArc + " " + cmd.sweepFlag + " " + eve.setDecimal(cmd.to[0], 3) + "," + eve.setDecimal(cmd.to[1], 3) + " ";
				} else if (cmd.type === "close") {
					svgPath += abbr + " ";
				}

			}
			if (this.closed) {
				svgPath += " Z";
			}
			return svgPath;
		};
	};
	eve.addPath = function(commands, closed) {
		var newPath = new eve.path(commands, closed);
		return newPath;
	};

	eve.getSvgPathSegment = function(path, point1, point2, step) {
		step = step || 1;
		var error = 3;
		var pathLength = 0;
		var dist = {start: 0, end: 0};
		var dp;
		while (pathLength < path.getTotalLength() && dist.start === 0) {
			pathLength += step;
			dp = path.getPointAtLength(pathLength);
			if (dp.x > point1.x - error && dp.x < point1.x + error && dp.y > point1.y - error && dp.y < point1.y + error) {
				dist.start = pathLength;
			}
		}
		pathLength = 0;
		while (pathLength < path.getTotalLength() && dist.end === 0) {
			pathLength += step;
			dp = path.getPointAtLength(pathLength);
			if (dp.x > point2.x - error && dp.x < point2.x + error && dp.y > point2.y - error && dp.y < point2.y + error) {
				dist.end = pathLength;
			}
		}
		return dist;
	};

	// Computes the slope from points p0 to p1.
	var lineSlope = function(p0, p1) {
		return (p1[1] - p0[1]) / (p1[0] - p0[0]);
	};
	// Get the length of a line between 2 points:
	var lineLength = function(p1, p2) {
		var w = p2[0] - p1[0], l = p2[1] - p1[1];
		return Math.sqrt(w * w + l * l);
	};

	// Linear interpolation; generates "lineTo" commands.
	var lineLinear = function(points) {
		var lComds = eve.addPath(null, false);
		for (var p = 0; p < points.length; p++) {
			lComds.addCommand("lineTo", points[p]);
		}
		return lComds;
	};

/*	var eve.quadratic = function(a, b, c){
		var result = (-1 * b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
		var result2 = (-1 * b - Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
		return result + "<br>" + result2;

		t = 0.5; // given example value
		x = (1 - t) * (1 - t) * p[0].x + 2 * (1 - t) * t * p[1].x + t * t * p[2].x;
		y = (1 - t) * (1 - t) * p[0].y + 2 * (1 - t) * t * p[1].y + t * t * p[2].y;

	};
*/
	eve.cubic = function(a,b,c,d)	{
		var m, m2, k, n, n2, x, r, rc, theta, sign, dans;

		var f = (((3 * c) / a) - (((b * b) / (a * a))))/3;
		var g = ((2 * ((b * b * b) / (a * a * a)) - (9 * b * c / (a * a)) + ((27 * (d / a))))) / 27;
		var h = ((g * g) / 4) + ((f * f * f) / 27);

		if (h > 0) {
			m = -(g / 2) + (Math.sqrt(h));
			k = m < 0 ? -1:1;
			m2 = Math.pow((m * k), (1 / 3));
			m2 = m2 * k;
			n = -(g / 2)- (Math.sqrt(h));
			k = n < 0 ? -1:1;
			n2 = Math.pow((n * k), (1 / 3));
			n2 = n2 * k;
			x = (m2 + n2) - (b / (3 * a));
		}
		else {
			r = (Math.sqrt((g * g / 4) - h));
			k = r < 0 ? -1:1;
			rc = Math.pow((r * k), (1 / 3)) * k;
			theta = Math.acos((-g / (2 * r)));
			x = 2 * (rc * Math.cos(theta / 3)) - (b / (3 * a));
			x = x * 1E+14;
			x = Math.round(x);
			x = (x / 1E+14);
		}

		if ((f + g + h) === 0){
			if (d < 0){
				sign = -1;
			}
			if (d >= 0) {
				sign = 1;
			}
			if (sign > 0){
				dans = Math.pow((d / a), (1 / 3));
				dans = dans * -1;
			}
			if (sign<0){
				d = d * -1;
				dans = Math.pow((d / a), (1 / 3));
			}
			x = dans;
		}
		return x;
	};

	eve.getCubicPoint = function(p1, c1, c2, p2, t){
		//p1 = first point - like p1 = {x:50, y: 100};
		//c1 = first controller point - like c1 = {x:50, y: 100};
		//p2 = second point - like p2 = {x:50, y: 100};
		//c2 = second controller point - like c2 = {x:50, y: 100};
		var point = {x: 0, y: 0};
		var mt  = 1 - t;
		var mt2 = mt * mt;
		var mt3 = mt2 * mt;

		//fx(t) = x1 * (1-t)³ + x2 * 3 * (1-t)²t + x3 * 3 * (1-t)t² + x4 * t³
		point.x = p1.x * mt2 + c1.x * 3 * mt2 * t + c2.x * 3 * mt * t * t + p2.x * t * t * t;
		//fy(t) = y1 * (1-t)³ + y2 * 3 * (1-t)²t + y3 * 3 * (1-t)t² + y4 * t³
		point.y = p1.y * mt3 + c1.y * 3 * mt2 * t + c2.y * 3 * mt * t * t + p2.y * t * t * t;
		return point;
	};
	/*eve.getQuadPoint = function(p1, c, p2, t){

	};*/

	// Legendre-Gauss abscissae (xi values, defined at i = n as the roots of the nth order Legendre polynomial Pn(x))
	var tValues = [
		[],
		[],
		[-0.5773502691896257645091487805019574556476, 0.5773502691896257645091487805019574556476],
		[0, -0.7745966692414833770358530799564799221665, 0.7745966692414833770358530799564799221665],
		[-0.3399810435848562648026657591032446872005, 0.3399810435848562648026657591032446872005, -0.8611363115940525752239464888928095050957, 0.8611363115940525752239464888928095050957],
		[0, -0.5384693101056830910363144207002088049672, 0.5384693101056830910363144207002088049672, -0.9061798459386639927976268782993929651256, 0.9061798459386639927976268782993929651256],
		[0.6612093864662645136613995950199053470064, -0.6612093864662645136613995950199053470064, -0.2386191860831969086305017216807119354186, 0.2386191860831969086305017216807119354186, -0.9324695142031520278123015544939946091347, 0.9324695142031520278123015544939946091347],
		[0, 0.4058451513773971669066064120769614633473, -0.4058451513773971669066064120769614633473, -0.7415311855993944398638647732807884070741, 0.7415311855993944398638647732807884070741, -0.9491079123427585245261896840478512624007, 0.9491079123427585245261896840478512624007],
		[-0.1834346424956498049394761423601839806667, 0.1834346424956498049394761423601839806667, -0.5255324099163289858177390491892463490419, 0.5255324099163289858177390491892463490419, -0.7966664774136267395915539364758304368371, 0.7966664774136267395915539364758304368371, -0.9602898564975362316835608685694729904282, 0.9602898564975362316835608685694729904282],
		[0, -0.8360311073266357942994297880697348765441, 0.8360311073266357942994297880697348765441, -0.9681602395076260898355762029036728700494, 0.9681602395076260898355762029036728700494, -0.3242534234038089290385380146433366085719, 0.3242534234038089290385380146433366085719, -0.6133714327005903973087020393414741847857, 0.6133714327005903973087020393414741847857],
		[-0.1488743389816312108848260011297199846175, 0.1488743389816312108848260011297199846175, -0.4333953941292471907992659431657841622000, 0.4333953941292471907992659431657841622000, -0.6794095682990244062343273651148735757692, 0.6794095682990244062343273651148735757692, -0.8650633666889845107320966884234930485275, 0.8650633666889845107320966884234930485275, -0.9739065285171717200779640120844520534282, 0.9739065285171717200779640120844520534282],
		[0, -0.2695431559523449723315319854008615246796, 0.2695431559523449723315319854008615246796, -0.5190961292068118159257256694586095544802, 0.5190961292068118159257256694586095544802, -0.7301520055740493240934162520311534580496, 0.7301520055740493240934162520311534580496, -0.8870625997680952990751577693039272666316, 0.8870625997680952990751577693039272666316, -0.9782286581460569928039380011228573907714, 0.9782286581460569928039380011228573907714],
		[-0.1252334085114689154724413694638531299833, 0.1252334085114689154724413694638531299833, -0.3678314989981801937526915366437175612563, 0.3678314989981801937526915366437175612563, -0.5873179542866174472967024189405342803690, 0.5873179542866174472967024189405342803690, -0.7699026741943046870368938332128180759849, 0.7699026741943046870368938332128180759849, -0.9041172563704748566784658661190961925375, 0.9041172563704748566784658661190961925375, -0.9815606342467192506905490901492808229601, 0.9815606342467192506905490901492808229601],
		[0, -0.2304583159551347940655281210979888352115, 0.2304583159551347940655281210979888352115, -0.4484927510364468528779128521276398678019, 0.4484927510364468528779128521276398678019, -0.6423493394403402206439846069955156500716, 0.6423493394403402206439846069955156500716, -0.8015780907333099127942064895828598903056, 0.8015780907333099127942064895828598903056, -0.9175983992229779652065478365007195123904, 0.9175983992229779652065478365007195123904, -0.9841830547185881494728294488071096110649, 0.9841830547185881494728294488071096110649],
		[-0.1080549487073436620662446502198347476119, 0.1080549487073436620662446502198347476119, -0.3191123689278897604356718241684754668342, 0.3191123689278897604356718241684754668342, -0.5152486363581540919652907185511886623088, 0.5152486363581540919652907185511886623088, -0.6872929048116854701480198030193341375384, 0.6872929048116854701480198030193341375384, -0.8272013150697649931897947426503949610397, 0.8272013150697649931897947426503949610397, -0.9284348836635735173363911393778742644770, 0.9284348836635735173363911393778742644770, -0.9862838086968123388415972667040528016760, 0.9862838086968123388415972667040528016760],
		[0, -0.2011940939974345223006283033945962078128, 0.2011940939974345223006283033945962078128, -0.3941513470775633698972073709810454683627, 0.3941513470775633698972073709810454683627, -0.5709721726085388475372267372539106412383, 0.5709721726085388475372267372539106412383, -0.7244177313601700474161860546139380096308, 0.7244177313601700474161860546139380096308, -0.8482065834104272162006483207742168513662, 0.8482065834104272162006483207742168513662, -0.9372733924007059043077589477102094712439, 0.9372733924007059043077589477102094712439, -0.9879925180204854284895657185866125811469, 0.9879925180204854284895657185866125811469],
		[-0.0950125098376374401853193354249580631303, 0.0950125098376374401853193354249580631303, -0.2816035507792589132304605014604961064860, 0.2816035507792589132304605014604961064860, -0.4580167776572273863424194429835775735400, 0.4580167776572273863424194429835775735400, -0.6178762444026437484466717640487910189918, 0.6178762444026437484466717640487910189918, -0.7554044083550030338951011948474422683538, 0.7554044083550030338951011948474422683538, -0.8656312023878317438804678977123931323873, 0.8656312023878317438804678977123931323873, -0.9445750230732325760779884155346083450911, 0.9445750230732325760779884155346083450911, -0.9894009349916499325961541734503326274262, 0.9894009349916499325961541734503326274262],
		[0, -0.1784841814958478558506774936540655574754, 0.1784841814958478558506774936540655574754, -0.3512317634538763152971855170953460050405, 0.3512317634538763152971855170953460050405, -0.5126905370864769678862465686295518745829, 0.5126905370864769678862465686295518745829, -0.6576711592166907658503022166430023351478, 0.6576711592166907658503022166430023351478, -0.7815140038968014069252300555204760502239, 0.7815140038968014069252300555204760502239, -0.8802391537269859021229556944881556926234, 0.8802391537269859021229556944881556926234, -0.9506755217687677612227169578958030214433, 0.9506755217687677612227169578958030214433, -0.9905754753144173356754340199406652765077, 0.9905754753144173356754340199406652765077],
		[-0.0847750130417353012422618529357838117333, 0.0847750130417353012422618529357838117333, -0.2518862256915055095889728548779112301628, 0.2518862256915055095889728548779112301628, -0.4117511614628426460359317938330516370789, 0.4117511614628426460359317938330516370789, -0.5597708310739475346078715485253291369276, 0.5597708310739475346078715485253291369276, -0.6916870430603532078748910812888483894522, 0.6916870430603532078748910812888483894522, -0.8037049589725231156824174550145907971032, 0.8037049589725231156824174550145907971032, -0.8926024664975557392060605911271455154078, 0.8926024664975557392060605911271455154078, -0.9558239495713977551811958929297763099728, 0.9558239495713977551811958929297763099728, -0.9915651684209309467300160047061507702525, 0.9915651684209309467300160047061507702525],
		[0, -0.1603586456402253758680961157407435495048, 0.1603586456402253758680961157407435495048, -0.3165640999636298319901173288498449178922, 0.3165640999636298319901173288498449178922, -0.4645707413759609457172671481041023679762, 0.4645707413759609457172671481041023679762, -0.6005453046616810234696381649462392798683, 0.6005453046616810234696381649462392798683, -0.7209661773352293786170958608237816296571, 0.7209661773352293786170958608237816296571, -0.8227146565371428249789224867127139017745, 0.8227146565371428249789224867127139017745, -0.9031559036148179016426609285323124878093, 0.9031559036148179016426609285323124878093, -0.9602081521348300308527788406876515266150, 0.9602081521348300308527788406876515266150, -0.9924068438435844031890176702532604935893, 0.9924068438435844031890176702532604935893],
		[-0.0765265211334973337546404093988382110047, 0.0765265211334973337546404093988382110047, -0.2277858511416450780804961953685746247430, 0.2277858511416450780804961953685746247430, -0.3737060887154195606725481770249272373957, 0.3737060887154195606725481770249272373957, -0.5108670019508270980043640509552509984254, 0.5108670019508270980043640509552509984254, -0.6360536807265150254528366962262859367433, 0.6360536807265150254528366962262859367433, -0.7463319064601507926143050703556415903107, 0.7463319064601507926143050703556415903107, -0.8391169718222188233945290617015206853296, 0.8391169718222188233945290617015206853296, -0.9122344282513259058677524412032981130491, 0.9122344282513259058677524412032981130491, -0.9639719272779137912676661311972772219120, 0.9639719272779137912676661311972772219120, -0.9931285991850949247861223884713202782226, 0.9931285991850949247861223884713202782226],
		[0, -0.1455618541608950909370309823386863301163, 0.1455618541608950909370309823386863301163, -0.2880213168024010966007925160646003199090, 0.2880213168024010966007925160646003199090, -0.4243421202074387835736688885437880520964, 0.4243421202074387835736688885437880520964, -0.5516188358872198070590187967243132866220, 0.5516188358872198070590187967243132866220, -0.6671388041974123193059666699903391625970, 0.6671388041974123193059666699903391625970, -0.7684399634756779086158778513062280348209, 0.7684399634756779086158778513062280348209, -0.8533633645833172836472506385875676702761, 0.8533633645833172836472506385875676702761, -0.9200993341504008287901871337149688941591, 0.9200993341504008287901871337149688941591, -0.9672268385663062943166222149076951614246, 0.9672268385663062943166222149076951614246, -0.9937521706203895002602420359379409291933, 0.9937521706203895002602420359379409291933],
		[-0.0697392733197222212138417961186280818222, 0.0697392733197222212138417961186280818222, -0.2078604266882212854788465339195457342156, 0.2078604266882212854788465339195457342156, -0.3419358208920842251581474204273796195591, 0.3419358208920842251581474204273796195591, -0.4693558379867570264063307109664063460953, 0.4693558379867570264063307109664063460953, -0.5876404035069115929588769276386473488776, 0.5876404035069115929588769276386473488776, -0.6944872631866827800506898357622567712673, 0.6944872631866827800506898357622567712673, -0.7878168059792081620042779554083515213881, 0.7878168059792081620042779554083515213881, -0.8658125777203001365364256370193787290847, 0.8658125777203001365364256370193787290847, -0.9269567721871740005206929392590531966353, 0.9269567721871740005206929392590531966353, -0.9700604978354287271239509867652687108059, 0.9700604978354287271239509867652687108059, -0.9942945854823992920730314211612989803930, 0.9942945854823992920730314211612989803930],
		[0, -0.1332568242984661109317426822417661370104, 0.1332568242984661109317426822417661370104, -0.2641356809703449305338695382833096029790, 0.2641356809703449305338695382833096029790, -0.3903010380302908314214888728806054585780, 0.3903010380302908314214888728806054585780, -0.5095014778460075496897930478668464305448, 0.5095014778460075496897930478668464305448, -0.6196098757636461563850973116495956533871, 0.6196098757636461563850973116495956533871, -0.7186613631319501944616244837486188483299, 0.7186613631319501944616244837486188483299, -0.8048884016188398921511184069967785579414, 0.8048884016188398921511184069967785579414, -0.8767523582704416673781568859341456716389, 0.8767523582704416673781568859341456716389, -0.9329710868260161023491969890384229782357, 0.9329710868260161023491969890384229782357, -0.9725424712181152319560240768207773751816, 0.9725424712181152319560240768207773751816, -0.9947693349975521235239257154455743605736, 0.9947693349975521235239257154455743605736],
		[-0.0640568928626056260850430826247450385909, 0.0640568928626056260850430826247450385909, -0.1911188674736163091586398207570696318404, 0.1911188674736163091586398207570696318404, -0.3150426796961633743867932913198102407864, 0.3150426796961633743867932913198102407864, -0.4337935076260451384870842319133497124524, 0.4337935076260451384870842319133497124524, -0.5454214713888395356583756172183723700107, 0.5454214713888395356583756172183723700107, -0.6480936519369755692524957869107476266696, 0.6480936519369755692524957869107476266696, -0.7401241915785543642438281030999784255232, 0.7401241915785543642438281030999784255232, -0.8200019859739029219539498726697452080761, 0.8200019859739029219539498726697452080761, -0.8864155270044010342131543419821967550873, 0.8864155270044010342131543419821967550873, -0.9382745520027327585236490017087214496548, 0.9382745520027327585236490017087214496548, -0.9747285559713094981983919930081690617411, 0.9747285559713094981983919930081690617411, -0.9951872199970213601799974097007368118745, 0.9951872199970213601799974097007368118745]
	];

	// Legendre-Gauss weights (wi values, defined by a function linked to in the Bezier primer article)
	var cValues = [
		[],[],
		[1.0, 1.0],
		[0.8888888888888888888888888888888888888888, 0.5555555555555555555555555555555555555555, 0.5555555555555555555555555555555555555555],
		[0.6521451548625461426269360507780005927646, 0.6521451548625461426269360507780005927646, 0.3478548451374538573730639492219994072353, 0.3478548451374538573730639492219994072353],
		[0.5688888888888888888888888888888888888888, 0.4786286704993664680412915148356381929122, 0.4786286704993664680412915148356381929122, 0.2369268850561890875142640407199173626432, 0.2369268850561890875142640407199173626432],
		[0.3607615730481386075698335138377161116615, 0.3607615730481386075698335138377161116615, 0.4679139345726910473898703439895509948116, 0.4679139345726910473898703439895509948116, 0.1713244923791703450402961421727328935268, 0.1713244923791703450402961421727328935268],
		[0.4179591836734693877551020408163265306122, 0.3818300505051189449503697754889751338783, 0.3818300505051189449503697754889751338783, 0.2797053914892766679014677714237795824869, 0.2797053914892766679014677714237795824869, 0.1294849661688696932706114326790820183285, 0.1294849661688696932706114326790820183285],
		[0.3626837833783619829651504492771956121941, 0.3626837833783619829651504492771956121941, 0.3137066458778872873379622019866013132603, 0.3137066458778872873379622019866013132603, 0.2223810344533744705443559944262408844301, 0.2223810344533744705443559944262408844301, 0.1012285362903762591525313543099621901153, 0.1012285362903762591525313543099621901153],
		[0.3302393550012597631645250692869740488788, 0.1806481606948574040584720312429128095143, 0.1806481606948574040584720312429128095143, 0.0812743883615744119718921581105236506756, 0.0812743883615744119718921581105236506756, 0.3123470770400028400686304065844436655987, 0.3123470770400028400686304065844436655987, 0.2606106964029354623187428694186328497718, 0.2606106964029354623187428694186328497718],
		[0.2955242247147528701738929946513383294210, 0.2955242247147528701738929946513383294210, 0.2692667193099963550912269215694693528597, 0.2692667193099963550912269215694693528597, 0.2190863625159820439955349342281631924587, 0.2190863625159820439955349342281631924587, 0.1494513491505805931457763396576973324025, 0.1494513491505805931457763396576973324025, 0.0666713443086881375935688098933317928578, 0.0666713443086881375935688098933317928578],
		[0.2729250867779006307144835283363421891560, 0.2628045445102466621806888698905091953727, 0.2628045445102466621806888698905091953727, 0.2331937645919904799185237048431751394317, 0.2331937645919904799185237048431751394317, 0.1862902109277342514260976414316558916912, 0.1862902109277342514260976414316558916912, 0.1255803694649046246346942992239401001976, 0.1255803694649046246346942992239401001976, 0.0556685671161736664827537204425485787285, 0.0556685671161736664827537204425485787285],
		[0.2491470458134027850005624360429512108304, 0.2491470458134027850005624360429512108304, 0.2334925365383548087608498989248780562594, 0.2334925365383548087608498989248780562594, 0.2031674267230659217490644558097983765065, 0.2031674267230659217490644558097983765065, 0.1600783285433462263346525295433590718720, 0.1600783285433462263346525295433590718720, 0.1069393259953184309602547181939962242145, 0.1069393259953184309602547181939962242145, 0.0471753363865118271946159614850170603170, 0.0471753363865118271946159614850170603170],
		[0.2325515532308739101945895152688359481566, 0.2262831802628972384120901860397766184347, 0.2262831802628972384120901860397766184347, 0.2078160475368885023125232193060527633865, 0.2078160475368885023125232193060527633865, 0.1781459807619457382800466919960979955128, 0.1781459807619457382800466919960979955128, 0.1388735102197872384636017768688714676218, 0.1388735102197872384636017768688714676218, 0.0921214998377284479144217759537971209236, 0.0921214998377284479144217759537971209236, 0.0404840047653158795200215922009860600419, 0.0404840047653158795200215922009860600419],
		[0.2152638534631577901958764433162600352749, 0.2152638534631577901958764433162600352749, 0.2051984637212956039659240656612180557103, 0.2051984637212956039659240656612180557103, 0.1855383974779378137417165901251570362489, 0.1855383974779378137417165901251570362489, 0.1572031671581935345696019386238421566056, 0.1572031671581935345696019386238421566056, 0.1215185706879031846894148090724766259566, 0.1215185706879031846894148090724766259566, 0.0801580871597602098056332770628543095836, 0.0801580871597602098056332770628543095836, 0.0351194603317518630318328761381917806197, 0.0351194603317518630318328761381917806197],
		[0.2025782419255612728806201999675193148386, 0.1984314853271115764561183264438393248186, 0.1984314853271115764561183264438393248186, 0.1861610000155622110268005618664228245062, 0.1861610000155622110268005618664228245062, 0.1662692058169939335532008604812088111309, 0.1662692058169939335532008604812088111309, 0.1395706779261543144478047945110283225208, 0.1395706779261543144478047945110283225208, 0.1071592204671719350118695466858693034155, 0.1071592204671719350118695466858693034155, 0.0703660474881081247092674164506673384667, 0.0703660474881081247092674164506673384667, 0.0307532419961172683546283935772044177217, 0.0307532419961172683546283935772044177217],
		[0.1894506104550684962853967232082831051469, 0.1894506104550684962853967232082831051469, 0.1826034150449235888667636679692199393835, 0.1826034150449235888667636679692199393835, 0.1691565193950025381893120790303599622116, 0.1691565193950025381893120790303599622116, 0.1495959888165767320815017305474785489704, 0.1495959888165767320815017305474785489704, 0.1246289712555338720524762821920164201448, 0.1246289712555338720524762821920164201448, 0.0951585116824927848099251076022462263552, 0.0951585116824927848099251076022462263552, 0.0622535239386478928628438369943776942749, 0.0622535239386478928628438369943776942749, 0.0271524594117540948517805724560181035122, 0.0271524594117540948517805724560181035122],
		[0.1794464703562065254582656442618856214487, 0.1765627053669926463252709901131972391509, 0.1765627053669926463252709901131972391509, 0.1680041021564500445099706637883231550211, 0.1680041021564500445099706637883231550211, 0.1540457610768102880814315948019586119404, 0.1540457610768102880814315948019586119404, 0.1351363684685254732863199817023501973721, 0.1351363684685254732863199817023501973721, 0.1118838471934039710947883856263559267358, 0.1118838471934039710947883856263559267358, 0.0850361483171791808835353701910620738504, 0.0850361483171791808835353701910620738504, 0.0554595293739872011294401653582446605128, 0.0554595293739872011294401653582446605128, 0.0241483028685479319601100262875653246916, 0.0241483028685479319601100262875653246916],
		[0.1691423829631435918406564701349866103341, 0.1691423829631435918406564701349866103341, 0.1642764837458327229860537764659275904123, 0.1642764837458327229860537764659275904123, 0.1546846751262652449254180038363747721932, 0.1546846751262652449254180038363747721932, 0.1406429146706506512047313037519472280955, 0.1406429146706506512047313037519472280955, 0.1225552067114784601845191268002015552281, 0.1225552067114784601845191268002015552281, 0.1009420441062871655628139849248346070628, 0.1009420441062871655628139849248346070628, 0.0764257302548890565291296776166365256053, 0.0764257302548890565291296776166365256053, 0.0497145488949697964533349462026386416808, 0.0497145488949697964533349462026386416808, 0.0216160135264833103133427102664524693876, 0.0216160135264833103133427102664524693876],
		[0.1610544498487836959791636253209167350399, 0.1589688433939543476499564394650472016787, 0.1589688433939543476499564394650472016787, 0.1527660420658596667788554008976629984610, 0.1527660420658596667788554008976629984610, 0.1426067021736066117757461094419029724756, 0.1426067021736066117757461094419029724756, 0.1287539625393362276755157848568771170558, 0.1287539625393362276755157848568771170558, 0.1115666455473339947160239016817659974813, 0.1115666455473339947160239016817659974813, 0.0914900216224499994644620941238396526609, 0.0914900216224499994644620941238396526609, 0.0690445427376412265807082580060130449618, 0.0690445427376412265807082580060130449618, 0.0448142267656996003328381574019942119517, 0.0448142267656996003328381574019942119517, 0.0194617882297264770363120414644384357529, 0.0194617882297264770363120414644384357529],
		[0.1527533871307258506980843319550975934919, 0.1527533871307258506980843319550975934919, 0.1491729864726037467878287370019694366926, 0.1491729864726037467878287370019694366926, 0.1420961093183820513292983250671649330345, 0.1420961093183820513292983250671649330345, 0.1316886384491766268984944997481631349161, 0.1316886384491766268984944997481631349161, 0.1181945319615184173123773777113822870050, 0.1181945319615184173123773777113822870050, 0.1019301198172404350367501354803498761666, 0.1019301198172404350367501354803498761666, 0.0832767415767047487247581432220462061001, 0.0832767415767047487247581432220462061001, 0.0626720483341090635695065351870416063516, 0.0626720483341090635695065351870416063516, 0.0406014298003869413310399522749321098790, 0.0406014298003869413310399522749321098790, 0.0176140071391521183118619623518528163621, 0.0176140071391521183118619623518528163621],
		[0.1460811336496904271919851476833711882448, 0.1445244039899700590638271665537525436099, 0.1445244039899700590638271665537525436099, 0.1398873947910731547221334238675831108927, 0.1398873947910731547221334238675831108927, 0.1322689386333374617810525744967756043290, 0.1322689386333374617810525744967756043290, 0.1218314160537285341953671771257335983563, 0.1218314160537285341953671771257335983563, 0.1087972991671483776634745780701056420336, 0.1087972991671483776634745780701056420336, 0.0934444234560338615532897411139320884835, 0.0934444234560338615532897411139320884835, 0.0761001136283793020170516533001831792261, 0.0761001136283793020170516533001831792261, 0.0571344254268572082836358264724479574912, 0.0571344254268572082836358264724479574912, 0.0369537897708524937999506682993296661889, 0.0369537897708524937999506682993296661889, 0.0160172282577743333242246168584710152658, 0.0160172282577743333242246168584710152658],
		[0.1392518728556319933754102483418099578739, 0.1392518728556319933754102483418099578739, 0.1365414983460151713525738312315173965863, 0.1365414983460151713525738312315173965863, 0.1311735047870623707329649925303074458757, 0.1311735047870623707329649925303074458757, 0.1232523768105124242855609861548144719594, 0.1232523768105124242855609861548144719594, 0.1129322960805392183934006074217843191142, 0.1129322960805392183934006074217843191142, 0.1004141444428809649320788378305362823508, 0.1004141444428809649320788378305362823508, 0.0859416062170677274144436813727028661891, 0.0859416062170677274144436813727028661891, 0.0697964684245204880949614189302176573987, 0.0697964684245204880949614189302176573987, 0.0522933351526832859403120512732112561121, 0.0522933351526832859403120512732112561121, 0.0337749015848141547933022468659129013491, 0.0337749015848141547933022468659129013491, 0.0146279952982722006849910980471854451902, 0.0146279952982722006849910980471854451902],
		[0.1336545721861061753514571105458443385831, 0.1324620394046966173716424647033169258050, 0.1324620394046966173716424647033169258050, 0.1289057221880821499785953393997936532597, 0.1289057221880821499785953393997936532597, 0.1230490843067295304675784006720096548158, 0.1230490843067295304675784006720096548158, 0.1149966402224113649416435129339613014914, 0.1149966402224113649416435129339613014914, 0.1048920914645414100740861850147438548584, 0.1048920914645414100740861850147438548584, 0.0929157660600351474770186173697646486034, 0.0929157660600351474770186173697646486034, 0.0792814117767189549228925247420432269137, 0.0792814117767189549228925247420432269137, 0.0642324214085258521271696151589109980391, 0.0642324214085258521271696151589109980391, 0.0480376717310846685716410716320339965612, 0.0480376717310846685716410716320339965612, 0.0309880058569794443106942196418845053837, 0.0309880058569794443106942196418845053837, 0.0134118594871417720813094934586150649766, 0.0134118594871417720813094934586150649766],
		[0.1279381953467521569740561652246953718517, 0.1279381953467521569740561652246953718517, 0.1258374563468282961213753825111836887264, 0.1258374563468282961213753825111836887264, 0.1216704729278033912044631534762624256070, 0.1216704729278033912044631534762624256070, 0.1155056680537256013533444839067835598622, 0.1155056680537256013533444839067835598622, 0.1074442701159656347825773424466062227946, 0.1074442701159656347825773424466062227946, 0.0976186521041138882698806644642471544279, 0.0976186521041138882698806644642471544279, 0.0861901615319532759171852029837426671850, 0.0861901615319532759171852029837426671850, 0.0733464814110803057340336152531165181193, 0.0733464814110803057340336152531165181193, 0.0592985849154367807463677585001085845412, 0.0592985849154367807463677585001085845412, 0.0442774388174198061686027482113382288593, 0.0442774388174198061686027482113382288593, 0.0285313886289336631813078159518782864491, 0.0285313886289336631813078159518782864491, 0.0123412297999871995468056670700372915759, 0.0123412297999871995468056670700372915759]
	];

	// LUT for binomial coefficient arrays per curve order 'n'
	var binomialCoefficients = [[1], [1, 1], [1, 2, 1], [1, 3, 3, 1]];

	// Look up what the binomial coefficient is for pair {n,k}
	function binomials(n, k) {
		return binomialCoefficients[n][k];
	}

	// Compute the curve derivative (hodograph) at t.
	function getDerivative(derivative, t, vs) {
		// the derivative of any 't'-less function is zero.
		var n = vs.length - 1, _vs, value, k;
		if (n === 0) {
			return 0;
		}
		// direct values? compute!
		if (derivative === 0) {
			value = 0;
			for (k = 0; k <= n; k++) {
				value += binomials(n, k) * Math.pow(1 - t, n - k) * Math.pow(t, k) * vs[k];
			}
			return value;
		} else {
			// Still some derivative? go down one order, then try for the lower order curves.
			_vs = new Array(n);
			for (k = 0; k < n; k++) {
				_vs[k] = n * (vs[k + 1] - vs[k]);
			}
			return getDerivative(derivative - 1, t, _vs);
		}
	}

	function B(xs, ys, t) {
		var xbase = getDerivative(1, t, xs);
		var ybase = getDerivative(1, t, ys);
		var combined = xbase * xbase + ybase * ybase;
		return Math.sqrt(combined);
	}

	eve.getArcLength = function (xs, ys, t, n){
		var z, sum, i, correctedT;
		if (xs.length >= tValues.length) {
			throw new Error("too high n bezier");
		}
		if (isNil(t)) {
			t = 1;
		}
		if (isNil(n)) {
			n = 20;
		}
		z = t / 2;
		sum = 0;
		for (i = 0; i < n; i++) {
			correctedT = z * tValues[n][i] + z;
			sum += cValues[n][i] * B(xs, ys, correctedT);
		}
		return z * sum;
	};

	eve.getPointOnPath = function(path, x, y, returnLength) {
		//requires an eve.path
		var coordVal = x;
		var coordGiven = 0;
		var coordReqd = 1;
		if (isNil(coordVal)){
			coordVal = y;
			coordGiven = 1;
			coordReqd = 0;
		}

		var pathLength = path.getTotalLength();
		var start = 0;
		var end = pathLength;
		var target = (start + end) / 2;

		// Ensure that x is within the range of the path
		coordVal = Math.max(coordVal, path.getPointAtLength(0)[coordGiven]);
		coordVal = Math.min(coordVal, path.getPointAtLength(pathLength)[coordGiven]);

		// Walk along the path using binary search to locate the point with the supplied value
		while (target >= start && target <= pathLength) {
			var pos = path.getPointAtLength(target);
			// use a threshold instead of strict equality
			// to handle javascript floating point precision
			if (Math.abs(pos[coordGiven] - coordVal) < 0.001) {
				if (returnLength){
					return [pos[coordReqd], target];
				}
				else{
					return pos[coordReqd];
				}
			}
			else if (pos[coordGiven] > coordVal) {
				end = target;
			}
			else {
				start = target;
			}
			target = (start + end) / 2;
		}
	};

	eve.getPointOnSvgPath = function(svgPath, x, y) {
		//requires a path ELEMENT, not a string "d" attribute
		var coordVal = x;
		var coordGiven = "x";
		var coordReqd = "y";
		if (isNil(coordVal)){
			coordVal = y;
			coordGiven = "y";
			coordReqd = "x";
		}

		var pathLength = svgPath.getTotalLength();
		var start = 0;
		var end = pathLength;
		var target = (start + end) / 2;

		// Ensure that x is within the range of the path
		coordVal = Math.max(coordVal, svgPath.getPointAtLength(0)[coordGiven]);
		coordVal = Math.min(coordVal, svgPath.getPointAtLength(pathLength)[coordGiven]);

		// Walk along the path using binary search to locate the point with the supplied value
		while (target >= start && target <= pathLength) {
			var pos = svgPath.getPointAtLength(target);
			// use a threshold instead of strict equality
			// to handle javascript floating point precision
			if (Math.abs(pos[coordGiven] - coordVal) < 0.001) {
				return pos[coordReqd];
			}
			else if (pos[coordGiven] > coordVal) {
				end = target;
			}
			else {
				start = target;
			}
			target = (start + end) / 2;
		}
	};

	var getPoint = function(point, path, interpolation, tangents, upright) {

		var p, h, t, h00, h10, h01, h11;
		var vertical = upright || false;
		var yPos = null,
			xPos = null;

		var xl = path.commands[0].to[0];
		var yl = path.commands[0].to[1];
		var ml = tangents[0];
		var xu = path.commands[path.commands.length - 1].to[0];
		var yu = path.commands[path.commands.length - 1].to[1];
		var mu = tangents[path.commands.length - 1];

		//unless the path is reversed:
		if ((!vertical && (xl > xu)) || (vertical && (yl > yu))) {
			xl = path.commands[path.commands.length - 1].to[0];
			yl = path.commands[path.commands.length - 1].to[1];
			ml = tangents[path.commands.length - 1];
			xu = path.commands[0].to[0];
			yu = path.commands[0].to[1];
			mu = tangents[0];
		}

		if (vertical) {
			//find the y-positions:
			for (p = 0; p < path.commands.length; p++) {
				if (point === path.commands[p].to[1]) {
					return path.commands[p].to[0];
				}
				if (path.commands[p].to[1] < point && (yl === null || path.commands[p].to[1] > yl)) {
					xl = path.commands[p].to[0];
					yl = path.commands[p].to[1];
					ml = tangents[p];
				}
				if (path.commands[p].to[1] > point && (yu === null || path.commands[p].to[1] < yu)) {
					xu = path.commands[p].to[0];
					yu = path.commands[p].to[1];
					mu = tangents[p];
				}
			}
			h = yu - yl;
			t = (point - yl) / h;
			if (yu === yl) {
				t = 0;
			}

			if (interpolation === "monotone") {
				h00 = (2 * (t * t * t)) - (3 * (t * t)) + 1;
				h10 = (t * t * t) - (2 * (t * t)) + t;
				h01 = (-2 * (t * t * t)) + (3 * (t * t));
				h11 = (t * t * t) - (t * t);
				xPos = ((xl * h00) + (h * ml * h10)) + (xu * h01) + (h * mu * h11);
			} else { //linear
				xPos = xl + ((xu - xl) * ((point - yl) / (yu - yl)));
			}
			return xPos;
		} else {
			//find the x-positions:
			for (p = 0; p < path.commands.length; p++) {
				if (point === path.commands[p].to[0]) {
					return path.commands[p].to[1];
				}
				if (path.commands[p].to[0] < point && (xl === null || path.commands[p].to[0] > xl)) {
					xl = path.commands[p].to[0];
					yl = path.commands[p].to[1];
					ml = tangents[p];
				}
				if (path.commands[p].to[0] > point && (xu === null || path.commands[p].to[0] < xu)) {
					xu = path.commands[p].to[0];
					yu = path.commands[p].to[1];
					mu = tangents[p];
				}
			}
			h = xu - xl;
			t = (point - xl) / h;
			if (h === 0) {
				t = 0;
			}

			if (interpolation === "monotone") {
				h00 = (2 * (t * t * t)) - (3 * (t * t)) + 1;
				h10 = (t * t * t) - (2 * (t * t)) + t;
				h01 = (-2 * (t * t * t)) + (3 * (t * t));
				h11 = (t * t * t) - (t * t);

				yPos = ((yl * h00) + (h * ml * h10)) + (yu * h01) + (h * mu * h11);
			} else { //linear
				yPos = yl + ((yu - yl) * ((point - xl) / (xu - xl)));
			}
			if (isNaN(yPos)) {
				if (point < path.commands[0].to[0]) {
					yPos = path.commands[0].to[1];
				} else {
					yPos = path.commands[path.commands.length - 1].to[1];
				}
			}
			return yPos;
		}

	};

	// Hermite spline construction; generates "C" commands.
	var lineHermite = function(points, tangents) {
		if (tangents.length < 1 || (points.length !== tangents.length && points.length - 1 !== tangents.length / 2)) {
			return lineLinear(points);
		}

		var hComds = eve.addPath(null, false);
		var v;
		var tCount = 0;
		for (v = 1; v < points.length; v++) {
			hComds.addCommand("cubic", [points[v][0], points[v][1]], [tangents[tCount][0], tangents[tCount][1]], [tangents[tCount + 1][0], tangents[tCount + 1][1]]);
			tCount += 2;
		}
		return hComds;
	};

	// Compute three-point differences for the given points.
	// http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Finite_difference
	var lineFiniteDifferences = function(points) {
		var i = -1,
			m = [],
			d = [];
		for (i = 0; i < points.length - 1; i++) {
			d.push(eve.setDecimal(lineSlope(points[i], points[i + 1]), 3));
		}
		for (i = 0; i < points.length; i++) {
			if (i === 0) {
				m[i] = d[i];
			} else {
				//if d[i-1] and d[i] have different signs, tangent m is 0.
				//d3 doesn't do this, although the Wikipedia article suggests it, and my curves do seem
				//more natural than d3's. If things start to look weird though, maybe take it out:
				if (!eve.sameSign(d[i - 1], d[i])) {
					m[i] = 0;
				} else {
					m[i] = eve.setDecimal((d[i - 1] + d[i]) / 2, 3);
				}
			}
		}
		m[points.length - 1] = d[d.length - 1];
		//console.log("m="+m);
		return m;
	};

	// Interpolates the given points using Fritsch-Carlson Monotone cubic Hermite
	// interpolation. Returns an array of tangent vectors. For details, see
	// http://en.wikipedia.org/wiki/Monotone_cubic_interpolation

	var lineMonotoneTangents = function(points, direction) {
		var tangents = [],
			d, a, b, s, i = -1,
			j = points.length - 1;
		var m = lineFiniteDifferences(points);

		// The first two steps are done by computing finite-differences:
		// 1. Compute the slopes of the secant lines between successive points.
		// 2. Initialize the tangents at every point as the average of the secants.

		// Then, for each segment…
		while (++i < j) {
			d = lineSlope(points[i], points[i + 1]);


			// 3. If two successive yk = y{k + 1} are equal (i.e., d is zero), then set
			// mk = m{k + 1} = 0 as the spline connecting these points must be flat to
			// preserve monotonicity. Ignore step 4 and 5 for those k.

			if (Math.abs(d) < Math.EPSILON) {
				m[i] = m[i + 1] = 0;
			} else {
				// 4. Let ak = mk / dk and bk = m{k + 1} / dk.
				a = m[i] / d;
				b = m[i + 1] / d;

				// 5. Prevent overshoot and ensure monotonicity by restricting the
				// magnitude of vector <ak, bk> to a circle of radius 3.
				s = a * a + b * b;
				if (s > 9) {
					s = d * 3 / Math.sqrt(s);
					m[i] = s * a;
					m[i + 1] = s * b;
				}
			}
		}

		// Compute the normalised tangent vector from the slopes. Note that if x is
		// not monotonic, it's possible that the slope will be infinite, so we protect
		// against NaN by setting the coordinate to zero.
		i = -1;
		while (++i <= j) {

			// According to https://en.wikipedia.org/wiki/Cubic_Hermite_spline#Representations
			// "you can express cubic Hermite interpolation in terms of cubic Bézier curves
			// with respect to the four values p0, p0 + m0 / 3, p1 - m1 / 3, p1".

			if (i < j) {
				if (direction === "up" || direction === "down") {
					var dy = points[i + 1][1] - points[i][1];
					var tX0 = points[i][0] + ((dy / 3) / m[i]);
					if (m[i] === 0) {
						tX0 = points[i][0];
					}
					var tX1 = points[i + 1][0] - ((dy / 3) / m[i + 1]);
					if (m[i + 1] === 0) {
						tX1 = points[i + 1][0];
					}

					tangents.push([tX0, points[i][1] + (dy / 3)]);
					tangents.push([tX1, points[i + 1][1] - (dy / 3)]);
				} else {
					var dx = points[i + 1][0] - points[i][0];
					//this still isn't quite right:
					//either we live with it, or
					//a) try and write a function based on the interpolated values to approximate the curve more closely, or
					//b) use this version of the curve and get our data values from it using Mike Bostock's "nearest point on path" method:
					tangents.push([points[i][0] + (dx / 3), points[i][1] + (m[i] * (dx / 3))]);
					tangents.push([points[i + 1][0] - (dx / 3), points[i + 1][1] - (m[i + 1] * (dx / 3))]);
				}
			}
		}
		return tangents;
	};

	//interpolations:
	//"linear", "linear-closed", "step", "step-before", "step-after", "basis", "basis-open", "basis-closed", "bundle", "cardinal", "cardinal-open", "cardinal-closed", "monotone"

	var setDirection = function(points) {
		var retVal = null;
		if (!isNil(points)) {
			var prevX = null;
			var prevY = null;
			var gapX = null;
			var gapY = null;
			var equalX = true;
			var equalY = true;
			var prevIncX = null;
			var prevIncY = null;
			var constantX = true;
			var constantY = true;
			var upX = true;
			var upY = true;
			for (var p = 0; p < points.length; p++) {
				if (p === 0) {
					prevX = points[p][0];
					prevY = points[p][1];
				} else {
					upX = (points[p][0] - prevX) > 1;
					upY = (points[p][1] - prevY) > 1;

					if (!isNil(gapX) && eve.setDecimal(points[p][0] - prevX, 2) !== gapX) {
						equalX = false;
					}
					gapX = eve.setDecimal(points[p][0] - prevX, 2);

					if (!isNil(gapY) && eve.setDecimal(points[p][1] - prevY, 2) !== gapY) {
						equalY = false;
					}
					gapY = eve.setDecimal(points[p][1] - prevY, 2);

					if (!isNil(prevIncX)) {
						if ((points[p][0] - prevX) > 1 !== prevIncX) {
							constantX = false;
						}
					}
					prevIncX = (points[p][0] - prevY) > 1;

					if (!isNil(prevIncY)) {
						if ((points[p][1] - prevY) > 1 !== prevIncY) {
							constantY = false;
						}
					}
					prevIncY = (points[p][1] - prevY) > 1;

					prevX = points[p][0];
					prevY = points[p][1];
				}
			}

			if (equalX) {
				if (upX) {
					retVal = "right";
				} else {
					retVal = "left";
				}
			} else if (equalY) {
				if (upY) {
					retVal = "down";
				} else {
					retVal = "up";
				}
			} else {
				if (constantX) {
					if (upX) {
						retVal = "right";
					} else {
						retVal = "left";
					}
				} else if (constantY) {
					if (upY) {
						retVal = "down";
					} else {
						retVal = "up";
					}
				}
			}
		}
		return retVal;
	};

	eve.makePath = function(points, interpolation, closed, direction) {
		var p, pClosed = closed || false, newPath = null;
		if (!isNil(points)) {
			newPath = eve.addPath(null, pClosed);
			if (interpolation === "monotone") {
				newPath = lineHermite(points, lineMonotoneTangents(points, direction));
				newPath.addCommand("moveTo", points[0], null, null, null, null, null, null, null, null, null, true);
			} else {
				newPath.addCommand("moveTo", points[0]);
				for (p = 1; p < points.length; p++) {
					newPath.addCommand("lineTo", points[p]);
				}
			}
		}
		return newPath;
	};

	//checks if property is null or undefined or "auto" or "default"
	var isDefault = function(p) {
		if (p === null || p === undefined || strIs(p, "auto") || strIs(p, "default")) {
			return true;
		} else {
			return false;
		}
	};

	//function to check if var is a numeric value
	eve.isNumeric = function(n) {
		if (typeof n === "string"){
			if (/\d/.test(n)){
				var nUnits = n.replace(parseFloat(n).toString(), "");
				return !/\d/.test(nUnits);
			}
			else{
				return false;
			}
		}
		else{
			return !isNaN(parseFloat(n)) && isFinite(parseFloat(n));
		}
	};

	eve.tickStep = function(start, stop, count) {
		var simpleSteps = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25];
		if (simpleSteps.indexOf(Math.abs(start - stop)) !== -1) {
			return 1;
		}
		var e10 = Math.sqrt(50);
		var e5 = Math.sqrt(10);
		var e2 = Math.sqrt(2);
		var step0 = Math.abs(stop - start) / Math.max(0, count),
			step1 = Math.pow(10, Math.floor(Math.log(step0) / Math.LN10)),
			error = step0 / step1;
		if (error >= e10) {
			step1 *= 10;
		} else if (error >= e5) {
			step1 *= 5;
		} else if (error >= e2) {
			step1 *= 2;
		}
		return stop < start ? -step1 : step1;
	};

	//return an array of property names from passed object
	var getPropNames = function(obj) {
		var retArr = [];
		var e;
		var prop;
		if (eve.isArray(obj)) {
			for (e = 0; e < obj.length; e++) {
				for (prop in obj[e]) {
					if (obj[e].hasOwnProperty(prop)) {
						if (retArr.indexOf(prop) === -1) {
							retArr.push(prop);
						}
					}
				}
			}
		} else {
			for (prop in obj) {
				if (obj.hasOwnProperty(prop)) {
					retArr.push(prop);
				}
			}
		}
		return retArr;
	};

	//return true if all values of property are numeric
	var propIsNumeric = function(obj, propName) {
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop)) {
				if (!eve.isNumeric(obj[propName])) {
					return false;
				}
			}
		}
		return true;
	};

	//remove clip-path attribute from SVG element:
	var removeClipPath = function(elm) {
		elm.removeAttribute('clip-path');
	};

	var removeNode = function(node) {
		if (node.parentNode) {
			//clear the listeners by cloning the node first:
			var clone = node.cloneNode(true);
			node.parentNode.replaceChild(clone, node);
			clone.parentNode.removeChild(clone);
		}
	};

	var fadeRemove = function(node, secs, delay, owner) {
		eve.tweenTo(node, secs, delay, null, null, {opacity: 0, onComplete: removeNode, onCompleteParams: [node]}, owner);
	};

	function getOffset(elm) {
		var at = elm.getBoundingClientRect();
		return {left: at.left + window.scrollX, top: at.top + window.scrollY};
	}

	//get width and height style attributes of an element:
	var getCssDims = function(elm) {
		var cn = elm.cloneNode();
		var div = document.createElement('div');
		div.appendChild(cn);
		div.style.display = 'none';
		document.body.appendChild(div);
		var cs = window.getComputedStyle ? getComputedStyle(cn, null) : cn.currentStyle;
		var ret = {width: cs.width, height: cs.height, left: cs.left, top: cs.top};
		document.body.removeChild(div);
		return ret;
	};

	var getElmPos = function(elm){
		var rect = elm.getBoundingClientRect(),
		scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
		scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		return {top: rect.top + scrollTop, left: rect.left + scrollLeft};
	};

	var splitAtBreaks = function (str){
		str = str.toString().replace(/<br \/>/g, '<br>').replace(/<br\/>/g, '<br>').replace(/<BR \/>/g, '<br>').replace(/<BR\/>/g, '<br>');
		return str.split("<br>");
	};

	var textSize = function(str, tFontFamily, tFontSize, tFontBold, tFontItalic, leading, drawMode, container, dftFontNormal, dftFontBold) {
		if (typeof container === "string"){
			container = get(container);
		}
		var strs = splitAtBreaks(str);
		if (isNil(leading)){
			leading = leading || parseFloat(tFontSize) * 1.45;
		}
		var tW = 0, tH = 0;
		for (var tm = 0; tm < strs.length; tm++){
			var mSize = textLineSize(str, tFontFamily, tFontSize, tFontBold, tFontItalic, drawMode, container, dftFontNormal, dftFontBold);
			if (tW < mSize.width){
				tW = mSize.width;
			}
			tH += mSize.height;
			if (tm < strs.length - 1){
				tH += leading;
			}
		}
		return {width: tW, height: tH};
	};

	var pdfTextHeight = function(str, container, fontSize){
		if (isNil(str) || str === ""){
			return 0;
		}
		if (isNil(container._font.layoutCache[str])){
			return container.heightOfString(str);
		}
		var glyphs = container._font.layoutCache[str].glyphs;
		var height = 0;
		for (var g = 0; g < glyphs.length; g++){
			var bb = glyphs[g]._getBBox();
			var h = bb.maxY - bb.minY;
			if (h > height){
				height = h;
			}
		}
		return (height / 1000.0) * fontSize;
	};

	var textLineSize = function(str, tFontFamily, tFontSize, tFontBold, tFontItalic, drawMode, container, dftFontNormal, dftFontBold) {

		var rWidth = 0, rFontSize = tFontSize, fUnits = "px";
		str = str.toString();

		tFontBold = !!tFontBold;
		tFontItalic = !!tFontItalic;
		tFontFamily = tFontFamily || "inherit";

		if (typeof container === "string"){
			container = get(container);
		}

		if (drawMode === "PDF"){
//			container.save();
			if (!isNil(container._fontFamilies[tFontFamily])){
				container.font(tFontFamily);
			}
			if (tFontFamily === "inherit"){
				if (!tFontBold && !isNil(dftFontNormal)){
					container.font(dftFontNormal);
				}
				else if (tFontBold && !isNil(dftFontBold)){
					container.font(dftFontBold);
				}
			}
			container.fontSize(tFontSize);
			rWidth = container.widthOfString(str);
			var strContiguous = str.replace(/\s/g,'').replace(/<br>/g,'');
			var rHeight = container.heightOfString(strContiguous);
			rHeight = pdfTextHeight(strContiguous, container, parseFloat(rFontSize));
			// console.log(str + " | " + rHeight);
//			container.restore();
			return {width: rWidth, height: rHeight};
		}
		else{

			if (container instanceof Element && tFontFamily === "inherit"){
				tFontFamily = getComputedStyle(container).fontFamily;
			}

			if (tFontSize) {
				fUnits = eve.getUnits(tFontSize) || "px";
			}
			tFontSize = parseFloat(tFontSize) || 12;

			var fString = (tFontSize + fUnits) + " " + tFontFamily;
			if (tFontBold) {
				fString = "bold " + fString;
			}
			if (tFontItalic) {
				fString = "italic" + fString;
			}

			var canvas = textSize.canvas || (textSize.canvas = document.createElement("canvas"));
			canvas.width = 600;
			canvas.height = tFontSize * 3;
			var cWidth = str.length * rFontSize * 2;
			if (canvas.width < cWidth){
				canvas.width = cWidth;
			}

			var context = canvas.getContext("2d");
			context.font = fString;
			context.textBaseline = "top";
			context.fillStyle = "white";
			context.fill();
			context.fillStyle = "black";
			fString = (tFontSize + fUnits) + " " + context.font.toString().substr(context.font.split(" ")[0].length + 1);
			context.font = fString;
			context.fillText(str, 0, 0);

			var metrics = context.measureText(str);

			var pixels = context.getImageData(0, 0, canvas.width, canvas.height).data;
			var first = null, last = null, r = canvas.height, c = 0;

			// Find the last line with a non-white pixel
			while(last === null && r > 0) {
				r--;
				for(c = 0; c < canvas.width; c++) {
					if(pixels[r * canvas.width * 4 + (c * 4 + 3)]) {
						last = r;
						break;
					}
				}
			}
			// Find the first line with a non-white pixel
			r = -1;
			while(r < canvas.height && first === null) {
				r++;
				for(c = 0; c < canvas.width; c++) {
					if(pixels[r * canvas.width * 4 + c * 4 + 3]) {
						first = r;
						break;
					}
				}
			}
			return {width: metrics.width, height: last - first + 1};
		}

	};

	function addToEventList(list, functionName, params) {
		var itsThere = false;
		var cIndex = -1;
		for (var e = 0; e < list.length; e++) {
			if (list[e].functionName === functionName) {
				if (list[e].added) {
					itsThere = true;
					cIndex = e;
				}
			}
		}
		if (!itsThere) {
			list.push({functionName: functionName, params: params, added: false});
		} else {
			list[cIndex].params = params;
		}
	}

	//get the nth member of an array, even if n is bigger than the array length
	//imagine the array repeats over and over until it's long enough
	Array.prototype.getNth = function(n) {
		return this[n - (Math.floor(n / this.length) * this.length)];
	};

	//add indexOf functionality to arrays for Internet Explorer 7 and 8
	if (!Array.prototype.indexOf) {
		Array.prototype.indexOf = function(vMember, nStartFrom) {
			if (this === null) {
				throw new TypeError("Array.prototype.indexOf() - can't convert `" + this + "` to object");
			}
			var nIdx = isFinite(nStartFrom) ? Math.floor(nStartFrom) : 0,
				oThis = this instanceof Object ? this : new Object(this),
				nLen = isFinite(oThis.length) ? Math.floor(oThis.length) : 0;
			if (nIdx >= nLen) {
				return -1;
			}
			if (nIdx < 0) {
				nIdx = Math.max(nLen + nIdx, 0);
			}
			if (vMember === undefined) {
				do {
					if (nIdx in oThis && oThis[nIdx] === undefined) {
						return nIdx;
					}
				} while (++nIdx < nLen);
			} else {
				do {
					if (oThis[nIdx] === vMember) {
						return nIdx;
					}
				} while (++nIdx < nLen);
			}
			return -1;
		};
	}

	//returns index of given property
	Array.prototype.indexOfProp = function(prop, val) {
		var ret = -1;
		for (var i = 0; i < this.length; ++i) {
			if (this[i].hasOwnProperty(prop)) {
				if (this[i][prop] === val) {
					ret = i;
					break;
				}
			}
		}
		return ret;
	};

	//returns a NEW array with duplicate members removed
	Array.prototype.unique = function(prop) {
		var a = this.concat();
		for (var i = 0; i < a.length; ++i) {
			for (var j = i + 1; j < a.length; ++j) {
				if (!isNil(prop)) {
					if (a[i].hasOwnProperty(prop) && a[j].hasOwnProperty(prop)) {
						if (a[i][prop] === a[j][prop]) {
							a.splice(j--, 1);
						}
					}
				} else {
					if (eve.isEqual(a[i], a[j])) {
						a.splice(j--, 1);
					}
				}
			}
		}
		return a;
	};

	//returns an array with passed element removed (element is passed by value, not index)
	Array.prototype.remove = function() {
		var what, a = arguments,
			L = a.length,
			ax;
		while (L && this.length) {
			what = a[--L];
			while ((ax = this.indexOf(what)) !== -1) {
				this.splice(ax, 1);
			}
		}
		return this;
	};

	//returns a copy of an array of arrays, keeping the original unaffected:
	Array.prototype.cloneOneLevel = function() {
		var arrNew = [];
		for (var i = 0; i < this.length; i++) {
			arrNew[i] = this[i].slice(0);
		}
		return arrNew;
	};

	//returns a copy of the array, keeping the original unaffected:
	Array.prototype.clone = function(goDeep) {
		if (goDeep) {
			var arrNew = [];
			for (var i = 0; i < this.length; i++) {
				if (eve.isArray(this[i])) {
					arrNew.push(this[i].clone(true));
				}
				else if(this[i] instanceof eve.linearGradient){
					arrNew.push(this[i].clone());
				}
				else if(this[i] instanceof eve.parallelHatch){
					arrNew.push(this[i].clone());
				}
				else if (typeof this[i] === "object" && !(this[i] instanceof eve.series)) {
					var newObj = {};
					for (var prop in this[i]) {
						if (this[i].hasOwnProperty(prop)) {
							newObj[prop] = this[i][prop];
						}
					}
					arrNew.push(newObj);
				} else if (this[i] instanceof eve.series) {
					arrNew.push(cloneObj(this[i]));
				} else {
					arrNew.push(this[i]);
				}
			}
			return arrNew;
		}
		return this.slice(0);
	};

	//returns the closest number to that sent
	Array.prototype.closest = function(n) {
		var closest = null;
		var ret = null;
		var dist = 0;
		for (var i = 0; i < this.length; i++) {
			dist = Math.abs(this[i] - n);
			if (closest === null || dist < closest) {
				closest = dist;
				ret = this[i];
			}
		}
		return ret;
	};

	function indexByPropertyValue(arr, prop, val) {
		for (var i = 0; i < arr.length; i += 1) {
			if (arr[i][prop] === val) {
				return i;
			}
		}
		return -1;
	}

	//convert a string to "proper" or "title" case
	String.prototype.toProperCase = function() {
		return this.replace(/\w\S*/g, function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
	};

	if (typeof String.prototype.endsWith !== 'function') {
		String.prototype.endsWith = function(suffix) {
			return this.indexOf(suffix, this.length - suffix.length) !== -1;
		};
	}

	var colourNames = {aliceblue: "#f0f8ff", antiquewhite: "#faebd7", aqua: "#00ffff", aquamarine: "#7fffd4", azure: "#f0ffff", beige: "#f5f5dc", bisque: "#ffe4c4", black: "#000000", blanchedalmond: "#ffebcd", blue: "#0000ff", blueviolet: "#8a2be2", brown: "#a52a2a", burlywood: "#deb887", cadetblue: "#5f9ea0", chartreuse: "#7fff00", chocolate: "#d2691e", coral: "#ff7f50", cornflowerblue: "#6495ed", cornsilk: "#fff8dc", crimson: "#dc143c", cyan: "#00ffff", darkblue: "#00008b", darkcyan: "#008b8b", darkgoldenrod: "#b8860b", darkgray: "#a9a9a9", darkgreen: "#006400", darkkhaki: "#bdb76b", darkmagenta: "#8b008b", darkolivegreen: "#556b2f", darkorange: "#ff8c00", darkorchid: "#9932cc", darkred: "#8b0000", darksalmon: "#e9967a", darkseagreen: "#8fbc8f", darkslateblue: "#483d8b", darkslategray: "#2f4f4f", darkturquoise: "#00ced1", darkviolet: "#9400d3", deeppink: "#ff1493", deepskyblue: "#00bfff", dimgray: "#696969", dodgerblue: "#1e90ff", firebrick: "#b22222", floralwhite: "#fffaf0", forestgreen: "#228b22", fuchsia: "#ff00ff", gainsboro: "#dcdcdc", ghostwhite: "#f8f8ff", gold: "#ffd700", goldenrod: "#daa520", gray: "#808080", green: "#008000", greenyellow: "#adff2f", honeydew: "#f0fff0", hotpink: "#ff69b4", indianred: "#cd5c5c", indigo: "#4b0082", ivory: "#fffff0", khaki: "#f0e68c", lavender: "#e6e6fa", lavenderblush: "#fff0f5", lawngreen: "#7cfc00", lemonchiffon: "#fffacd", lightblue: "#add8e6", lightcoral: "#f08080", lightcyan: "#e0ffff", lightgoldenrodyellow: "#fafad2", lightgrey: "#d3d3d3", lightgreen: "#90ee90", lightpink: "#ffb6c1", lightsalmon: "#ffa07a", lightseagreen: "#20b2aa", lightskyblue: "#87cefa", lightslategray: "#778899", lightsteelblue: "#b0c4de", lightyellow: "#ffffe0", lime: "#00ff00", limegreen: "#32cd32", linen: "#faf0e6", magenta: "#ff00ff", maroon: "#800000", mediumaquamarine: "#66cdaa", mediumblue: "#0000cd", mediumorchid: "#ba55d3", mediumpurple: "#9370d8", mediumseagreen: "#3cb371", mediumslateblue: "#7b68ee", mediumspringgreen: "#00fa9a", mediumturquoise: "#48d1cc", mediumvioletred: "#c71585", midnightblue: "#191970", mintcream: "#f5fffa", mistyrose: "#ffe4e1", moccasin: "#ffe4b5", navajowhite: "#ffdead", navy: "#000080", oldlace: "#fdf5e6", olive: "#808000", olivedrab: "#6b8e23", orange: "#ffa500", orangered: "#ff4500", orchid: "#da70d6", palegoldenrod: "#eee8aa", palegreen: "#98fb98", paleturquoise: "#afeeee", palevioletred: "#d87093", papayawhip: "#ffefd5", peachpuff: "#ffdab9", peru: "#cd853f", pink: "#ffc0cb", plum: "#dda0dd", powderblue: "#b0e0e6", purple: "#800080", rebeccapurple: "#663399", red: "#ff0000", rosybrown: "#bc8f8f", royalblue: "#4169e1", saddlebrown: "#8b4513", salmon: "#fa8072", sandybrown: "#f4a460", seagreen: "#2e8b57", seashell: "#fff5ee", sienna: "#a0522d", silver: "#c0c0c0", skyblue: "#87ceeb", slateblue: "#6a5acd", slategray: "#708090", snow: "#fffafa", springgreen: "#00ff7f", steelblue: "#4682b4", tan: "#d2b48c", teal: "#008080", thistle: "#d8bfd8", tomato: "#ff6347", turquoise: "#40e0d0", violet: "#ee82ee", wheat: "#f5deb3", white: "#ffffff", whitesmoke: "#f5f5f5", yellow: "#ffff00", yellowgreen: "#9acd32"};

	var svgns = "http://www.w3.org/2000/svg";

	eve.palette = [
		["#2a3a3f", "#951765", "#ce1517", "#ee791c", "#fcac0f", "#34c489", "#7cb828", "#1199d7", "#216fce", "#073f64", "#7a9baa", "#2f6f71"],
		["#2d2a95", "#70338f", "#8c2b62", "#b94729", "#f05d4d", "#f6861e", "#7f8d3c", "#87c33f", "#6f8d8f", "#15bedb", "#1569db", "#394956"],
		["#015d8c", "#0088bc", "#00adbb", "#723b98", "#95469c", "#d0238f", "#ea6a23", "#f7901f", "#fec84a", "#8abe4f", "#3c8540", "#093f63"],
		["#6f9dad", "#6f71cb", "#327ca9", "#1e94bb", "#36b1af", "#35b288", "#8db542", "#ecc319", "#f59c1a", "#ed5f2d", "#e04235", "#2d4b56"],
		["#fcbd3c", "#f78d41", "#e55143", "#c22d53", "#96235c", "#762c81", "#4c3b97", "#11578b", "#328ca9", "#37b9d3", "#57bc82", "#aacc3a"],
		["#046880", "#0a8ab4", "#35bfc9", "#054d73", "#44758a", "#363a39", "#6e7474", "#999a9c", "#7cb099", "#4b6b32", "#6aad43", "#8cd95f"]
	];

	eve.ease = function(t, b, c, d, type, path) {
		//t = time so far, b=beginning value, c=change (final value - beginning value), d=duration total
		var ep;
		var ea;
		var es;
		if (type === "custom") {
			if (isNil(path)){
				return c * t / d + b;
			}
			else{
				return eve.getPointOnPath(path, t / d) * c + b;
			}
		}
		if (type === "linear") { //easeNone
			return c * t / d + b;
		}
		if (type === "gravity"){
			return Math.min((c * (Math.pow(Math.sqrt(d) * (t / d), 2) / d)) + b, b + c);
		}
		if (type === "gravityBounce"){
			if (isNil(path)){
				path = eve.svgPathToEvePath("M0,600 C233,600 391,147 448,0 C475,75 530,75 553,0 C565,18 587,18 600,0");
				path.unitise();

			}
			return  eve.getPointOnPath(path, t / d) * c + b;
		}
		if (type === "easeIn") {
			return (t === 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
		}
		if (type === "easeOut") {
			t /= d;
			return -c * t * (t - 2) + b;
		}
		if (type === "easeOutIn") {
			if (isNil(path)){
				path = eve.svgPathToEvePath("M0,600 C60,480 165,300 300,300 S560,350 600,0");
				path.unitise();
			}
			return  eve.getPointOnPath(path, t / d) * c + b;
		}
		if (type === "easeOutCubic") {
			t /= d;
			t--;
			return c * (t * t * t + 1) + b;
		}
		if (type === "easeInOut") {
			if (t === 0) {
				return b;
			}
			if (t === d) {
				return b + c;
			}
			if ((t /= d / 2) < 1) {
				return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
			}
			return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
		}
		if (type === "bounce") {
			//0.363636363636
			if ((t /= d) < (1 / 2.75)) {
				return c * (7.5625 * t * t) + b;
			//0.72727272727272
			} else if (t < (2 / 2.75)) {
				return c * (7.5625 * (t -= (1.5 / 2.75)) * t + 0.75) + b;
			//0.909090909090909
			} else if (t < (2.5 / 2.75)) {
				return c * (7.5625 * (t -= (2.25 / 2.75)) * t + 0.9375) + b;
			} else {
				return c * (7.5625 * (t -= (2.625 / 2.75)) * t + 0.984375) + b;
			}
		}
		if (type === "easeInElastic") {
			es = 1.70158;
			ep = 0;
			ea = c;
			if (t === 0){
				return b;
			}
			if ((t /= d) === 1){
				return b + c;
			}
			if (!ep){
				ep = d * 0.3;
			}
			if (ea < Math.abs(c)) {
				ea = c;
				es = ep/4;
			}
			else{
				es = ep / ( 2 * Math.PI) * Math.asin (c / ea);
			}
			return -(ea * Math.pow(2, 10 * (t -= 1)) * Math.sin( (t * d - es) * (2 * Math.PI) / ep)) + b;
		}
		if (type === "easeOutElastic") {
			if (t === 0) {
				return b;
			}
			if ((t /= d) === 1) {
				return b + c;
			}
			ep = d * 0.5;
			ea = c;
			es = ep / 4;
			return ea * Math.pow(2, -10 * t) * Math.sin((t * d - es) * (2 * Math.PI) / ep) + c + b;
		}

		if (type === "easeInOutElastic") {
			if (t === 0) {
				return b;
			}
			if ((t /= d / 2) === 2) {
				return b + c;
			}
			ep = d * (0.3 * 1.5);
			ea = c;
			es = ep / 4;
			if (t < 1) {
				return -0.5 * (ea * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - es) * (2 * Math.PI) / ep)) + b;
			}
			return ea * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - es) * (2 * Math.PI) / ep) * 0.5 + c + b;
		}
	};

	var needsUnits = ["drawSVG", "x", "y", "left", "top", "right", "bottom", "width", "height"];
	var transforms = ["x", "y", "rotation", "scale", "scaleX", "scaleY"];

	eve.growArray = function(arr, dim, interpolate) {
		//resize an array and add new members by interpolating original values:
		var newArr = [];
		var step = (dim - 1) / (arr.length - 1);

		for (var m = 0; m < arr.length; m++) {
			newArr[Math.round(m * step)] = arr[m];
		}
		newArr[0] = arr[0];
		newArr[dim - 1] = arr[arr.length - 1];
		for (var n = 0; n < dim; n++) {
			var nextVal = null;
			var lookStep = 0;
			if (isNil(newArr[n])) {
				while (isNil(nextVal)) {
					lookStep++;
					nextVal = newArr[n + lookStep];
				}
				if (nextVal !== null && typeof nextVal === 'object') {
					if (interpolate) {
						newArr[n] = {};
						for (var prop in nextVal) {
							if (nextVal.hasOwnProperty(prop)) {
								newArr[n][prop] = newArr[n - 1][prop] + ((nextVal[prop] - newArr[n - 1][prop]) / (lookStep + 1));
							}
						}
					} else {
						newArr[n] = nextVal;
					}
				} else {
					newArr[n] = newArr[n - 1] + ((nextVal - newArr[n - 1]) / (lookStep + 1));
				}
			}
		}
		return newArr;
	};

	eve.growPoints = function(points, dim, path) {
		var newPoints = [];
		var step = (dim - 1) / (points.length - 1);

		for (var m = 0; m < points.length; m++) {
			newPoints[Math.round(m * step)] = points[m];
		}
		newPoints[0] = points[0];
		newPoints[dim - 1] = points[points.length - 1];
		for (var n = 0; n < dim; n++) {
			var nextVal = null;
			var lookStep = 0;
			if (isNil(newPoints[n])) {
				while (isNil(nextVal)) {
					lookStep++;
					nextVal = newPoints[n + lookStep];
				}
				if (nextVal !== null) {
					var segment = eve.getSvgPathSegment(path, newPoints[n - 1], nextVal, 1);
					newPoints[n] = path.getPointAtLength(segment.start + ((segment.end - segment.start) / (lookStep + 1)));
				}
			}
		}
		return newPoints;
	};

	var getSvgElmPosn = function(elm) {
		var getPos = function(elm) {
			var posn = {x: 0, y: 0};
			if (elm.parentNode instanceof SVGElement && elm.parentNode.tagName !== "svg") {
				posn = getPos(elm.parentNode);
			}
			if (elm.tagName === "circle") {
				posn.x += parseFloat(elm.getAttribute("cx"));
				posn.y += parseFloat(elm.getAttribute("cy"));
			} else {
				var et = eve.getSvgTransforms(elm);
				posn.x = et.x;
				posn.y = et.y;
			}
			return posn;
		};
		return getPos(elm);
	};

	var getSvgProperty = function(elm, attr) { //SVG elements inherit styles from their parents, so we may need to iterate up to get them for tweening:
		var obj = elm;
		var ret = null;

		if (attr === "path") {
			ret = eve.svgPathToEvePath(obj.getAttribute("d"));
		} else if (attr === "sector") {
			ret = elm.sector;
		} else {
			while ((isNil(ret) || ret === "") && !isNil(obj) && !isNil(obj.style)) {
				//try an actual property first,
				//then the SVG attribute,
				//then finally the CSS style
				if (!isNil(obj[attr])) {
					ret = obj[attr].baseVal.value;
				}
				if (isNil(ret) || ret === "") {
					ret = obj.getAttribute[attr];
				}
				if (isNil(ret) || ret === "") {
					ret = obj.style[attr];
				}
				ret = eve.makeNum(ret);
				obj = obj.parentNode;
			}
		}
		return ret;
	};

	var getCssTransform = function(elm) {

		var elmStyle = window.getComputedStyle(elm);
		var elmMatrix = elmStyle.getPropertyValue("-webkit-transform") ||
			elmStyle.getPropertyValue("-moz-transform") ||
			elmStyle.getPropertyValue("-ms-transform") ||
			elmStyle.getPropertyValue("-o-transform") ||
			elmStyle.getPropertyValue("transform");

		// this happens when there was no rotation yet in CSS
		if (elmMatrix === 'none') {
			elmMatrix = 'matrix(0,0,0,0,0,0)';
		}
		var obj = {};
		var val = elmMatrix.split('(')[1].split(')')[0].split(',');

		var scaleX = parseFloat(val[0].split(' ').join(''));
		var skewX = parseFloat(val[1].split(' ').join(''));
//		var skewY = parseFloat(val[2].split(' ').join(''));
		var scaleY = parseFloat(val[3].split(' ').join(''));
		var translateX = parseFloat(val[4].split(' ').join(''));
		var translateY = parseFloat(val[5].split(' ').join(''));

		obj.rotate = (Math.round(Math.atan2(parseFloat(skewX), parseFloat(scaleX)) * (180 / Math.PI)) || 0).toString() + 'deg';
		obj.scale = Math.sqrt(scaleX * scaleX + skewX * skewX);
		obj.scaleX = scaleX;
		obj.scaleY = scaleY;
		obj.translateX = translateX + "px";
		obj.translateY = translateY + "px";

		return obj;

	};

	var getTweenProps = function(tween) {
		var attr = null;
		var startVal = null;
		var endVal = null;
		var preEndVal = null;
		var props = [];
		var tPropCount;

		for (tPropCount = 0; tPropCount < Object.keys(tween.endProps).length; tPropCount++) {
			var prop = Object.keys(tween.endProps)[tPropCount];
			var units = null;
			if (tween.endProps.hasOwnProperty(prop)) {
				attr = prop;
				if (prop === "onComplete") {
					tween.endFunction = tween.endProps[prop];
				} else if (prop === "onCompleteParams") {
					tween.endFunctionParams = tween.endProps[prop];
				} else if (prop === "start") {
					tween.start = tween.endProps[prop];
				} else if (prop === "end") {
					tween.end = tween.endProps[prop];
				} else {
					startVal = 0;
					if (isNil(tween.startProps) || isNil(tween.startProps[prop])) {
						if (transforms.indexOf(attr) !== -1) {
							if (isSVG(tween.elm)) {
								if (attr === "x") {
									if (!isNil(tween.elm.oX)) {
										startVal = eve.makeNum(tween.elm.oX) || 0;
									} else {
										startVal = eve.makeNum(tween.elm.eX) || 0;
									}
								}
								if (attr === "y") {
									if (!isNil(tween.elm.oY)) {
										startVal = eve.makeNum(tween.elm.oY) || 0;
									} else {
										startVal = eve.makeNum(tween.elm.eY) || 0;
									}
								}
								if (attr === "scaleX") {
									startVal = eve.makeNum(tween.elm.eScaleX) || 1;
								}
								if (attr === "scaleY") {
									startVal = eve.makeNum(tween.elm.eScaleY) || 1;
								}
								if (attr === "rotation") {
									if (!isNil(tween.elm.oRot)) {
										startVal = eve.makeNum(tween.elm.oRot) || 0;
									} else {
										startVal = eve.makeNum(tween.elm.eRot) || 0;
									}
								}
							} else {
								var elmTransform = getCssTransform(tween.elm);
								if (attr === "x") {
									startVal = eve.makeNum(elmTransform.translateX);
								}
								if (attr === "y") {
									startVal = eve.makeNum(elmTransform.translateY);
								}
								if (attr === "scaleX") {
									startVal = eve.makeNum(elmTransform.scaleX);
								}
								if (attr === "scaleY") {
									startVal = eve.makeNum(elmTransform.scaleY);
								}
								if (attr === "rotation") {
									startVal = eve.makeNum(elmTransform.rotate);
								}
							}
						} else {
							//TODO: this caused  a bug because there was no "path" property - the series was a bar. Can we convert a rectangle to a path here? Hmmm, maybe not - that should be a node. Dunno
							if (isSVG(tween.elm)) {
								startVal = getSvgProperty(tween.elm, prop);
								if (prop === "stroke" && !isNil(tween.elm.sGradient)) {
									startVal = tween.elm.sGradient;
								}
								else if ((prop === "stroke" || prop === "fill") && tween.elm.style[prop].indexOf("eve_hatch") !== -1) {
									// tween.elm = get(tween.elm.style.fill.replace("url(\"#","").substr(0, tween.elm.style.fill.replace("url(\"#","").length - 2));
									// startVal = tween.elm.firstChild.style.stroke;
								}
								else if (prop === "width" && !isNil(tween.elm.oWidth)) {
									startVal = eve.makeNum(startVal = tween.elm.oWidth);
								} else if (prop === "height" && !isNil(tween.elm.oHeight)) {
									startVal = eve.makeNum(startVal = tween.elm.oHeight);
								} else if (prop === "sector") {
									if (!isNil(tween.elm.sector) && !isNil(tween.elm.oSector)) {
										startVal = tween.elm.oSector;
									} else {
										startVal = eve.svgPathToEveSector(tween.endProps[prop]);
									}
								} else if (prop === "path") {
									if (!isNil(tween.elm.path) && !isNil(tween.elm.oPath)) {
										startVal = tween.elm.oPath;
									} else {
										startVal = eve.svgPathToEvePath(tween.endProps[prop]);
									}
									if (!pathsAreMorphable(startVal, tween.elm.path) && startVal.commands.length < tween.elm.path.commands.length) {
										//add points to the start path
										startVal = makeMorphPath(startVal, tween.elm.path, tween.elm.interpolation);
									}
								}
							} else {
								startVal = eve.makeNum(tween.elm.style[prop], true);
								if (isNil(startVal)) {
									startVal = eve.makeNum(tween.endProps[prop]);
								}
							}
							if (prop === "opacity" && tween.elm.style[prop] === "") {
								if (window.getComputedStyle(tween.elm).opacity){
									startVal = window.getComputedStyle(tween.elm).opacity;
								}
								else{
									startVal = 1;
								}
							}
						}
					} else {
						startVal = eve.makeNum(tween.startProps[prop]);
					}

					if (prop === "path") {
						if (!isNil(tween.elm.path) && !isNil(tween.elm.oPath)) {
							endVal = tween.elm.path;
						} else {
							endVal = eve.svgPathToEvePath(tween.endProps[prop]);
						}
						if (!pathsAreMorphable(startVal, endVal) && startVal.commands.length > endVal.commands.length) {
							//create a "pre-end" target path
							preEndVal = makeMorphPath(startVal, endVal, tween.elm.interpolation);
						}
					} else if (prop === "sector") {
						if (!isNil(tween.elm.sector) && !isNil(tween.elm.oSector)) {
							endVal = tween.elm.sector;
						} else {
							endVal = eve.svgPathToEveSector(tween.endProps[prop]);
						}
					} else {
						if ((prop === "stroke" || prop === "fill") && tween.endProps[prop] instanceof eve.parallelHatch) {
							endVal = tween.endProps[prop].strokeColour;
						}
						else{
							endVal = eve.makeNum(tween.endProps[prop]);
						}
					}

					if (needsUnits.indexOf(attr) !== -1) {
						if (tween.endProps[prop].toString().indexOf("%") !== -1) {
							units = "%";
						} else {
							units = "px";
						}
					}
					else{
						units = "";
					}
					props.push({attr: attr, startVal: startVal, endVal: endVal, preEndVal: preEndVal, _units: units});
				}
			}
		}
		return props;
	};

	var isSVG = function(elm) {
		return elm instanceof SVGElement;
	};

	var tweenLoop = function(tween) {
		var t = Date.now() - tween.startTime;
		var val = [];
		var knot;
		var kc;
		var pc;
		var vc;

		if (!tween.cancelled) {

			for (pc = 0; pc < tween.props.length; pc++) {

				if (t < tween.duration * 1000.0) {
					var amount = eve.ease(t / 1000.0, 0, 1, tween.duration, tween.ease, tween.path);

					if (!isNil(tween.start) && !isNil(tween.end)) {
						var tweenDuration = tween.end - tween.start;
						if (amount < tween.start) {
							amount = 0;
						} else if (amount >= tween.end) {
							amount = 1;
						} else {
							amount = (amount - tween.start) / tweenDuration;
						}
					}
					var useEndVal = tween.props[pc].endVal;
					if (!isNil(tween.props[pc].preEndVal)) {
						useEndVal = tween.props[pc].preEndVal;
					}

					if (tween.props[pc].attr === "fill" || tween.props[pc].attr === "stroke") {
						if (tween.props[pc].startVal instanceof eve.linearGradient) {
							var s;
							for (s = 0; s < tween["tweenGradient_" + tween.props[pc].attr].stops.length; s++){
								tween["tweenGradient_" + tween.props[pc].attr].stops[s].colour = eve.blendColours(tween.props[pc].startVal.stops[s].colour, tween.props[pc].endVal.stops[s].colour, amount);
								tween["tweenGradient_" + tween.props[pc].attr].stops[s].opacity = ((tween.props[pc].endVal.stops[s].opacity - tween.props[pc].startVal.stops[s].opacity) * amount) + tween.props[pc].startVal.stops[s].opacity;
								tween["tweenGradient_" + tween.props[pc].attr].stops[s].offset = ((tween.props[pc].endVal.stops[s].offset - tween.props[pc].startVal.stops[s].offset) * amount) + tween.props[pc].startVal.stops[s].offset;
								tween["svgTweenGradient_" + tween.props[pc].attr].childNodes[s].style.stopColor = tween["tweenGradient_" + tween.props[pc].attr].stops[s].colour;
								tween["svgTweenGradient_" + tween.props[pc].attr].childNodes[s].style.stopOpacity = tween["tweenGradient_" + tween.props[pc].attr].stops[s].opacity;
								tween["svgTweenGradient_" + tween.props[pc].attr].childNodes[s].setAttribute("offset", tween["tweenGradient_" + tween.props[pc].attr].stops[s].offset);
							}
						}
						else if (tween.props[pc].startVal instanceof eve.parallelHatch) {
							var tweenHatch = tween.tweenHatch;
							if (!isNil(tweenHatch)) {
								tweenHatch.strokeColour = eve.blendColours(tween.props[pc].startVal.strokeColour, tween.props[pc].endVal.strokeColour, amount);
//								val[pc] = tweenHatch.strokeColour;
								get(tweenHatch.id).firstChild.style.stroke = tweenHatch.strokeColour;
							}
						}
						else if (tween.props[pc].startVal.toString().indexOf("url(") === 0 || tween.props[pc].startVal.toString().indexOf("url(\"#") === 0) {
							var fromColour;
							var toColour;
							if (tween.props[pc].startVal.indexOf("eve_hatch") !== -1){
								var hatchID = tween.props[pc].startVal.replace("url(\"#","").substr(0, tween.props[pc].startVal.replace("url(\"#","").length - 2);
								var hatchElm = get(hatchID);
								fromColour = eve.getItemByID(eve.hatches, hatchID).strokeColour;
								toColour = tween.props[pc].endVal;
//								console.log(toColour);
								hatchElm.firstChild.style.stroke = eve.blendColours(fromColour, toColour, amount);
							}
							else{
								var tweenGradient = tween.elm.tweenGradient;
								if (!isNil(tweenGradient)) {
									while (tweenGradient.firstChild) {
										tweenGradient.removeChild(tweenGradient.firstChild);
									}
									var fromStops = tween.elm.fromStrokeGradient.getElementsByTagName("stop");
									var toStops = tween.elm.strokeGradient.getElementsByTagName("stop");
									for (var stc = 0; stc < fromStops.length; stc++) {
										fromColour = fromStops[stc].style.stopColor;
										var fromOffset = parseFloat(fromStops[stc].getAttribute("offset"));
										var fromOpacity = parseFloat(fromStops[stc].style.stopOpacity);
										toColour = toStops[stc].style.stopColor;
										var toOffset = parseFloat(toStops[stc].getAttribute("offset"));
										var toOpacity = parseFloat(toStops[stc].style.stopOpacity);
										var twColour = eve.blendColours(fromColour, toColour, amount);
										var twOffset = ((toOffset - fromOffset) * amount) + fromOffset;
										var twOpacity = ((toOpacity - fromOpacity) * amount) + fromOpacity;
										var tStop = document.createElementNS(svgns, "stop");
										tStop.style.stopColor = twColour;
										tStop.style.stopOpacity = twOpacity.toString();
										tStop.setAttribute("offset", twOffset.toString());
										tweenGradient.appendChild(tStop);
										tween.elm.style.stroke = "url(\"#" + tweenGradient.id + "\")";
									}
									val[pc] = tweenGradient;
								}
							}
						}
						else {
							val[pc] = eve.blendColours(tween.props[pc].startVal, tween.props[pc].endVal, amount);
						}
					}
					else if ((tween.props[pc].attr === "drawSVG" || tween.props[pc].attr === "drawSvgDashed") && (tween.elm.tagName === "line" || tween.elm.tagName === "path")) {
						val[pc] = tween.elm.getTotalLength() * amount;
						if (!isNil(tween.elm._knots)) {
							for (kc = 0; kc < tween.elm._knots.length; kc++) {
								knot = get(tween.elm._knots[kc].id, tween.elm);
								if (!isNil(knot)) {
									if (tween.elm.getPointAtLength(val[pc]).x >= knot.getAttribute("cx") && knot.shown !== true) {
										knot.shown = true;
										knot.style.opacity = 0.5;
										knot.targetRad = knot.getAttribute("r");
										knot.setAttribute("r", knot.targetRad / 2);
										eve.tweenTo(knot, 0.1, 0, "easeIn", null, {opacity: 1, r: knot.targetRad}, tween.owner);
										if (tween.elm._knots[kc]._glow){
											eve.tweenTo(get(tween.elm._knots[kc]._glow.id, tween.elm), 0.1, 0, "easeIn", null, {opacity: 1}, tween.owner);
										}
									}
								}
							}
						}
					}
					else if (tween.props[pc].attr === "sector") {

						var tSector = eve.addSector();
						var tCenX = tween.props[pc].startVal.centre[0] + ((tween.props[pc].endVal.centre[0] - tween.props[pc].startVal.centre[0]) * amount);
						var tCenY = tween.props[pc].startVal.centre[1] + ((tween.props[pc].endVal.centre[1] - tween.props[pc].startVal.centre[1]) * amount);
						tSector.centre = [tCenX, tCenY];
						tSector.startAngle = tween.props[pc].startVal.startAngle + ((tween.props[pc].endVal.startAngle - tween.props[pc].startVal.startAngle) * amount);
						tSector.angleRange = tween.props[pc].startVal.angleRange + ((tween.props[pc].endVal.angleRange - tween.props[pc].startVal.angleRange) * amount);
						tSector.radOuter = tween.props[pc].startVal.radOuter + ((tween.props[pc].endVal.radOuter - tween.props[pc].startVal.radOuter) * amount);
						tSector.radInner = tween.props[pc].startVal.radInner + ((tween.props[pc].endVal.radInner - tween.props[pc].startVal.radInner) * amount);
						tween.elm.setAttribute("d", tSector.toSVG(true));

					}
					else if (tween.props[pc].attr === "path") {
						val[pc] = eve.addPath(null, tween.props[pc].startVal.closed);
						val[pc].knotPoint = useEndVal.knotPoint;

						for (vc = 0; vc < tween.props[pc].startVal.commands.length; vc++) {
							var comd = cloneEveObj(tween.props[pc].startVal.commands[vc]);
							if (!isNil(tween.props[pc].startVal.commands[vc].from)) {
								comd.from = [tween.props[pc].startVal.commands[vc].from[0], tween.props[pc].startVal.commands[vc].from[1]];
							}

							if (tween.path.type === "arc") {

								//where our point STARTED (before tweening):
								var startPoint = [eve.setDecimal(tween.props[pc].startVal.commands[vc].to[0], 3), eve.setDecimal(tween.props[pc].startVal.commands[vc].to[1], 3)];
								var startAngle = cartesianToPolar(tween.path.centre, startPoint).radians;
								//the point's FINAL position (at the end of the tween):
								var endPoint = [eve.setDecimal(useEndVal.commands[vc].to[0], 3), eve.setDecimal(useEndVal.commands[vc].to[1], 3)];
								var endAngle = cartesianToPolar(tween.path.centre, endPoint).radians;

								var rad = Math.abs(lineLength(tween.path.centre, startPoint));
								var arcInside = false; //donut charts have an inner and outer arc; we need to know which we're considering since they run in opposite directions
								if (vc > 1) {
									arcInside = true;
								}

								var moveClockwise = true;
								if (!tween.elm.clockwise) {
									moveClockwise = false;
								}

								if (tween.path.direction === "closest") {
									var changeAngle = endAngle - startAngle;
									if (Math.abs(changeAngle) === Math.PI * 2) {
										changeAngle = 0;
									}
									if (changeAngle < 0) {
										moveClockwise = !tween.elm.clockwise;
									}
								}

								if (arcInside) {
									moveClockwise = !moveClockwise;
								}

								if (tween.path.direction !== "closest") {
									if (tween.elm.clockwise) {
										if (moveClockwise && endAngle < startAngle) {
											endAngle += (Math.PI) * 2;
										}
										if (!moveClockwise && startAngle > endAngle) {
											startAngle -= (Math.PI) * 2;
										}
									} else {
										if (moveClockwise && endAngle > startAngle) {
											endAngle -= (Math.PI) * 2;
										}
										if (!moveClockwise && startAngle < endAngle) {
											startAngle += (Math.PI) * 2;
										}
									}
								}

								var thetaTotal = endAngle - startAngle;
								var theta = startAngle + (thetaTotal * amount);

								comd.to = eve.polarToCartesian(theta, rad);
								comd.to[0] += tween.path.centre[0];
								comd.to[1] = tween.path.centre[1] - comd.to[1];

								var comdFrom = [null, null];
								var comdTo = [null, null];
								var angleFrom = null;
								var angleTo = null;
								var wedgeAngle = null;

								if (vc > 0) {

									//start point of our wedge (AFTER current tween step):
									comdFrom = [val[pc].commands[vc - 1].to[0], val[pc].commands[vc - 1].to[1]];
									//end point of our wedge (AFTER current tween step):
									comdTo = [comd.to[0], comd.to[1]];

									angleFrom = eve.setDecimal(cartesianToPolar(tween.path.centre, comdFrom).radians, 5);
									angleTo = eve.setDecimal(cartesianToPolar(tween.path.centre, comdTo).radians, 5);

									if (tween.path.direction === "closest") {
										if (angleFrom === 0) {
											if (arcInside && tween.elm.clockwise) {
												angleFrom = Math.PI * 2;
											}
											if (!arcInside && !tween.elm.clockwise) {
												angleFrom = Math.PI * 2;

											}
										}
										if (angleFrom === eve.setDecimal(Math.PI * 2, 5)) {
											if (arcInside && !tween.elm.clockwise) {
												angleFrom = 0;
											}
											if (!arcInside && tween.elm.clockwise) {
												angleFrom = 0;
											}
										}
										if (angleTo === 0) {
											if (arcInside && !tween.elm.clockwise) {
												angleTo = Math.PI * 2;
											}
											if (!arcInside && tween.elm.clockwise) {
												angleTo = Math.PI * 2;
											}
										}
										if (angleTo === eve.setDecimal(Math.PI * 2, 5)) {
											if (arcInside && !tween.elm.clockwise) {
												angleTo = 0;
											}
										}
									} else {
										if (moveClockwise && angleTo < angleFrom) {
											angleTo += (Math.PI) * 2;
										}
										if (!moveClockwise && angleFrom < angleTo) {
											angleFrom += (Math.PI) * 2;
										}
									}
									wedgeAngle = Math.abs(angleTo - angleFrom);
								}

								var largeArc = 0;
								if (Math.abs(wedgeAngle) > Math.PI) {
									largeArc = 1;
								}

								comd.largeArc = largeArc;

							} else {
								comd.to[0] = comd.to[0] + ((useEndVal.commands[vc].to[0] - comd.to[0]) * amount);
								comd.to[1] = comd.to[1] + ((useEndVal.commands[vc].to[1] - comd.to[1]) * amount);
							}
							if (!isNil(comd.handleTo) && !isNil(useEndVal.commands[vc].handleTo)) {
								comd.handleTo[0] = comd.handleTo[0] + ((useEndVal.commands[vc].handleTo[0] - comd.handleTo[0]) * amount);
								comd.handleTo[1] = comd.handleTo[1] + ((useEndVal.commands[vc].handleTo[1] - comd.handleTo[1]) * amount);
							}
							if (!isNil(comd.handleFrom) && !isNil(useEndVal.commands[vc].handleFrom)) {
								comd.handleFrom[0] = comd.handleFrom[0] + ((useEndVal.commands[vc].handleFrom[0] - comd.handleFrom[0]) * amount);
								comd.handleFrom[1] = comd.handleFrom[1] + ((useEndVal.commands[vc].handleFrom[1] - comd.handleFrom[1]) * amount);
							}
							if (!isNil(comd.radius) && !isNil(useEndVal.commands[vc].radius)) {
								comd.radius[0] = comd.radius[0] + ((useEndVal.commands[vc].radius[0] - comd.radius[0]) * amount);
								comd.radius[1] = comd.radius[1] + ((useEndVal.commands[vc].radius[1] - comd.radius[1]) * amount);
							}
							if (!isNil(comd.length) && !isNil(useEndVal.commands[vc].length)) {
								comd.length = comd.length + ((useEndVal.commands[vc].length - comd.length) * amount);
							}

							val[pc].commands.push(comd);
						}
						for (vc = 0; vc < tween.props[pc].startVal.commands.length; vc++) {
							if (!isNil(tween.elm._knots) && !isNil(tween.elm._knots[vc])) {
								knot = get(tween.elm._knots[vc].id, tween.elm);
								if (!isNil(knot)) {
									if (!isNil(val[pc].knotPoint)) {
										knot.setAttribute("cx", val[pc].commands[val[pc].knotPoint[vc]].to[0]);
										knot.setAttribute("cy", val[pc].commands[val[pc].knotPoint[vc]].to[1]);
									} else {
										knot.setAttribute("cx", val[pc].commands[vc].to[0]);
										knot.setAttribute("cy", val[pc].commands[vc].to[1]);
									}
								}
							}
						}

					}
					else if (tween.props[pc].attr === "strokeDasharray") {
						var arrStart = tween.props[pc].startVal.toString().replace(/ /g, '').split(",");
						for (var ac = 0; ac < arrStart.length; ac++) {
							arrStart[ac] = parseFloat(arrStart[ac]);
						}
						var arrEnd = useEndVal.replace(/ /g, '').split(",");
						for (ac = 0; ac < arrEnd.length; ac++) {
							arrEnd[ac] = parseFloat(arrEnd[ac]);
						}
						val[pc] = "";
						for (var sdCount = 0; sdCount < arrStart.length; sdCount++) {
							val[pc] += arrStart[sdCount] + ((arrEnd[sdCount] - arrStart[sdCount]) * amount) + ",";
						}
						val[pc] = val[pc].substring(0, val[pc].length - 1);
					} else {
						if (eve.isArray(tween.props[pc].startVal) && eve.isArray(tween.props[pc].endVal)){
							val[pc] = [];
							for (var am = 0; am < tween.props[pc].startVal.length; am++){
								val[pc].push(tween.props[pc].startVal[am] + (tween.props[pc].endVal[am] - tween.props[pc].startVal[am]) * amount);
							}
						}
						else{
							val[pc] = (tween.props[pc].startVal + (tween.props[pc].endVal - tween.props[pc].startVal) * amount).toString(); // +tween.props[pc]._units;
						}
					}
				} else {
					//set final properties
					if (tween.props[pc].attr === "fill" || tween.props[pc].attr === "stroke"){
						if (tween.props[pc].startVal instanceof eve.linearGradient || tween.props[pc].endVal instanceof eve.linearGradient){
							if (tween.props[pc].startVal instanceof eve.linearGradient && !(tween.props[pc].endVal instanceof eve.linearGradient)){
								// it blended from a gradient, but its end value is NOT a gradient - for now assume it's just a colour:
								tween.elm.style[tween.props[pc].attr] = tween.props[pc].endVal;
							}
							else{
								//TODO: lose this when we've sorted out how eve gradients get added to the defs (when we first build the SVG, presumably?) :
								var svgEndGrad = document.getElementById(tween.props[pc].endVal.id);
								if (isNil(svgEndGrad)){
									tween.elm._defs.appendChild(tween.props[pc].endVal.toSVGGradient());
								}
								tween.elm.style[tween.props[pc].attr] = "url(\"#" + tween.props[pc].endVal.id + "\")";
							}
							removeNode(tween["svgTweenGradient_" + tween.props[pc].attr]);

						}
						if (tween.props[pc].startVal instanceof eve.parallelHatch || tween.props[pc].endVal instanceof eve.parallelHatch){
							if (tween.props[pc].startVal instanceof eve.parallelHatch && !(tween.props[pc].endVal instanceof eve.parallelHatch)){
								tween.elm.style[tween.props[pc].attr] = tween.props[pc].endVal;
							}
							else{
								//TODO: lose this when we've sorted out how eve gradients get added to the defs (when we first build the SVG, presumably?) :
								var svgEndHatch = document.getElementById(tween.props[pc].endVal.id);
								if (isNil(svgEndHatch)){
									tween.elm._defs.appendChild(tween.props[pc].endVal.toSVGPattern());
								}
								tween.elm.style[tween.props[pc].attr] = "url(\"#" + tween.props[pc].endVal.id + "\")";
							}
							removeNode(tween.svgTweenHatch);
						}
						if (strIs(tween.elm.tagName, "pattern") && tween.elm.id.indexOf("eve_hatch") !== -1){
							tween.elm.firstChild.style.stroke = tween.props[pc].endVal;
						}
					}
					else if ((tween.props[pc].attr === "drawSVG" || tween.props[pc].attr === "drawSvgDashed") && (tween.elm.tagName === "line" || tween.elm.tagName === "path")) {
						val[pc] = tween.elm.getTotalLength();
						if (!isNil(tween.elm._knots)) {
							for (kc = 0; kc < tween.elm._knots.length; kc++) {
								knot = get(tween.elm._knots[kc].id, tween.elm);
								if (!isNil(knot) && knot.shown !== true) {
									knot.shown = true;
									knot.style.opacity = 0.5;
									knot.targetRad = knot.getAttribute("r");
									knot.setAttribute("r", knot.targetRad / 2);
									eve.tweenTo(knot, 0.1, 0, "easeIn", null, {opacity: 1, r: knot.targetRad}, tween.owner);
									if (tween.elm._knots[kc]._glow){
										eve.tweenTo(get(tween.elm._knots[kc]._glow.id, tween.elm), 0.1, 0, "easeIn", null, {opacity: 1}, tween.owner);
									}
								}
							}
						}
					} else if (tween.props[pc].attr === "sector") {
						tween.elm.setAttribute("d", tween.props[pc].endVal.toSVG());
					}
					else {
						val[pc] = tween.props[pc].endVal;
					}
					if (tween.props[pc].attr === "path") {
						if (!isNil(tween.elm.fPath)) {
							tween.props[pc].endVal = tween.elm.fPath;
							val[pc] = tween.elm.fPath;
							tween.elm.setAttribute("fill-rule", "evenodd");
						}
						for (vc = 0; vc < tween.props[pc].endVal.commands.length; vc++) {
							if (!isNil(tween.elm._knots) && !isNil(tween.elm._knots[vc])) {
								knot = get(tween.elm._knots[vc].id, tween.elm);
								if (!isNil(val[pc].knotPoint)) {
									knot.setAttribute("cx", val[pc].commands[val[pc].knotPoint[vc]].to[0]);
									knot.setAttribute("cy", val[pc].commands[val[pc].knotPoint[vc]].to[1]);
								} else {
									knot.setAttribute("cx", val[pc].commands[vc].to[0]);
									knot.setAttribute("cy", val[pc].commands[vc].to[1]);
								}
							}
						}
					}
					tween.cancelled = true;
				}
			}

			for (pc = 0; pc < tween.props.length; pc++) {

				if (transforms.indexOf(tween.props[pc].attr) !== -1) {
					//do translate instead of changing style properties
					if (isSVG(tween.elm)) {
						if (tween.props[pc].attr === "x") {
							tween.elm.eX = val[pc];
							if (tween.elm.tagName === "tspan") {
								tween.elm.setAttribute("x", tween.elm.eX);
							} else if (tween.elm.tagName === "circle") {
								tween.elm.setAttribute("cx", tween.elm.eX);
							} else {
								eve.setSvgTransform(tween.elm, "translate", [tween.elm.eX, tween.elm.eY]);
							}
						} else if (tween.props[pc].attr === "y") {
							tween.elm.eY = val[pc];
							if (tween.elm.tagName === "tspan") {
								tween.elm.setAttribute("y", tween.elm.eY);
							} else if (tween.elm.tagName === "circle") {
								tween.elm.setAttribute("cy", tween.elm.eY);
							} else {
								eve.setSvgTransform(tween.elm, "translate", [tween.elm.eX, tween.elm.eY]);
							}
						} else if (tween.props[pc].attr === "rotation") {
							tween.elm.eRot = val[pc];
							if (tween.path.centre){
								eve.setSvgTransform(tween.elm, "rotate", [tween.elm.eRot, tween.path.centre[0], tween.path.centre[1]]);
							}
							else{
								eve.setSvgTransform(tween.elm, "rotate", tween.elm.eRot);
								if (tween.elm._eveElm._updateNum && tween.owner._numLabel){
									var numVal = tween.elm.eRot[0] / 360 * tween.owner._numLabelSteps;
									if (strIs(tween.owner._type, "dial")){
										numVal += 0.5;
									}
									if (numVal < 0){
										numVal = 0;
									}
									if (numVal > tween.owner._numLabelSteps){
										numVal = tween.owner._numLabelSteps;
									}
									get(tween.owner._numLabel).firstChild.textContent = eve.formatNumber(Math.round(numVal), tween.owner._numLabelFormat);
								}
								if (tween.elm._eveElm._updateSector){
									var lSector = tween.elm._eveElm._updateSector.sector;
									var domSector = get(tween.elm._eveElm._updateSector.id);
									lSector.startAngle = Math.floor(Math.round(tween.elm.eRot[0]) / lSector.angleRange) * lSector.angleRange;
									if (lSector.startAngle < 0){
										lSector.startAngle = 0;
									}
									if (lSector.startAngle > 360 - lSector.angleRange){
										lSector.startAngle = 360 - lSector.angleRange;
									}
									domSector.setAttribute("d", lSector.toSVG());
								}
							}
						}
					} else {
						// TODO: don't we need to combine the transforms rather than just setting it to the current one?
						if (tween.props[pc].attr === "x") {
							tween.elm.eX = val[pc];
							tween.elm.style.transform = "translateX(" + val[pc] + tween.props[pc]._units + ")";
						} else if (tween.props[pc].attr === "y") {
							tween.elm.eY = val[pc];
							tween.elm.style.transform = "translateY(" + val[pc] + tween.props[pc]._units + ")";
						} else if (tween.props[pc].attr === "scaleX") {
							tween.elm.style.transform = "scaleX(" + val[pc] + ")";
						} else if (tween.props[pc].attr === "scaleY") {
							tween.elm.style.transform = "scaleY(" + val[pc] + ")";
						}
					}
				} else {
					if (isSVG(tween.elm)) {
						if (tween.props[pc].attr === "drawSVG" && (tween.elm.tagName === "line" || tween.elm.tagName === "path")) {
							if (val[pc] === tween.elm.getTotalLength()) {
								tween.elm.style.strokeDasharray = "";
								tween.elm.style.strokeDashoffset = "";
							} else {
								tween.elm.style.strokeDasharray = tween.elm.getTotalLength();
								tween.elm.style.strokeDashoffset = tween.elm.getTotalLength() - val[pc];
							}
						} else if (tween.props[pc].attr === "drawSvgDashed" && (tween.elm.tagName === "line" || tween.elm.tagName === "path")) {
							if (val[pc] === tween.elm.getTotalLength()) {
								tween.elm.style.strokeDasharray = tween.elm.dashArray;
								tween.elm.style.strokeDashoffset = "";
							} else {
								tween.elm.style.strokeDasharray = tween.elm.dashArray + ", " + tween.elm.getTotalLength();
								tween.elm.style.strokeDashoffset = tween.elm.getTotalLength() - val[pc];
								tween.elm.style.visibility = "inherit";
							}
						} else if (tween.props[pc].attr === "fill" || tween.props[pc].attr === "stroke") {
							if (strIs(tween.elm.tagName, "pattern") && tween.elm.id.indexOf("eve_hatch") !== -1){
								tween.elm.firstChild.style.stroke = val[pc];
							}
							else{
								tween.elm.style[tween.props[pc].attr] = val[pc];
							}
						} else if (tween.props[pc].attr === "path") {
							tween.elm.setAttribute("d", val[pc].toSVG());
						} else if (tween.props[pc].attr === "strokeDasharray") {
							tween.elm.style.strokeDasharray = val[pc];
						} else {
							if (svgAttributes.indexOf(tween.props[pc].attr) === -1) {
								if (!isNil(tween.props[pc]._units) && tween.props[pc]._units !== "") {
									tween.elm.style[tween.props[pc].attr] = val[pc].toString() + tween.props[pc]._units;
								} else {
									tween.elm.style[tween.props[pc].attr] = val[pc];
								}
							} else {
								if (!isNil(tween.props[pc]._units) && tween.props[pc]._units !== "") {
									tween.elm.setAttribute(tween.props[pc].attr, val[pc].toString() + tween.props[pc]._units);
								} else {
									tween.elm.setAttribute(tween.props[pc].attr, val[pc]);
								}
							}
						}
					} else {
						if (svgAttributes.indexOf(tween.props[pc].attr) === -1 || tween.props[pc].attr === "opacity") {
							tween.elm.style[tween.props[pc].attr] = val[pc] + tween.props[pc]._units;
						} else {
							tween.elm.setAttribute(tween.props[pc].attr, val[pc]);
						}
					}
				}
			}

			if (t < tween.duration * 1000.0) {
				requestAnimationFrame(function() {
					tweenLoop(tween);
				});
			} else {
				if (!isNil(tween.endFunction)) {
					if (!isNil(tween.endFunctionParams)) {
						tween.endFunction.apply(this, tween.endFunctionParams);
					} else {
						tween.endFunction();
					}
					tween.completed = true;
				}
				tween.cancelled = true;
			}
		} else {
			//remove tween from eve.tweens array?
			eve.removeTween(tween);
		}

	};

	eve.tweens = [];

	eve.removeTween = function(tween) {
		for (var t = 0; t < eve.tweens.length; t++) {
			if (eve.tweens[t] === tween) {
				eve.tweens.splice(t, 1);
				break;
			}
		}
	};

	eve.killTweensOf = function(elm) {
		for (var t = 0; t < eve.tweens.length; t++) {
			if (eve.tweens[t].elm === elm) {
				eve.tweens[t].cancelled = true;
			}
		}
	};

	eve.tweenObj = function(elm, duration, delay, ease, path, endProps, startProps, owner) {

		this.owner = owner || window;
		this.path = path || {
			type: "linear"
		};
		this.elm = elm;
		this.duration = duration || 0;
		this.startTime = Date.now();
		this.delay = delay || 0;
		this.ease = ease || "linear";
		this.endProps = endProps || null;
		this.startProps = startProps || null;
		this.endFunction = null;
		this.endFunctionParams = null;

		this.props = getTweenProps(this);

		this.beginTween = function() {
			var pc;
			var kc;
			var endColour;
			this.started = true;

			if (ease === "custom"){
				if (!(this.path instanceof eve.path) && typeof this.path === "string"){
					this.path = eve.svgPathToEvePath(this.path);
					this.path.unitise();
				}
			}
			else if (ease === "gravityBounce"){
				this.path = eve.svgPathToEvePath("M0,600 C233,600 391,147 448,0 C475,75 530,75 553,0 C565,18 587,18 600,0");
				this.path.unitise();
			}
			else if (ease === "easeOutIn"){
				this.path = eve.svgPathToEvePath("M0,600 C60,480 165,300 300,300 S560,350 600,0");
				this.path.unitise();
			}

			for (pc = 0; pc < this.props.length; pc++) {
				if (transforms.indexOf(this.props[pc].attr) !== -1) {
					//do translate instead of changing style properties
					if (isSVG(this.elm)) {
						if (this.props[pc].attr === "x") {
							this.elm.eX = this.props[pc].startVal;
							if (this.elm.tagName === "tspan") {
								this.elm.setAttribute("x", this.props[pc].startVal);
							} else if (this.elm.tagName === "circle") {
								this.elm.setAttribute("cx", this.props[pc].startVal);
							} else {
								eve.setSvgTransform(this.elm, "translate", [this.props[pc].startVal, (this.elm.eY || 0)]);
							}
						} else if (this.props[pc].attr === "y") {
							this.elm.eY = this.props[pc].startVal;
							if (this.elm.tagName === "tspan") {
								this.elm.setAttribute("y", this.props[pc].startVal);
							} else if (this.elm.tagName === "circle") {
								this.elm.setAttribute("cy", this.props[pc].startVal);
							} else {
								eve.setSvgTransform(this.elm, "translate", [this.elm.eX, this.props[pc].startVal]);
							}
						} else if (this.props[pc].attr === "rotation") {
							eve.setSvgTransform(this.elm, "rotate", this.props[pc].startVal);
						}
					} else {
						if (this.props[pc].attr === "x") {
							this.elm.style.transform = "translateX(" + this.props[pc].startVal + this.props[pc]._units + ")";
						} else if (this.props[pc].attr === "y") {
							this.elm.style.transform = "translateY(" + this.props[pc].startVal + this.props[pc]._units + ")";
						}
					}
				} else {
					if (this.props[pc].attr === "strokeDasharray") {
						var arrStart = this.props[pc].startVal.toString().replace(/ /g, '').split(",");
						for (var ac = 0; ac < arrStart.length; ac++) {
							arrStart[ac] = parseFloat(arrStart[ac]);
						}
						var arrEnd = this.props[pc].endVal.toString().replace(/ /g, '').split(",");
						for (ac = 0; ac < arrEnd.length; ac++) {
							arrEnd[ac] = parseFloat(arrEnd[ac]);
						}
						while (arrStart.length < arrEnd.length) {
							arrStart.push(0);
						}
						while (arrEnd.length < arrStart.length) {
							arrEnd.push(0);
						}
						this.props[pc].startVal = arrStart.toString();
						this.props[pc].preEndVal = arrEnd.toString();
						this.elm.style.strokeDasharray = this.props[pc].startVal;
					}
					else if (this.props[pc].attr === "drawSVG" && (this.elm.tagName === "line" || this.elm.tagName === "path")) {
						this.elm.style.strokeDasharray = this.elm.getTotalLength();
						this.elm.style.strokeDashoffset = (this.elm.getTotalLength() * (this.props[pc].startVal / 100.0)) - this.elm.getTotalLength();
					}
					else if (this.props[pc].attr === "drawSvgDashed" && (this.elm.tagName === "line" || this.elm.tagName === "path")) {
						this.elm.style.strokeDasharray = this.elm.dashArray;
						this.elm.style.strokeDashoffset = (this.elm.getTotalLength() * (this.props[pc].startVal / 100.0)) - this.elm.getTotalLength();
					}
					else if (this.props[pc].attr === "fill" || this.props[pc].attr === "stroke") {
						var cGrad;
						if (!isNil(this.props[pc].startVal) && !(this.props[pc].startVal instanceof eve.linearGradient)){
							if (!isNil(this.props[pc].startVal.tagName) && strIs(this.props[pc].startVal.tagName, "lineargradient")) {
								cGrad = this.props[pc].startVal;
							}
							else if (this.props[pc].startVal.toString().indexOf("url(\"#") === 0 || this.props[pc].startVal.toString().indexOf("url(#") === 0) {
								cGrad = document.getElementById(this.props[pc].startVal.replace(/"/g, "").substr(5).replace(")", ""));
							}
							if (!isNil(cGrad) && cGrad.nodeName === "linearGradient"){
								this.props[pc].startVal = eve.svgLinearGradientToEve(cGrad);
							}
						}

						if (!isNil(this.props[pc].endVal) && !(this.props[pc].endVal instanceof eve.linearGradient)){
							if (!isNil(this.props[pc].endVal.tagName) && strIs(this.props[pc].endVal.tagName, "lineargradient")) {
								cGrad = this.props[pc].endVal;
							}
							else if (this.props[pc].endVal.toString().indexOf("url(\"#") === 0 || this.props[pc].endVal.toString().indexOf("url(#") === 0) {
								cGrad = document.getElementById(this.props[pc].endVal.replace(/"/g, "").substr(5).replace(")", ""));
							}
							if (!isNil(cGrad) && cGrad.nodeName === "linearGradient"){
								this.props[pc].endVal = eve.svgLinearGradientToEve(cGrad);
							}
						}

						if (this.props[pc].startVal instanceof eve.linearGradient || this.props[pc].endVal instanceof eve.linearGradient){
							//we're blending a gradient
							var s;
							if (this.props[pc].startVal instanceof eve.linearGradient && !(this.props[pc].endVal instanceof eve.linearGradient)){
								//we're starting with a gradient but not ending with one - assume for now it's a solid fill:
								endColour = this.props[pc].endVal;
								this.props[pc].endVal = this.props[pc].startVal.clone();
								for (s = 0; s < this.props[pc].endVal.stops.length; s++){
									this.props[pc].endVal.stops[s].colour = endColour;
									this.props[pc].endVal.stops[s].opacity = 1;
								}
							}
							else if (this.props[pc].endVal instanceof eve.linearGradient && !(this.props[pc].startVal instanceof eve.linearGradient)){
								//we're ending with a gradient but not starting with one - assume for now it's a solid fill:
								var startColour = this.props[pc].startVal;
								this.props[pc].startVal = this.props[pc].endVal.clone();
								for (s = 0; s < this.props[pc].startVal.stops.length; s++){
									this.props[pc].startVal.stops[s].colour = startColour;
									this.props[pc].startVal.stops[s].opacity = 1;
								}
							}
							else{
								// they're both gradients - check they have the same number of stops:
								if (this.props[pc].startVal.stops.length < this.props[pc].endVal.stops.length){
									endColour = this.props[pc].startVal.stops[this.props[pc].startVal.stops.length - 1].colour;
									var endOpacity = this.props[pc].startVal.stops[this.props[pc].startVal.stops.length - 1].opacity;
									for (s = this.props[pc].startVal.stops.length; s < this.props[pc].endVal.stops.length; s++){
										this.props[pc].startVal.addStop(endColour, endOpacity, 1);
									}
								}
								if (this.props[pc].endVal.stops.length < this.props[pc].startVal.stops.length){
									for (s = this.props[pc].endVal.stops.length; s < this.props[pc].startVal.stops.length; s++){
										this.props[pc].endVal.addStop(this.props[pc].endVal.stops[0].colour, this.props[pc].endVal.stops[0].opacity, 1);
									}
								}
							}
							this["tweenGradient_" + this.props[pc].attr] = this.props[pc].startVal.clone();
//							console.log(this.tweenGradient.id);
//							this.tweenGradient.id = this.elm.id + "_tween_gradient";
							this.elm._defs = eve.getSvgDefs(this.elm.ownerSVGElement);
							this["svgTweenGradient_" + this.props[pc].attr] = this["tweenGradient_" + this.props[pc].attr].toSVGGradient();
							this.elm._defs.appendChild(this["svgTweenGradient_" + this.props[pc].attr]);
							this.elm.style[this.props[pc].attr] = "url(\"#" + this["svgTweenGradient_" + this.props[pc].attr].id + "\")";
						}
						else if(this.props[pc].startVal instanceof eve.parallelHatch){
							this.elm._defs = eve.getSvgDefs(this.elm.ownerSVGElement);
							this.elm._defs.appendChild(this.props[pc].endVal.toSVGPattern());
							this.tweenHatch = this.props[pc].startVal.clone();
							this.svgTweenHatch = this.tweenHatch.toSVGPattern();
							this.elm._defs.appendChild(this.svgTweenHatch);
							this.elm.style[this.props[pc].attr] = "url(\"#" + this.svgTweenHatch.id + "\")";
						}
						else if (this.props[pc].startVal.toString().indexOf("url(") === 0 || this.props[pc].startVal.toString().indexOf("url(\"#") === 0) {
							//fGradient - fill gradient (eve.lineargradient)
							//sGradient - stroke gradient (eve.lineargradient)
							if (!isNil(this.elm.fGradient)) {
								this.elm.style[this.props[pc].attr] = this.props[pc].startVal;
								this.props[pc].endVal = "url(\"#" + this.elm.fGradient.id + "\")";
							}
							else if (!isNil(this.elm.lGradient)) {
								this.elm.style[this.props[pc].attr] = this.props[pc].startVal;
								this.props[pc].endVal = "url(\"#" + this.elm.lGradient.id + "\")";
							}
						}
						else {
							//this line causes a flash when we hover nodes that have rgba fills because this clears the opacity:
							//this.elm.style[this.props[pc].attr] = eve.colourToHex(this.props[pc].startVal);
							if (strIs(this.elm.tagName, "pattern") && this.elm.id.indexOf("eve_hatch") !== -1){
								this.elm.firstChild.style.stroke = this.props[pc].startVal;
							}
							else{
								this.elm.style[this.props[pc].attr] = this.props[pc].startVal;
							}
						}
					} else if (this.props[pc].attr === "sector") {
						this.props[pc].startVal = this.elm.oSector;
						this.props[pc].endVal = this.elm.sector;
						this.elm.setAttribute("d", this.props[pc].startVal.toSVG());
					} else if (this.props[pc].attr === "path") {
						if (!isNil(this.elm._knots)) {
							for (kc = 0; kc < this.elm._knots.length - 1; kc++) {
								var knot = get(this.elm._knots[kc].id, this.elm);
								if (!isNil(knot)) {
									if (!knot.shown) {
										knot.shown = true;
										knot.style.opacity = 0.5;
										knot.targetRad = knot.getAttribute("r");
										knot.setAttribute("r", knot.targetRad / 2);
										eve.tweenTo(knot, 0.1, 0, "easeIn", null, {opacity: 1, r: knot.targetRad}, this.owner);
										if (this.elm._knots[kc]._glow){
											eve.tweenTo(get(this.elm._knots[kc]._glow.id, this.elm), 0.1, 0, "easeIn", null, {opacity: 1}, this.owner);
										}
									}
								}
							}
						}
						if (this.props[pc].startVal.length < this.props[pc].endVal.length) {
							this.props[pc].startVal = eve.growArray(this.props[pc].startVal, this.props[pc].endVal.length, true);
						}
						if (this.props[pc].endVal.length < this.props[pc].startVal.length) {
							this.props[pc].preEndVal = eve.growArray(this.props[pc].endVal, this.props[pc].startVal.length, true);
						}
						this.elm.setAttribute("d", this.props[pc].startVal.toSVG());
					} else {
						if (svgAttributes.indexOf(this.props[pc].attr) === -1 || this.props[pc].attr === "opacity") {
							this.elm.style[this.props[pc].attr] = this.props[pc].startVal;
						} else {
							this.elm.setAttribute(this.props[pc].attr, this.props[pc].startVal);
						}
					}
				}
			}
		};

		if (this.delay <= 0) {
			this.beginTween();
		}

		var t = this;
		this.to = function() {
			setTimeout(function() {
				t.startTime = Date.now();
				if (t.delay > 0) {
					t.beginTween();
				}
				//only remove the tween if it's tweening the same property:
				var a, b, c;
				for (a = 0; a < eve.tweens.length; a++) {
					if (eve.tweens[a].elm === t.elm && eve.tweens[a].id !== t.id && eve.tweens[a].started) {
						for (b = 0; b < eve.tweens[a].props.length; b++) {
							for (c = 0; c < t.props.length; c++) {
								if (eve.tweens[a].props[b].attr === t.props[c].attr) {
									eve.tweens[a].props.splice(b, 1);
									break;
								}
							}
						}
					}
					if (isNil(eve.tweens[a].props) || eve.tweens[a].props.length === 0) {
						if (!isNil(eve.tweens[a].endFunction) && !eve.tweens[a].completed) {
							if (!isNil(eve.tweens[a].endFunctionParams)) {
								eve.tweens[a].endFunction.apply(this, eve.tweens[a].endFunctionParams);
							} else {
								eve.tweens[a].endFunction();
							}
						}
						eve.tweens[a].cancelled = true;
					}
				}
				if (!t.cancelled) {
					tweenLoop(t);
				}
			}, this.delay * 1000.0);
		};

	};

	eve.tweenTo = function(elm, duration, delay, ease, path, endProps, owner) {
		var newTween = new eve.tweenObj(elm, duration, delay, ease, path, endProps, null, owner);
		newTween.id = "eveTween" + eve.tweens.length + 1;
		newTween.started = false;
		eve.tweens.push(newTween);
		newTween.to();
	};

	eve.tweenFromTo = function(elm, duration, delay, ease, path, startProps, endProps, owner) {
		var newTween = new eve.tweenObj(elm, duration, delay, ease, path, endProps, startProps, owner);
		newTween.id = "eveTween" + eve.tweens.length + 1;
		newTween.started = false;
		eve.tweens.push(newTween);
		newTween.to();
	};

	function clearProp(e, prop) {
		if (isNil(e[prop])) {
			e["_" + prop] = null;
		}
	}

	var setNodeProps = function(node) {
		var prop;
		var sProp;
		var propRoot;
		var sameName = ["fillOpacity", "strokeWidth", "strokeOpacity", "strokeColour", "strokeHoverColour"];
		var ignore = ["data"];
		var nSeries = getEveElmByID(node._series);
		for (prop in node) {
			if (node.hasOwnProperty(prop)) {
				propRoot = prop.split("_").join("");
				if (ignore.indexOf(propRoot) === -1){
					if (isNil(node[propRoot])) {
						sProp = propRoot;
						if (propRoot === "labelLeaderColour") {
							sProp = "colour";
						}
						if (sameName.indexOf(propRoot) === -1) {
							sProp = "node" + propRoot.charAt(0).toUpperCase() + propRoot.slice(1);
						}
						if (nSeries.hasOwnProperty("_" + sProp)) {
							if (eve.isArray(nSeries["_" + sProp])) {
								node["_" + propRoot] = nSeries["_" + sProp].clone();
							} else {
								node["_" + propRoot] = nSeries["_" + sProp];
							}
						}
					}
				}
			}
		}
	};

	eve.seriesNode = function(series) {
		eve.setObjProps(this, true);
		this._id = this.id = addEveID(null, this._idPrefix, this);
		if (!isNil(series) && !isNil(series.id)){
			this._series = this.series = series.id;
		}
		this.mediaProps = this._mediaProps;
	};

	eve.seriesLineSegment = function(series) {
		eve.setObjProps(this, true);
		this._id = this.id = addEveID(null, this._idPrefix, this);
		if (!isNil(series) && !isNil(series.id)){
			this._series = this.series = series.id;
		}
		this.mediaProps = this._mediaProps;
	};

	eve.highlight = function(series, criteria, domain) {
		eve.setObjProps(this, true);
		this._id = this.id = addEveID(null, this._idPrefix, this);
		if (!isNil(series) && !isNil(series.id)){
			this._series = this.series = series.id;
		}
		this._criteria = this.criteria = criteria;
		this._domain = this.domain = this._criteriaDomain = this._criteriaDomain = domain;
		this.mediaProps = this._mediaProps;
	};

	eve.leader = function(label) {
		eve.setObjProps(this, true);
		this._id = this.id = addEveID(null, this._idPrefix, this);
		if (!isNil(label) && !isNil(label.id)){
			this._label = this.label = label.id;
		}
		this.mediaProps = this._mediaProps;
	};

	eve.intNode = function(label, series, bottom) {
		eve.setObjProps(this, true);
		this._id = this.id = addEveID(null, this._idPrefix, this);
		if (!isNil(label) && !isNil(label.id)){
			this._label = this.label = label.id;
		}
		if (!isNil(series) && !isNil(series.id)){
			this._series = this.series = series.id;
		}
		this._bottom = this.bottom = bottom;
		this.mediaProps = this._mediaProps;
	};

	eve.series = function(chart, type, data, axisX, axisY) {

		eve.setObjProps(this, true);

		this._id = this.id = addEveID(null, this._idPrefix, this);
		if (!isNil(chart) && !isNil(chart.id)){
			this._chart = this.chart = chart.id;
		}
		this._data = this.data = data;
		if (!isNil(axisX) && !isNil(axisX.id)){
			this._axisX = this.axisX = axisX.id;
		}
		if (!isNil(axisY) && !isNil(axisY.id)){
			this._axisY = this.axisY = axisY.id;
		}
		this._type = this.type = getType(type);
		if (this._type === "" && !isNil(this._chart)) {
			if (getType(getEveElmByID(this._chart)._type) === "sector") {
				this.type = this._type = "sector";
			}
		}

		if (this._type === "line" || this._type === "linearea" || this._type === "scatter" || this._type === "curve"){
			this._strokeWidth = 2;
		}

		this.animBirth = new eve.elmAnim();
		this.animAdd = new eve.elmAnim();
		this.animChange = new eve.elmAnim();
		this.animDeath = new eve.elmAnim();
		this.animCurrent = new eve.elmAnim();
		this.mediaProps = this._mediaProps;

		this.updateNodes = function() {
			var nCount;
			if (this._type === "curve") {
				if (!isNil(this._axisX) && !isNil(this._axisY)) {
					if (isNil(this._pointOriginX)) {
						//this._pointOriginX = this._chart._plotOriginX;
						this._pointOriginX = 0;
					}
					if (isNil(this._pointOriginY)) {
						//this._pointOriginY = this._chart._plotOriginY;
						this._pointOriginY = 0;
					}
					if (isNil(this._pointEndX)) {
						//this._pointEndX = this._chart._plotWidth;
						this._pointEndX = 1;
					}
					if (isNil(this._pointEndY)) {
						//this._pointEndY = this._chart._plotHeight;
						this._pointEndY = 1;
					}
					if (isNil(this._handleOriginX)) {
						//this._handleOriginX = (this._pointEndX - this._pointOriginX) * 0.25;
						this._handleOriginX = 0.25;
					}
					if (isNil(this._handleOriginY)) {
						//this._handleOriginY = (this._pointEndY - this._pointOriginY) * 0.75;
						this._handleOriginY = 0.75;
					}
					if (isNil(this.handleEndX)) {
						//this._handleEndX = (this._pointEndX - this._pointOriginX) * 0.75;
						this._handleEndX = 0.75;
					}
					if (isNil(this.handleEndY)) {
						//this._handleEndY = (this._pointEndY - this._pointOriginY) * 0.25;
						this._handleEndY = 0.25;
					}
					this._data = [];
					var dObj = {};
					//dObj[this._axisX._domain] = this._axisX.getVal(this._pointOrigin.x);
					dObj[getEveElmByID(this._axisX)._domain] = getEveElmByID(this._axisX).getVal(getEveElmByID(this._chart)._plotWidth * this._pointOriginX);
					//dObj[this._axisY._domain] = this._axisY.getVal(this._chart._plotHeight - this._pointOrigin.y);
					dObj[getEveElmByID(this._axisY)._domain] = getEveElmByID(this._axisY).getVal(getEveElmByID(this._chart)._plotHeight * (1 - this._pointOriginY));
					this._data.push(dObj);
					dObj = {};
					//dObj[this._axisX._domain] = this._axisX.getVal(this._pointEnd.x);
					dObj[getEveElmByID(this._axisX)._domain] = getEveElmByID(this._axisX).getVal(getEveElmByID(this._chart)._plotWidth * this._pointEndX);
					//dObj[this._axisY._domain] = this._axisY.getVal(this._chart._plotHeight - this._pointEnd.y);
					dObj[getEveElmByID(this._axisY)._domain] = getEveElmByID(this._axisY).getVal(getEveElmByID(this._chart)._plotHeight * (1 - this._pointEndY));
					this._data.push(dObj);
				} else {
					this._nodes = [];
					for (nCount = 0; nCount < 2; nCount++) {
						var emptyNode = new eve.seriesNode(this);
//						emptyNode._id = "node" + nCount;
						this._nodes.push(emptyNode._id);
					}
				}
			}
			var nData = this._fdata;
			if (isNil(nData)) {
				nData = this._data;
			}
			if (!isNil(nData)) {
				var oldNodes = [];
				for (nCount = 0; nCount < this._nodes.length; nCount++) {
					oldNodes.push(cloneEveObj(getEveElmByID(this._nodes[nCount])));
				}
				var newNodes = [];
				var n;
				var refNode = null;
				var newData = nData.clone(true);

				var minVal = null;
				var maxVal = null;
				if (this._measureProp) {
					for (nCount = 0; nCount < newData.length; nCount++) {
						if (minVal === null || minVal > newData[nCount][this._measureProp]) {
							minVal = newData[nCount][this._measureProp];
						}
						if (maxVal === null || maxVal < newData[nCount][this._measureProp]) {
							maxVal = newData[nCount][this._measureProp];
						}
					}
				}
				for (nCount = 0; nCount < newData.length; nCount++) {
					refNode = new eve.seriesNode(this);
					for (n = 0; n < oldNodes.length; n++) {
						if (!oldNodes[n].removeMe) {
							if (oldNodes[n]._targetted) {
								if (oldNodes[n]._locator === "index" && nCount === oldNodes[n]._locatorVal) {
									refNode = cloneEveObj(oldNodes[n]);
									oldNodes[n].removeMe = true;
								} else if (oldNodes[n]._locator === "value" && eve.isEqual(oldNodes[n]._locatorVal, newData[nCount])) {
									refNode = cloneEveObj(oldNodes[n]);
									oldNodes[n].removeMe = true;
								} else if (oldNodes[n]._locator === "label" && !isNil(oldNodes[n]._label) && oldNodes[n]._label === oldNodes[n]._label._locatorVal) {
									refNode = cloneEveObj(oldNodes[n]);
									oldNodes[n].removeMe = true;
								} else if (oldNodes[n]._locator === "min" && newData[nCount] === minVal) {
									refNode = cloneEveObj(oldNodes[n]);
									oldNodes[n].removeMe = true;
								} else if (oldNodes[n]._locator === "max" && newData[nCount] === maxVal) {
									refNode = cloneEveObj(oldNodes[n]);
									oldNodes[n].removeMe = true;
								}
							} else {
								if (this._labelProp && !isNil(newData[nCount][this._labelProp]) && oldNodes[n]._data[this._labelProp] === newData[nCount][this._labelProp]) {
									refNode = cloneEveObj(oldNodes[n]);
									oldNodes[n].removeMe = true;
								} else if (!isNil(oldNodes[n]._data) && eve.isEqual(getEveElmByID(this._nodes[n])._data, newData[nCount])) {
									refNode = cloneEveObj(oldNodes[n]);
									oldNodes[n].removeMe = true;
								}
							}
						}
					}
					refNode.nIndex = nCount;
//					refNode._id = "node" + nCount;
					refNode._data = newData[nCount];
					//this line added to make sure node colours change if the series' colour changes
					if (!refNode.colour) {
						refNode._colour = null;
					}
					newNodes.push(refNode._id);
				}
				this._nodes = newNodes.slice(0);
			}
		};

		this.addNode = function() {
			var newNode = new eve.seriesNode(this);
			this._nodes.push(newNode);
			return newNode;
		};

		this.getNode = function(locator) {
			//curve doesn't have any data, so...
			if (this._type === "curve" || this._nodes.length === 0) {
				this.updateNodes();
			}
			var retNode = null;
			if (!isNil(locator)) {
				var sn;
				var arrCriteria = ["max", "min"];

				if (typeof locator === "object") {
					if (arrCriteria.indexOf(locator.criteria) !== -1) {
						if (!isNil(locator.domain && !isNil(this._data))) {
							if (locator.criteria === "max") {
								//find biggest value:
								var maxVal = null;
								for (sn = 0; sn < this._nodes.length; sn++) {
									if (!isNil(this._segments[sn]._dataEnd[locator.domain])) {
										if (maxVal === null || maxVal < this._nodes[sn]._dataEnd[locator.domain]) {
											retNode = this._nodes[sn];
											retNode._locator = "max";
											maxVal = this._nodes[sn]._dataEnd[locator.domain];
										}
									}
								}
							}
							if (locator.criteria === "min") {
								//find smallest value:
								var minVal = null;
								for (sn = 0; sn < this._nodes.length; sn++) {
									if (!isNil(this._segments[sn]._dataEnd[locator.domain])) {
										if (minVal === null || minVal > this._nodes[sn]._dataEnd[locator.domain]) {
											retNode = this._nodes[sn];
											retNode._locator = "min";
											minVal = this._nodes[sn]._dataEnd[locator.domain];
										}
									}
								}
							}
						}
					} else {
						//finding a node by the value of a property:
						for (sn = 0; sn < this._nodes.length; sn++) {
							var prNames = Object.keys(locator);
							var match = true;
							for (var pn = 0; pn < prNames.length; pn++) {
								if (getEveElmByID(this._nodes[sn])._data[prNames[pn]] !== locator[prNames[pn]]) {
									match = false;
								}
							}
							if (match) {
								retNode = getEveElmByID(this._nodes[sn]);
								retNode._locator = "value";
								retNode._locatorVal = locator[prNames[pn]];
								break;
							}
						}
					}
				}
				//find by label
				else if (typeof locator === "string") {
					sFind:
					for (sn = 0; sn < this._nodes.length; sn++) {
						if (!isNil(this._nodes[sn]._label) && this._nodes[sn]._label === locator) {
							retNode = getEveElmByID(this._nodes[sn]);
							retNode._locator = "label";
							retNode._locatorVal = locator;
							break;
						} else {
							for (var prop in this._nodes[sn]._data) {
								if (this._nodes[sn]._data.hasOwnProperty(prop)) {
									if (this._nodes[sn]._data[prop] === locator) {
										retNode = getEveElmByID(this._nodes[sn]);
										break sFind;
									}
								}
							}
						}
					}
				} else if (eve.isNumeric(locator)) {
					//finding it by index:
					if (locator < this._nodes.length) {
						retNode = getEveElmByID(this._nodes[locator]);
						retNode._locator = "index";
						retNode._locatorVal = locator;
					}
				}

			}
			if (!isNil(retNode)) {
				retNode._targetted = true;
				retNode._criteria = locator.criteria;
				retNode._criteriaDomain = locator.domain;
			}
			return retNode;
		};

		this.updateSegments = function() {
			if (!isNil(this._data)) { // && data.length>2){ //if there are only 2 data entries, it's a single line
				for (var i = 0; i < this._data.length - 1; i++) {
					var refSegment = this._segments[i];
					if (isNil(refSegment)) {
						refSegment = new eve.seriesLineSegment(this);
						this._segments.push(refSegment);
					}
					refSegment._id = "segment" + i;
					refSegment._dataStart = this._data[i];
					refSegment._dataEnd = this._data[i + 1];
					refSegment.sIndex = i;
				}
			}
		};

		this.addSegment = function() {
			var newSegment = new eve.seriesLineSegment(this);
			this._segments.push(newSegment);
			return newSegment;
		};

		this.getSegment = function(locator) {
			if (this._segments.length === 0) {
				this.updateSegments();
			}
			var retSegment = null;
			var ss;
			var change;
			var arrCriteria = ["maxFall", "maxRise", "max", "min"];

			if (!isNil(locator)) {
				if (typeof locator === "object") {
					if (arrCriteria.indexOf(locator.criteria) !== -1) {
						if (!isNil(locator.domain && !isNil(this._data))) {

							if (locator.criteria === "max") {
								//find biggest value:
								var maxVal = null;
								for (ss = 0; ss < this._segments.length; ss++) {
									if (!isNil(this._segments[ss]._dataEnd[locator.domain])) {
										if (maxVal === null || maxVal < this._segments[ss]._dataEnd[locator.domain]) {
											retSegment = this._segments[ss];
											maxVal = this._segments[ss]._dataEnd[locator.domain];
										}
									}
								}
							} else if (locator.criteria === "min") {
								//find smallest value:
								var minVal = null;
								for (ss = 0; ss < this._segments.length; ss++) {
									if (!isNil(this._segments[ss]._dataEnd[locator.domain])) {
										if (minVal === null || minVal > this._segments[ss]._dataEnd[locator.domain]) {
											retSegment = this._segments[ss];
											minVal = this._segments[ss]._dataEnd[locator.domain];
										}
									}
								}
							} else if (locator.criteria === "maxRise") {
								//find biggest rise:
								var highVal = null;
								for (ss = 0; ss < this._segments.length; ss++) {
									if (!isNil(this._segments[ss]._dataEnd[locator.domain]) && !isNil(this._segments[ss]._dataStart[locator.domain])) {
										change = (this._segments[ss]._dataEnd[locator.domain] - this._segments[ss]._dataStart[locator.domain]) / this._segments[ss]._dataStart[locator.domain];
										if (highVal === null || highVal < change) {
											retSegment = this._segments[ss];
											highVal = change;
										}
									}
								}
							} else if (locator.criteria === "maxFall") {
								//find biggest fall:
								var lowVal = null;
								for (ss = 0; ss < this._segments.length; ss++) {
									if (!isNil(this._segments[ss]._dataEnd[locator.domain]) && !isNil(this._segments[ss]._dataStart[locator.domain])) {
										change = (this._segments[ss]._dataEnd[locator.domain] - this._segments[ss]._dataStart[locator.domain]) / this._segments[ss]._dataStart[locator.domain];
										if (lowVal === null || lowVal > change) {
											retSegment = this._segments[ss];
											lowVal = change;
										}
									}
								}
							}
						}

					} else {
						//finding a segment by the value of a property:
						for (ss = 0; ss < this._segments.length; ss++) {
							var prNames = Object.keys(locator);
							var match = true;
							for (var pn = 0; pn < prNames.length; pn++) {
								if (this._segments[ss]._dataEnd[prNames[pn]] !== locator[prNames[pn]]) {
									match = false;
								}
							}
							if (match) {
								retSegment = this._segments[ss];
								break;
							}
						}
					}
				} else if (eve.isNumeric(locator)) {
					//finding it by index:
					if (locator < this._segments.length) {
						retSegment = this._segments[locator];
					}
				}
			}
			if (!isNil(retSegment)) {
				retSegment._criteria = locator.criteria;
				retSegment._criteriaDomain = locator.domain;
				retSegment.myIndex = retSegment.sIndex;
			}
			return retSegment;
		};

		this.updateHighlights = function() {
			for (var h = 0; h < this._highlights.length; h++) {
				var hi = getEveElmByID(this._highlights[h]);
				if (this._type === "line" || this._type === "linearea") {
					var refSeg = this.getSegment({criteria: hi._criteria, domain: hi._criteriaDomain});
					hi._elmRef = refSeg;
					hi.change = refSeg._dataEnd[hi._criteriaDomain] - refSeg._dataStart[hi._criteriaDomain];
					hi.changePercent = (hi.change / refSeg._dataStart[hi._criteriaDomain]) * 100;
				} else {
					var refNod = this.getNode({criteria: hi._criteria, domain: hi._criteriaDomain});
					hi._elmRef = refNod;
				}
			}
		};
		this.addHighlight = function(criteria, domain) {
			var newHighlight = new eve.highlight(this, criteria, domain);
			if (this._type === "line" || this._type === "linearea") {
				var refSegment = this.getSegment({criteria: criteria, domain: domain});
				newHighlight._elmRef = refSegment;
				newHighlight.change = refSegment._dataEnd[domain] - refSegment._dataStart[domain];
				newHighlight.changePercent = (newHighlight.change / refSegment._dataStart[domain]) * 100;
				newHighlight._type = "segment";
			} else {
				var refNode = this.getNode({criteria: criteria, domain: domain});
				newHighlight._elmRef = refNode;
				newHighlight._type = "node";
			}
			this._highlights.push(newHighlight.id);
			return newHighlight;
		};

		this.remove = function() {
			this._age = -1;
		};

		this.cloneRef = function() {
			var copy = new eve.series(this._chart, this._type);
			for (var p = 0; p < seriesPropsRedraw.length; p++) {
				if (seriesPropsRedraw[p] === "_data") {
					copy._data = [];
					for (var d = 0; d < this._data.length; d++) {
						copy._data[d] = cloneObj(this._data[d]);
					}
				} else {
					if (seriesPropsRedraw[p] === "_axisX" && !isNil(this._axisX)) {
						copy._axisX = getEveElmByID(this._axisX).cloneRef()._id;
					} else if (seriesPropsRedraw[p] === "_axisY" && !isNil(this._axisY)) {
						copy._axisY = getEveElmByID(this._axisY).cloneRef()._id;
					} else {
						copy[seriesPropsRedraw[p]] = this[seriesPropsRedraw[p]];
					}
				}
			}
			return copy;
		};

//		this.updateNodes();
//		this.updateSegments();

	};

	var getAutoNum = function(strNum, asInt) {
		if (typeof strNum === "string") {
			if (asInt) {
				return parseInt(strNum.replace("auto|", ""));
			}
			return parseFloat(strNum.replace("auto|", ""));
		}
		if (asInt) {
			return parseInt(strNum);
		}
		return strNum;
	};

	var cloneObj = function (objSrc){
		if (objSrc === null) {
			return null;
		}
		var retObj = {};
		for (var prop in objSrc) {
			if (objSrc.hasOwnProperty(prop)) {
				retObj[prop] = objSrc[prop];
			}
		}
		return retObj;
	};

	var cloneEveObj = function(a, returnEmpty) {
		var b;
		var prop;
		if (typeof a === "string"){
			if (returnEmpty) {
				return "";
			}
			else{
				return a;
			}
		}
		else if(a instanceof eve.linearGradient || a instanceof eve.radialGradient || a instanceof eve.gradStop) {
			return a.clone();
		}
		else{
			b = new a.constructor();
			if (returnEmpty) {
				return b;
			}
			for (prop in a) {
				if (a.hasOwnProperty(prop)) {
					if (a[prop] instanceof eve.series) {
						b[prop] = a[prop];
					}
					else if (a[prop] instanceof eve.linearGradient || a[prop] instanceof eve.radialGradient || a[prop] instanceof eve.gradStop) {
						b[prop] = a[prop].clone();
					}
					else if (eve.isArray(a[prop])) {
						b[prop] = a[prop].clone();
					}
					else if (eve.isObject(a[prop])) {
						b[prop] = cloneObj(a[prop]);
					}
					else {
						b[prop] = a[prop];
					}
				}
			}
		}
		return b;
	};


	eve.label = function(chart, id) {

		eve.setObjProps(this, true);
		this._id = this.id = addEveID(id, this._idPrefix, this);
		if (!isNil(chart) && !isNil(chart.id)){
			this._chart = this.chart = chart.id;
		}

		this.children = [];
		this.animBirth = new eve.elmAnim();
		this.animCurrent = new eve.elmAnim();
		this.animAdd = new eve.elmAnim();
		this.animChange = new eve.elmAnim();
		this.animDeath = new eve.elmAnim();
		this.mediaProps = [];

		this.addLeader = function() {
			var newLeader = new eve.leader(this);
			this._leaders.push(newLeader.id);
			return newLeader;
		};

		this.addIntNode = function(series, bottom) {
			var newInt = new eve.intNode(this, series, bottom);
			this._intNodes.push(newInt.id);
			return newInt;
		};

		this.cloneRef = function() {
			var lcopy = new eve.label();
			for (var lprop = 0; lprop < labelPropsRedraw.length; lprop++) {
				lcopy[labelPropsRedraw[lprop]] = this[labelPropsRedraw[lprop]];
			}
			return lcopy;
		};

		this.updateText = function(){
			var textElm = get(this._textElm, chart._container);
			var tContent = eve.makeText(this._chart, this._chart._blueprintGroup, this._chart._drawMode, {id: textElm.id, parentElm: this._chart._artboard, holderObj: this, string: this.text, className: textElm.className, fontSize: this._fontSize, fontFamily: this._fontFamily, fontColour: this._fontColour, fontWeight: this._fontWeight, fontStyle: this._fontStyle, textAlign: this._textAlign, leading: this._leading}).toSVG();
			//remove the existing nodes first:
			while (textElm.firstChild) {
				textElm.removeChild(textElm.firstChild);
			}
			//then add our new content:
			while (tContent.firstChild) {
				textElm.appendChild(tContent.firstChild);
			}
		};

		this.update = function(duration){
			duration = duration || 0;
			var lblElm = get(this._domElmID, chart._container);
			lblElm.axisVal = this.axisVal;
			lblElm._dragPosX = null;
			lblElm._dragPosY = null;
			if (this._link && this._link instanceof eve.axis && !isNil(this.axisVal) && this._link._horizontal) {
				eve.tweenTo(lblElm, duration, 0, null, null, {x: this._link.getScreenPos(this.axisVal) - lblElm._offsetX, y: eve.getSvgTransforms(lblElm).y});
			}
			if (this._link && this._link instanceof eve.axis && !isNil(this.axisVal) && !this._link._horizontal) {
				eve.tweenTo(lblElm, duration, 0, null, null, {x:eve.getSvgTransforms(lblElm).x, y: this._link.getScreenPos(this.axisVal)});
			}

			var uTime = Date.now();
			function uChildren(){
				var tGone = Date.now() - uTime;
				updateChildren(lblElm, false);
				if (tGone / 1000.0 < duration){
					requestAnimationFrame(uChildren);
				}
			}
			uChildren();

			var lbl = this;
			setTimeout(
				function(){
					lbl.updateText();
					updateChildren(lblElm, false);
				},
				duration * 1000.0
			);
		};

	};

	eve.axis = function(chart, position, type, domain) {

		eve.setObjProps(this, true);
		this._id = this.id = addEveID(null, this._idPrefix, this);
		if (!isNil(chart) && !isNil(chart.id)){
			this._chart = this.chart = chart.id;
		}
		this._position = position;
		this._type = type || "linear";
		this._domain = domain;
		this.animBirth = new eve.elmAnim();
		this.animAdd = new eve.elmAnim();
		this.animChange = new eve.elmAnim();
		this.animDeath = new eve.elmAnim();
		this.animCurrent = new eve.elmAnim();
		this.mediaProps = this._mediaProps;

		this.remove = function() {
			this._age = -1;
		};

		this.getScreenPos = function(val, includeOffset) {
			//pass in a value and get back a screen coordinate
//			var add = 0;
			var c = getEveElmByID(this._chart);
			if (this._horizontal) {
				if (includeOffset) {
//					add = this._chart._marginLeft + this._chart._paddingLeft;
					if (this._origin === "right") {
//						add = this._chart._marginRight + this._chart._paddingRight;
					}
				}
				if (this._origin === "left") {
					return (((val - this._rangeMin) / this._range) * c._plotWidth); // - add;
				} else {
					return (c._plotWidth - (((val - this._rangeMin) / this._range) * c._plotWidth)); // - add;
				}

			} else {
				if (includeOffset) {
//					add = this._chart._marginTop + this._chart._paddingTop;
					if (this._origin === "top") {
//						add = this._chart._marginBottom + this._chart._paddingBottom;
					}
				}
				if (this._origin === "bottom") {
					return (c._plotHeight - (((val - this._rangeMin) / this._range) * c._plotHeight)); // - add;
				} else {
					return (((val - this._rangeMin) / this._range) * c._plotHeight); // - add;
				}
			}
		};

		this.getVal = function(pos, returnCoord) {
			//pass in a screen coordinate and get back a value along the axis:
			//or vice-versa
			var c = getEveElmByID(this._chart);
			var retVal = null;
			var plotDim = c._width - (c._marginLeft + c._marginRight + c._paddingLeft + c._paddingRight);
			if (!this._horizontal) {
				plotDim = c._height - (c._marginTop + c._marginBottom + c._paddingTop + c._paddingBottom);
			}
			//_range is not set for band axes. Fix this if/when we do this properly:
			if (isNil(this._range)) {
				this._range = this._rangeMax - this._rangeMin + 1;
			}
			if (returnCoord) {
				if (this._type === "band") {
					// retVal = (plotDim / this._range) * pos;
					retVal = ((pos - this._rangeMin) / this._range) * plotDim;
					//nudge it half a band:
					retVal += plotDim / (this._range * 2);
				}
				else{
					retVal = ((pos - this._rangeMin) / this._range) * plotDim;
					if (this._origin === "right" || this._origin === "bottom") {
						//y-axis draws from the top:
						retVal = plotDim - retVal;
					}
				}
			} else {
				if (this._type === "band") {
					var bIndex = Math.floor((this._bands.length / plotDim) * pos);
					if (bIndex > this._bands.length - 1) {
						bIndex = this._bands.length - 1;
					}
					if (bIndex < 0) {
						bIndex = 0;
					}
					retVal = this._bands[bIndex]._label;
					if (this._origin === "right") {
						retVal = this._bands[this._bands.length - bIndex]._label;
					} else if (this._origin === "bottom") {
						retVal = this._bands[this._bands.length - bIndex]._label;
					} else if (this._origin === "top") {
						retVal = this._bands[bIndex]._label;
					}
					if (retVal === "") {
						retVal = this._bands[bIndex]._value.toString();
						if (this._origin === "right") {
							retVal = this._bands[this._bands.length - bIndex]._value.toString();
						} else if (this._origin === "bottom") {
							retVal = this._bands[this._bands.length - bIndex]._value.toString();
						} else if (this._origin === "top") {
							retVal = this._bands[bIndex]._value.toString();
						}
					}
				} else { //linear axes
					retVal = ((pos / plotDim) * this._range) + this._rangeMin;
					if (this._origin === "right") {
						retVal = this._range - retVal;
					} else if (this._origin === "bottom") {
						//y-axis draws from the top:
						retVal = this._range - ((pos / plotDim) * this._range) + this._rangeMin;
					} else if (this._origin === "top") {
						retVal = ((pos / plotDim) * this._range) + this._rangeMin;
					}
				}
			}
			return retVal;
		};

		this.getBandIndexByLabel = function(lbl){
			for (var i = 0; i < this._bands.length; i++) {
				if (this._bands[i]._label === lbl || this._bands[i]._label === parseFloat(lbl)) {
					return i;
				}
			}
		};

		this.cloneRef = function() {
			var copy = new eve.axis();
			for (var aprop = 0; aprop < axisPropsRedraw.length; aprop++) {
				copy[axisPropsRedraw[aprop]] = this[axisPropsRedraw[aprop]];
			}
			return copy;
		};

		this.autoRange = function(){
			var minVal = null;
			var maxVal = null;
			if (eve.isNumeric(this._bands[0]._label)){
				for (var b = 0; b < this._bands.length; b++){
					if (eve.isNumeric(this._bands[b]._label)){
						var val = parseFloat(this._bands[b]._label);
						if (val < minVal || minVal === null){
							minVal = val;
						}
						if (val < maxVal || maxVal === null){
							maxVal = val;
						}
					}
				}
			}
			else{
				minVal = 1;
				maxVal = this._bands[0].length;
			}
			if (!this.rangeMin){
				this._rangeMin = minVal;
			}
			if (!this.rangeMax){
				this._rangeMax = maxVal;
			}
			this._range = getAutoNum(this._numBands, true);
		};

		this.autoTicks = function() {
			var c = getEveElmByID(this._chart);
			var bc;
			this._autoRangeMin = c._propMin[c._domains.indexOf(this._domain)];
			this._autoRangeMax = c._propMax[c._domains.indexOf(this._domain)];
			if (strIs(this._type, "label-band")){
				this._autoRangeMin = 0;
				this._autoRangeMax = c._series.length;
				this._numBands = this._autoRangeMax;
			}
			if (this._autoRangeMin === this._autoRangeMax) {
				if (this._autoRangeMax > 0) {
					this._autoRangeMin = 0;
				} else if (this._autoRangeMin < 0) {
					this._autoRangeMax = 0;
				} else if (this._autoRangeMax === 0 && this._autoRangeMin === 0) {
					this._autoRangeMax = 10;
				}
			}

			var stackedAxis = false;
			for (var s = 0; s < c._series.length; s++) {
				if (getEveElmByID(c._series[s])._baseAxis !== this) {
					stackedAxis = true;
					break;
				}
			}

			if (c._seriesLayout === "stacked" && stackedAxis) {
				if (this._autoRangeMax < c._propMaxAdditive[c._domains.indexOf(this._domain)]) {
					this._autoRangeMax = c._propMaxAdditive[c._domains.indexOf(this._domain)];
				}
				if (this._autoRangeMin > c._propMinAdditive[c._domains.indexOf(this._domain)]) {
					this._autoRangeMin = cancelAnimationFrame._propMinAdditive[c._domains.indexOf(this._domain)];
				}
			}

			this._rangeMin = this._autoRangeMin;
			this._rangeMax = this._autoRangeMax;

			if (!isNil(this.rangeMin)) {
				this.rangeMin = parseFloat(this.rangeMin);
			}
			if (!isNil(this.rangeMax)) {
				this.rangeMax = parseFloat(this.rangeMax);
			}
			if (!isNil(this.numBands)) {
				this.numBands = parseFloat(this.numBands);
			}

			if (this.rangeMin >= this._rangeMax) {
				console.log("Eve Error: rangeMin cannot exceed rangeMax");
				this.rangeMin = null;
			}
			if (this.rangeMax <= this._rangeMin) {
				console.log("Eve Error: rangeMax cannot be less than rangeMin");
				this.rangeMax = null;
			}

			if (!isNil(this.rangeMin)) {
				this._rangeMin = this.rangeMin;
			}
			if (!isNil(this.rangeMax)) {
				this._rangeMax = this.rangeMax;
			}

			if (!isNil(this.numBands)) {
				this._numBands = this.numBands;
			}
			var checkBands = getAutoNum(this._numBands, true);
			if (this._numBands.toString().indexOf("auto|") === 0) {
				this._numBands = "auto|10";
				checkBands = 10;
			}
			this._bandSize = eve.tickStep(this._rangeMin, this._rangeMax, checkBands);

			if (this._numBands < 1){
				this._numBands = 1;
			}

			//if rangeMax is > 0 and rangeMin is < 0, we tend to miss the tick at zero, so use from zero to the larger of the two to evaluate the tickStep:
			if (this._rangeMin < 0 && this._rangeMax > 0) {
				var bandRatio;
				// commented these lines out because I was getting weird bands. didn't have time to check what it was supposed to be doing though:
//				var bandRatio = Math.abs(this._rangeMin) / (Math.abs(this._rangeMin) + Math.abs(this._rangeMax));
//				checkBands = Math.floor(checkBands * bandRatio);
				if (checkBands < 1) {
					checkBands = 1;
				}
				if (Math.abs(this._rangeMin) > Math.abs(this._rangeMax)) {
					this._bandSize = eve.tickStep(this._rangeMin, this._rangeMax, checkBands);
				} else {
					bandRatio = Math.abs(this._rangeMax) / (Math.abs(this._rangeMin) + Math.abs(this._rangeMax));
					checkBands = Math.floor(checkBands * bandRatio);
					if (checkBands < 1) {
						checkBands = 1;
					}
					this._bandSize = eve.tickStep(this._rangeMin, this._rangeMax, checkBands);
				}
				this._rangeMax = (Math.floor(this._rangeMax / this._bandSize) + 1) * this._bandSize;
				this._rangeMin = Math.floor(this._rangeMin / this._bandSize) * this._bandSize;
			}

			if (this._rangeMin > 0) {
				if (this._rangeMin - this._bandSize < 0 && isNil(this.rangeMin)) {
					this._rangeMin = 0;
				}
			}
			if (this._rangeMax < 0) {
				if (this._rangeMax + this._bandSize > 0 && isNil(this.rangeMax)) {
					this._rangeMax = 0;
				}
			}
			if (this._numBands.toString().indexOf("auto|") === 0) {
				if (!isNil(this.rangeMax)) {
					this._numBands = "auto|" + ((this._rangeMax - this._rangeMin) / this._bandSize).toString();
				} else {
					this._numBands = "auto|" + (Math.round((this._rangeMax - this._rangeMin) / this._bandSize)).toString();
				}
			} else {
				if (!isNil(this.rangeMax)) {
					this._numBands = (this._rangeMax - this._rangeMin) / this._bandSize;
				} else {
					this._numBands = Math.round((this._rangeMax - this._rangeMin) / this._bandSize);
				}
			}

			var tMax = this._rangeMin + (getAutoNum(this._numBands, true) * this._bandSize);
			var tempNumBands = getAutoNum(this._numBands, true);

			while (tMax < this._rangeMax) {
				tempNumBands++;
				tMax = this._rangeMin + (tempNumBands * this._bandSize);
			}
			if (this._numBands.toString().indexOf("auto|") === 0) {
				this._numBands = "auto|" + tempNumBands.toString();
			} else {
				this._numBands = tempNumBands;
			}
			//if the user has specified the number of bands, this will override the rangeMax value
			if (isNil(this.rangeMax) || !isNil(this.numBands)) {
				this._rangeMax = tMax;
			} else {
				this._rangeMax = this.rangeMax;
			}
			if (this._rangeMax < tMax) {
				this._rangeMax = tMax;
			}
			this._range = this._rangeMax - this._rangeMin;

			var bandWidth = 1 / getAutoNum(this._numBands, true);
			this._bands = [];
			for (bc = 0; bc < getAutoNum(this._numBands, true); bc++) {
				var startPos = bandWidth * bc;
				var endPos = startPos + bandWidth;
				this._bands[bc] = {_label: null, _value: null, _labelPos: "end", _labelOffsetX: 0, _labelOffsetY: 0, _tickPos: "end", _start: startPos, _end: endPos};
			}

			this._autoSubTicks = parseInt(this._bandSize.toString().replace(/0/g, "").replace(/\./g, ""));
			if (this._autoSubTicks === 1) {
				this._autoSubTicks = 10;
			}
			this._useSubTicks = this._autoSubTicks;
			if (this._subTicks.toString().indexOf("auto|") !== 0) {
				this._useSubTicks = this._subTicks;
			}
		};

		this.addBandLabels = function() {
			var bLabels = eve.fillArray(this._bandLabels, getAutoNum(this._numBands, true) + 1);
			var maxDecs = 0;
			for (var tb = 0; tb <= getAutoNum(this._numBands, true); tb++) {
				var tickVal = (Math.round((this._rangeMin + (this._bandSize * tb)) * 1000)) / 1000; //to get around floating point precision problems
				if (tickVal.toString().indexOf(".") !== -1) {
					var decs = tickVal.toString().split(".")[1].length;
					if (decs > maxDecs) {
						maxDecs = decs;
					}
				}
				var tickText = tickVal.toString();
				if (this._tickTextFormat !== "auto") {
					tickText = eve.formatNumber(tickText, this._tickTextFormat);
				}
				if (!isNil(bLabels)) {
					tickText = bLabels[tb];
				}
				if (tb === 0) {
					this._bands[0]._startLabel = tickText;
					this._bands[0]._startValue = tickVal;
				} else {
					if (tb > this._bands.length) {
						this._bands[tb - 1] = ({_label: tickText});
						this._bands[tb - 1] = ({_value: tickVal});
					} else {
						this._bands[tb - 1]._label = tickText;
						this._bands[tb - 1]._value = tickVal;
					}
				}
			}
			if (this._tickTextFormat === "auto" && isNil(this._bandLabels)) {
				if (this._bands.length > 0) {
					this._bands[0]._startLabel = eve.formatNumber(this._bands[0]._startLabel, ",." + maxDecs);
					for (tb = 0; tb < getAutoNum(this._numBands, true); tb++) {
						this._bands[tb]._label = eve.formatNumber(this._bands[tb]._label, ",." + maxDecs);
					}
				}
			}
		};
	};

	//the blueprint is a kind of repository for visual elements - we build the blueprint elements before converting them to SVG or Flash or HTML elements
	eve.blueprint = function(container) {
		this._container = container;
		this.nodes = [];
		var newGroup = new eve.bpElement(null, null, null, null, null, container);
		this.nodes.push(newGroup);
		return newGroup;
	};

	var findElement = function(rootNode, elmID) {
		var result = null;
		if (elmID !== "") {
			if (rootNode.id === elmID) {
				result = rootNode;
			} else {
				if (rootNode.nodes.length > 0) {
					for (var n = 0; n < rootNode.nodes.length; n++) {
						result = findElement(rootNode.nodes[n], elmID);
						if (result) {
							break;
						}
					}
				}
			}
		}
		return result;
	};

	var getOldProp = function(elm, strProp) {
		var oldPropName = "o" + strProp.substr(0, 1).toUpperCase() + strProp.substr(1);
		if (!isNil(elm[oldPropName])) {
			return elm[oldPropName];
		} else {
			return null;
		}
	};

	var passProps = function(srcObj, tgtObj) {
		var p, arrProps = ["_knots", "_targets", "points"];
		for (p = 0; p < arrProps.length; p++) {
			if (!isNil(srcObj[arrProps[p]])) {
				tgtObj[arrProps[p]] = srcObj[arrProps[p]];
			}
		}
	};

	eve.bpElement = function(type, id, className, attributes, styles, container, age) {
		this.container = this._container = container;
		this.type = type || "group";
		this.id = id || "";
		this.className = className || "";
		this.attributes = [];
		this.styles = [];
		this.oldClassName = "";
		this.oldAttributes = [];
		this.oldStyles = [];
		this.anims = [];

		this.animBirth = new eve.elmAnim();
		this.animAdd = new eve.elmAnim();
		this.animChange = new eve.elmAnim();
		this.animDeath = new eve.elmAnim();
		this._animCurrent = cloneEveObj(this.animBirth);

		this.age = age || 0;
		this.events = [];
		this.mouseOver = [];
		this.mouseLeave = [];
		this.mouseMove = [];
		this.mouseDown = [];
		this.mouseUp = [];
		this.touchStart = [];
		this.touchMove = [];
		this.touchEnd = [];

		var atCount;
		var stCount;

		if (!isNil(attributes)) {
			for (var attProperty in attributes) {
				if (attributes.hasOwnProperty(attProperty)) {
					this.attributes.push({name: attProperty, value: attributes[attProperty]});
				}
			}
		}

		if (isNil(styles)) {
			this.styles = [{}];
		} else {
			for (var styleProperty in styles) {
				if (styles.hasOwnProperty(styleProperty)) {
					this.styles.push({name: styleProperty, value: styles[styleProperty]});
				}
			}
			this.styles.concat([{}]);
		}

		this.parent = null;
		this.nodes = [];

		this.getRoot = function(p) {
			while (!isNil(p.parent)) {
				p = p.parent;
			}
			return p;
		};

		this.addEvent = function(eventName, functionName, vars) {
			this.events.push([eventName, functionName, vars]);
		};

		this.removeNode = function(nodeID) {
			var tNodes = [];
			for (var n = 0; n < this.nodes.length; n++) {
				if (this.nodes[n].id !== nodeID) {
					tNodes.push(this.nodes[n]);
				}
			}
			this.nodes = tNodes;
		};

		this.getProp = function(n, prop) {
			var ret = null;
			if (n === null) {
				n = this;
			}
			if (!isNil(n[prop])) {
				ret = n[prop];
			} else {
				if (!isNil(n.parent)) {
					ret = this.getProp(n.parent, prop);
				}
			}
			return ret;
		};

		this.getElement = function(type, age, id, className, attributes, styles) {
			var cElm = findElement(this.getRoot(this), id);
			if (!isNil(cElm)) {
				cElm.oldClassName = cElm.className;
				cElm.oldAttributes = [];
				for (var atr = 0; atr < cElm.attributes.length; atr++) {
					if (cElm.attributes[atr].name === "points") {
						var oldPoints = [];
						for (var po = 0; po < cElm.attributes[atr].value.length; po++) {
							oldPoints.push(cElm.attributes[atr].value[po]);
						}
						cElm.oldAttributes.push({name: cElm.attributes[atr].name, value: oldPoints});
					} else {
						cElm.oldAttributes.push({name: cElm.attributes[atr].name, value: cElm.attributes[atr].value});
					}
				}
				cElm.oldStyles = [];
				for (var stl = 0; stl < cElm.styles.length; stl++) {
					cElm.oldStyles.push({name: cElm.styles[stl].name, value: cElm.styles[stl].value});
				}
				cElm.className = className;
				cElm.attributes = [];
				if (!isNil(attributes)) {
					for (var attProperty in attributes) {
						if (attributes.hasOwnProperty(attProperty)) {
							cElm.attributes.push({name: attProperty, value: attributes[attProperty]});
						}
					}
				}
				cElm.styles = [];
				for (var styleProperty in styles) {
					if (styles.hasOwnProperty(styleProperty)) {
						cElm.styles.push({name: styleProperty, value: styles[styleProperty]});
					}
				}
				cElm.age = age;
			} else {
				cElm = new eve.bpElement(type, id, className, attributes, styles, this._container);
				cElm.age = 0; //this prevents objects getting deleted! We need a way of incrementing the age on subsequent draws, even if the element isn't referenced - maybe a .drawn property?
				cElm.parent = this;
				this.nodes.push(cElm);
			}
			return cElm;
		};

		this.setAttribute = function(aName, aVal, setOld) {
			//see if it's already there - if it is, update it:
			var done = false;
			var attrs = this.attributes;
			var i, l;
			if (setOld) {
				attrs = this.oldAttributes;
			}
			for (i = 0, l = attrs.length; i < l; i++) {
				var at = attrs[i];
				if (at !== undefined && at.name === aName) {
					at.value = aVal;
					done = true;
				}
			}
			if (!done) {
				attrs.push({name: aName, value: aVal});
			}
			if (this.domElm) {
				this.domElm.setAttribute(aName, aVal);
			}
			return this;
		};
		this.setStyle = function(sName, sVal) {
			//see if it's already there - if it is, update it:
			var done = false;
			var i, l;
			for (i = 0, l = this.styles.length; i < l; i++) {
				var st = this.styles[i];
				if (st !== undefined && st.name === sName) {
					st.value = sVal;
					done = true;
				}
			}
			if (!done) {
				this.styles.push({name: sName, value: sVal});
			}
			if (this.domElm) {
				this.domElm.style[sName] = sVal;
			}
			return this;
		};

		this.getAttribute = function(aName, old) {
			var attrs = this.attributes;
			if (old) {
				attrs = this.oldAttributes;
			}
			for (var i = 0, na = attrs.length; i < na; i++) {
				var att = attrs[i];
				if (att.name === aName) {
					return att.value;
				}
			}
		};

		this.getStyle = function(sName, old) {
			if (old) {
				styles = this.oldStyles;
			}
			for (var i = 0, ns = this.styles.length; i < ns; i++) {
				var st = this.styles[i];
				if (st !== undefined && st.name === sName) {
					return st.value;
				}
			}
		};

		this.removeAttribute = function(aName) {
			for (atCount = 0; atCount < this.attributes.length; atCount++) {
				if (this.attributes[atCount].name === aName) {
					this.attributes.splice(atCount, 1);
				}
			}
		};
		this.removeStyle = function(sName) {
			for (stCount = 0; stCount < this.styles.length; stCount++) {
				if (this.styles[stCount].name === sName) {
					this.styles.splice(stCount, 1);
				}
			}
		};

		this.toSVG = function() {

			var eType = this.type;
			if (eType === "group") {
				eType = "g";
			} else if (eType === "sector") {
				eType = "path";
			}
			var retSVG = document.createElementNS(svgns, eType);
			retSVG.id = this.id;
			if (eve.isArray(this.className)) {
				retSVG.setAttribute("class", this.className.join(" "));
			} else {
				retSVG.setAttribute("class", this.className);
			}

			var textNode;
			if ((this.type === "text" || this.type === "tspan") && (!isNil(this.text) && this.text !== "")) {
				textNode = document.createTextNode(this.text);
				retSVG.appendChild(textNode);
			}

			var sX = null;
			var sY = null;
			var sRot = 0;
			if (!isNil(this.eX)) {
				sX = parseFloat(this.eX);
			} else {
				if (this.type !== "tspan") {
					sX = 0;
				}
			}
			if (!isNil(this.eY)) {
				sY = parseFloat(this.eY);
			} else {
				if (this.type !== "tspan") {
					sY = 0;
				}
			}
			sRot = this.eRot;

			if (this.type !== "circle") {
				if (this.type === "tspan") {
					if (!isNil(sX)) {
						retSVG.setAttribute("x", sX.toFixed(6));
					}
					if (!isNil(sY)) {
						retSVG.setAttribute("y", sY.toFixed(6));
					}
				} else {
					if (!strIs(retSVG.tagName, "defs")) {
						eve.setSvgTransform(retSVG, "translate", [sX, sY]);
						if (!isNil(sRot)) {
							if (eve.isArray(sRot)) {
								eve.setSvgTransform(retSVG, "rotate", sRot);
							} else {
								eve.setSvgTransform(retSVG, "rotate", [sRot]);
							}
						}
					}
				}
				if (this.eWidth) {
					retSVG.setAttribute("width", this.eWidth);
				}
				if (this.eHeight) {
					retSVG.setAttribute("height", this.eHeight);
				}
			} else {
				retSVG.setAttribute("cx", sX.toFixed(6));
				retSVG.setAttribute("cy", sY.toFixed(6));
			}
			if (this.path) {
				retSVG.setAttribute("d", this.path.toSVG());
			} else if (this.sector) {
				retSVG.setAttribute("d", this.sector.toSVG());
				retSVG.setAttribute("fill-rule", "evenodd");
			}

			passProps(this, retSVG);
			var i, l;
			for (i = 0, l = this.attributes.length; i < l; i++) {
				var a = this.attributes[i];
				if (a && a.name !== "text") {
					retSVG.setAttribute(a.name, a.value);
				}
			}
			for (i = 0, l = this.styles.length; i < l; i++) {
				var s = this.styles[i];
				if (s !== undefined) {
					retSVG.style[s.name] = s.value;
				}
			}
			for (var c = 0; c < this.nodes.length; c++) {
				retSVG.appendChild(this.nodes[c].toSVG());
			}

			return retSVG;
		};

	};

	var calcDim = function(dim) {
		var units = eve.getUnits(dim);
		//TODO: I had to put this in because label positions don't get evaluated until they're built, so if it's a percentage
		//we need to know that at that point. Is there a way of doing it better?
		if (units === "%"){
			return dim;
		}
		if (dim === "auto"){
			return dim;
		}
		return valToPx(parseFloat(dim), units);
	};

	var valToPx = function(val, unit) {
		if (!isNil(unit)) {
			unit = unit.toString().toLowerCase();
		} else {
			unit = "";
		}

		var bodyFontSize = 16;
//		if (typeof window !== "undefined"){
//			bodyFontSize = parseFloat(window.getComputedStyle(document.body).fontSize);
//		}
		var dpi = 96; //because we have to assume something
		switch (unit) {
			case "":
				return val;
			case "px":
				return val;
			case "pt":
				return (val / 72) * dpi;
			case "em":
				return val * bodyFontSize;
			case "rem":
				return val * bodyFontSize;
			case "ex":
				return (val * bodyFontSize) / 2;
			case "ch":
				//roughly
				return (val * bodyFontSize) / 2;
			case "mm":
				return (val * dpi) / 25.4;
			case "cm":
				return (val * dpi) / 2.54;
			case "in":
				return val * dpi;
//			case "vh":
//				return document.documentElement.clientHeight * (val / 100.0);
//			case "vw":
//				return document.documentElement.clientWidth * (val / 100.0);
//			case "vmin":
//				return Math.min(document.documentElement.clientHeight, document.documentElement.clientWidth) * (val / 100.0);
//			case "vmax":
//				return Math.max(document.documentElement.clientHeight, document.documentElement.clientWidth) * (val / 100.0);
			case "%":
				return val / 100.0;
		}
	};

	eve.makeText = function(chart, parent, drawMode, tOptions) {

		//measuring the actual height of the text gives us a value way bigger than the actual text - because it includes
		//all possible character ascenders and descenders, apparently. So let's optionally just use the fontsize:
		//var useFontSizeForHeight = true;
		var parentElm;

		var defOpts = {
			x: 0,
			y: 0,
			rotation: 0,
			width: null,
			height: null,
			id: "",
			drawCall: 0,
			holderObj: null,
			string: "",
			className: "eve-chart-text",
			textAlign: "left",
			verticalAlign: "top",
			fontSize: 12,
			//fontFamily: "inherit",
			fontFamily: null,
			fontColour: null,
			fontWeight: null,
			fontStyle: null,
			leading: "auto",
			offsetLeft: 0,
			offsetTop: 0
		};

		var options = tOptions || defOpts;

		options.id = options.id || defOpts.id;
		options.drawCall = options.drawCall || defOpts.drawCall;
		options.holderObj = options.holderObj || defOpts.holderObj;
		options.string = options.string || defOpts.string;
		options.className = options.className || defOpts.className;
		options.textAlign = options.textAlign || defOpts.textAlign;
		options.verticalAlign = options.verticalAlign || defOpts.verticalAlign;
		options.fontSize = options.fontSize || defOpts.fontSize;
		options.fontFamily = options.fontFamily || defOpts.fontFamily;
		options.fontColour = options.fontColour || defOpts.fontColour;
		options.fontWeight = options.fontWeight || defOpts.fontWeight;
		options.fontStyle = options.fontStyle || defOpts.fontStyle;
		options.x = options.x || defOpts.x;
		options.y = options.y || defOpts.y;
		options.rotation = options.rotation || defOpts.rotation;
		options.width = options.width || defOpts.width;
		options.height = options.height || defOpts.height;
		options.leading = options.leading || defOpts.leading;
		options.offsetTop = options.offsetTop || defOpts.offsetTop;
		options.offsetLeft = options.offsetLeft || defOpts.offsetLeft;

		var chartFontNormal = null;
		var chartFontBold = null;
		if (drawMode !== "PDF"){
			if (parent instanceof eve.bpElement) {
				parentElm = get(parent.id, parent._container);
			} else {
				parentElm = parent;
				parent = parentElm._eveElm;
			}
			if (options.parentElm) {
				parentElm = options.parentElm;
				//parent = parentElm._eveElm;
			}
		}
		else{
			if (!isNil(chart.fontNormal)){
				chartFontNormal = chart.fontNormal;
			}
			if (!isNil(chart.fontBold)){
				chartFontBold = chart.fontBold;
			}
		}

		var rFontHeight = 0;
		if (options.fontSize) {
			rFontHeight = parseFloat(options.fontSize);
		}

		var text = options.string.toString() || "";
//		var parentLabel = null;
//		if (options.holderObj) {
//			for (var l = 0; l < options.holderObj._chart._labels.length; l++) {
//				for (var c = 0; c < options.holderObj._chart._labels.length; c++) {
//					if (options.holderObj._chart._labels[l]._children[c] === options.holderObj) {
//						parentLabel = options.holderObj._chart._labels[l];
//						break;
//					}
//				}
//			}
//		}

		if (text.indexOf("$$") !== -1) {
			var lText = "";
			var strLbl = text.split("$$");
			if (strLbl.length > 2) {
				for (var tc = 0; tc < strLbl.length; tc++) {
					//split removes the $$, so even numbers will be the text outside of the eval parts:
					if (eve.isEven(tc)) {
						lText += strLbl[tc];
					} else {
						//odd numbers will be the parts we need to evaluate:
						//get the number of open parentheses:
						var openCount = strLbl[tc].match(/\(/g).length;
						//get the number of close parentheses:
						var closeCount = strLbl[tc].match(/\)/g).length;
						//make sure the string is well-formed:
						if (openCount === closeCount) {
							var strEval = strLbl[tc];
							var params = null;
							var target = null;
							//work through each function from the inside out:
							for (var fc = openCount - 1; fc >= 0; fc--) {
								var func = null;
								//get the parameters:
								var strParams = strEval.substring(strEval.lastIndexOf("(") + 1, strEval.indexOf(")"));
								var strFunction = strEval.substring(0, strEval.lastIndexOf("("));
								if (fc > 0) {
									strFunction = strFunction.substring(strFunction.lastIndexOf("(") + 1);
								}
								//split into an array, preserving commas inside sub-strings:
								params = strParams.match(/(".*?"|[^",\s]+)(?=\s*,|\s*$)/g);

								//see if a parameter is intended to be a variable:
								for (var pa = 0; pa < params.length; pa++) {
									params[pa] = params[pa].trim();
									//split it up if it's in dot-notation:
									//if it's a string, it's not a var:
									if (params[pa].indexOf("'") === 0 && params[pa].lastIndexOf("'") === params[pa].length - 1) {
										params[pa] = params[pa].split("'")[1];
									} else if (params[pa].indexOf("\"") === 0 && params[pa].lastIndexOf("\"") === params[pa].length - 1) {
										params[pa] = params[pa].split("\"")[1];
									}
									//or if it's a number, it's not a var:
									else if (eve.isNumeric(params[pa])) {
										params[pa] = parseFloat(params[pa]);
									} else {
										var pSplit = params[pa].split(".");
										//see if it's targetting the holderObj first:
										var pc;
										if (!isNil(options.holderObj)) {
											target = options.holderObj;
											for (pc = 0; pc < pSplit.length; pc++) {
												if (!isNil(target) && !isNil(target[pSplit[pc]])) {
													target = target[pSplit[pc]];
												} else {
													target = null;
												}
											}
											if (!isNil(target)) {
												if (!isNil(options.holderObj._link) && options.holderObj._link instanceof eve.axis && pSplit[pSplit.length - 1] === "axisVal") {
													params[pa] = options.holderObj.axisVal;
												} else {
													params[pa] = target;
												}
											}
										}
										//if that didn't find anything, check the window:
										if (isNil(target) && typeof window !== "undefined"){
											target = window;
											for (pc = 0; pc < pSplit.length; pc++) {
												target = target[pSplit[pc]];
											}
											if (!isNil(target)) {
												params[pa] = target;
											}
										}
									}
								}
								//try and find the function:

								strFunction = strFunction.trim();
								//split it up if it's in dot-notation:
								var fSplit = strFunction.split(".");
								//see if it's targetting the window first:
								target = null;
								var ff;
								target = window;
								for (ff = 0; ff < fSplit.length; ff++) {
									target = target[fSplit[ff]];
								}
								if (!isNil(target) && typeof target === "function") {
									func = target;
								}
								//if that didn't find anything, check eve:
								if (func === null) {
									target = eve;
									for (ff = 0; ff < fSplit.length; ff++) {
										target = target[fSplit[ff]];
									}
									if (!isNil(target) && typeof target === "function") {
										func = target;
									}
								}

								if (!isNil(func)) {
									var fResult = func.apply(window, params);
									lText += fResult;
									var strEnd = strEval.substring(strEval.indexOf(")") + 1);
									strEval = strEval.substring(0, strEval.lastIndexOf("("));
									strEval = strEval.substring(0, strEval.lastIndexOf("(") + 1) + fResult + strEnd;
								} else {
									//can't find the function - abort:
									console.log("Eve error: malformed string");
									lText += strLbl[tc];
								}
							}
						} else {
							//if not, just add the string to our text:
							console.log("Eve error: malformed string");
							lText += strLbl[tc];
						}
					}
				}
			}
			text = lText;
		}

		//allow <b>, <i>, <br> and <font> tags
		var fontBold = false;
		if (options.fontWeight && (strIs(options.fontWeight, "bold") || options.fontWeight > 400)) {
			fontBold = true;
		}

		var fontItalic = false;
		if (options.fontStyle && (strIs(options.fontStyle, "italic") || strIs(options.fontStyle, "oblique"))) {
			fontItalic = true;
		}

		var textElm = parent.getElement("text", options.drawCall, options.id, options.className);
		textElm.dStyle = {
			lineHeight: "normal"
		};
		if (options.fontFamily) {
			textElm.setStyle("font-family", options.fontFamily);
			textElm.dStyle.fontFamily = options.fontFamily;
		}
		if (options.fontSize) {
			textElm.setStyle("font-size", options.fontSize + "px");
			textElm.dStyle.fontSize = options.fontSize;
		}
		if (fontBold) {
			textElm.setStyle("font-weight", "bold");
			textElm.dStyle.fontWeight = "bold";
		}
		if (options.fontColour) {
			textElm.setStyle("fill", options.fontColour);
			textElm.dStyle.fontColour = options.fontColour;
		}

		textElm.setAttribute("white-space", "nowrap");
		var tAnchor = "left";
		if (strIs(options.textAlign, "centre") || strIs(options.textAlign, "center")) {
			tAnchor = "middle";
		} else if (strIs(options.textAlign, "right")) {
			tAnchor = "end";
		}
		textElm.setAttribute("text-anchor", tAnchor);
		textElm.dStyle.textAnchor = tAnchor;
		var tElms = [];
//		var divHolder = [];
		var tl;
		var te;
		var tLines;
		var oSize = {width: 0, height: 0}; //overall size of all text together

		//split into lines at <br> tags:
		var tRows = splitAtBreaks(text);

		for (tl = 0; tl < tRows.length; tl++) {
			if (tRows[tl] === "") {
				tRows.splice(tl, 1);
			}
		}

		for (tl = 0; tl < tRows.length; tl++) {
			tElms[tl] = splitParse(tRows[tl]);
		}

		tLines = [];
		for (tl = 0; tl < tElms.length; tl++) {
			tLines[tl] = [];
			var lHeight = options.fontSize;
			var lWidth = 0;
			for (te = 0; te < tElms[tl].length; te++) {
				var tObj = tElms[tl][te].clone();
				tObj.height = tObj.height = 0;
				tObj.fontColour = tElms[tl][te].colour;
				if (isDefault(tObj.fontColour)){
					tObj.fontColour = options.fontColour;
				}
				var fontLeading = tElms[tl][te].leading;
				if (!isNil(fontLeading) && fontLeading !== "" && fontLeading !== "auto") {
					tObj.fontLeading = fontLeading;
				}
				if (!isNil(tElms[tl][te].size) && tElms[tl][te].size !== "") {
					tObj.fontSize = tElms[tl][te].size;
					if (tObj.fontSize === "auto"){
						tObj.fontSize = rFontHeight;
					}
					if (lHeight < parseFloat(tObj.fontSize)) {
						lHeight = parseFloat(tObj.fontSize);
					}
				}
				if (!isNil(tElms[tl][te].face) && tElms[tl][te].face !== "") {
					tObj.fontFamily = tElms[tl][te].face;
				}
				var tFontFamily = tObj.fontFamily;
				if (!tFontFamily) {
					tFontFamily = options.fontFamily;
				}
				var tFontSize = tObj.fontSize;
				if (!tFontSize) {
					tFontSize = options.fontSize;
				}
				var tBold = tObj.bold;
				if (!tBold) {
					tBold = fontBold;
				}
				var tItalic = tObj.italic;
				if (!tItalic) {
					tItalic = fontItalic;
				}

				// var tSize = textSize(tObj.text, tFontFamily, tFontSize, tBold, tItalic, drawMode, parent._container);
				var tSize = textSize(tObj.text, tFontFamily, tFontSize, tBold, tItalic, 0, drawMode, parent._container, chartFontNormal, chartFontBold);

				tObj.width = tSize.width;
				tObj.height = tSize.height;
				// when we have multiple text elements that should be aligned on the baseline, nudging them based on their actual height
				// can lead to them being misaligned with one another, so when the vertical alignment is "bottom", use the font height instead:
				if (options.verticalAlign === "bottom"){
					tObj.height = rFontHeight * 0.95;
				}

				lWidth += tObj.width;
				if (lHeight < options.fontSize) {
					lHeight = options.fontSize;
				}
				// tLines[tl].height = lHeight;
				if (isNil(tLines[tl].height) || tLines[tl].height < tObj.height){
					tLines[tl].height = tObj.height;
				}
				tLines[tl].width = lWidth;
				tLines[tl].push(tObj);
				if (oSize.width < lWidth) {
					oSize.width = lWidth;
				}
			}
		}

		var tBaseline = 0;
		var lBottom = 0;
		var tsCount = 0;
		for (tl = 0; tl < tElms.length; tl++) {

			if (tl === 0) {
				tBaseline = tLines[tl].height; // * 0.5;
				lBottom = tLines[tl].height; // * 0.5;
			} else {
				if (tLines[tl - 1][0].fontLeading){
					tBaseline += parseFloat(tLines[tl - 1][0].fontLeading) + parseFloat(tLines[tl].height);
					lBottom += parseFloat(tLines[tl - 1][0].fontLeading) + parseFloat(tLines[tl].height);
				}
				else if (options.leading === "normal" || options.leading === "auto" || options.leading === "inherit") {
					//45% of the font height works well as default leading:
					tBaseline += parseFloat(rFontHeight) * 1.45;
					lBottom += parseFloat(rFontHeight) * 1.45;
				}
				else {
					tBaseline += parseFloat(options.leading) + parseFloat(tLines[tl].height);
					lBottom += parseFloat(options.leading) + parseFloat(tLines[tl].height);
				}
			}

			for (te = 0; te < tLines[tl].length; te++) {
				tsCount++;

				tLines[tl][te].tspan = textElm.getElement("tspan", options.drawCall, options.id + "_tspan" + tsCount, "", {alignmentBaseline: "alphabetic", textAnchor: tAnchor});
				if (tLines[tl][te].fontColour) {
					tLines[tl][te].tspan.setStyle("fill", tLines[tl][te].fontColour);
				}
				if (tLines[tl][te].fontSize) {
					tLines[tl][te].tspan.setStyle("font-size", parseFloat(tLines[tl][te].fontSize) + "px");
				}
				if (tLines[tl][te].fontFamily) {
					tLines[tl][te].tspan.setStyle("font-family", tLines[tl][te].fontFamily);
				}
				if (tLines[tl][te].bold) {
					tLines[tl][te].tspan.setStyle("font-weight", "bold");
				}
				if (tLines[tl][te].italic) {
					tLines[tl][te].tspan.setStyle("font-style", "italic");
				}

				tLines[tl][te].tspan.text = tLines[tl][te].text;
				tLines[tl][te].tspan.dStyle = {line: tl, y: tBaseline};
				if (te === 0) {
					tLines[tl][te].tspan.eX = 0;
					tLines[tl][te].tspan.eY = tBaseline;
//					tLines[tl][te].tspan.eY = 0;
				} else {
					tLines[tl][te].tspan.eX = null;
					tLines[tl][te].tspan.eY = null;
				}
			}
		}

		oSize.height = lBottom;
		textElm.width = options.width || oSize.width;
		textElm.height = oSize.height;

		textElm.eX = options.x;
		textElm.eY = options.y;

		if (strIs(options.verticalAlign, "middle")) {
			textElm.eY -= oSize.height / 2;
		} else if (strIs(options.verticalAlign, "bottom")) {
			textElm.eY -= oSize.height;
		}

//		if (drawMode === "PDF"){
//
//		}
//		else{
//			//the measured text height generally comes back too large - fudge it:
//			// var fudgeTop = 0.2;
//			// textElm.nudgeTop = options.fontSize * fudgeTop;
//			// textElm.eY -= textElm.nudgeTop;
//		}

		if (options.rotation !== 0) {
			var rX = parseFloat(options.offsetLeft);
			var rY = options.offsetTop;
			if (strIs(options.verticalAlign, "middle")) {
				rY = (oSize.height / 2) + parseFloat(options.offsetTop);
			}
			else if (strIs(options.verticalAlign, "top")) {
				rY = oSize.height + parseFloat(options.offsetTop);
			}
			textElm.eRot = [options.rotation, rX, rY];
		}
		else{
			if (drawMode === "PDF"){
				// textElm.eY -= parent._container._font.ascender / 1000 * options.fontSize;
			}
			// n._container.y = dims.y - n._container._font.ascender / 1000 * tAttrs.size;
			if (textElm.eRot !== 0){
				textElm.eRot = [0, 0, 0];
			}
		}

		textElm.setAttribute("dx", options.offsetLeft);
		textElm.setAttribute("dy", options.offsetTop);
		textElm.dStyle = {offsetTop: options.offsetTop, offsetLeft: options.offsetLeft};
		textElm._buildOptions = options;
		return textElm;

	};

	eve.chart = function(container, type, x, y, width, height, drawMode, chartID) {

		eve.setObjProps(this, true);

		this._width = this.width = width;
		this._height = this.height = height;
		this._x = this.x = x;
		this._y = this.y = y;
		this._id = this.id = chartID || addEveID(chartID, this._idPrefix, this);
		this._drawMode = this.drawMode = drawMode || "SVG";
		this._container = this.container = container;
		if (typeof container === "string"){
			this._container = this.container = get(container);
		}
		this._type = this.type = getType(type) || "XY";
		this.mediaProps = this._mediaProps;

		var inheritedCSS;
		if (this.drawMode === "PDF"){
			this.fontFamily = this.container._font.name;
		}
		else {
			inheritedCSS = window.getComputedStyle(document.body);
			if (!isNil(this.container)) {
				inheritedCSS = window.getComputedStyle(this.container);
			}
			this.fontFamily = inheritedCSS.fontFamily;
		}

		this.getData = function(duration, rebuild, rejigOnly) {
			if (isNil(this._data) && this._series.length < 1) {
				this._data = dataDefault;
				this._title = "Reasons for running Internet Explorer";
				this._series = [this.addSeries("bar", this._data)];
			}

			this._domains = []; //list of all property names
			this._propCats = []; //list of categories (for "banded" values)
			this._propMax = []; //list of max values for each property
			this._propMin = []; //list of min values for each property
			this._propMaxAdditive = []; //list of max values for each property
			this._propMinAdditive = []; //list of min values for each property

			var pNames = [];
			var dc, nc, sc, xc;
			var s;

			for (sc = 0; sc < this._series.length; sc++) {
				s = getEveElmByID(this._series[sc]);
				if (!isNil(s._data)) {

					eve.setObjProps(s);
					if (eve.isArray(s._data)) {

						var sData = s._data;

						if (s._type === "funnel") { // && eve.isArray(s._data)[0]){

							s._groupedData = null;
							var slice;
							//assume our first property is the repeater and the second is the values
							var pRepeater = null;
							var pVals = null;

							if (isNil(s._fringe)) {
								s._fringe = 0;
							}

							//where we've got multiple funnels that all use the same data, we don't want to have to do this over and over, so if we've done it already, use the previous data:
							for (var ps = 0; ps < sc; ps++) {
								var sp = getEveElmByID(this._series[ps]);
//								if (eve.isEqual(s._data, sp._data)) {
								if (s._data === sp._data) {
									s._groupedData = sp._groupedData;
//									slice = sp._groupedData.clone(true);
									slice = sp._groupedData.cloneOneLevel();
									break;
								}
							}

							var dataObj = s._data[0];
							if (eve.isArray(s._data[0])) {
								dataObj = s._data[0][0];
							}

							for (var prop in dataObj) {
								if (dataObj.hasOwnProperty(prop)) {
									if (isNil(pRepeater)) {
										pRepeater = prop;
									} else if (isNil(pVals)) {
										pVals = prop;
									}
								}
							}

							var sl;
							if (eve.isArray(s._data[0])) {
								if (s._groupedData === null) {
									slice = [];
									for (dc = 0; dc < s._data.length; dc++) {
										for (var y = 0; y < s._data[dc].length; y++) {
											if (isNil(slice[s._data[dc][y][pRepeater]])) {
												slice[s._data[dc][y][pRepeater]] = [];
											}
											slice[s._data[dc][y][pRepeater]].push(s._data[dc][y][pVals]);
										}
									}
									for (sl = 0; sl < slice.length; sl++) {
										slice[sl].sort(eve.sortNumber);
									}
//									s._groupedData = slice.clone(true);
									s._groupedData = slice.cloneOneLevel();
//									s._groupedData = slice;
								}

								if (s._fringe > 0) {
									for (sl = 0; sl < slice.length; sl++) {
										var lopOff = parseInt(slice[sl].length * (s._fringe / 2));
										slice[sl].splice(slice[sl].length - lopOff);
										slice[sl].splice(0, lopOff);
									}
								}

								s._fdata = [];
								for (sl = 0; sl < slice.length; sl++) {
									s._fdata[sl] = {};
									s._fdata[sl][pRepeater] = sl;
									s._fdata[sl][pVals] = slice[sl][0];
									s._fdata[(slice.length * 2) - (sl + 1)] = {};
									s._fdata[(slice.length * 2) - (sl + 1)][pRepeater] = sl;
									s._fdata[(slice.length * 2) - (sl + 1)][pVals] = slice[sl][slice[sl].length - 1];
								}
							} else {
								s._fdata = s._data.clone(true);
							}
							if (s._shapeNodes){
								s._nodes = [];
								s.updateNodes();
							}
							sData = s._fdata;

						}

						for (dc = 0; dc < sData.length; dc++) {
							pNames = getPropNames(sData[dc]);
							for (nc = 0; nc < pNames.length; nc++) {
								if (this._domains.indexOf(pNames[nc]) === -1) {
									this._domains.push(pNames[nc]);
									this._propCats.push([]);
									this._propMax.push(null);
									this._propMin.push(null);
								}
								if (s._domains.indexOf(pNames[nc]) === -1) {
									s._domains.push(pNames[nc]);
								}
							}
							for (xc = 0; xc < this._domains.length; xc++) {
								var vals = [];
								if (eve.isObject(sData[dc][this._domains[xc]])){
									for (var key in sData[dc][this._domains[xc]]) {
										if (Object.prototype.hasOwnProperty.call(sData[dc][this._domains[xc]], key)) {
												vals.push(sData[dc][this._domains[xc]][key]);
											}
									}
								}
								else{
									vals = [sData[dc][this._domains[xc]]];
								}
								if (eve.isNumeric(vals[0])) {
									for (var v = 0; v < vals.length; v++){
										if (vals[v] > this._propMax[xc] || isNil(this._propMax[xc])) {
											this._propMax[xc] = vals[v];
										}
										if (vals[v] < this._propMin[xc] || isNil(this._propMin[xc])) {
											this._propMin[xc] = vals[v];
										}
									}
								}
								if (this._propCats[xc].indexOf(sData[dc][this._domains[xc]]) === -1) {
									this._propCats[xc].push(sData[dc][this._domains[xc]]);
								}
							}
						}
					} else {
						pNames = getPropNames(s._data);
						for (nc = 0; nc < pNames.length; nc++) {
							if (this._domains.indexOf(pNames[nc]) === -1) {
								this._domains.push(pNames[nc]);
								this._propCats.push([]);
								this._propMax.push(0);
								this._propMin.push(0);
							}
							if (s._domains.indexOf(pNames[nc]) === -1) {
								s._domains.push(pNames[nc]);
							}
						}
						for (xc = 0; xc < this._domains.length; xc++) {
							if (eve.isNumeric(s._data[this._domains[xc]])) {
								if (s._data[this._domains[xc]] > this._propMax[xc] || isNil(this._propMin[xc])) {
									this._propMax[xc] = s._data[this._domains[xc]];
								}
								if (s._data[this._domains[xc]] < this._propMin[xc] || isNil(this._propMax[xc])) {
									this._propMin[xc] = s._data[this._domains[xc]];
								}
							} else {
								if (this._propCats[xc].indexOf(s._data[this._domains[xc]] === -1)) {
									this._propCats[xc].push(s._data[this._domains[xc]]);
								}
							}
						}
					}
				}
			}

			for (xc = 0; xc < this._domains.length; xc++) {
				this._propMinAdditive[xc] = 0;
				this._propMaxAdditive[xc] = 0;
				for (sc = 0; sc < this._series.length; sc++) {
					s = getEveElmByID(this._series[sc]);
					if (!isNil(s._data)) {
						var max = null;
						var min = null;
						for (dc = 0; dc < s._data.length; dc++) {
							if (eve.isNumeric(s._data[dc][this._domains[xc]])) {
								if (s._data[dc][this._domains[xc]] > max || isNil(max)) {
									max = s._data[dc][this._domains[xc]];
								}
								if (s._data[dc][this._domains[xc]] < min || isNil(min)) {
									min = s._data[dc][this._domains[xc]];
								}
							}
						}
						this._propMinAdditive[xc] += min;
						this._propMaxAdditive[xc] += max;
					}
				}
			}

			if (s._type === "curve"){
				s.updateNodes();
			}
			this.checkData(duration, rebuild, rejigOnly);
		};

		this.addLabel = function(id) {
			this._lblCount++;
			var newLabel = new eve.label(this, id);
			this._labels.push(newLabel._id);
			return newLabel;
		};

		this.addAxis = function(position, type, domain, label, animIn) {
			var newAxis = new eve.axis(this, position, type);
			if (!isNil(position)){
				newAxis.position = newAxis._position = position.toLowerCase();
			}
			newAxis._horizontal = newAxis._position === "top" || newAxis._position === "bottom";
			newAxis._type = type;
			newAxis._domain = domain;
			newAxis._label = label || "auto|";
			newAxis._animBirth = animIn || new eve.elmAnim();
			this._axes.push(newAxis._id);
			if (isNil(this._xAxis) && newAxis._horizontal) {
				newAxis._origin = "left";
				if (!isNil(this._yAxis)) {
					newAxis._origin = this._yAxis._position;
				}
				this._xAxis = newAxis._id;
			}
			if (isNil(this._yAxis) && !newAxis._horizontal) {
				newAxis._origin = "bottom";
				if (!isNil(this._xAxis)) {
					newAxis._origin = this._xAxis._position;
				}
				this._yAxis = newAxis._id;
			}
			return newAxis;
		};

		this.addSeries = function(type, data, axisX, axisY) {
			var newSeries = new eve.series(this, type, data, axisX, axisY);
			this._series.push(newSeries._id);
			return newSeries;
		};

		this.addBlueprint = function(domElm) {
			var newBlueprint = new eve.blueprint(this._container);
			if (domElm) {
				newBlueprint.domElm = domElm;
				newBlueprint.id = domElm.id;
			}
			return newBlueprint;
		};

		this.checkData = function(duration, rebuild, rejigOnly) {
			// this.getData();

			var ac, sc, bc, xc;
			var a, s;
			if (isNil(this._container) && this._drawMode !== "PDF") {
				this._container = document.createElement("div");
				this._container.style.width = this._width + this._units;
				this._container.style.height = this._height + this._units;
				this._container.style.background = "white";
				this._container.style.zIndex = 10;
				document.body.appendChild(this._container);
			}
			if (isNil(this._blueprint)) {
				this._blueprint = this.addBlueprint(this._artboard);
			}
			if (this._series.length < 1) {
				this._series = [this.addSeries("bar", this._data)];
			}

			if (this._type === "XY") {

				var needsAxes = false;
				var domainNames = [];
				if (isNil(this._xAxis) || isNil(this._yAxis)) {
					for (sc = 0; sc < this._series.length; sc++) {
						s = getEveElmByID(this._series[sc]);
						if (s._type !== "sector") { //it needs X and Y axes
							needsAxes = true;
							for (xc = 0; xc < s._domains.length; xc++) {
								if (domainNames.indexOf(s._domains[xc]) === -1) {
									domainNames.push(s._domains[xc]);
								}
							}
						}
					}
				}

				if (needsAxes) {
					for (ac = 0; ac < this._axes.length; ac++) {
						domainNames.remove(getEveElmByID(this._axes[ac])._domain);
					}
					if (isNil(this._xAxis)) {
						if (domainNames.length < 1) {
							this._xAxis = this.addAxis("bottom", "linear", "X");
						} else {
							if ((eve.isArray(getEveElmByID(this._series[0])._data[0]) && propIsNumeric(getEveElmByID(this._series[0])._data[0][0], domainNames[0])) || propIsNumeric(getEveElmByID(this._series[0])._data[0], domainNames[0])) {
								this._xAxis = this.addAxis("bottom", "linear", domainNames[0]);
							} else {
								if (propIsNumeric(getEveElmByID(this._series[0])._data[0], domainNames[0])) {
									this._xAxis = this.addAxis("bottom", "linear", domainNames[0]);
								} else {
									this._xAxis = this.addAxis("bottom", "band", domainNames[0]);
									this._xAxis._numBands = "auto|" + getEveElmByID(this._series[0])._data.length.toString();
									for (bc = 0; bc < getAutoNum(getEveElmByID(this._axes[ac])._numBands, true); bc++) {
										getEveElmByID(this._xAxis)._bands[bc] = {_label: this._propCats[this._domains.indexOf(domainNames[0][bc])], _labelPos: "centre", _labelOffsetX: 0, _labelOffsetY: 0, _tickPos: "end", _start: null, _end: null};
									}
								}
							}
							domainNames.remove(domainNames[0]);
						}
					}
					if (isNil(this._yAxis)) {
						if (domainNames.length < 1) {
							this._yAxis = this.addAxis("left", "linear", "Y");
						} else {
							if ((eve.isArray(getEveElmByID(this._series[0])._data[0]) && propIsNumeric(getEveElmByID(this._series[0])._data[0][0], domainNames[1])) || propIsNumeric(getEveElmByID(this._series[0])._data[0], domainNames[0])) {
								this._yAxis = this.addAxis("left", "linear", domainNames[0]);
							} else {
								this._yAxis = this.addAxis("left", "band", domainNames[0]);
								this._yAxis._numBands = "auto|" + getEveElmByID(this._series[0])._data.length.toString();
								var na = getAutoNum(getEveElmByID(this._yAxis)._numBands, true);
								for (bc = 0; bc < na; bc++) {
									getEveElmByID(this._xAxis)._bands[bc] = {
										_label: this._propCats[this._domains.indexOf(domainNames[0][bc])],
										_value: this._propCats[this._domains.indexOf(domainNames[0][bc])]
									};
								}
							}
						}
					}
				}

				for (sc = 0; sc < this._series.length; sc++) {
					s = getEveElmByID(this._series[sc]);
					if (isNil(s._axisX)) {
						for (ac = 0; ac < this._axes.length; ac++) {
							a = getEveElmByID(this._axes[ac]);
							if (a._horizontal) {
								for (xc = 0; xc < s._domains.length; xc++) {
									if (a._domain === s._domains[xc]) {
										s._axisX = a._id;
										break;
									}
								}
							}
						}
						if (isNil(s._axisX)) {
							s._axisX = getEveElmByID(this._xAxis)._id;
						}
					}
					if (isNil(s._axisY)) {
						for (ac = 0; ac < this._axes.length; ac++) {
							a = getEveElmByID(this._axes[ac]);
							if (!a._horizontal) {
								for (xc = 0; xc < s._domains.length; xc++) {
									if (a._domain === s._domains[xc]) {
										s._axisY = a._id;
										break;
									}
								}
							}
						}
						if (isNil(s._axisY)) {
							s._axisY = this._yAxis;
						}
					}
					//set the base axis to the x-axis:
					s._baseAxis = s._axisX;
					if (getEveElmByID(s._axisX)._type !== "band" && getEveElmByID(s._axisY)._type === "band") {
						//if the x-axis is not a category axis but the y-axis is, set the base axis to the y-axis:
						s._baseAxis = s._axisY;
					}
				}

				for (ac = 0; ac < this._axes.length; ac++) {
					a = getEveElmByID(this._axes[ac]);
//					eve.setObjProps(a, true, true, true);
					eve.setObjProps(a);
					if (a._type === "band") {
						if (a._subTicks.indexOf("auto") === 0) {
							a._useSubTicks = 0;
						}
						if (this._domains.indexOf(a._domain) === -1 && this._type !== "label-band") {
							console.log("Eve Error: Could not find axis domain \"" + a._domain + "\" in the data.");
						}
						else {
							if (this._propCats[this._domains.indexOf(a._domain)].length > 0) {
								if (a._numBands.toString().indexOf("auto|") === 0) {
									a._numBands = "auto|" + this._propCats[this._domains.indexOf(a._domain)].length.toString();
								}
								var bandWidth = 1 / getAutoNum(a._numBands, true);
								a._bands = [];

								var bLabels = [];
								var bco;
								for (bco = 0; bco < getAutoNum(a._numBands, true); bco++) {
									bLabels.push(this._propCats[this._domains.indexOf(a._domain)][bco]);
								}
								if (!isNil(a.bandLabels)) {
									a._bandLabels = a.bandLabels;
									bLabels = eve.fillArray(a.bandLabels, getAutoNum(a._numBands, true) + 1);
								}
								if (!isNil(a._m_bandLabels)) {
									a._bandLabels = a._m_bandLabels;
									bLabels = eve.fillArray(a._m_bandLabels, getAutoNum(a._numBands, true) + 1);
								}
								for (bc = 0; bc < getAutoNum(a._numBands, true); bc++) {
									var startPos = bandWidth * bc;
									var endPos = startPos + bandWidth;
									a._bands[bc] = {_label: bLabels[bc], _value: this._propCats[this._domains.indexOf(a._domain)][bc], _labelPos: "centre", _labelOffsetX: 0, _labelOffsetY: 0, _tickPos: "end", _start: startPos, _end: endPos};
								}
								// c._axes[axCount]._rangeMax = c._axes[axCount]._range = getAutoNum(c._axes[axCount]._numBands, true);
								a.autoRange();
							}
							else {
								a.autoTicks();
								a.addBandLabels();
							}
						}
					}
					else {
						a.autoTicks();
						a.addBandLabels();
					}
				}
				if (getEveElmByID(this._xAxis)._rangeMin < 0 && getEveElmByID(this._xAxis)._rangeMax > 0) {
					this._plotOrigin.x = (Math.abs(getEveElmByID(this._xAxis)._rangeMin) / getEveElmByID(this._xAxis)._range) * this._plotWidth;
				}
				if (getEveElmByID(this._yAxis)._rangeMin < 0 && getEveElmByID(this._yAxis)._rangeMax > 0) {
					this._plotOrigin.y = (Math.abs(getEveElmByID(this._yAxis)._rangeMin) / getEveElmByID(this._yAxis)._range) * this._plotHeight;
				}

				for (ac = 0; ac < this._axes.length; ac++) {
					a = getEveElmByID(this._axes[ac]);
					if (!a._horizontal) {
						//y-axis:
						if (!isNil(this._xAxis)) {
							if (isNil(a.origin)) {
								a._origin = getEveElmByID(this._xAxis)._position;
							}
						}
					} else {
						//x-axis:
						if (!isNil(this._yAxis)) {
							if (isNil(a.origin)) {
								a._origin = getEveElmByID(this._yAxis)._position;
							}
						}
					}
					for (bc = 0; bc < a._bands.length; bc++) {
						if (isNil(a._bands[bc]._labelOffsetX)) {
							a._bands[bc]._labelOffsetX = 0;
						}
						if (isNil(a._bands[bc]._labelOffsetY)) {
							a._bands[bc]._labelOffsetY = 0;
						}
					}
				}
			}
			this.continueBuild(duration, rebuild, rejigOnly);
		};

		this.initAnims = function(duration, rebuild) {
			//AXES AND GRIDLINES:
			var animTime = 0;
			var animAxes = 0;
			var prop;
			var ac, sc, lc;
			var a, s, i, l;

			var lgdRatio = 0;
			if(this._showLegend){
				for (sc = 0; sc < this._series.length; sc++){
					s = getEveElmByID(this._series[sc]);
					if (s._showLegend && s._legend !== ""){
						lgdRatio = this._animInRatios.legend;
					}
				}
			}
			var lblRatio = 0;
			if(this._labels.length > 0){
				lblRatio = this._animInRatios.overlays;
			}
			var axsRatio = 0;
			if(this._axes.length > 0){
				axsRatio = this._animInRatios.axes;
			}
			var totRatios = this._animInRatios.series + axsRatio + lgdRatio + lblRatio;

			this._animInRatios.series = this._animInRatios.series / totRatios;
			this._animInRatios.axes = axsRatio / totRatios;
			this._animInRatios.legend = lgdRatio / totRatios;
			this._animInRatios.overlays = lblRatio / totRatios;

			var animSeriesTime = this._animInRatios.series * duration;
			var animAllSeries = false;
			for (ac = 0; ac < this._axes.length; ac++) {
				a = getEveElmByID(this._axes[ac]);
				if (a._animBirth.mode === null) {
					a._animBirth.mode = "draw";
				}
				if (a._animAdd.mode === null) {
					a._animAdd.mode = a._animBirth.mode;
				}
				if (a._animChange.mode === null) {
					a._animChange.mode = "move";
				}
				if (a._animDeath.mode === null) {
					a._animDeath.mode = "fade and remove";
				}
				if (a._animCurrent.ease === null) {
					a._animCurrent.ease = "easeOut";
				}

				if (!a._visible) {
					a._redrawRequired = false;
				} else {
					if (this._drawCall === 1 || rebuild) {
						a._redrawRequired = true;
						a._animCurrent = a._animBirth;
						if (isNil(a._animCurrent.delay) || rebuild) {
							a._animCurrent.delay = animTime;
						}
						if (isNil(a._animCurrent.duration) || rebuild) {
							a._animCurrent.duration = duration * (this._animInRatios.axes / this._axes.length);
						}
						a._age = 0;
						animAxes++;
					} else {
						if (!a.redrawQueued) {
							if (a._age === 0) {
								a._redrawRequired = true;
								a._animCurrent.mode = a._animAdd.mode;
								animAxes++;
							} else if (a._age < this._drawCall) {
								a._redrawRequired = true;
								a._age = -1;
								a._animCurrent.mode = a._animDeath.mode;
								animAxes++;
							} else {
								if (a._rangeMin !== a._oldProps.rangeMin || a._rangeMax !== a._oldProps.rangeMax || a._origin !== a._oldProps.origin || a._position !== a._oldProps.position || a._domain !== a._oldProps.domain || a._type !== a._oldProps.type) {
									animAllSeries = true;
								}
								a._redrawRequired = false;
								a._animCurrent = new eve.elmAnim();
								for (prop in a._oldProps) {
									if (!eve.isEqual(a["_" + prop], a._oldProps[prop])) {
										a._redrawRequired = true;
										a._animCurrent = a._animChange;
										animAxes++;
										break;
									}
								}
							}
						}
					}
				}
			}

			for (ac = 0; ac < this._axes.length; ac++) {
				a = getEveElmByID(this._axes[ac]);
				if (a._redrawRequired) {
					a._animCurrent.delay = animTime;
					a._animCurrent.duration = duration * (this._animInRatios.axes / animAxes);
					if (isNil(a._animCurrent.ease)) {
						a._animCurrent.ease = "easeOut";
					}
					animTime += a._animCurrent.duration;
				}
				if (a._age !== -1) {
					a._age = this._drawCall + 1;
				}

			}

			//SERIES
//			var animSeries = 0;
			if (animAxes === 0) {
				animSeriesTime += (this._animInRatios.axes * duration);
				animTime = 0;
			}
			for (sc = 0; sc < this._series.length; sc++) {
				s = getEveElmByID(this._series[sc]);
				var sBaseAxis = getEveElmByID(s._baseAxis);
				var lAxis = getEveElmByID(s._axisX);
				if (lAxis === sBaseAxis) {
					lAxis = getEveElmByID(s._axisY);
				}
				if (isNil(s._animBirth.mode) || s._animBirth.mode === "") {
					if (s._type === "bar") {
						s._animBirth.mode = "growUp";
						if (sBaseAxis._position === "top") {
							s._animBirth.mode = "growDown";
						} else if (sBaseAxis._position === "left") {
							s._animBirth.mode = "growRight";
						} else if (sBaseAxis._position === "right") {
							s._animBirth.mode = "growLeft";
						}
					} else if (s._type === "shape" || s._type === "funnel") {
						s._animBirth.mode = "revealUp";
						if (lAxis._position === "top") {
							s._animBirth.mode = "revealDown";
						} else if (lAxis._position === "left") {
							s._animBirth.mode = "revealRight";
						} else if (lAxis._position === "right") {
							s._animBirth.mode = "revealLeft";
						}
					} else if (s._type === "pod") {
						s._animBirth.mode = "revealUp";
						if (sBaseAxis._position === "top") {
							s._animBirth.mode = "revealDown";
						} else if (sBaseAxis._position === "left") {
							s._animBirth.mode = "revealRight";
						} else if (sBaseAxis._position === "right") {
							s._animBirth.mode = "revealLeft";
						}
					} else if (s._type === "scatter") {
						s._animBirth.mode = "fade in";
					} else if (s._type === "line" || s._type === "linearea" || s._type === "curve") {
						s._animBirth.mode = "draw";
					} else {
						s._animBirth.mode = "fade in";
					}
				}

				if (isNil(s._animAdd.mode) || s._animAdd.mode === "") {
					s._animAdd.mode = s._animBirth.mode;
				}
				if (isNil(s._animChange.mode) || s._animChange.mode === "") {
					s._animChange.mode = "morph";
					if (s._type === "scatter") {
						s._animChange.mode = "move";
					}
				}

				if (isNil(s._animDeath.mode) || s._animDeath.mode === "") {
					s._animDeath.mode = "fade and remove";
				}

				if (isNil(s._animCurrent.ease)) {
					s._animCurrent.ease = "easeOut";
				}

				if (!s._visible) {
					s._redrawRequired = false;
				} else {
					if (this._drawCall === 1 || rebuild) {
						s._redrawRequired = true;
						s._animCurrent = cloneEveObj(s._animBirth);
						if (!isNil(s.animBirth) && s.animBirth.mode) {
							s._animCurrent = cloneEveObj(s.animBirth);
						}
						s._age = 0;
//						animSeries++;
					} else {
						if (!s._redrawQueued) {
							if (s._age === 0) {
								s._redrawRequired = true;
								s._animCurrent.mode = s._animAdd.mode;
								if (s.animAdd.mode) {
									s._animCurrent = cloneEveObj(s.animAdd);
								}
//								animSeries++;
							} else if (s._age < this._drawCall) {
								s._redrawRequired = true;
								s._age = -1;
								s._animCurrent.mode = s._animDeath.mode;
								if (s.animDeath.mode) {
									s._animCurrent = cloneEveObj(s.animDeath);
								}
//								animSeries++;
							} else {
								if (animAllSeries) {
									s._redrawRequired = true;
									s._animCurrent = new eve.elmAnim(s._animChange.mode);
//									animSeries++;
								} else {
									s._redrawRequired = false;
									s._animCurrent = new eve.elmAnim();
									for (prop in s._oldProps) {
										if (!eve.isEqual(s["_" + prop], s._oldProps[prop])) {
											s._redrawRequired = true;
											s._animCurrent.mode = s._animChange.mode;
//											animSeries++;
											break;
										}
									}
								}
							}
						}
					}
				}
			}

			//animate all existing series together after first draw - it looks weird otherwise:
			var prevSeries = 0;
			var animChunk = 0;
			if (this._drawCall > 1 && !rebuild) {
				//count the number of existing series':
				for (sc = 0; sc < this._series.length; sc++) {
					s = getEveElmByID(this._series[sc]);
					if (s._redrawRequired && s._age > 1) {
						prevSeries++;
					}
				}
				animChunk = prevSeries;
				for (sc = 0; sc < this._series.length; sc++) {
					s = getEveElmByID(this._series[sc]);
					if (s._redrawRequired) {
						if (s._age === 1) {
							s._animAt = animChunk;
							animChunk++;
						} else {
							s._animAt = 0;
						}
					} else {
						s._animAt = -1;
					}
				}
			} else {
				animChunk = this._series.length;
				for (sc = 0; sc < this._series.length; sc++) {
					s = getEveElmByID(this._series[sc]);
					s._animAt = sc;
				}
			}

			animChunk -= 1;
			if (animChunk < 1) {
				animChunk = 1;
			}
			var seriesDuration = animSeriesTime / animChunk;

			for (sc = 0; sc < this._series.length; sc++) {
				s = getEveElmByID(this._series[sc]);
				if (s._redrawRequired) {
					if (isNil(s._animCurrent.delay)) {
						s._animCurrent.delay = animTime + (seriesDuration * s._animAt);
					}
					if (isNil(s._animCurrent.duration)) {
						s._animCurrent.duration = seriesDuration;
					}
				}
				if (s._age !== -1) {
					s._age = this._drawCall + 1;
				}
			}
			animTime += animSeriesTime;

			//LABELS
			var animLabels = 0;
			var lbl, lblLink, lblLinkSeries;
			for (i = 0, l = this._labels.length; i < l; i++) {
				lbl = getEveElmByID(this._labels[i]);
				lblLink = getEveElmByID(lbl._link);
				if (!isNil(lblLink)){
					lblLinkSeries = getEveElmByID(lblLink._series);
				}

				lbl._animCurrent = new eve.elmAnim();
				if (isNil(lbl._animBirth.mode) || lbl._animBirth.mode === "") {
					lbl._animBirth.mode = "fade in";
				}
				if (isNil(lbl._animAdd.mode) || lbl._animAdd.mode === "") {
					lbl._animAdd.mode = lbl._animBirth.mode;
				}
				if (isNil(lbl._animChange.mode) || lbl._animChange.mode === "") {
					lbl._animChange.mode = "move";
				}
				if (isNil(lbl._animDeath.mode) || lbl._animDeath.mode === "") {
					lbl._animDeath.mode = "fade and remove";
				}

				if (isNil(lbl._animCurrent.ease)) {
					lbl._animCurrent.ease = "easeOut";
				}
				if (this._drawCall === 1 || lbl._age === null || rebuild) {
					lbl._animCurrent = cloneEveObj(lbl._animBirth);
					lbl._redrawRequired = true;
					animLabels++;
				} else {
					if (!lbl._redrawQueued) {
						if (lbl._age === null) {
							lbl._redrawRequired = true;
							lbl._animCurrent.mode = lbl._animAdd.mode;
							animLabels++;
						} else if (lbl._age < this._drawCall && lbl._age !== 0) {
							lbl._redrawRequired = true;
							lbl._age = -1;
							lbl._animCurrent.mode = lbl._animDeath.mode;
							animLabels++;
						} else {
							lbl._redrawRequired = false;
							lbl._animCurrent = new eve.elmAnim();
							for (prop in lbl._oldProps) {
								if (!eve.isEqual(lbl["_" + prop], lbl._oldProps[prop])) {
									lbl._redrawRequired = true;
									lbl._animCurrent.mode = lbl._animChange.mode;
									animLabels++;
									break;
								}
							}
							//if the label is linked to a series or an axis, and the linked object has changed, update the label:
							if (!isNil(lblLink) && !isNil(lblLinkSeries) && lblLinkSeries instanceof eve.series && lblLinkSeries._redrawRequired) {
								if (!lbl._redrawRequired) {
									animLabels++;
									lbl._redrawRequired = true;
									lbl._animCurrent.mode = lbl._animChange.mode;
								}
							} else if (!isNil(lblLink) && lblLink instanceof eve.axis && lblLink._redrawRequired) {
								if (!lbl._redrawRequired) {
									animLabels++;
									lbl._redrawRequired = true;
									lbl._animCurrent.mode = lbl._animChange.mode;
								}
							}
						}
					}
				}
			}

			for (lc = 0; lc < this._labels.length; lc++) {
				lbl = getEveElmByID(this._labels[lc]);
				lblLink = getEveElmByID(lbl._link);
//				eve.setObjProps(lbl, true, true, true);
				eve.setObjProps(lbl);
				if (lbl._redrawRequired) {
					if (isNil(lbl._animCurrent.duration) || rebuild) {
						if (!isNil(lblLink) && !isNil(lblLinkSeries) && lblLinkSeries instanceof eve.series) {
							lbl._animCurrent.duration = lblLinkSeries._animCurrent.duration;
						} else {
							lbl._animCurrent.duration = (duration * this._animInRatios.overlays) / animLabels;
						}
					}
					if (isNil(lbl._animCurrent.delay) || rebuild) {
						if (!isNil(lblLink) && !isNil(lblLinkSeries) && lblLinkSeries instanceof eve.series) {
							lbl._animCurrent.delay = lblLinkSeries._animCurrent.delay;
						} else {
							lbl._animCurrent.delay = animTime;
						}
					}
					animTime += lbl._animCurrent.duration;
				}
				if (lbl._age !== -1) {
					if (lbl._age === null || rebuild) {
						lbl._age = 0;
					} else {
						lbl._age = this._drawCall + 1;
					}
				}
			}

		};

		this.build = function(duration, rebuild, rejigOnly) {
			var sc;
			duration = duration || 0;

			eve.setObjProps(this);
			setChartSize(this);

			if (this._drawMode === "PDF"){
				this._artboard = this._container;
			}
			else{
				this._artboard = eve.getSVG(this._container, this._id, "eve-chart", this._width, this._height, this._width, this._height);
			}

			this._plotHeight = this._height - (this._marginTop + this._paddingTop + this._marginBottom + this._paddingBottom);
			this._plotWidth = this._width - (this._marginLeft + this._paddingLeft + this._marginRight + this._paddingRight);

			this._plotOrigin.x = 0;
			this._plotOrigin.y = 0;

			if (this._drawCall === 1) {

				this._blueprintGroup = this.addBlueprint(this._artboard);
				this._blueprintGroup.immortal = true;

				this._vDefs = this._blueprintGroup.getElement("defs", 0, this.id + "_defs", "eve-chart-defs");
				this._vDefs.immortal = true;

				this._vGroup = this._blueprintGroup.getElement("group", 0, this.id + "_group", "eve-chart-artboard");
				if (this._drawMode === "PDF"){
					this._vGroup.eX = this._x;
					this._vGroup.eY = this._y;
				}

				this._bgBox = this._vGroup.getElement("rect", this._drawCall, this.id + "_bg_box", "eve-chart-bg", null, {fill: this._backgroundColour});
				this._bgBox.eX = this._marginLeft;
				this._bgBox.eY = this._marginTop;
				this._bgBox.eWidth = this._width - (this._marginLeft + this._marginRight);
				this._bgBox.eHeight = this._height - (this._marginTop + this._marginBottom);
				this._bgBox.immortal = true;
				this._vGroup.immortal = true;

				this._plotBox = this._vGroup.getElement("rect", this._drawCall, this.id + "_plot_box", "eve-chart-plot-bg", null, {fill: this._plotBackgroundColour});
				this._plotBox.immortal = true;
				this._plotBox._animCurrent = new eve.elmAnim("fade in", duration / 4);

				this._vGroupBlocks = this._vGroup.getElement("group", 0, this.id + "_block_group", "eve-chart-blocks");
				this._vGroupBlocks.eX = this._marginLeft + this._paddingLeft;
				this._vGroupBlocks.eY = this._marginTop + this._paddingTop;
				this._vGroupBlocks.immortal = true;

				this._vGroupGridlines = this._vGroup.getElement("group", 0, this.id + "_gridlines_group", "eve-chart-gridlines-group", null, {"pointerEvents": "none"});
				this._vGroupGridlines.immortal = true;

				this._vGroupSeries = this._vGroup.getElement("group", 0, this.id + "_series_group", "eve-chart-series-group");
				this._vGroupSeries.immortal = true;

				this._vGroupAxes = this._vGroup.getElement("group", 0, this.id + "_axis_group", "eve-chart-axis-group");
				this._vGroupAxes.immortal = true;

				this._vGroupSeriesTop = this._vGroup.getElement("group", 0, this.id + "_series_group_top", "eve-chart-series-group");
				this._vGroupSeriesTop.immortal = true;

				this._vGroupMeasures = this._vGroup.getElement("group", 0, this.id + "_measures_group", "eve-chart-measures-group");
				this._vGroupMeasures.immortal = true;

				this._vAxes = [];
				this._vGridlinesHor = null;
				this._vGridlinesVer = null;
			}
			else {
				if (!isNil(this._vDefs)) {
					this._vDefs._id = this._id + "_defs";
				}
				this._plotBox._animCurrent.mode = null;
			}

			this._plotBox.eX = this._marginLeft + this._paddingLeft;
			this._plotBox.eY = this._marginTop + this._paddingTop;
			this._plotBox.eWidth = this._plotWidth;
			this._plotBox.eHeight = this._plotHeight;

			for (sc = 0; sc < this._series.length; sc++){
				var s = getEveElmByID(this._series[sc]);
				if (!isNil(s.data)){
					s._data = s.data;
				}
			}

//			wkGetData(this, duration, rebuild, rejigOnly);
			this.getData(duration, rebuild, rejigOnly);

		};

		this.continueBuild = function(duration, rebuild, rejigOnly) {

			var a;
			var ac, lc, bc, nc;
			var animTime = 0;

			this._vGroupLegend = this._vGroup.getElement("group", this._drawCall, this.id + "_legend_group", "eve-chart-legend-group");
			this._vGroupTitle = this._vGroup.getElement("group", this._drawCall, this.id + "_title_group", "eve-chart-title-group");
			this._vGroupOverlays = this._vGroup.getElement("group", this._drawCall, this.id + "_overlays_group", "eve-chart-overlays-group");
			this._vGroupDraggables = this._vGroup.getElement("group", this._drawCall, this.id + "_draggables_group", "eve-chart-draggables-group");
			this._vGroupToolTips = this._vGroup.getElement("group", this._drawCall, this.id + "_tooltips_group", "eve-chart-tooltips-group", null, {pointerEvents: "none"});
			this._vGroupToolTips.eX = this._paddingLeft + this._marginLeft;
			this._vGroupToolTips.eY = this._paddingTop + this._marginTop;
			this._vGroupOverlays.eX = this._paddingLeft + this._marginLeft;
			this._vGroupOverlays.eY = this._paddingTop + this._marginTop;
			this._vGroupDraggables.eX = this._paddingLeft + this._marginLeft;
			this._vGroupDraggables.eY = this._paddingTop + this._marginTop;

			var nudgeTitX = parseFloat(this._titleOffsetX) || 0;
			var nudgeTitY = parseFloat(this._titleOffsetY) || (parseFloat(this._titleFontSize) / 2);
			var titX = (this._width / 2) + nudgeTitX;
			var titY = this._marginTop + (parseFloat(this._titleFontSize) || 0) + nudgeTitY;
			if (this._type === "XY" && this._xAxis._position === "top") {
				titY = this._height - this._marginBottom + nudgeTitY;
			}

			var titOptions = {id: this.id + "_title", drawCall: this._drawCall, string: this._title, className: "eve-chart-title", fontSize: this._titleFontSize, fontColour: this._titleFontColour, fontWeight: this._titleFontWeight, textAlign: "centre", verticalAlign: "bottom"};

			this._vTitle = eve.makeText(this, this._vGroupTitle, this._drawMode, titOptions);
			this._vTitle.eX = titX;
			this._vTitle.eY = titY;
			if (this._drawCall > 1) {
				this._vTitle._animCurrent.mode = "fadeDownChangeFadeUp";
				if (this._titleChangeAnim !== null) {
					this._vTitle._animCurrent.mode = this._titleChangeAnim;
				}
				this._vTitle._animCurrent.duration = duration / 3;
			}

			for (ac = 0; ac < this._axes.length; ac++) {
				eve.setObjProps(getEveElmByID(this._axes[ac]));
			}
			for (lc = 0; lc < this._labels.length; lc++) {
				eve.setObjProps(getEveElmByID(this._labels[lc]));
			}

			this.initAnims(duration, rebuild);

			//AXES AND GRIDLINES

			var aLine;
			var evX = 0;
			var evY = 0;

			for (ac = 0; ac< this._axes.length; ac++) {
				a = getEveElmByID(this._axes[ac]);

				if (a._age > -1 && a._type !== "gauge") {

					var xCoord = 0;
					var yCoord = 0;
					var gLine;

					if (isNil(a._subTickLength)) {
						a._subTickLength = a._tickLength / 2;
					}
					if (isNil(a.axisWidth)) {
						a._axisWidth = 1;
					}

					if (isNil(a.autoRotateLabels)) {
						a._autoRotateLabels = true;
					} else {
						a._autoRotateLabels = a.autoRotateLabels;
					}

					var prevBandLabelRotation = a._bandLabelRotation;

					var tickMultiplier = 1;
					var tickOffset = 0;
					var rootTickOffset = 0;
					var tickTextOffset = 0;
					var tick;
					var tick_root;
					var tLabel;
					var tRootLabel;
					var subDivWidth;
					var tSub;
					var gsLine;
					var lblAlign;
					var lblVerAlign = "top";

					var aLabel;
					var lblID, labelChanged, labelElm;
					var ga;

					var endX = 0;
					var endY = 0;

					var tSize;
					a._tickTextSize.width = a._tickTextSize.height = 0;
					for (var bd = 0, ble = a._bands.length; bd < ble; bd++) {
						if (!isNil(a._bands[bd]._startLabel) && a._bands[bd]._startLabel !== "") {
							// tSize = textSize(a._bands[bd]._startLabel, a._fontFamily, a._tickFontSize, null, null, this._drawMode, this._artboard);
							tSize = textSize(a._bands[bd]._startLabel, a._fontFamily, a._tickFontSize, null, null, 0, this._drawMode, this._artboard);
							if (a._tickTextSize.width < tSize.width) {
								a._tickTextSize.width = tSize.width;
							}
							if (a._tickTextSize.height < tSize.height) {
								a._tickTextSize.height = tSize.height;
							}
						}
						if (!isNil(a._bands[bd]._label) && a._bands[bd]._label !== "") {
							// tSize = textSize(a._bands[bd]._label, a._fontFamily, a._tickFontSize, null, null, this._drawMode, this._artboard);
							tSize = textSize(a._bands[bd]._label, a._fontFamily, a._tickFontSize, null, null, 0, this._drawMode, this._artboard);
							if (a._tickTextSize.width < tSize.width) {
								a._tickTextSize.width = tSize.width;
							}
							if (a._tickTextSize.height < tSize.height) {
								a._tickTextSize.height = tSize.height;
							}
						}
					}

					var tickTextPos = {x: 0, y: 0};

					if (a._label.indexOf("auto|") === 0) {
						a._label = "auto|" + a._domain.toProperCase();
					}
					a._labelLocn = {x: 0, y: 0};

					if (a._showGridLines) {
						if (a._horizontal) {
							this._vGridlinesVer = this._vGroupGridlines.getElement("group", this._drawCall, this.id + "_gridlines_ver", "eve-chart-gridlines");
							this._vGridlinesVer.eX = this._marginLeft + this._paddingLeft;
							this._vGridlinesVer.eY = this._marginTop + this._paddingTop;
							this._vGridlinesVer._animCurrent = cloneEveObj(a._animCurrent);
							if (this._vGridlinesVer._animCurrent.mode === "flipText") {
								this._vGridlinesVer._animCurrent.mode = "move";
							}
						} else {
							this._vGridlinesHor = this._vGroupGridlines.getElement("group", this._drawCall, this.id + "_gridlines_hor", "eve-chart-gridlines");
							this._vGridlinesHor.eX = this._marginLeft + this._paddingLeft;
							this._vGridlinesHor.eY = this._marginTop + this._paddingTop;
							this._vGridlinesHor._animCurrent = cloneEveObj(a._animCurrent);
							if (this._vGridlinesHor._animCurrent.mode === "flipText") {
								this._vGridlinesHor._animCurrent.mode = "move";
							}
						}
					}

					a._bandLabelRotation = 0;

					//HORIZONTAL AXES
					if (a._horizontal) {

						this._vAxes[ac] = this._vGroupAxes.getElement("group", this._drawCall, this.id + "_axis" + ac, "eve-chart-axis");
						if (!a._visible) {
							this._vAxes[ac].setStyle("visibility", "hidden");
						}

						if (a._position === "top") {
							this._vAxes[ac].eX = this._marginLeft + this._paddingLeft;
							this._vAxes[ac].eY = this._marginTop + this._paddingTop + this._plotOrigin.y;
							tickMultiplier = -1;
						} else {
							this._vAxes[ac].eX = this._marginLeft + this._paddingLeft;
							this._vAxes[ac].eY = this._marginTop + this._paddingTop + this._plotHeight - this._plotOrigin.y;
						}

						var startX = -a._overhangOrigin;
						endX = this._plotWidth + a._overhangEnd;
						if (a._origin === "right") {
							startX = this._plotWidth + a._overhangOrigin;
							endX = -a._overhangEnd;
						}
						if (!strIs(a._type, "label-band") && a._axisOpacity > 0){
							aLine = this._vAxes[ac].getElement("path", this._drawCall, this.id + "_axis" + ac + "_path", "eve-chart-axis-line", {vectorEffect: "non-scaling-stroke"}, {shapeRendering: "crispEdges", stroke: a._axisColour, strokeWidth: a._axisWidth, strokeOpacity: a._axisOpacity});

							if (!isNil(aLine.path)) {
								aLine.oPath = cloneEveObj(aLine.path);
							}
							aLine.path = eve.addPath(null, false);
							aLine.path.addCommand("moveTo", [startX, 0]);
							aLine.path.addCommand("lineTo", [endX, 0]);

							if (this._drawCall === 1 || aLine.age === 0 || rebuild) {
								aLine._animCurrent = new eve.elmAnim("draw", a._animCurrent.duration, a._animCurrent.delay, "easeOut");
							} else {
								aLine._animCurrent.mode = "none";
							}
							if (duration === 0) {
								aLine._animCurrent.mode = "none";
							}
						}
						//draw ticks:
						for (bc = 0; bc < a._bands.length; bc++) {
							idSuffix = bc.toString();
							//var suffixName = a._bands[bc]._label.toString().replace(/\s/g, "_").split("+").join("plus");
							var suffixName = eve.replaceSpecialChars(a._bands[bc]._label.toString().split("+").join("plus"));
							if (a._type === "band" && suffixName !== "") {
								idSuffix = "_" + suffixName;
							}

							ga = this._vAxes[ac].getElement("group", this._drawCall, this.id + "_axis" + ac + "_band" + idSuffix, "eve-chart-band");
							if (a._redrawRequired) {
								ga._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc));
								if (this._drawCall > 1 && ga.age > 0 && !rebuild) {
									ga._animCurrent.mode = "move";
								}
							} else {
								ga._animCurrent = cloneEveObj(a._animCurrent);
							}

							if (a._origin === "left") {
								ga.eX = a._bands[bc]._start * this._plotWidth;
							} else {
								ga.eX = this._plotWidth - (a._bands[bc]._start * this._plotWidth);
							}
							tickTextPos.y = a._tickLength * tickMultiplier;

							tickOffset = 0;
							if (a._bands[bc]._tickPos === "centre") {
								tickOffset = ((a._bands[bc]._end - a._bands[bc]._start) / 2) * this._plotWidth;
							}
							if (a._bands[bc]._tickPos === "end") {
								tickOffset = (a._bands[bc]._end - a._bands[bc]._start) * this._plotWidth;
							}

							tickTextOffset = 0;
							if (a._bands[bc]._labelPos === "centre") {
								tickTextPos.x = (((a._bands[bc]._end - a._bands[bc]._start) / 2) * this._plotWidth) + a._bands[bc]._labelOffsetX;
							}
							if (a._bands[bc]._labelPos === "end") {
								tickTextPos.x = ((a._bands[bc]._end - a._bands[bc]._start) * this._plotWidth) + a._bands[bc]._labelOffsetX;
							}

							rootTickOffset = tickOffset - ((a._bands[0]._end - a._bands[0]._start) * this._plotWidth);
							if (a._origin === "right") {
								tickOffset = -tickOffset;
								tickTextPos.x = -tickTextPos.x;
								rootTickOffset = tickOffset + ((a._bands[0]._end - a._bands[0]._start) * this._plotWidth);
							}

							if (a._showTicks && !strIs(a._type, "label-band")) {
								if (a._tickOpacity > 0) {
									if (bc === 0) {
										//put an extra one in at the origin:
										tick_root = ga.getElement("line", this._drawCall, this.id + "_axis" + ac + "_tick_root", "eve-chart-tick", {x1: 0, y1: 0, x2: 0, y2: a._tickLength * tickMultiplier, vectorEffect: "non-scaling-stroke"}, {shapeRendering: "crispEdges", stroke: a._tickColour, strokeWidth: a._tickWidth, opacity: a._tickOpacity});
										tick_root.eX = rootTickOffset;
										if (a._redrawRequired) {
											tick_root._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._tickOpacity);
											if (this._drawCall > 1 && tick_root.age > 0 && !rebuild) {
												tick_root._animCurrent.mode = "move";
											}
										} else {
											tick_root._animCurrent = cloneEveObj(a._animCurrent);
										}
									}

									//TICK:
									tick = ga.getElement("line", this._drawCall, this.id + "_axis" + ac + "_tick" + idSuffix, "eve-chart-tick", {x1: (a._bands[bc]._end - a._bands[bc]._start) * this._plotWidth, y1: 0, x2: (a._bands[bc]._end - a._bands[bc]._start) * this._plotWidth, y2: a._tickLength * tickMultiplier, vectorEffect: "non-scaling-stroke"}, {shapeRendering: "crispEdges", stroke: a._tickColour, strokeWidth: a._tickWidth, opacity: a._tickOpacity});
									if (a._redrawRequired) {
										tick._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._tickOpacity);
										if (this._drawCall > 1 && tick.age > 0 && !rebuild) {
											tick._animCurrent.mode = "move";
										}
									} else {
										tick._animCurrent = cloneEveObj(a._animCurrent);
									}
								}
							}

							if (a._position === "top") {
								lblVerAlign = "bottom";
							}

							if (a._showBandLabels && !strIs(a._type, "label-band")) {
								var baWidth = (a._bands[bc]._end - a._bands[bc]._start) * this._plotWidth;
								if (a.bandLabelRotation){
									a._bandLabelRotation = a.bandLabelRotation;
								}
								else if (a._m_bandLabelRotation){
									a._bandLabelRotation = a._m_bandLabelRotation;
								}
								else{
									a._bandLabelRotation = 0;
									if (baWidth < a._tickTextSize.width && a._autoRotateLabels) {
										a._bandLabelRotation = -90;
									}
								}

								lblAlign = "centre";
								if (a._bandLabelRotation !== 0) {
									lblAlign = "right";
									lblVerAlign = "middle";
								}

								//EXTRA TICK LABEL AT ORIGIN:
								if (!isNil(a._bands[bc]._startLabel) && a._bands[bc]._startLabel !== "") {
									evX = tickTextPos.x - ((a._bands[bc]._end - a._bands[bc]._start) * this._plotWidth);
									if (a._origin === "right") {
										evX = tickTextPos.x + ((a._bands[bc]._end - a._bands[bc]._start) * this._plotWidth);
									}

									tRootLabel = eve.makeText(this, ga, this._drawMode, {id: this.id + "_axis" + ac + "_ticklabel_root", drawCall: this._drawCall, string: a._bands[bc]._startLabel, x: evX, y: tickTextPos.y + (a._tickTextMargin * tickMultiplier), className: "eve-chart-tick-text eve-chart-tick-text-" + a._position, fontSize: a._tickFontSize, fontColour: a._tickFontColour, fontFamily: a._fontFamily, textAlign: lblAlign, verticalAlign: lblVerAlign, rotation: a._bandLabelRotation});
									tRootLabel.setStyle("opacity", a._tickTextOpacity);
									tRootLabel.opacity = a._tickTextOpacity;

									if (a._redrawRequired) {
										tRootLabel._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay, null, a._tickTextOpacity);
										if (this._drawCall > 1 && tRootLabel.age > 0 && !rebuild) {
											tRootLabel._animCurrent.mode = "fadeDownChangeFadeUp";
											if (a._animCurrent.mode === "flipText") {
												tRootLabel._animCurrent.mode = "flipText";
											}
										}
									} else {
										tRootLabel._animCurrent = cloneEveObj(a._animCurrent);
									}
								}

								//TICK LABEL:
								tLabel = eve.makeText(this, ga, this._drawMode, {id: this.id + "_axis" + ac + "_ticklabel" + idSuffix, drawCall: this._drawCall, string: a._bands[bc]._label, x: tickTextPos.x, y: tickTextPos.y + (a._tickTextMargin * tickMultiplier), className: "eve-chart-tick-text eve-chart-tick-text-" + a._position, fontSize: a._tickFontSize, fontColour: a._tickFontColour, fontFamily: a._fontFamily, textAlign: lblAlign, verticalAlign: lblVerAlign, rotation: a._bandLabelRotation});
								tLabel.setStyle("opacity", a._tickTextOpacity);
								tLabel.opacity = a._tickTextOpacity;

								if (!a._redrawRequired){
									if (prevBandLabelRotation !== a._bandLabelRotation) {
										a._redrawRequired = true;
									}
									if (get(tLabel.id, this._container) && eve.getSvgTransforms(get(tLabel.id, this._container)).rotate !== a._bandLabelRotation){
										a._redrawRequired = true;
									}
								}

								if (a._redrawRequired) {
									tLabel._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._tickTextOpacity);
									if (this._drawCall > 1 && tLabel.age > 0 && !rebuild) {
										tLabel._animCurrent.mode = "fadeDownChangeFadeUp";
										if (a._animCurrent.mode === "flipText") {
											tLabel._animCurrent.mode = "flipText";
										}
									}
								} else {
									tLabel._animCurrent = cloneEveObj(a._animCurrent);
								}
							}

							//gridlines:
							if (a._showGridLines && !strIs(a._type, "label-band")) {

								if (a._origin === "right") {
									xCoord = this._plotWidth - (a._bands[bc]._start * this._plotWidth) + tickOffset;
								} else {
									xCoord = (a._bands[bc]._start * this._plotWidth) + tickOffset;
								}
								xCoord = xCoord.toFixed(6);

								gLine = this._vGridlinesVer.getElement("line", this._drawCall, this.id + "_axis" + ac + "_gridline" + idSuffix, "eve-chart-gridline", {x1: xCoord, y1: 0, x2: xCoord, y2: this._plotHeight}, {shapeRendering: "geometricPrecision", vectorEffect: "non-scaling-stroke", strokeWidth: a._gridLineWidth, stroke: a._gridLineColour, opacity: a._gridLineOpacity});

								if (a._redrawRequired) {
									gLine._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._gridLineOpacity);
									if (this._drawCall > 1 && gLine.age > 0 && !rebuild) {
										gLine._animCurrent.mode = "move";
									}
								} else {
									gLine._animCurrent = cloneEveObj(a._animCurrent);
								}
							}

							//minor ticks and grid:
							for (var stCount = 1; stCount < a._useSubTicks; stCount++) {

								subDivWidth = ((a._bands[bc]._end - a._bands[bc]._start) * this._plotWidth) / a._useSubTicks;
								if (a._origin === "right") {
									subDivWidth = -subDivWidth;
								}

								if (a._showSubTicks) {
									//subticks:
									tSub = ga.getElement("line", this._drawCall, this.id + "_axis" + ac + "_band" + idSuffix + "_subtick" + stCount, "eve-chart-sub-tick", {x1: subDivWidth * stCount, y1: 0, x2: subDivWidth * stCount, y2: a._subTickLength * tickMultiplier});
									tSub.eX = subDivWidth * stCount;
									tSub.eY = 0;

									if (a._redrawRequired) {
										tSub._animCurrent = new eve.elmAnim("appear", ((a._animCurrent.duration / getAutoNum(a._numBands, true)) / a._useSubTicks) * stCount, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc) + (((a._animCurrent.duration / getAutoNum(a._numBands, true)) / a._useSubTicks) * stCount), null, a._subTickOpacity);
										if (this._drawCall > 1 && tSub.age > 0 && !rebuild) {
											tSub._animCurrent.mode = "move";
										}
									} else {
										tSub._animCurrent = cloneEveObj(a._animCurrent);
									}
								}

								//sub gridlines:
								if (a._showGridLines && a._showSubGridLines) {
									evX = xCoord - (subDivWidth * stCount);
									gsLine = this._vGridlinesVer.getElement("line", this._drawCall, this.id + "_axis" + ac + "_band" + idSuffix + "_subgridline" + stCount, "eve-chart-sub-gridline", {x1: 0, y1: 0, x2: 0, y2: this._plotHeight}, {shapeRendering: "geometricPrecision", vectorEffect: "non-scaling-stroke", strokeWidth: a._subGridLineWidth, stroke: a._subGridLineColour, opacity: a._subGridLineOpacity});

									gsLine.eX = evX;

									if (a._redrawRequired) {
										gsLine._animCurrent = new eve.elmAnim("appear", ((a._animCurrent.duration / getAutoNum(a._numBands, true)) / a._useSubTicks) * stCount, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc) + (((a._animCurrent.duration / getAutoNum(a._numBands, true)) / a._useSubTicks) * stCount), null, a._subGridLineOpacity);
										if (this._drawCall > 1 || gsLine.age > 0 && !rebuild) {
											gsLine._animCurrent.mode = "move";
										}
									} else {
										gsLine._animCurrent = cloneEveObj(a._animCurrent);
									}
								}
							}
						}

						if (!strIs(a._type, "label-band")){
							lblID = this.id + "_axis" + ac + "_label";

							a._labelLocn.x = (this._plotWidth / 2) + a._labelOffsetX;
							a._labelLocn.y = a._tickLength + a._tickTextSize.height + a._labelOffsetY + (a._tickTextMargin * tickMultiplier);
							a._vAlign = "top";
							if (a._position === "top") {
								a._labelLocn.y = a._labelOffsetY - a._labelLocn.y;
								a._vAlign = "bottom";
							} else {
								a._labelLocn.y += a._labelOffsetY;
							}

							aLabel = eve.makeText(this, this._vAxes[ac], this._drawMode, {id: lblID, drawCall: this._drawCall, x: a._labelLocn.x, y: a._labelLocn.y, string: a._label.replace("auto|", ""), fontSize: a._labelFontSize, fontFamily: a._fontFamily, fontColour: a._labelFontColour, fontWeight: a._labelFontWeight, className: "eve-chart-axis-label eve-chart-axis-label-" + a._position, textAlign: "centre", verticalAlign: a._vAlign});

							if (this._drawCall === 1 || aLabel.age === 0 || rebuild) {
								aLabel._animCurrent = new eve.elmAnim("fade in", a._animCurrent.duration / 2, (a._animCurrent.duration / 4) + a._animCurrent.delay, null, a._labelOpacity);
							} else {
								if (a._redrawRequired) {
									labelChanged = false;
									labelElm = get(lblID, this._container);
									if (!isNil(labelElm) && !isNil(labelElm.firstChild)) {
										if (labelElm.firstChild.firstChild.nodeValue !== a._label.replace("auto|", "")) {
											labelChanged = true;
										}
										if (parseFloat(labelElm.firstChild.style.fontSize) !== parseFloat(a._labelFontSize)) {
											labelChanged = true;
										}
										if (labelElm.firstChild.style.fontFamily !== a._fontFamily) {
											labelChanged = true;
											if ((isNil(labelElm.firstChild.style.fontFamily) || labelElm.firstChild.style.fontFamily === "") && (isNil(a._fontFamily) || a._fontFamily === "")) {
												labelChanged = false;
											}
										}
										if (eve.colourToHex(labelElm.firstChild.style.fill) !== eve.colourToHex(a._labelFontColour)) {
											labelChanged = true;
										}
										if (labelElm.firstChild.style.fontWeight !== a._labelFontWeight) {
											labelChanged = true;
										}
									}
									if (labelChanged) {
										aLabel._animCurrent.mode = "fadeDownChangeFadeUp";
										if (a._animCurrent.mode === "flipText") {
											aLabel._animCurrent.mode = "flipText";
										}
									} else {
										aLabel._animCurrent.mode = "none";
									}
								} else {
									aLabel._animCurrent.mode = "none";
								}
							}
						}
					}

					//VERTICAL AXES
					else if (!a._horizontal) {

						lblVerAlign = "middle";

						this._vAxes[ac] = this._vGroupAxes.getElement("group", this._drawCall, this.id + "_axis" + ac, "eve-chart-axis");
						if (!a._visible) {
							this._vAxes[ac].setStyle("visibility", "hidden");
						}

						var tickTextAlign = "left";
						if (a._position === "right") {
							tickMultiplier = 1;
							this._vAxes[ac].eX = this._marginLeft + this._paddingLeft + this._plotWidth - this._plotOrigin.x;
							this._vAxes[ac].eY = this._marginTop + this._paddingTop;
						} else {
							tickTextAlign = "right";
							tickMultiplier = -1;
							this._vAxes[ac].eX = this._marginLeft + this._paddingLeft + this._plotOrigin.x;
							this._vAxes[ac].eY = this._marginTop + this._paddingTop;
						}
						a._labelLocn.y = this._plotHeight / 2;

						var startY = a._overhangOrigin + this._plotHeight;
						endY = -a._overhangEnd;
						if (a._origin === "top") {
							startY = -a._overhangOrigin;
							endY = this._plotHeight + a._overhangEnd;
						}
						if (!strIs(a._type, "label-band") && a._axisOpacity > 0){
							aLine = this._vAxes[ac].getElement("path", this._drawCall, this.id + "_axis" + ac + "_path", "eve-chart-axis-line", {vectorEffect: "non-scaling-stroke"}, {shapeRendering: "crispEdges", stroke: a._axisColour, strokeWidth: a._axisWidth, opacity: a._axisOpacity});

							if (!isNil(aLine.path)) {
								aLine.oPath = cloneEveObj(aLine.path);
							}
							aLine.path = eve.addPath(null, false);
							aLine.path.addCommand("moveTo", [0, startY]);
							aLine.path.addCommand("lineTo", [0, endY]);

							if (this._drawCall === 1 || aLine.age === 0 || rebuild) {
								aLine._animCurrent = {mode: "draw", duration: a._animCurrent.duration, delay: a._animCurrent.delay, ease: "easeOut"};
							} else {
								aLine._animCurrent.mode = "none";
							}
						}

						//draw ticks:
						for (bc = 0; bc < a._bands.length; bc++) {
							idSuffix = bc.toString();
							ga = this._vAxes[ac].getElement("group", this._drawCall, this.id + "_axis" + ac + "_band" + idSuffix, "eve-chart-band");
							if (a._redrawRequired) {
								ga._animCurrent = {mode: "appear", delay: a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), duration: ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc)};
								if (this._drawCall > 1 && ga.age > 0 && !rebuild) {
									ga._animCurrent.mode = "move";
								}
							} else {
								ga._animCurrent = cloneEveObj(a._animCurrent);
							}
							ga.eX = 0;
							ga.eY = 0;

							if (a._type === "band") {
								idSuffix = "_" + eve.replaceSpecialChars(a._bands[bc]._label.toString().split("+").join("plus"));
							}

							if (a._origin === "bottom") {
								ga.eY = this._plotHeight - (a._bands[bc]._start * this._plotHeight);
							} else {
								ga.eY = a._bands[bc]._start * this._plotHeight;
							}
							tickTextPos.x = a._tickLength * tickMultiplier;

							tickOffset = 0;
							rootTickOffset = 0;
							if (a._bands[bc]._tickPos === "centre") {
								tickOffset = ((a._bands[bc]._end - a._bands[bc]._start) / 2) * this._plotHeight;
							}
							if (a._bands[bc]._tickPos === "end") {
								tickOffset = (a._bands[bc]._end - a._bands[bc]._start) * this._plotHeight;
							}

							tickTextOffset = 0;
							if (a._bands[bc]._labelPos === "centre") {
								tickTextOffset = (((a._bands[bc]._end - a._bands[bc]._start) / 2) * this._plotHeight) + a._bands[bc]._labelOffsetY;
							}
							if (a._bands[bc]._labelPos === "end") {
								tickTextOffset = ((a._bands[bc]._end - a._bands[bc]._start) * this._plotHeight) + a._bands[bc]._labelOffsetY;
							}
							if (a._origin === "bottom") {
								tickOffset = -tickOffset;
								tickTextOffset = -tickTextOffset;
							}
							rootTickOffset = tickOffset + ((a._bands[0]._end - a._bands[0]._start) * this._plotHeight);
							if (a._origin === "top") {
								rootTickOffset = tickOffset - ((a._bands[0]._end - a._bands[0]._start) * this._plotHeight);
							}

							if (a._showTicks && !strIs(a._type, "label-band")) {
								//ticks:
								if (a._tickOpacity > 0) {
									//put an extra one in at the origin:
									if (bc === 0) {
										tick_root = ga.getElement("line", this._drawCall, this.id + "_axis" + ac + "_tick_root", "eve-chart-tick", {x1: a._tickLength * tickMultiplier, y1: rootTickOffset, x2: 0, y2: rootTickOffset, vectorEffect: "non-scaling-stroke"}, {shapeRendering: "crispEdges", stroke: a._tickColour, strokeWidth: a._tickWidth, opacity: a._tickOpacity});
										if (a._redrawRequired) {
											tick_root._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._tickOpacity);
											if (this._drawCall > 1 && tick_root.age > 0 && !rebuild) {
												tick_root._animCurrent.mode = "move";
											}
										} else {
											tick_root._animCurrent = cloneEveObj(a._animCurrent);
										}
									}

									tick = ga.getElement("line", this._drawCall, this.id + "_axis" + ac + "_tick" + idSuffix, "eve-chart-tick", {x1: a._tickLength * tickMultiplier, y1: tickOffset, x2: 0, y2: tickOffset, vectorEffect: "non-scaling-stroke"}, {shapeRendering: "crispEdges", stroke: a._tickColour, strokeWidth: a._tickWidth, opacity: a._tickOpacity});

									if (a._redrawRequired) {
										tick._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._tickOpacity);
										if (this._drawCall > 1 && tick.age > 0 && !rebuild) {
											tick._animCurrent.mode = "move";
										}
									} else {
										tick._animCurrent = cloneEveObj(a._animCurrent);
									}
								}
							}

							if (a._showBandLabels  && !strIs(a._type, "label-band")) {
								if (!isNil(a._bands[bc]._startLabel) && a._bands[bc]._startLabel !== "") {
									tickTextPos.y = tickTextOffset + ((a._bands[bc]._end - a._bands[bc]._start) * this._plotHeight);
									if (a._origin === "top") {
										tickTextPos.y = tickTextOffset - ((a._bands[bc]._end - a._bands[bc]._start) * this._plotHeight);
									}

									tRootLabel = eve.makeText(this, ga, this._drawMode, {id: this.id + "_axis" + ac + "_ticklabel_root", drawCall: this._drawCall, string: a._bands[bc]._startLabel, x: tickTextPos.x + (a._tickTextMargin * tickMultiplier), y: tickTextPos.y, className: "eve-chart-tick-text eve-chart-tick-text-" + a._position, fontSize: a._tickFontSize, fontColour: a._tickFontColour, fontFamily: a._fontFamily, textAlign: tickTextAlign, verticalAlign: lblVerAlign, rotation: a._bandLabelRotation});
									tRootLabel.setStyle("opacity", a._tickTextOpacity);
									tRootLabel.opacity = a._tickTextOpacity;

									if (a._redrawRequired) {
										tRootLabel._animCurrent = new eve.elmAnim("move", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._tickTextOpacity);
										if (this._drawCall > 1 && tRootLabel.age > 0 && !rebuild) {
											tRootLabel._animCurrent.mode = "fadeDownChangeFadeUp";
										}
									} else {
										tRootLabel._animCurrent = cloneEveObj(a._animCurrent);
									}
								}

								tickTextPos.y = tickTextOffset;

								tLabel = eve.makeText(this, ga, this._drawMode, {id: this.id + "_axis" + ac + "_ticklabel" + idSuffix, drawCall: this._drawCall, string: a._bands[bc]._label, fontSize: a._tickFontSize, fontColour: a._tickFontColour, fontFamily: a._fontFamily, x: tickTextPos.x + (a._tickTextMargin * tickMultiplier), y: tickTextPos.y, className: "eve-chart-tick-text eve-chart-tick-text-" + a._position, textAlign: tickTextAlign, verticalAlign: lblVerAlign, rotation: a._bandLabelRotation});
								tLabel.setStyle("opacity", a._tickTextOpacity);
								tLabel.opacity = a._tickTextOpacity;

								if (a._redrawRequired) {
									tLabel._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._tickTextOpacity);
									if (this._drawCall > 1 && tLabel.age > 0 && !rebuild) {
										tLabel._animCurrent.mode = "fadeDownChangeFadeUp";
										if (a._animCurrent.mode === "flipText") {
											tLabel._animCurrent.mode = "flipText";
										}
									}
								} else {
									tLabel._animCurrent = cloneEveObj(a._animCurrent);
								}
							}

							//gridlines:
							if (a._showGridLines && !strIs(a._type, "label-band")) {

								if (a._chart._xAxis && strIs(a._chart._xAxis._type, "label-band") && bc === 0){
									if (a._origin === "bottom") {
										yCoord = this._plotHeight - (a._bands[0]._start * this._plotHeight);
									} else {
										yCoord = (a._bands[0]._start * this._plotHeight);
									}
									yCoord = yCoord.toFixed(6);
									gLine = this._vGridlinesHor.getElement("line", this._drawCall, this.id + "_axis" + ac + "_gridline_root", "eve-chart-gridline", {x1: 0, y1: yCoord, x2: this._plotWidth, y2: yCoord}, {shapeRendering: "geometricPrecision", vectorEffect: "non-scaling-stroke",  strokeWidth: a._gridLineWidth, stroke: a._gridLineColour, opacity: a._gridLineOpacity});
									if (a._redrawRequired) {
										gLine._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay, null, a._gridLineOpacity);
										if (this._drawCall > 1 && gLine.age > 0 && !rebuild) {
											gLine._animCurrent.mode = "move";
										}
									} else {
										gLine._animCurrent = cloneEveObj(a._animCurrent);
									}
								}

								if (a._origin === "bottom") {
									yCoord = this._plotHeight - (a._bands[bc]._start * this._plotHeight) + tickOffset;
								} else {
									yCoord = (a._bands[bc]._start * this._plotHeight) + tickOffset;
								}
								yCoord = yCoord.toFixed(6);

								gLine = this._vGridlinesHor.getElement("line", this._drawCall, this.id + "_axis" + ac + "_gridline" + idSuffix, "eve-chart-gridline", {x1: 0, y1: yCoord, x2: this._plotWidth, y2: yCoord}, {shapeRendering: "geometricPrecision", vectorEffect: "non-scaling-stroke",  strokeWidth: a._gridLineWidth, stroke: a._gridLineColour, opacity: a._gridLineOpacity});

								if (a._redrawRequired) {
									gLine._animCurrent = new eve.elmAnim("appear", (a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc), null, a._gridLineOpacity);
									if (this._drawCall > 1 && gLine.age > 0 && !rebuild) {
										gLine._animCurrent.mode = "move";
									}
								} else {
									gLine._animCurrent = cloneEveObj(a._animCurrent);
								}
							}

							//minor ticks and grid:
							if (a._showSubTicks) {
								for (var u = 1; u < a._useSubTicks; u++) {
									subDivWidth = ((a._bands[bc]._end - a._bands[bc]._start) * this._plotHeight) / a._useSubTicks;
									if (a._origin === "top") {
										subDivWidth = -subDivWidth;
									}

									//sub ticks:
									tSub = ga.getElement("line", this._drawCall, this.id + "_axis" + ac + "_band" + idSuffix + "_subtick" + u, "eve-chart-sub-tick", {x1: a._subTickLength * tickMultiplier, y1: tickOffset + (subDivWidth * u), x2: 0, y2: tickOffset + (subDivWidth * u)});

									if (a._redrawRequired) {
										tSub._animCurrent = new eve.elmAnim("appear", ((a._animCurrent.duration / getAutoNum(a._numBands, true)) / a._useSubTicks) * u, a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc) + (((a._animCurrent.duration / getAutoNum(a._numBands, true)) / a._useSubTicks) * u), null, a._subTickOpacity);
										if (this._drawCall > 1 && tSub.age > 0 && !rebuild) {
											tSub._animCurrent.mode = "move";
										}
									} else {
										tSub._animCurrent = cloneEveObj(a._animCurrent);
									}

									if (a._showGridLines && a._showSubGridLines) {
										evY = yCoord + (subDivWidth * u);

										gsLine = this._vGridlinesHor.getElement("line", this._drawCall, this.id + "_axis" + ac + "_band" + idSuffix + "_subgridline" + u, "eve-chart-sub-gridline", {x1: 0, y1: 0, x2: this._plotWidth, y2: 0}, {shapeRendering: "geometricPrecision", vectorEffect: "non-scaling-stroke", strokeWidth: a._subGridLineWidth, stroke: a._subGridLineColour, opacity: a._subGridLineOpacity});
										gsLine.eY = evY;

										if (a._redrawRequired) {
											gsLine._animCurrent = {mode: "appear", delay: a._animCurrent.delay + ((a._animCurrent.duration / getAutoNum(a._numBands, true)) * bc) + (((a._animCurrent.duration / getAutoNum(a._numBands, true)) / a._useSubTicks) * u), duration: ((a._animCurrent.duration / getAutoNum(a._numBands, true)) / a._useSubTicks) * u};
											if (this._drawCall > 1 && gsLine.age > 0 && !rebuild) {
												gsLine._animCurrent.mode = "move";
											}
										} else {
											gsLine._animCurrent = cloneEveObj(a._animCurrent);
										}
									}

								}
							}

						}

						if (!strIs(a._type, "label-band")){
							lblID = this.id + "_axis" + ac + "_label";

							a._labelLocn.x = (a._tickLength + a._tickTextMargin + a._tickTextSize.width + a._labelOffsetX + (a._labelFontSize / 2)) * tickMultiplier;

							aLabel = eve.makeText(this, this._vAxes[ac], this._drawMode, {id: lblID, drawCall: this._drawCall, string: a._label.replace("auto|", ""), fontSize: a._labelFontSize, fontFamily: a._fontFamily, fontColour: a._labelFontColour, fontWeight: a._labelFontWeight, className: "eve-chart-axis-label eve-chart-axis-label-" + a._position, textAlign: "centre", verticalAlign: "middle", x: a._labelLocn.x, y: a._labelLocn.y + a._labelOffsetY, rotation: -90});

							if (a._redrawRequired) {
								if (this._drawCall === 1 || aLabel.age === 0 || rebuild) {
									aLabel._animCurrent = new eve.elmAnim("fade in", a._animCurrent.duration / 2, (a._animCurrent.duration / 4) + a._animCurrent.delay, null, a._labelOpacity);
								} else {
									labelChanged = false;
									labelElm = get(lblID, this._container);
									if (!isNil(labelElm) && !isNil(labelElm.firstChild)) {
										if (labelElm.firstChild.firstChild.nodeValue !== a._label.replace("auto|", "")) {
											labelChanged = true;
										}
										if (parseFloat(labelElm.firstChild.style.fontSize) !== parseFloat(a._labelFontSize)) {
											labelChanged = true;
										}
										if (labelElm.firstChild.style.fontFamily !== a._fontFamily) {
											labelChanged = true;
											if ((isNil(labelElm.firstChild.style.fontFamily) || labelElm.firstChild.style.fontFamily === "") && (isNil(a._fontFamily) || a._fontFamily === "")) {
												labelChanged = false;
											}
										}
										if (eve.colourToHex(labelElm.firstChild.style.fill) !== eve.colourToHex(a._labelFontColour)) {
											labelChanged = true;
										}
										if (labelElm.firstChild.style.fontWeight !== a._labelFontWeight) {
											labelChanged = true;
										}
									}
									if (labelChanged) {
										aLabel._animCurrent.mode = "fadeDownChangeFadeUp";
										if (a._animCurrent.mode === "flipText") {
											aLabel._animCurrent.mode = "flipText";
										}
									} else {
										aLabel._animCurrent.mode = "none";
									}
								}
							} else {
								aLabel._animCurrent = cloneEveObj(a._animCurrent);
							}
						}
					}
					animTime += a._animCurrent.duration;

				} else {
					a = undefined;
					this._axes.remove(a);
				}

			}

			//SERIES

			var nodeDimX = []; // distance from the axis to the nodePoint - necessary because the nodePoint will change for stacked series'
			var nodeDimY = []; // distance from the axis to the nodePoint - necessary because the nodePoint will change for stacked series'
			var nodeBaseOffset = [];
			var nodeCentroid = [];
			var nodePoint = [];

			var smudgeSeries = 0;
			var barSeries = 0;
//			var lineSeries = 0;
//			var curveSeries = 0;

			var smudgesDrawn = -1;
			var barsDrawn = -1;
//			var linesDrawn = -1;
//			var curvesDrawn = -1;
			var idSuffix = "";
			var nodeFill = null;
			var nodeFillHover = null;
//			var nodeFillOpacity = null;
//			var nodeFillHoverOpacity = null;
//			var nodeStroke = null;
//			var nodeStrokeHover = null;
//			var nodeStrokeOpacity = null;
//			var nodeStrokeHoverOpacity = null;
			var s;
			var sc, xc, dc;

			for (sc = 0; sc < this._series.length; sc++) {
			// function setSeriesProps(sc, c){
				s = getEveElmByID(this._series[sc]);
				if (strIs(s._type, "pod")){
					s._showMeasureLines = false;
				}
				eve.setObjProps(s);
				if (!isNil(s.data)) {
					s._data = s.data;
				}

				if (s._highlights.length > 0){
					s.updateSegments();
				}
//				s.updateHighlights();
				if (s._nodes.length > 0 || s._shapeNodes){
					s.updateNodes();
				}
				if (s._type === "smudge") {
					smudgeSeries++;
				}
				if (s._type === "bar") {
					barSeries++;
				}
			}

			function setPalette(colour, dataLength, defaultPalette) {
				var rColour = defaultPalette;
				if (isDefault(colour)) {
					colour = defaultPalette;
				}
				//if (!isDefault(colour)) {
					if (eve.isArray(colour)) {
						rColour = colour.clone();
						if (rColour.length < dataLength) {
							eve.fillArray(rColour, dataLength);
						}
					} else if (eve.isNumeric(colour)) {
						return eve.palette.getNth(colour);
					} else {
						rColour = colour;
					}
				//}
				return rColour;
			}

			function getColour(palette, index) {
				var i;
				if (eve.isArray(palette)) {
					return palette.getNth(index);
				}
				else if (eve.isNumeric(palette)) {
					var totalColours = 0;
					for (i = 0; i < eve.palette.length; i++) {
						totalColours += eve.palette[i].length;
					}
					while (palette > totalColours) {
						palette -= totalColours;
					}
					for (i = 0; i < eve.palette.length; i++) {
						if (palette > eve.palette[i].length) {
							palette -= eve.palette[i].length;
						} else {
							return eve.palette[i][palette];
						}
					}
				} else {
					return palette;
				}
			}

			var colourOn, paletteOn;

			for (sc = 0; sc < this._series.length; sc++) {

				s = getEveElmByID(this._series[sc]);

				if (sc === 0){
					colourOn = -1;
					paletteOn = -1;
				}

				var sNode;
				var rNode;
				var nodeDC;
				var c;

				if (s._age > -1) {

					var gshape;
					var gTargets;
					var drawData = s._data;
					if (s._type === "funnel" && !isNil(s._fdata)) {
						drawData = s._fdata;
					}

//					s.id = this.id + "_series" + sc;
					var gs;
					var gl;

					if (strIs(this._type, "ring") || strIs(this._type, "dial")) {
						s._aboveAxes = true;
					}

					if (s._aboveAxes) {
						gs = this._vGroupSeriesTop.getElement("group", this._drawCall, s.id, "eve-chart-series");
						gl = this._vGroupSeriesTop.getElement("group", this._drawCall, s.id + "_labels", "eve-chart-series-labels");
					} else {
						gs = this._vGroupSeries.getElement("group", this._drawCall, s.id, "eve-chart-series");
						gl = this._vGroupSeries.getElement("group", this._drawCall, s.id + "_labels", "eve-chart-series-labels");
					}
					gs.eX = gl.eX = this._marginLeft + this._paddingLeft;
					gs.eY = gl.eY = this._marginTop + this._paddingTop;

					var sPalette = null;

					if (!isDefault(s._colour)) {
						if (eve.isNumeric(s._colour)) {
							//use chosen palette:
							s._colour = parseInt(s._colour);
							paletteOn = s._colour - (Math.floor(s._colour / eve.palette.length) * eve.palette.length);
							s._paletteColour = eve.palette[paletteOn];
							colourOn = -1;
						} else {
							//it's an array of colours (i.e. a new palette), or a single colour
							if (eve.isArray(s._colour)) {
								sPalette = s._colour;
							} else {
								s._paletteColour = s._colour;
							}
						}
					} else {
						if (s._type === "sector") {
							//if it's a pie chart, assume we'll use all the colours in the palette
							paletteOn++;
							if (paletteOn > eve.palette.length - 1) {
								paletteOn = -1;
							}
							s._paletteColour = eve.palette[paletteOn];
							colourOn = 0;
						}
					}

					if (paletteOn < 0) {
						paletteOn = 0;
					}
					if (paletteOn >= eve.palette.length) {
						paletteOn = 0;
					}
					if (isNil(sPalette)) {
						sPalette = eve.palette[paletteOn];
					}

					//get next available colour or palette:
					if (s._type !== "sector") {
						colourOn++;
						if (colourOn > sPalette.length - 1) {
							colourOn = 0;
							paletteOn++;
							if (paletteOn > sPalette.length - 1 && !eve.isArray(s._colour)) {
								paletteOn = 0;
								sPalette = eve.palette[paletteOn];
							}
						}
						s._paletteColour = sPalette[colourOn];
					}

					s._paletteColour = setPalette(s._colour, drawData.length, s._paletteColour);

					s._paletteHoverColour = [];
					s._paletteStrokeColour = [];
					s._paletteHoverStrokeColour = [];

					if (eve.isArray(s._paletteColour)) {
						for (c = 0; c < s._paletteColour.length; c++) {
							s._paletteHoverColour.push(eve.shadeColour(s._paletteColour[c], -0.25));
							s._paletteStrokeColour.push(eve.shadeColour(s._paletteColour[c], -0.35));
							s._paletteHoverStrokeColour.push(eve.shadeColour(s._paletteStrokeColour[c], -0.15));
						}
					} else {
						s._paletteHoverColour = eve.shadeColour(s._paletteColour, -0.25);
						s._paletteStrokeColour = eve.shadeColour(s._paletteColour, -0.35);
						s._paletteHoverStrokeColour = eve.shadeColour(s._paletteStrokeColour, -0.15);
					}

					s._paletteHoverColour = setPalette(s._hoverColour, drawData.length, s._paletteHoverColour);
					s._paletteStrokeColour = setPalette(s._strokeColour, drawData.length, s._paletteStrokeColour);
					s._paletteHoverStrokeColour = setPalette(s._strokeHoverColour, drawData.length, s._paletteHoverStrokeColour);

					//if (isDefault(s._lineColour)) {
					if (isNil(s.lineColour)) {
						if (eve.isArray(s._paletteColour)) {
							s._lineColour = cloneEveObj(s._paletteColour[0]);
						} else {
							s._lineColour = cloneEveObj(s._paletteColour);
						}
					}
					//if (isDefault(s._lineHoverColour)) {
					if (isNil(s.lineHoverColour)) {
						s._lineHoverColour = eve.shadeColour(s._lineColour, -0.15);
					}

					nodeDimX[sc] = [];
					nodeDimY[sc] = [];
					nodePoint[sc] = [];
					nodeBaseOffset[sc] = [];
					nodeCentroid[sc] = [];

					if (isNil(s._legend) && s._legend !== "none" && s._legend !== "") {
						s._legend = "Series " + (sc + 1);
					}

					if (isDefault(s._fillOpacity)) {
						if (s._type === "shape" || s._type === "funnel") {
							s._fillOpacity = 0.15;
						}
						else {
							s._fillOpacity = 1;
						}
					}

					if (isDefault(s._strokeOpacity)) {
						s._strokeOpacity = s._fillOpacity;
					}

					if (isDefault(s._strokeHoverOpacity)) {
						s._strokeHoverOpacity = s._strokeOpacity;
					}

					if (isNil(s.nodeLabelOffsetX)) {
						s._nodeLabelOffsetX = 0;
					}
					if (isNil(s.nodeLabelOffsetY)) {
						s._nodeLabelOffsetY = 0;
					}
					if (isNil(s.nodeLabelShowNumber)) {
						s._nodeLabelShowNumber = true;
					}
					if (isNil(s.nodeLabelShowLabel)) {
						s._nodeLabelShowLabel = true;
					}
					if (isNil(s.nodeLabelNumberFirst)) {
						s._nodeLabelNumberFirst = true;
					}
					if (isNil(s.nodeLabelNumberFormat)) {
						s._nodeLabelNumberFormat = "";
					}
					if (isNil(s.nodeLabelNumberTextGap)) {
						s._nodeLabelNumberTextGap = "auto";
					}
					if (isNil(s._nodeLabelTextFontSize)) {
						s._nodeLabelTextFontSize = 12;
					}
					if (isNil(s._nodeLabelNumberFontSize)) {
						s._nodeLabelNumberFontSize = parseFloat(s._nodeLabelTextFontSize) * 2;
					}
					if (isNil(s.nodeLabelLeaderDotSize)) {
						s._nodeLabelLeaderDotSize = 6;
					}
					if (isNil(s.nodeLabelLeaderLength)) {
						s._nodeLabelLeaderLength = 10;
					}

					if (isNil(s.nodeLabelLeaderStrokeWeight)) {
						s._nodeLabelLeaderStrokeWeight = 1;
					}

					//set node style props:
					if (s._nodeType !== "none" && s._nodeType !== ""){
						if (((s._type === "shape" || s._type == "funnel") && s._shapeNodes) || !(s._type === "shape" || s._type == "funnel")){

							s.updateNodes();

							for (dc = 0; dc < drawData.length; dc++) {

								nodeDC = getEveElmByID(s._nodes[dc]);
								clearProp(nodeDC, "labelTextFontSize");
								clearProp(nodeDC, "labelNumberFontSize");

								setNodeProps(nodeDC);
								eve.setObjProps(nodeDC);

								//set style properties to parent series' properties if they're not defined:
								if (isDefault(nodeDC._colour)) {
									if (this._drawCall > 1 && eve.isArray(s._paletteColour)) {
										//TODO: can we do this better?
										var availablePalette = s._paletteColour.clone();
										for (var dd = 0; dd < drawData.length; dd++) {
											var nodeDD = getEveElmByID(s._nodes[dd]);
											if (!isNil(nodeDD._colour) && availablePalette.indexOf(nodeDD._colour) !== -1) {
												availablePalette.remove(nodeDD._colour);
											}
											if (availablePalette.length === 0) {
												availablePalette = s._paletteColour.clone();
											}
										}
										nodeDC._colour = availablePalette[0];
									} else {
										nodeDC._colour = getColour(s._paletteColour, dc);
									}
								}
								if (isDefault(nodeDC._hoverColour)) {
									if (isDefault(s._hoverColour)) {
										nodeDC._hoverColour = eve.shadeColour(nodeDC._colour, -0.25);
										if (s._type === "funnel" || s._type === "shape" || s._type === "pod" || s._type === "ring" || s._type === "dial"){
											nodeDC._hoverColour = nodeDC._colour;
										}
									}
									else {
										nodeDC._hoverColour = getColour(s._paletteHoverColour, dc);
									}
								}

//								if (!strIs(s._type, "bar") && !strIs(s._type, "smudge")){
									if (isDefault(nodeDC._strokeColour)) {
										nodeDC._strokeColour = getColour(s._paletteStrokeColour, dc);
									}
									if (isDefault(nodeDC._strokeHoverColour)) {
										nodeDC._strokeHoverColour = getColour(s._paletteHoverStrokeColour, dc);
									}
//								}

								//	if (nodeDC._labelFontColour === null){
								if (isDefault(nodeDC.labelFontColour)) {

									if (isDefault(s._nodeLabelFontColour)) {
										if (nodeDC._labelPosn === "outside") {
											nodeDC._labelFontColour = nodeDC._colour;
//											if (getColourLuma(nodeDC._colour) > 200) {
//												nodeDC._labelFontColour = "#1e1e1e";
//											}
										} else {
											if (strIs(this._type, "ring") || strIs(this._type, "dial")) {
												nodeDC._labelFontColour = nodeDC._colour;
											} else {
												if (getColourLuma(nodeDC._colour) > 127) {
													nodeDC._labelFontColour = "#1e1e1e";
												} else {
													nodeDC._labelFontColour = "white";
												}
											}
										}
									}
									else{
										nodeDC._labelFontColour = getColour(s._nodeLabelFontColour, dc);
									}
									if(nodeDC._labelFontColour instanceof eve.parallelHatch){
										nodeDC._labelFontColour = nodeDC._labelFontColour.strokeColour;
									}
								}
							}

						}
					}

					if (s._showMeasureLines && this._drawMode !== "PDF") {
						gTargets = gs.getElement("group", this._drawCall, s.id + "_targets", "eve-chart-series-node-targets");
					}

					if (s._type === "shape" || s._type === "funnel") {
						gshape = gs.getElement("path", this._drawCall, this.id + "_series" + sc + "_shape", "eve-chart-shape", null, {
							fill: s._paletteColour,
							fillOpacity: s._fillOpacity,
							strokeWidth: s._strokeWidth,
							stroke: s._strokeColour
						});
						if (eve.isArray(s._paletteColour)) {
							gshape.setStyle("fill", s._paletteColour[0]);
						}
						gshape.fillhover = s._hoverColour;
					}

					var markerElm;
					var sLine;
					var sPoints = [];
					var hPoints = [];
					var knots = [];
					var targets = [];
					var anWedges = [];

					if (s._type === "sector" || this._type === "ring" || this._type === "dial") {

						s._measureProp = s._domains[0];
						s._labelProp = s._domains[1];
						for (xc = 0; xc < s._domains.length; xc++) {
							var itsNumeric = true;
							var valTotal = 0;
							for (var dn = 0; dn < drawData.length; dn++) {
								if (!eve.isNumeric(drawData[dn][s._domains[xc]])) {
									itsNumeric = false;
									break;
								} else {
									valTotal += parseFloat(drawData[dn][s._domains[xc]]);
								}
							}
							if (itsNumeric) {
								s._measureProp = s._domains[xc];
								s._sumVals = valTotal;
							} else {
								s._labelProp = s._domains[xc];
							}
						}

						var lblGap = 0;
						var lblPadding = 10;
						//loop through all the labels - set their text and find the largest
						for (w = 0; w < drawData.length; w++) {
							nodeDC = getEveElmByID(s._nodes[w]);
							var nodeLabelText = "";
							var nodeLabelNumber = "";
							var nodeLeading = 0.3 * (Math.max(parseFloat(nodeDC._labelNumberFontSize), parseFloat(nodeDC._labelTextFontSize)));
							if (s.nodeLabelTextLeading){
								nodeLeading = s.nodeLabelTextLeading;
							}
							var leaderY = 0;
							if (nodeDC._labels) {
								var lSize = {width: 0, height: 0};
								var nSize = {width: 0, height: 0};
								var ltSize;
								if (nodeDC._labelShowLabel){
									nodeLabelText = drawData[w][s._labelProp];
									lSize = textSize(nodeLabelText, nodeDC._labelFontFamily, nodeDC._labelTextFontSize, null, null, nodeLeading, this._drawMode, this._artboard);
								}
								if (nodeDC._labelShowNumber) {
									nodeLabelNumber = eve.formatNumber(drawData[w][s._measureProp], nodeDC._labelNumberFormat);
									nSize = textSize(nodeLabelNumber, nodeDC._labelFontFamily, nodeDC._labelNumberFontSize, null, null, nodeLeading, this._drawMode, this._artboard);
								}

								ltSize = {width: Math.max(lSize.width, nSize.width), height: lSize.height + nSize.height};

								if (nodeDC._labelShowNumber) {
									if (nodeDC._labelShowLabel){

										if (nodeDC._labelNumberFirst){
											leaderY = nSize.height;
										}
										else{
											leaderY = lSize.height;
										}

										if (!isNil(nodeDC._labelNumberTextGap) && nodeDC._labelNumberTextGap !== "auto") {
											ltSize.height += nodeDC._labelNumberTextGap;
											leaderY += nodeDC._labelNumberTextGap / 2;
										}
										else{
											ltSize.height += nodeLeading;
											leaderY += nodeLeading / 2;
										}
									}
									else{
										leaderY = ltSize.height;
									}
								}
								else if (nodeDC._labelShowLabel){
									leaderY = ltSize.height;
								}

								if (nodeDC._labelLeaderAlign === "top"){
									leaderY = 0;
								}
								else if (nodeDC._labelLeaderAlign === "bottom"){
									leaderY = ltSize.height;
								}
								if (!isNil(nodeDC._labelLeaderShoulderOffset)){
									leaderY += parseFloat(nodeDC._labelLeaderShoulderOffset);
								}

								nodeDC.nLabel = {leaderY: leaderY, width: ltSize.width, height: ltSize.height, leading: nodeLeading, diagonal: Math.abs(lineLength([0, 0], [ltSize.width, ltSize.height]))};
								var lColour = nodeDC._colour;
								if (nodeDC._colour instanceof eve.parallelHatch){
									lColour = nodeDC._colour.strokeColour;
								}
								if (!isNil(s.nodeLabelFontColour)){
									lColour = s.nodeLabelFontColour;
								}

								if (s._nodeLabelNumberFirst) {
									nodeDC.nLabel.text = "<font leading='" + nodeDC._labelNumberTextGap + "' size='" + nodeDC._labelNumberFontSize + "' color='" + lColour + "'>" + nodeLabelNumber + "</font><br />" + nodeLabelText;
								} else {
									if (this._type === "ring" || this._type === "dial") {
										if(nodeDC._labelShowLabel){
											nodeDC.nLabel.startText = nodeLabelText + "<br /><font size='" + nodeDC._labelNumberFontSize + "' color='" + lColour + "'><b>" + eve.formatNumber(0, nodeDC._labelNumberFormat) + "</b></font>";
											nodeDC.nLabel.text = nodeLabelText + "<br /><font size='" + nodeDC._labelNumberFontSize + "' color='" + lColour + "'><b>" + nodeLabelNumber + "</b></font>";
										}
										else{
											nodeDC.nLabel.startText = "<font size='" + nodeDC._labelNumberFontSize + "' color='" + lColour + "'><b>" + eve.formatNumber(0, nodeDC._labelNumberFormat) + "</b></font>";
											var lNum = nodeLabelNumber;
											if (lNum > this._axes[0]._rangeMax){
												lNum = this._axes[0]._rangeMax;
											}
											nodeDC.nLabel.text = "<font size='" + nodeDC._labelNumberFontSize + "' color='" + lColour + "'><b>" + lNum + "</b></font>";
										}
									} else {
										nodeDC.nLabel.text = nodeLabelText + "<br /><font size='" + nodeDC._labelNumberFontSize + "' color='" + lColour + "'>" + nodeLabelNumber + "</font>";
									}
								}
								if (lblGap < nodeDC.nLabel.diagonal) {
									lblGap = nodeDC.nLabel.diagonal;
								}
							}
						}

						var minSize = 0.5;
						lblGap += lblPadding * 2;

						if (isNil(s._offsetX)) {
							s._offsetX = 0;
						}
						if (isNil(s._offsetY)) {
							s._offsetY = 0;
						}

						var cCentreX = (this._plotWidth / 2) + s._offsetX;
						var cCentreY = (this._plotHeight / 2) + s._offsetY;
						var cDiameter = this._plotWidth;
						if (cDiameter > this._plotHeight) {
							cDiameter = this._plotHeight;
						}

						var pCushion = 0;
						if (isDefault(this._cushion)) {
							if (s._nodeLabels && s._nodeLabelPosn === "outside") {
								pCushion = lblGap;
							} else {
								pCushion = cDiameter / 8;
							}
						}
						else{
							if (this._cushion.toString().indexOf("px") !== -1){
								pCushion = parseFloat(this._cushion);
							}
							else{
								if (parseFloat(this._cushion) > 1 && this._cushion.toString().indexOf("%") !== -1) {
									pCushion = (parseFloat(this._cushion) / 100.0) * cDiameter;
								} else {
									pCushion = parseFloat(this._cushion) * cDiameter;
								}
							}
						}

						var outerRad = (cDiameter - parseFloat(pCushion)) / 2;
						if (outerRad * 2 < minSize * cDiameter) {
							outerRad = (cDiameter / 2) * minSize;
							for (w = 0; w < drawData.length; w++) {
								nodeDC = getEveElmByID(s._nodes[w]);
								if (nodeDC._labels) {
									nodeDC.nLabel.width *= (outerRad * 2) / cDiameter;
									nodeDC.nLabel.height *= (outerRad * 2) / cDiameter;
								}
							}
						}

						var innerRad = s._innerRadius;
						if (isNil(innerRad)) {
							innerRad = 0;
						}
						if (innerRad.toString().indexOf("%") !== -1) {
							innerRad = (parseFloat(innerRad) / 100.0) * outerRad;
						} else {
							innerRad = valToPx(parseFloat(innerRad), eve.getUnits(innerRad));
						}

						var angleStart = s._rotation;
						var lblSpacer = 5;
						var wedges = [];
						var w;

						for (w = 0; w < gs.nodes.length; w++) {
							if (gs.nodes[w].obsolete) {
								gs.removeNode(gs.nodes[w].id);
							}
						}

						if (s._shadow) {
							if (isNil(s._shadowOpacity)) {
								s._shadowOpacity = 0.25;
							}
							var shadOpacity = s._shadowOpacity;
							if (isNil(s._shadowSteps)) {
								s._shadowSteps = 50;
							}
							if (!isNil(s._shadowSteps)) {
								shadOpacity = (shadOpacity / s._shadowSteps) * 2.5;
							}
							if (isNil(s._shadowOffsetX)) {
								s._shadowOffsetX = 0;
							}
							if (isNil(s._shadowOffsetY)) {
								s._shadowOffsetY = 0;
							}

							var shadHeight = (outerRad / 2) * 0.5;
							var radX = outerRad;
							var radY = shadHeight / 2;

							var gShad = gs.getElement("g", this._drawCall, this.id + "_series_shadow", "eve-chart-ground-shadow");

							if (gShad.age === 0) {
								gShad._animCurrent = new eve.elmAnim("fade in", s._animCurrent.duration * 0.75, s._animCurrent.delay, "easeInOut");
							} else {
								gShad._animCurrent.mode = "none";
							}

							for (var ss = 0; ss < s._shadowSteps; ss++) {
								var se = gShad.getElement("ellipse", this._drawCall, this.id + "_series_shadow_" + ss, "eve-chart-ground-shadow-ellipse", {rx: (radX / s._shadowSteps) * ss, ry: (radY / s._shadowSteps) * ss}, {fill: "black", fillOpacity: shadOpacity});
								se.eX = cCentreX + s._shadowOffsetX;
								se.eY = (this._marginTop + this._paddingTop + this._plotHeight) - (shadHeight / 2) + s._shadowOffsetY;
							}
						}

						var quadOn = 1; // top right
						var sectOn = 1; // top
						var sGauge;

						for (w = 0; w < drawData.length; w++) {

							nodeDC = getEveElmByID(s._nodes[w]);
							var secRatio = drawData[w][s._measureProp] / s._sumVals;
							if (strIs(this._type, "ring")) {
								if (this._axes.length > 0) {
									this._gauge = this._axes[0];
									sGauge = getEveElmByID(this._gauge);
									if (sGauge._rangeMax > s._sumVals) {
										secRatio = s._sumVals / sGauge._rangeMax;
									}
								}
							}
							else if (strIs(this._type, "dial")) {
								if (this._axes.length > 0) {
									this._gauge = this._axes[0];
									sGauge = getEveElmByID(this._gauge);
									if (sGauge._rangeMax + 0.5 >= s._sumVals) {
										secRatio = (s._sumVals - 0.5) / sGauge._rangeMax;
									}
									else if (sGauge._rangeMax + 0.5 < s._sumVals) {
										// secRatio = (s._sumVals - 0.5) / this._gauge._rangeMax;
										secRatio = 1;
									}
								}
							}
							if (secRatio < 0){
								secRatio = 0;
							}
							var angleRange = secRatio * 360.0;
							if (strIs(this._type, "dial")){
								// angleRange -= 0.5 * (360 / this._gauge._rangeMax);
							}

							var sOffset = eve.polarToCartesian(eve.toRadians(angleStart + (angleRange / 2)), s._hoverOffset);
							sOffset[1] = -sOffset[1];

							if (!s._clockwise) {
								angleStart -= angleRange;
								sOffset = eve.polarToCartesian(eve.toRadians(angleStart + (angleRange / 2)), s._hoverOffset);
								sOffset[1] = -sOffset[1];
							}

							//gauge for ring chart:
							var tickAngle;
							var numTicks;
							if ((strIs(this._type, "ring") || strIs(this._type, "dial")) && this._axes.length > 0) {
								this._gauge = this._axes[0];
								sGauge = getEveElmByID(this._gauge);
								eve.setObjProps(sGauge);
								if (strIs(this._type, "dial")){
									sGauge._rangeMax = s._sumVals - 0.5;
								}
								else{
									sGauge._rangeMax = s._sumVals;
								}
								if (sGauge.rangeMax){ // && this._gauge.rangeMax > this._gauge._rangeMax) {
									sGauge._rangeMax = sGauge.rangeMax;
								}
								var gSector, gauge, t;
								this._vGroupAxes.eX = gs.eX;
								this._vGroupAxes.eY = gs.eY;
								numTicks = sGauge.numBands || (sGauge._rangeMax - (sGauge._rangeMin || 0));
								tickAngle = 360 / numTicks;

								if (!sGauge._gaugeColour) {
									sGauge._gaugeColour = "black";
								}
								if (!sGauge._gaugeOpacity) {
									sGauge._gaugeOpacity = 0.15;
								}

								if (sGauge._showTicks) {

									if (!sGauge._gaugeColourEnd) {
										sGauge._gaugeColourEnd = "black";
									}

									for (t = 0; t < numTicks; t++) {
										gSector = eve.addSector([cCentreX, cCentreY], angleStart + (tickAngle * t), tickAngle, outerRad, innerRad);
										gauge = this._vGroupAxes.getElement("sector", this._drawCall, this.id + "_gauge_" + t, "eve-chart-gauge", null, {fill: eve.blendColours(sGauge._gaugeColour, sGauge._gaugeColourEnd, t / numTicks), fillOpacity: sGauge._gaugeOpacity});
										gauge.sector = gSector;
										gauge.oSector = eve.addSector([cCentreX, cCentreY], s._rotation, 0, outerRad, innerRad); //morph path to grow from
										gauge._animCurrent = new eve.elmAnim("rotateAndGrow", sGauge._animCurrent.duration, sGauge._animCurrent.delay, "easeInOut");
										if (this._drawCall > 1 && !sGauge._redrawRequired){
											gauge._animCurrent.mode = "none";
										}
									}
									for (t = 0; t < numTicks; t++) {
										var gaugeLine = this._vGroupAxes.getElement("path", this._drawCall, this.id + "_gauge_line_" + t, "eve-chart-gauge-line", null, {shapeRendering: "geometricPrecision", stroke: sGauge._tickColour, strokeWidth: sGauge._tickWidth, opacity: sGauge._tickOpacity});
										var gPath = new eve.path(null, false);
										gPath.addCommand("moveTo", eve.polarToCartesian(eve.toRadians(angleStart + (tickAngle * t)), innerRad, cCentreX, cCentreY, true));
										gPath.addCommand("lineTo", eve.polarToCartesian(eve.toRadians(angleStart + (tickAngle * t)), outerRad, cCentreX, cCentreY, true));
										gaugeLine.path = gPath;
										gaugeLine._animCurrent = new eve.elmAnim("fade in", (sGauge._animCurrent.duration / 2) / numTicks, (sGauge._animCurrent.duration / 2) + sGauge._animCurrent.delay + (((sGauge._animCurrent.duration / 2) / numTicks) * t), "linear");
										if (this._drawCall > 1 && !sGauge._redrawRequired){
											gaugeLine._animCurrent.mode = "none";
										}
									}
								} else {
									gSector = eve.addSector([cCentreX, cCentreY], angleStart, 360, outerRad, innerRad);
									gauge = this._vGroupAxes.getElement("sector", this._drawCall, this.id + "_gauge", "eve-chart-gauge", null, {fill: sGauge._gaugeColour, fillOpacity: sGauge._gaugeOpacity});
									gauge.sector = gSector;
									gauge._animCurrent = new eve.elmAnim("fade in", s._animCurrent.duration, 0);
									if (this._drawCall > 1 && !sGauge._redrawRequired){
										gauge._animCurrent.mode = "none";
									}
								}

								if (s._icon) {
									var gIcon = this._vGroupOverlays.getElement("group", this._drawCall, this.id + "_icon_group", "eve-chart-icon-group");
									if (this._drawCall > 1){
										gIcon._animCurrent.mode = "none";
									}
									else{
										gIcon._animCurrent = new eve.elmAnim("fade in", (this._animInRatios.legend + this._animInRatios.overlays) * duration, s._animCurrent.duration + s._animCurrent.delay, "linear");
									}
									var sIcon = gIcon.getElement("circle", this._drawCall, this.id + "_icon_surround", "eve-chart-icon-surround", null, {fill: s._iconContainerStrokeColour});

									var icoWidth = outerRad * 0.72;
									var icoLoc = [cCentreX, cCentreY + innerRad + ((outerRad - innerRad) / 2)];

									sIcon.setAttribute("r", (icoWidth / 2) * 1.08333);
									sIcon.eX = icoLoc[0];
									sIcon.eY = icoLoc[1];

									var bIcon = gIcon.getElement("circle", this._drawCall, this.id + "_icon_bg", "eve-chart-icon-bg", {r: icoWidth / 2}, {fill: nodeDC._colour});
									bIcon.eX = icoLoc[0];
									bIcon.eY = icoLoc[1];

									var pIcon;
									var icoPath = new eve.path(null, false);
									var icoPoints = [
										[0, -32.62],
										[25.181, 0.125],
										[9.254, 0.125],
										[9.254, 30.06],
										[-9.254, 30.06],
										[-9.254, 0.125],
										[-25.181, 0.125]
									];
									if (strIs(s._icon, "tick")) {
										icoPoints = [
											[-6.934, 30.757],
											[32.84, -9.018],
											[16.636, -25.222],
											[-6.934, -1.652],
											[-18.719, -13.347],
											[-34.924, 2.768]
										];
									}

									pIcon = gIcon.getElement("path", this._drawCall, this.id + "_icon_path", "eve-chart-icon");
									icoPath.addCommand("moveTo", [icoLoc[0] + icoPoints[0][0] * (icoWidth / 100.0), icoLoc[1] + icoPoints[0][1] * (icoWidth / 100.0)]);
									for (var ip = 0; ip < icoPoints.length; ip++) {
										icoPath.addCommand("lineTo", [icoPoints[ip][0] * (icoWidth / 100.0) + icoLoc[0], icoPoints[ip][1] * (icoWidth / 100.0) + icoLoc[1]]);
									}
									icoPath.addCommand("close");
									pIcon.path = icoPath;
									pIcon.setStyle("fill", "white");

								}

							}

							var sSector;
							if (strIs(this._type, "dial")){
								sSector = gs.getElement("sector", this._drawCall, this.id + "_series" + sc + "_sector_target", "eve-chart-sector", null, {fill: nodeDC._colour, fillOpacity: s._fillOpacity, strokeWidth: s._strokeWidth, stroke: s._strokeColour});
								sSector._hoverOffset = 0;
								sSector._hoverColour = nodeDC._colour;
								if (nodeDC.hoverColour){
									sSector._hoverColour = nodeDC.hoverColour;
								}

								if (sSector.sector){
									sSector.oSector = sSector.sector;
								}
								else{
									sSector.oSector = eve.addSector([cCentreX, cCentreY], 0, tickAngle, outerRad, innerRad);
								}
								sSector.sector = eve.addSector([cCentreX, cCentreY], angleStart + (Math.floor(angleRange / 360 * numTicks) * tickAngle), tickAngle, outerRad, innerRad);
								// eve.addSector = function(centre, startAngle, angleRange, radOuter, radInner) {

								sSector._animCurrent = cloneEveObj(s._animCurrent);
								if (this._drawCall === 1){
//									sSector._animCurrent.mode = "rotateAndFadeIn";
									sSector._animCurrent.mode = "fade in";
								}
//								else{
//									sSector._animCurrent.mode = "rotateAndGrow";
//								}
							}

							// if (strIs(chart._type, "ring")){
							else {
								//s.sectorID = chart.id + "_series" + sc + "_sector_" + drawData[w][s._labelProp].split(" ").join("_").split("<br>").join("_").split("<br/>").join("_").split("<BR>").join("_").split("<BR/>").join("_").split("<br />").join("_").split("<BR />").join("_");
								s.sectorID = this.id + "_series" + sc + "_sector_" + eve.replaceSpecialChars(drawData[w][s._labelProp].split("<br>").join("_").split("<br/>").join("_").split("<BR>").join("_").split("<BR/>").join("_").split("<br />").join("_").split("<BR />").join("_"));

								var sector = eve.addSector([cCentreX, cCentreY], angleStart, angleRange, outerRad, innerRad);
								var mSector = eve.addSector([cCentreX, cCentreY], angleStart, 0, outerRad, innerRad); //morph path to grow from

								sSector = gs.getElement("sector", this._drawCall, s.sectorID, "eve-chart-sector", null, {fill: nodeDC._colour, fillOpacity: s._fillOpacity, strokeWidth: s._strokeWidth, stroke: s._strokeColour});
								sSector._hoverOffset = sOffset;
								sSector._hoverColour = nodeDC._hoverColour;

								if (s._editable) {
									var rWidth = 16;
									var rHeight = (outerRad - innerRad) + 8;
									var eHandle = this._vGroupDraggables.getElement("rect", this._drawCall, this.id + "_series" + sc + "_edit_handle", "", {width: rWidth, height: rHeight}, {fill: "red", fillOpacity: 0});

									eHandle.eWidth = rWidth;
									eHandle.eHeight = rHeight;
									eHandle.eX = cCentreX - rWidth / 2;
									eHandle.eY = cCentreY - innerRad - ((outerRad - innerRad) / 2) - (rHeight / 2);
									eHandle.eRot = [angleRange - angleStart, rWidth / 2, cCentreY - eHandle.eY];
									eHandle._rotateCentreX = rWidth / 2;
									eHandle._rotateCentreY = cCentreY - eHandle.eY;
									eHandle.series = s;
									eHandle._animCurrent = new eve.elmAnim("move", s._animCurrent.duration, s._animCurrent.delay, "easeInOut");

									// eHandle.setStyle("cursor", "url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"32px\" height=\"32px\" viewBox=\"0 0 32 32\"><path style=\"fill:black\" d=\"M16.5,0.6 L7.8,9 L14,9 L14,14 L0,14 L0,18 L14,18 L14,22 L7.8,22 L16.5,30.4 L25.2,22 L19,22 L19,18 L32,18 L32,14 L19,14 L19,9 L25.2,9\"></path><path style=\"fill:white\" d=\"M31,17 L31,15 L18,15 L18,8 L22.8,8 L16.5,2 L10.3,8 L15,8 L15,15 L1,15 L1,17 L15,17 L15,23 L10.3,23 L16.5,29 L22.8,23 L18,23 L18,17\"></path></svg>')");
									eHandle.setStyle("cursor", "pointer");

									addToEventList(eHandle.mouseDown, startDrag);
								}

								var wedgeCentre = [
									cCentreX + eve.polarToCartesian(eve.toRadians(angleStart + (angleRange / 2)), innerRad + ((outerRad - innerRad) / 2))[0],
									cCentreY - eve.polarToCartesian(eve.toRadians(angleStart + (angleRange / 2)), innerRad + ((outerRad - innerRad) / 2))[1]
								];
								if (drawData.length === 1 && innerRad === 0) {
									wedgeCentre = [cCentreX, cCentreY];
								}
								var lblStartPoint = wedgeCentre.clone();
								if (innerRad === 0 && drawData.length > 1) {
									//if it's a pie chart (not a doughnut), the middle of the wedge is too close to the centre - because the wedge comes to a point,
									// it's very tight closer to the centre, so let's move the text out a bit - say, 60% of the way out:
									var lblDist = 0.6;
									lblStartPoint = [
										cCentreX + eve.polarToCartesian(eve.toRadians(angleStart + (angleRange / 2)), outerRad * lblDist)[0],
										cCentreY - eve.polarToCartesian(eve.toRadians(angleStart + (angleRange / 2)), outerRad * lblDist)[1]
									];
								}
							}

							if (nodeDC._labels) {
								var lblLeft = 0;
								var sLabel, ldrOff, ldrPointStart, ldrPointEnd;

								if (nodeDC._labelPosn === "outside") {

									var ldrStart = [lblStartPoint[0], lblStartPoint[1]];
									// ldrStart = [cCentreX, cCentreY];
									var sLabelG = gl.getElement("group", this._drawCall, s.sectorID + "_label_group", "eve-chart-node-label-group", null);
									var lblIcon, iPath;

									if (!isNil(nodeDC._icon)){
										var icoFill = nodeDC._iconFillColour;
										if (isNil(icoFill)){
											icoFill = nodeDC._colour;
										}
										lblIcon = sLabelG.getElement("group", this._drawCall, s.sectorID + "_label_icon", "eve-chart-label-icon", {fill: icoFill});
//										lblIcon.eScale = nodeDC._iconScale;
										if (eve.isObject(nodeDC._icon) && !isNil(nodeDC._icon.path)){
											iPath = nodeDC._icon;
										}
										else{
											iPath = eve.getItemByID(icoShapes, nodeDC._icon);
										}
										if (!isNil(iPath)){
											var icoElm = lblIcon.getElement("path", this._drawCall, s.sectorID + "_label_icon_elm");
											icoElm.eScale = nodeDC._iconScale;
											icoElm.path = eve.svgPathToEvePath(iPath.path);
											icoElm.eX = nodeDC._iconOffsetX;
											icoElm.eY = nodeDC._iconOffsetY;
										}
										lblIcon._animCurrent = cloneEveObj(s._animCurrent);
										if (this._drawCall === 1) {
											lblIcon._animCurrent = new eve.elmAnim("fade in", s._animCurrent.duration / drawData.length, s._animCurrent.delay + (w * (s._animCurrent.duration / drawData.length)) + (s._animCurrent.duration / 4), "easeIn");
										}
									}

									sLabel = eve.makeText(this, sLabelG, this._drawMode, {id: s.sectorID + "_label", drawCall: this._drawCall, string: nodeDC.nLabel.text, className: "eve-chart-node-label", fontSize: parseFloat(nodeDC._labelTextFontSize), fontFamily: nodeDC._labelFontFamily, fontColour: nodeDC._labelFontColour, textAlign: "left", verticalAlign: "middle", leading: nodeDC.nLabel.leading});

									//put the labels halfway between the edge of the chart area (padding) and the edge of the pie:
									var midRad = outerRad + (((cDiameter / 2) - outerRad) / 2);
									var lblAngle = angleStart + (angleRange / 2);
									if (lblAngle > 90) {
										quadOn = 2; //bottom right
									}
									if (lblAngle > 180) {
										quadOn = 3; //bottom left
									}
									if (lblAngle > 270) {
										quadOn = 4; //top right
									}
									if (lblAngle > 45){
										sectOn = 2; // right
									}
									if (lblAngle > 135){
										sectOn = 3; // bottom
									}
									if (lblAngle > 225){
										sectOn = 4; // left
									}
									if (lblAngle > 315){
										sectOn = 1; // top
									}
									var lblPoint = eve.polarToCartesian(eve.toRadians(lblAngle), midRad);
									lblPoint[0] += (cCentreX - (sLabel.width / 2));
									lblPoint[1] = cCentreY - lblPoint[1] - (sLabel.height / 2);

									// if the leaders are spokes, align the labels to the end of the spokes:
									if (nodeDC._labelLeaderSpoke){
										lblPoint = eve.polarToCartesian(eve.toRadians(lblAngle), outerRad + nodeDC._labelLeaderLength);
										lblPoint[0] += cCentreX;
										lblPoint[1] = cCentreY - lblPoint[1];
										if (sectOn === 1){ // top
											lblPoint[0] = lblPoint[0] - ((sLabel.width + lblPadding) / 2);
											lblPoint[1] = lblPoint[1] - (sLabel.height + lblPadding);
										}
										else if (sectOn === 2){ // right
											lblPoint[0] = lblPoint[0];
											lblPoint[1] = lblPoint[1] - ((sLabel.height + lblPadding) / 2);
										}
										else if (sectOn === 3){ // bottom
											lblPoint[0] = lblPoint[0] - ((sLabel.width + lblPadding) / 2);
											if (!isNil(nodeDC._icon) && !isNil(lblIcon)){
												lblPoint[1] += (iPath.height * nodeDC._iconScale) + 20;
											}
										}
										else{ // left
											lblPoint[0] = lblPoint[0] - (sLabel.width + lblPadding);
											lblPoint[1] = lblPoint[1] - ((sLabel.height + lblPadding) / 2);
										}
										// fudge for wrong text size measurement:
										lblPoint[0] += 8;
										lblPoint[1] += 5;
									}
									lblPoint[0] += parseFloat(nodeDC._labelOffsetX);
									lblPoint[1] += parseFloat(nodeDC._labelOffsetY);

									var lblRect = {left: lblPoint[0] - (lblPadding / 2), top: lblPoint[1] - (lblPadding / 2), width: sLabel.width + lblPadding, height: sLabel.height + lblPadding};

									//make sure we don't clash with previous label:
									if (w > 0) {
										var prevNodeDC = getEveElmByID(s._nodes[w - 1]);
										if (rectanglesIntersect(lblRect, prevNodeDC.lblRect)) {
											var overlap = {left: 0, right: 0, top: 0, bottom: 0};
											overlap.left = Math.abs(lblRect.left - (prevNodeDC.lblRect.left + prevNodeDC.lblRect.width));
											overlap.right = Math.abs(prevNodeDC.lblRect.left - (lblRect.left + lblRect.width));
											overlap.top = Math.abs((prevNodeDC.lblRect.top + prevNodeDC.lblRect.height) - lblRect.top);
											overlap.bottom = Math.abs((lblRect.top + lblRect.height) - prevNodeDC.lblRect.top);

											if (quadOn === 1) {
												//top right
												if (Math.abs(overlap.left) < Math.abs(overlap.top)) {
													//move right
													lblRect.left += overlap.left + lblPadding;
													lblRect.top = cCentreY - (eve.getArcY((lblRect.left + (lblRect.width / 2)) - cCentreX, midRad) + (lblRect.height / 2));
												} else {
													//move down
													lblRect.top += overlap.top + lblPadding;
													lblRect.left = cCentreX + eve.getArcX(cCentreY - (lblRect.top + (lblRect.height / 2)), midRad) - (lblRect.width / 2);
												}
											} else if (quadOn === 2) {
												//bottom right
												if (Math.abs(overlap.left) < Math.abs(overlap.top)) {
													//move right
													lblRect.left += overlap.left + lblPadding;
													lblRect.top = cCentreY + eve.getArcY((lblRect.left + (lblRect.width / 2) - cCentreX), midRad) - (lblRect.height / 2);
												} else {
													//move down
													lblRect.top += overlap.top + lblPadding;
													lblRect.left = cCentreX + eve.getArcX((lblRect.top + (lblRect.height / 2)) - cCentreY, midRad) - (lblRect.width / 2);
												}
											} else if (quadOn === 3) {
												//bottom left
												if (Math.abs(overlap.left) < Math.abs(overlap.top)) {
													//move right
													lblRect.left += overlap.left + lblPadding;
													lblRect.top = cCentreY + eve.getArcY((cCentreX - (lblRect.left + (lblRect.width / 2))), midRad) - (lblRect.height / 2);
												} else {
													//move up
													lblRect.top -= overlap.top + lblPadding;
													lblRect.left = cCentreX - eve.getArcX((lblRect.top + (lblRect.height / 2)) - cCentreY, midRad) - (lblRect.width / 2);
												}
											} else {
												//top left
												if (Math.abs(overlap.left) < Math.abs(overlap.top)) {
													//move right
													lblRect.left += overlap.left + lblPadding;
													lblRect.top = cCentreY - eve.getArcY((lblRect.left + (lblRect.width / 2)) - cCentreX, midRad) - (lblRect.height / 2);
												} else {
													//move up
													lblRect.top -= overlap.top + lblPadding;
													lblRect.left = cCentreX - eve.getArcX(cCentreY - (lblRect.top + (lblRect.height / 2)), midRad) - (lblRect.width / 2);
												}
											}
										}
										lblPoint[0] = lblRect.left;
										lblPoint[1] = lblRect.top;
									}

									//stop the leader line passing through the label:
									if (!nodeDC._labelLeaderSpoke){
										if (lblStartPoint[0] > lblPoint[0] && lblStartPoint[0] < lblPoint[0] + sLabel.width) {
											if (quadOn > 2) {
												//left-hand side of pie
												lblPoint[0] = lblStartPoint[0] - (sLabel.width + lblSpacer);
												lblRect.left = lblPoint[0];
												if (w > 0 && rectanglesIntersect(lblRect, prevNodeDC.lblRect)) {
													lblPoint[0] = lblStartPoint[0] + lblSpacer;
												}
											} else {
												//right-hand side of pie
												lblPoint[0] = lblStartPoint[0] + lblSpacer;
												lblRect.left = lblPoint[0];
												if (w > 0 && rectanglesIntersect(lblRect, prevNodeDC.lblRect)) {
													lblPoint[0] = lblStartPoint[0] - (sLabel.width + lblSpacer);
												}
											}
										}
									}

									sLabel.eX = lblPoint[0];
									sLabel.eY = lblPoint[1];
									sLabel._animCurrent = cloneEveObj(s._animCurrent);
									lblLeft = sLabel.eX;

									var ldrEnd = [lblPoint[0] + sLabel.width, sLabel.eY + nodeDC.nLabel.leaderY];
									if (ldrStart[0] > sLabel.eX) {
										ldrEnd[0] = lblLeft;
									}
									if (nodeDC._labelLeaderSpoke){
										var lEndRad = outerRad + nodeDC._labelLeaderLength;
										ldrEnd = eve.polarToCartesian(eve.toRadians(lblAngle), lEndRad);
										ldrEnd[0] += cCentreX;
										ldrEnd[1] = cCentreY - ldrEnd[1];
									}
									//sLabel.toSVG(true, chart._drawCall);
//									if (!isNil(nodeDC._labelLeaderShoulderOffset)){
//										ldrEnd[1] += nodeDC._labelLeaderShoulderOffset;
//									}

									if (nodeDC._labelLeaderDoubleStroke && !isNil(nodeDC._labelLeaderDoubleStrokeColour)) {
										ldrOff = gl.getElement("path", this._drawCall, s.sectorID + "_leader_offset", "eve-chart-leader-offset", null, {fill: "none", fillOpacity: 0, stroke: nodeDC._labelLeaderDoubleStrokeColour, strokeWidth: nodeDC._labelLeaderStrokeWeight});
										var ldrOffPath = new eve.path(null, false);
										if (nodeDC._labelLeaderSpoke){
											ldrOffPath.addCommand("moveTo", [ldrStart[0] + nodeDC._labelLeaderStrokeWeight, ldrStart[1]]);
											ldrOffPath.addCommand("lineTo", [ldrEnd[0] + nodeDC._labelLeaderStrokeWeight, ldrEnd[1]]);
										}
										else{
											ldrOffPath.addCommand("moveTo", [ldrStart[0], ldrStart[1]]);
											ldrOffPath.addCommand("lineTo", [ldrStart[0], ldrEnd[1] + nodeDC._labelLeaderStrokeWeight]);
											ldrOffPath.addCommand("lineTo", [ldrEnd[0], ldrEnd[1] + nodeDC._labelLeaderStrokeWeight]);
										}
										if (!isNil(ldrOff.path)) {
											ldrOff.oPath = ldrOff.path;
										}
										ldrOff.path = ldrOffPath;
									}

									var ldr = gl.getElement("path", this._drawCall, s.sectorID + "_leader", "eve-chart-leader", null, {fill: "none", fillOpacity: 0, strokeWidth: nodeDC._labelLeaderStrokeWeight});
									if (nodeDC._labelLeaderDoubleStroke && !isNil(nodeDC._labelLeaderDoubleStrokeColour)) {
										ldr.setStyle("stroke", nodeDC._colour);
										if (nodeDC._colour instanceof eve.parallelHatch){
											ldr.setStyle("stroke", nodeDC._colour.strokeColour);
										}
									} else {
										ldr.setStyle("stroke", "black");
									}
									var ldrPath = new eve.path(null, false);
									if (nodeDC._labelLeaderSpoke){
										ldrPath.addCommand("moveTo", [ldrStart[0], ldrStart[1]]);
										ldrPath.addCommand("lineTo", [ldrEnd[0], ldrEnd[1]]);
									}
									else{
										if (nodeDC._labelLeaderDoubleStroke && !isNil(nodeDC._labelLeaderDoubleStrokeColour)) {
											ldrPath.addCommand("moveTo", [ldrStart[0] - nodeDC._labelLeaderStrokeWeight, ldrStart[1]]);
											ldrPath.addCommand("lineTo", [ldrStart[0] - nodeDC._labelLeaderStrokeWeight, ldrEnd[1]]);
											ldrPath.addCommand("lineTo", [ldrEnd[0], ldrEnd[1]]);
										} else {
											ldrPath.addCommand("moveTo", [ldrStart[0], ldrStart[1]]);
											ldrPath.addCommand("lineTo", [ldrStart[0], ldrEnd[1]]);
											ldrPath.addCommand("lineTo", [ldrEnd[0], ldrEnd[1]]);
										}
									}
									if (!isNil(ldr.path)) {
										ldr.oPath = ldr.path;
									}
									ldr.path = ldrPath;

									if (nodeDC._labelLeaderDotSize > 0) {
										ldrPointStart = gl.getElement("circle", this._drawCall, s.sectorID + "_leader_start_point", "eve-chart-dot-start", {r: nodeDC._labelLeaderDotSize / 2});
										ldrPointStart.eX = ldrStart[0];
										ldrPointStart.eY = ldrStart[1];
										if (nodeDC._labelLeaderDoubleStroke && !isNil(nodeDC._labelLeaderDoubleStrokeColour)) {
											ldrPointStart.setStyle("fill", nodeDC._labelLeaderDoubleStrokeColour);
										} else {
											ldrPointStart.setStyle("fill", "black");
										}
									}

									if (nodeDC._labelLeaderDotSize > 0) {
										ldrPointEnd = gl.getElement("circle", this._drawCall, s.sectorID + "_leader_end_point", "eve-chart-dot-end", {r: nodeDC._labelLeaderDotSize / 2}, {fill: "black"});
										ldrPointEnd.eX = ldrEnd[0];
										ldrPointEnd.eY = ldrEnd[1];
										if (nodeDC._labelLeaderDoubleStroke && !isNil(nodeDC._labelLeaderDoubleStrokeColour)) {
											ldrPointEnd.setStyle("fill", nodeDC._colour);
											if (nodeDC._colour instanceof eve.parallelHatch){
												ldrPointEnd.setStyle("fill", nodeDC._colour.strokeColour);
											}
										} else {
											ldrPointEnd.setStyle("fill", "black");
										}
									}

									if (ldr.age === 0) {
										ldr._animCurrent = new eve.elmAnim("draw", s._animCurrent.duration / 2, s._animCurrent.delay + (w * (s._animCurrent.duration / drawData.length)), "easeInOut");
										if (!isNil(ldrOff)){
											ldrOff._animCurrent = cloneEveObj(ldr._animCurrent);
										}
										if (nodeDC._labelLeaderDotSize > 0) {
											ldrPointStart._animCurrent = new eve.elmAnim("fade in", 0.1, ldr._animCurrent.delay, "easeInOut");
											ldrPointEnd._animCurrent = new eve.elmAnim("fade in", 0.1, ldr._animCurrent.delay + ldr._animCurrent.duration - 0.1, "easeInOut");
										}
									} else {
										ldr._animCurrent = new eve.elmAnim("morph", s._animCurrent.duration, s._animCurrent.delay, "easeInOut");
										if (!isNil(ldrOff)){
											ldrOff._animCurrent = new eve.elmAnim("morph", s._animCurrent.duration, s._animCurrent.delay, "easeInOut");
										}
										if (nodeDC._labelLeaderDotSize > 0) {
											ldrPointStart._animCurrent = cloneEveObj(ldr._animCurrent);
											ldrPointStart._animCurrent.mode = "move";
											ldrPointEnd._animCurrent = cloneEveObj(ldr._animCurrent);
											ldrPointEnd._animCurrent.mode = "move";
										}
									}
								} else if (strIs(this._type, "ring") || strIs(this._type, "dial")) {

									// var lblText = nodeDC.nLabel.startText;
									// if (this._drawCall > 1) {
									// 	lblText = nodeDC.nLabel.text;
									// }
									var lblText = nodeDC.nLabel.text;

									lblID = s.sectorID + "_label"
									if (strIs(this._type, "dial")){
										lblID = this.id + "_numLabel";
									}
									// in a dial chart, the number is updated by the dial itself, so don't set the number before the animation, else we see it flicker:
									if (strIs(this._type, "dial") && this._drawCall > 1){
										lblText = "<font size='" + nodeDC._labelNumberFontSize + "' color='" + nodeDC._colour + "'><b>" + get(lblID).firstChild.textContent + "</b></font>";
									}

									sLabel = eve.makeText(this, gl, this._drawMode, {id: lblID, drawCall: this._drawCall, string: lblText, className: "eve-chart-node-label", fontSize: nodeDC._labelTextFontSize, fontFamily: nodeDC._labelFontFamily, fontColour: nodeDC._labelFontColour, textAlign: "centre", verticalAlign: "middle", leading: (nodeDC._labelTextLeading || Math.max(parseFloat(nodeDC._labelTextFontSize), parseFloat(nodeDC._labelNumberFontSize)) * 0.1)});
									sLabel.eX = cCentreX;
									sLabel.eY = cCentreY - (sLabel.height / 2);
									if (s._nodeLabelShowLabel){
										sLabel.eY = cCentreY - (sLabel.height / 2);
									}
									if (!isNil(s._icon)){
										var iWidth = ((outerRad * 0.72) / 2) * 1.08333;
										sLabel.eY -= (iWidth / 5);
									}
									sLabel._animCurrent = cloneEveObj(s._animCurrent);
									lblLeft = sLabel.eX - cCentreX;

									if (this._drawCall === 1) {
										var fromLabel = eve.makeText(this, gl, this._drawMode, {id: s.sectorID + "_from_label", drawCall: this._drawCall, string: nodeDC.nLabel.startText, className: "eve-chart-node-label", fontSize: nodeDC._labelTextFontSize, fontFamily: nodeDC._labelFontFamily, fontColour: nodeDC._labelFontColour, textAlign: "centre", verticalAlign: "middle", leading: parseFloat(Math.max(nodeDC._labelTextFontSize), parseFloat(nodeDC._labelNumberFontSize)) * 0.2});
										// sLabel.toText = [];
										sLabel.fromText = [];
										for (var fc = 0; fc < fromLabel.nodes.length; fc++) {
											sLabel.fromText.push(fromLabel.nodes[fc].text);
											// sLabel.toText.push(toLabel.nodes[toc].text);
										}
										fromLabel.parent.removeNode(fromLabel.id);
										if (s._nodeLabelNumberFirst) {
											s._numTextElm = sLabel.nodes[0];
										} else {
											s._numTextElm = sLabel.nodes[sLabel.nodes.length - 1];
										}
									}

								} else {
									sLabel = eve.makeText(this, gl, this._drawMode, {id: s.sectorID + "_label", drawCall: this._drawCall, string: nodeDC.nLabel.text, className: "eve-chart-node-label", fontSize: nodeDC._labelTextFontSize, fontFamily: nodeDC._labelFontFamily, fontColour: nodeDC._labelFontColour, textAlign: "centre", verticalAlign: "middle"});
									sLabel.eX = lblStartPoint[0];
									sLabel.eY = lblStartPoint[1];
									sLabel._animCurrent = cloneEveObj(s._animCurrent);
									lblLeft = sLabel.eX - sLabel.width / 2;
								}

								if (!isNil(nodeDC._icon) && !isNil(lblIcon)){
									icoElm.eX = lblLeft + nodeDC._iconOffsetX + (lblRect.width / 2) - ((iPath.width * nodeDC._iconScale) / 2);
									icoElm.eY = sLabel.eY - (iPath.height * nodeDC._iconScale) + nodeDC._iconOffsetY;
									icoElm.eScale = nodeDC._iconScale;
									if (nodeDC._iconAlign === "left"){
										icoElm.eX = lblLeft + nodeDC._iconOffsetX;
									}
									else if (nodeDC._iconAlign === "right"){
										icoElm.eX = lblLeft + nodeDC._iconOffsetX + lblRect.width - iPath.width;
									}
								}

								if (this._drawCall === 1) {
									if (this._type === "ring") {
										sLabel._animCurrent = new eve.elmAnim("fadeAndFlipText", s._animCurrent.duration, s._animCurrent.delay, "easeInOut");
									} else if (this._type !== "dial") {
										sLabel._animCurrent = new eve.elmAnim("fade in", s._animCurrent.duration / drawData.length, s._animCurrent.delay + (w * (s._animCurrent.duration / drawData.length)) + (s._animCurrent.duration / 4), "easeIn");
									}
								}
								else if (this._type !== "dial") {
									sLabel._animCurrent = new eve.elmAnim("flipText", s._animCurrent.duration, s._animCurrent.delay, "easeInOut");
								}
								this._numLabel = sLabel.id;
								this._numLabelFormat = s._nodeLabelNumberFormat;
								this._numLabelSteps = numTicks;
								nodeDC.lblRect = {left: lblLeft, top: sLabel.eY - sLabel.height / 2, width: sLabel.width, height: sLabel.height};
							}


							if (s._clockwise) {
								angleStart += angleRange;
							}

							if (!strIs(this._type, "dial")){
								sSector.nodeProps = {
									fill: nodeDC._colour,
									fillHover: nodeDC._hoverColour,
									fillOpacity: nodeDC._fillOpacity,
									fillHoverOpacity: nodeDC._fillHoverOpacity,
									stroke: nodeDC._strokeColour,
									strokeHover: nodeDC._strokeHoverColour,
									strokeOpacity: nodeDC._strokeOpacity,
									strokeHoverOpacity: nodeDC._strokeHoverOpacity
								};
								fixNodeProps(sSector);
								addToEventList(sSector.mouseOver, nodeOver, [getEveElmByID(s._chart), s, false, true, false]);
								addToEventList(sSector.mouseLeave, nodeLeave, [getEveElmByID(s._chart), s, false, true, false]);

								if (!isNil(sSector.sector)) {
									sSector.oSector = sSector.sector;
								}

								sSector.sector = sector;
								var oSector = eve.addSector([cCentreX, cCentreY], s._rotation, 0, outerRad, innerRad); //start path to grow from

								if (this._drawCall === 1 || sSector.age === 0) {
									//default to "rotateAndGrow" animation
									sSector._animCurrent = cloneEveObj(s._animCurrent);
									sSector._animCurrent.mode = "rotateAndGrow";
									sSector._animCurrent.ease = "easeOut";
									if (this._type === "ring"){
										sSector._animCurrent.ease = "easeOutElastic";
									}
									sSector.oSector = oSector;
								} else {
									//default to "rotateAndGrow" animation
									sSector._animCurrent = cloneEveObj(s._animCurrent);
									sSector._animCurrent.mode = "morph";
									sSector._animCurrent.ease = "easeOut";
									if (this._type === "ring"){
										sSector._animCurrent.ease = "easeOutElastic";
									}
									if (isNil(sSector.oSector)) {
										sSector.oSector = mSector;
									}
								}

								wedges.push({id: sSector.id, sector: sector, sc: w, drawn: this._drawCall});
								wedges[wedges.length - 1].colour = sSector.getStyle("fill");
								wedges[wedges.length - 1].fillOpacity = sSector.getStyle("fill-opacity");
								wedges[wedges.length - 1].stroke = sSector.getStyle("stroke");
								wedges[wedges.length - 1].strokeWidth = sSector.getStyle("stroke-width");
								wedges[wedges.length - 1].hoverColour = nodeDC._hoverColour;
							}

						}

						if (strIs(this._type, "dial")) {
							var cDialShadow = gs.getElement("group", this._drawCall, this.id + "_dial_shadow", "eve-chart-dial-shadow", null, {opacity: s._dialShadowOpacity});
							cDialShadow.eX = cCentreX;
							cDialShadow.eY = cCentreY;
							var shadRad = innerRad + ((outerRad - innerRad) * 0.5);
//							var cDialShadowMain = cDialShadow.getElement("circle", chart._drawCall, chart.id + "_dial_shadow_main", "eve-chart-dial-shadow-main",	{r: shadRad}, {fill: s._dialShadowFillColour});
							cDialShadow.getElement("circle", this._drawCall, this.id + "_dial_shadow_main", "eve-chart-dial-shadow-main",	{r: shadRad}, {fill: s._dialShadowFillColour});
							// var cDialShadowPointer = cDialShadow.getElement("rect", this._drawCall, chart.id + "_dial_shadow_pointer", "eve-chart-dial-shadow-pointer",	{width: rectSide, height: rectSide, transform: "translate(0, -" + (innerRad + 11 + ((outerRad - innerRad) * 0.5)) + ") rotate(45)"}, {fill: s._dialShadowFillColour});
							// cDialShadowPointer.eRot = cDialShadowPointer.oRot = 45;
							// cDialShadowPointer.eY = -(innerRad + 11 + ((outerRad - innerRad) * 0.5));
							var cDial = gs.getElement("group", this._drawCall, this.id + "_dial", "eve-chart-dial");
							cDial._updateNum = true;
							cDial.eX = cCentreX;
							cDial.eY = cCentreY;
							cDial._updateSector = sSector;
//							var cDialMain = cDial.getElement("circle", this._drawCall, this.id + "_dial_main", "eve-chart-dial-main",	{r: innerRad}, {fill: s._dialFillColour});
							cDial.getElement("circle", this._drawCall, this.id + "_dial_main", "eve-chart-dial-main",	{r: innerRad}, {fill: s._dialFillColour});
							// draw the "pointer" as a square from the centre of the circle extending out to halfway between the inner and outer radii
							var rectDiag = shadRad;
							var rectSide = Math.sqrt((rectDiag * rectDiag) / 2);
							var cDialPointer = cDial.getElement("rect", this._drawCall, this.id + "_dial_pointer", "eve-chart-dial-pointer", null, {fill: s._dialFillColour});
							cDialPointer.eWidth = cDialPointer.eHeight = rectSide;
							cDialPointer.eRot = -135;
							// cDialPointer.setAttribute("stroke", "red");
							// cDialPointer.eRot = cDialPointer.oRot = 45;
							// cDialPointer.eY = -(innerRad + 11);
							// this._dialFillColour = "white";
							// this._dialShadowFillColour = "black";
							// this._dialShadowOpacity = 0.2;
							cDial.eRot = [angleRange, 0, 0];
							cDialShadow.eRot = [angleRange, 0, 0];
							if (this._drawCall === 1){
								cDial.eRot = [angleRange, 0, 0];
								cDial._animCurrent = new eve.elmAnim("rotateAndFadeIn", s._animCurrent.duration, s._animCurrent.delay, "easeOut");
								cDialShadow._animCurrent = new eve.elmAnim("rotateAndFadeIn", s._animCurrent.duration, s._animCurrent.delay, "easeOut");
							}
							else{
								cDial._animCurrent = new eve.elmAnim("rotate", s._animCurrent.duration, s._animCurrent.delay, "easeOut");
								cDialShadow._animCurrent = new eve.elmAnim("rotate", s._animCurrent.duration, s._animCurrent.delay, "easeOut");
							}
							if (!isNil(s.animCurrent.mode)){
								cDial._animCurrent.mode = cDialShadow._animCurrent.mode = s._animCurrent.mode;
							}
							if (!isNil(s.animCurrent.duration)){
								cDial._animCurrent.duration = cDialShadow._animCurrent.duration = s._animCurrent.duration;
							}
							if (!isNil(s.animCurrent.delay)){
								cDial._animCurrent.delay = cDialShadow._animCurrent.delay = s._animCurrent.delay;
							}
							if (!isNil(s.animCurrent.ease)){
								cDial._animCurrent.ease = cDialShadow._animCurrent.ease = s._animCurrent.ease;
							}
						}

						if (this._drawCall === 1) {
							gs.wedges = [];
						}
						gs.wedges[this._drawCall] = wedges;

						if (this._drawCall > 1) {

							var oldWedges = gs.wedges[this._drawCall - 1].clone();
							var newWedges = gs.wedges[this._drawCall].clone();

							//merge the old and new arrays, omitting duplicate IDs (start with the new array, that way we honour the new order):
							anWedges = newWedges.concat(oldWedges).unique("id");
							//tag any new members as new:
							for (var nw = 0; nw < anWedges.length; nw++) {
								if (oldWedges.indexOfProp("id", anWedges[nw].id) === -1) {
									anWedges[nw].new = true;
								}
							}

							//check that the order of the old members hasn't changed
							//if it has, tag them for moving:
							for (var o = 0; o < oldWedges.length; o++) {
								var n = anWedges.indexOfProp("id", oldWedges[o].id);
								var to = 0;
								var tmp = null;
								var prev = null;
								var next = null;
								if (o > 0) {
									to = o - 1;
									tmp = oldWedges[to];
									while (to > 0 && anWedges.indexOfProp("id", tmp.id) === -1) {
										tmp = oldWedges[to];
										to -= 1;
									}
									prev = tmp;
								}
								if (o < oldWedges.length - 1) {
									to = o + 1;
									tmp = oldWedges[to];
									while (to < oldWedges.length - 1 && anWedges.indexOfProp("id", tmp.id) === -1) {
										tmp = oldWedges[to];
										to += 1;
									}
									next = tmp;
								}
								if (!isNil(prev)) {
									if (prev.sc >= anWedges[n].sc) {
										anWedges[n].moveMe = true;
									}
								}
								if (!isNil(next)) {
									if (next.sc <= anWedges[n].sc) {
										anWedges[n].moveMe = true;
									}
								}
							}

							//create duplicate wedges for any that need to move so that we can animate them collapsing:
							for (n = 0; n < anWedges.length; n++) {
								if (anWedges[n].moveMe) {
									anWedges[n].moveMe = false;
									var oldWedge = cloneObj(oldWedges[oldWedges.indexOfProp("id", anWedges[n].id)]);
									//oldWedge.sector = oldWedge.sector.clone();
									oldWedge.sector = cloneEveObj(oldWedge.sector);
									var oldIndex = 0;
									if (oldWedges.indexOfProp("id", oldWedge.id) > 0) {
										var prevID = oldWedges[oldWedges.indexOfProp("id", oldWedge.id) - 1].id;
										oldIndex = anWedges.indexOfProp("id", prevID) + 1;
									}
									oldWedge.id += "_old";
									oldWedge.removeMe = true;
									anWedges[n].moved = true;
									anWedges.splice(oldIndex, 0, cloneObj(oldWedge));
								}
							}

							//tag any old wedges that are obsolete for removal
							for (n = 0; n < anWedges.length; n++) {
								if (newWedges.indexOfProp("id", anWedges[n].id) === -1) {
									anWedges[n].removeMe = true;
								}
							}

							//sort wedges by their preferred order:
							anWedges.sort(sortByProperty("sc"));

							var aDeath = new eve.elmAnim("rotateCollapse", s._animCurrent.duration, s._animCurrent.delay, s._animCurrent.ease);
							var oAngle = s._rotation;
							var nAngle = s._rotation;

							//set up the wedges' change or removal animations and before/after sector shapes:
							for (w = 0; w < anWedges.length; w++) {
								var aSector = gs.getElement("sector", this._drawCall, anWedges[w].id, "", null, {fill: anWedges[w].colour, fillOpacity: anWedges[w].fillOpacity, strokeWidth: anWedges[w].strokeWidth, stroke: anWedges[w].stroke});

								if (anWedges[w].removeMe) {
									aSector._animCurrent = cloneEveObj(aDeath);
									//aSector.oSector = anWedges[w].sector.clone();
									aSector.oSector = cloneEveObj(anWedges[w].sector);
									aSector.sector = eve.addSector([cCentreX, cCentreY], nAngle, 0, outerRad, innerRad);
									if (s._clockwise) {
										oAngle = anWedges[w].sector.startAngle + anWedges[w].sector.angleRange;
									}
//									else {
//
//									}
								} else if (anWedges[w].moved || anWedges[w].new) {
									//aSector.oSector = aSector.sector.clone();
									aSector.oSector = cloneEveObj(aSector.sector);
									aSector.oSector.angleRange = 0;
									aSector.oSector.startAngle = oAngle;
								}

								if (s._clockwise) {
									oAngle = aSector.oSector.startAngle + aSector.oSector.angleRange;
									nAngle = aSector.sector.startAngle + aSector.sector.angleRange;
								} else {
									oAngle -= aSector.oSector.angleRange;
									nAngle -= aSector.sector.angleRange;
								}
								anWedges[w].removeMe = anWedges[w].moved = null;
								aSector.nodeProps = {
									fill: anWedges[w].colour,
									fillHover: anWedges[w].hoverColour,
									fillOpacity: anWedges[w].fillOpacity,
									fillHoverOpacity: anWedges[w].fillHoverOpacity,
									stroke: anWedges[w].stroke,
									strokeHover: anWedges[w].strokeHoverColour,
									strokeOpacity: anWedges[w].strokeOpacity,
									strokeHoverOpacity: anWedges[w].strokeHoverOpacity
								};
								fixNodeProps(aSector);
								addToEventList(aSector.mouseOver, nodeOver, [getEveElmByID(s._chart), s, false, false, false]);
								addToEventList(aSector.mouseLeave, nodeLeave, [getEveElmByID(s._chart), s, false, false, false]);

							}

						}

					}

					var bandWidthX;
					var bandWidthY;
					var sAxisX;
					var sAxisY;
					var sBaseAxis;
					if (this._type === "XY") {

						sAxisX =  getEveElmByID(s._axisX);
						sAxisY =  getEveElmByID(s._axisY);
						sBaseAxis = getEveElmByID(s._baseAxis);

						bandWidthX = (this._plotWidth / sAxisX._bands.length);
						bandWidthY = (this._plotHeight / sAxisY._bands.length);
						if (s._type === "bar" || s._type === "smudge") {
							s._nodeWidth = bandWidthX - (s._barPadding + this._barGap);
							if (!sBaseAxis._horizontal) {
								s._nodeWidth = bandWidthY - (s._barPadding + this._barGap);
							}
							if (this._seriesLayout === "grouped") {
								if (s._type === "bar") {
									s._nodeWidth = s._nodeWidth / barSeries;
								}
								else{
									s._nodeWidth = s._nodeWidth / smudgeSeries;
								}
							}
						}

						s._measureProp = sAxisY._domain;
						if (!sBaseAxis._horizontal) {
							s._measureProp = sAxisX._domain;
						}

						s._domain = sBaseAxis._domain;

						if (s._type === "smudge") {
							smudgesDrawn++;
						}
						if (s._type === "bar") {
							barsDrawn++;
						}
//						if (s._type === "line" || s._type === "linearea") {
//							linesDrawn++;
//						}
//						if (s._type === "curve") {
//							curvesDrawn++;
//						}

						if (s._type === "curve") {
//							curvesDrawn++;
							s.updateNodes();
						}

						for (dc = 0; dc < drawData.length; dc++) {

							var nodeOffsetX = 0;
							var nodeOffsetY = 0;
							nodeCentroid[sc][dc] = 0;

							if (s._type === "curve") {
								var handle = [s._handleOriginX * this._plotWidth, this._plotHeight * (1 - s._handleOriginY)];
								if (dc > 0) {
									handle = [s._handleEndX  * this._plotWidth, this._plotHeight * (1 - s._handleEndY)];
								}
								hPoints.push(handle);
							}

							nodeDimX[sc][dc] = drawData[dc][sAxisX._domain];
							if (strIs(sAxisX._type, "label-band")){
								nodeDimX[sc][dc] = sc + 0.5;
							}
							if (sAxisX._rangeMin > 0) {
								nodeDimX[sc][dc] -= sAxisX._rangeMin;
							}
							if (sAxisX._type === "band") {
								nodeDimX[sc][dc] = (bandWidthX * indexByPropertyValue(sAxisX._bands, "_value", drawData[dc][sAxisX._domain])) + (bandWidthX / 2);
							} else {
								nodeDimX[sc][dc] = (this._plotWidth / (sAxisX._range / nodeDimX[sc][dc]));
							}

							nodeDimY[sc][dc] = drawData[dc][sAxisY._domain];
							if (eve.isObject(drawData[dc][sAxisY._domain])){
								var maxVal = null;
								var minVal = null;
								var vals = [];
								for (var key in drawData[dc][sAxisY._domain]) {
									if (Object.prototype.hasOwnProperty.call(drawData[dc][sAxisY._domain], key)) {
										vals.push(drawData[dc][sAxisY._domain][key]);
										if (maxVal === null || drawData[dc][sAxisY._domain][key] > maxVal){
											maxVal = drawData[dc][sAxisY._domain][key];
										}
										if (minVal === null || drawData[dc][sAxisY._domain][key] < minVal){
											minVal = drawData[dc][sAxisY._domain][key];
										}
									}
								}
								nodeDimY[sc][dc] = maxVal - minVal;
								nodeBaseOffset[sc][dc] = this._plotHeight / (sAxisY._range / minVal);
								nodeCentroid[sc][dc] = 1 - ((eve.getMidVal(vals) - minVal) / (maxVal - minVal));
							}
							if (sAxisY._rangeMin > 0) {
								nodeDimY[sc][dc] -= sAxisY._rangeMin;
							}
							if (sAxisY._type === "band") {
								nodeDimY[sc][dc] = (bandWidthY * indexByPropertyValue(sAxisY._bands, "_value", drawData[dc][sAxisY._domain])) + (bandWidthY / 2);
							} else {
								// nodeDimY[sc][dc] = this._plotHeight / (s._axisY._range / nodeDimY[sc][dc]);
								nodeDimY[sc][dc] = this._plotHeight * (nodeDimY[sc][dc] / sAxisY._range);
							}

							nodePoint[sc][dc] = [nodeDimX[sc][dc] + this._plotOrigin.x, this._plotHeight - (this._plotOrigin.y + nodeDimY[sc][dc])];
							if (sAxisY._origin === "top") {
								nodePoint[sc][dc][1] = this._plotOrigin.y + nodeDimY[sc][dc];
							}
							if (sAxisX._origin === "right") {
								nodePoint[sc][dc][0] = this._plotWidth - nodeDimX[sc][dc];
							}

							if (s._type === "bar") {
								if (this._seriesLayout === "stacked") {

									//the member "b" of the previous series is not necessarily member "b" from chart series, so we need to step through them all to find correlating entries:
									for (var e = 0; e < sc; e++) {
										if (getEveElmByID(this._series[e])._type === s._type) {
											for (var p = 0; p < getEveElmByID(this._series[e])._data.length; p++) {
												if (getEveElmByID(this._series[e])._data[p][sBaseAxis._domain] === drawData[dc][sBaseAxis._domain]) {
													nodeOffsetX += nodeDimX[e][p];
													nodeOffsetY += nodeDimY[e][p];
												}
											}
										}
									}
									if (sBaseAxis._position === "top") {
										nodePoint[sc][dc][1] += nodeOffsetY;
									} else if (sBaseAxis._position === "bottom") {
										nodePoint[sc][dc][1] -= nodeOffsetY;
									}
									if (sBaseAxis._position === "left") {
										nodePoint[sc][dc][0] += nodeOffsetX;
									} else if (sBaseAxis._position === "right") {
										nodePoint[sc][dc][0] -= nodeOffsetX;
									}
								} else { //GROUPED BARS
									if (sBaseAxis._horizontal) {
										//BARS ARE VERTICAL
										//use the "band" attribute to "link" each bar to the appropriate band on the axis so we can move them together:
										if (sAxisX._origin === "right") {
											nodePoint[sc][dc][0] += (s._nodeWidth * ((barSeries / 2) - 0.5)) - (barsDrawn * s._nodeWidth) - (this._barGap / 2);
										} else {
											nodePoint[sc][dc][0] += (barsDrawn * s._nodeWidth) - (s._nodeWidth * ((barSeries / 2) - 0.5)) + (this._barGap / 2);
										}
									} else {
										//BARS ARE HORIZONTAL
										if (sAxisY._origin === "bottom") {
											nodePoint[sc][dc][1] += (s._nodeWidth * ((barSeries / 2) - 0.5)) - (barsDrawn * s._nodeWidth);
										} else {
											nodePoint[sc][dc][1] += (barsDrawn * s._nodeWidth) - (s._nodeWidth * ((barSeries / 2) - 0.5));
										}
									}
								}
							}

							if (s._type === "smudge") {
								if (sBaseAxis._horizontal) {
									//BARS ARE VERTICAL
									//use the "band" attribute to "link" each bar to the appropriate band on the axis so we can move them together:
									if (sAxisX._origin === "right") {
										nodePoint[sc][dc][0] += (s._nodeWidth * ((smudgeSeries / 2) - 0.5)) - (smudgesDrawn * s._nodeWidth);
									} else {
										nodePoint[sc][dc][0] += (smudgesDrawn * s._nodeWidth) - (s._nodeWidth * ((smudgeSeries / 2) - 0.5));
										nodePoint[sc][dc][0] += (sc * this._barGap) - (this._barGap / 2);
									}
									if (sAxisY._origin === "bottom") {
										nodePoint[sc][dc][1] -= nodeBaseOffset[sc][dc];
									}
								} else {
									//BARS ARE HORIZONTAL
									if (sAxisY._origin === "bottom") {
										nodePoint[sc][dc][1] += (s._nodeWidth * ((smudgeSeries / 2) - 0.5)) - (smudgesDrawn * s._nodeWidth);
									} else {
										nodePoint[sc][dc][1] += (smudgesDrawn * s._nodeWidth) - (s._nodeWidth * ((smudgeSeries / 2) - 0.5));
									}
								}
							}

							if (s._type === "scatter" || s._type === "line" || s._type === "linearea" || s._type === "curve") {
								sPoints.push(nodePoint[sc][dc]);
							}

						}

						if (s._type === "curve") {
							var curveID = this.id + "_series" + sc + "_path";
							var sCurve = gs.getElement("path", this._drawCall, curveID, "eve-chart-series-curve-path", null, {fill: "none", stroke: s._lineColour, strokeWidth: s._lineWidth, strokeDasharray: s._lineDash, opacity: s._lineOpacity, strokeLinejoin: s._lineJoin, strokeLinecap: s._lineCap});
							sCurve.lineHover = s._lineHoverColour;

							addToEventList(sCurve.mouseOver, changeColour, [sCurve.lineHover, 0.15, "stroke"]);
							addToEventList(sCurve.mouseLeave, changeColour, [s._lineColour, 0.15, "stroke"]);

							var cPath = eve.addPath(null, false);
							cPath.addCommand("moveTo", sPoints[0]);
							cPath.addCommand("cubic", sPoints[1], hPoints[0], hPoints[1]);

							if (!isNil(sCurve.path)) {
								//sCurve.oPath = sCurve.path.clone();
								sCurve.oPath = cloneEveObj(sCurve.path);
							}
							sCurve.path = cPath;

							sCurve.handles = hPoints;
							if (!isNil(sCurve.points)) {
								sCurve.oPoints = sCurve.points.slice();
							}
							sCurve.points = sPoints;
							sCurve._knots = knots;
							sCurve._targets = targets;
							sCurve._plotHeight = this._plotHeight;
							sCurve._plotWidth = this._plotWidth;
							sCurve._plotOriginX = this._plotOrigin.x;
							sCurve._plotOriginY = this._plotOrigin.y;

							var outputID = this.id + "_series" + sc + "_edit_output";
							var editOutput;
							editOutput = get(outputID, this._container);

							if (s._editable) {

								var dRect = {xMin: -(this._marginLeft + this._paddingLeft), yMin: -(this._marginBottom + this._paddingBottom), xMax: this._width + this._marginRight + this._paddingRight, yMax: this._height + this._paddingTop + this._marginTop};
								var animEditElms = new eve.elmAnim("fade in", s._animCurrent.duration, s._animCurrent.delay, s._animCurrent.ease);

								var handleOriginID = this.id + "_series" + sc + "_handle_origin";
								var handleEndID = this.id + "_series" + sc + "_handle_end";
								var bandOriginID = this.id + "_series" + sc + "_handle_line_origin";
								var bandEndID = this.id + "_series" + sc + "_handle_line_end";

								var pointOrigin = gs.getElement("circle", this._drawCall, this.id + "_series" + sc + "_point_origin", "eve-chart-curve-point", {r: 12}, {stroke: "#4c4c4c", fill: "#4c4c4c", fillOpacity: 0, strokeWidth: 2, cursor: "move"});
								pointOrigin._chart = this.id;
								pointOrigin.bandline = bandOriginID;
								pointOrigin.handle = handleOriginID;
								pointOrigin.output = outputID;
								pointOrigin.curve = curveID;
								pointOrigin.eX = sPoints[0][0];
								pointOrigin.eY = sPoints[0][1];

								addToEventList(pointOrigin.mouseDown, startDrag, [dRect]);
								pointOrigin._animCurrent = animEditElms;

								var handleOrigin = gs.getElement("circle", this._drawCall, handleOriginID, "eve-chart-curve-handle", {r: 8}, {stroke: "#4c4c4c", fill: "#4c4c4c", fillOpacity: 0, strokeWidth: 2, cursor: "move"});
								handleOrigin._chart = this.id;
								handleOrigin.bandline = bandOriginID;
								handleOrigin.output = outputID;
								handleOrigin.curve = curveID;
								handleOrigin.eX = hPoints[0][0];
								handleOrigin.eY = hPoints[0][1];
								addToEventList(handleOrigin.mouseDown, startDrag, [dRect]);
								handleOrigin._animCurrent = animEditElms;

								var bandOrigin = gs.getElement("line", this._drawCall, bandOriginID, "eve-chart-curve-handle-line", {x1: sPoints[0][0], y1: sPoints[0][1], x2: hPoints[0][0], y2: hPoints[0][1]}, {stroke: "#000000", strokeWidth: 1, strokeDasharray: "5, 2", opacity: 0.35});
								bandOrigin._animCurrent = animEditElms;

								var pointEnd = gs.getElement("circle", this._drawCall, this.id + "_series" + sc + "_point_end", "eve-chart-curve-point", {r: 12}, {stroke: "#4c4c4c", fill: "#4c4c4c", fillOpacity: 0, strokeWidth: 2, cursor: "move"});
								pointEnd._chart = this.id;
								pointEnd.bandline = bandEndID;
								pointEnd.handle = handleEndID;
								pointEnd.output = outputID;
								pointEnd.curve = curveID;
								pointEnd.eX = sPoints[1][0];
								pointEnd.eY = sPoints[1][1];
								pointEnd._animCurrent = animEditElms;
								addToEventList(pointEnd.mouseDown, startDrag, [dRect]);

								var handleEnd = gs.getElement("circle", this._drawCall, handleEndID, "eve-chart-curve-handle", {r: 8}, {stroke: "#4c4c4c", fill: "#4c4c4c", fillOpacity: 0, strokeWidth: 2, cursor: "move"});
								handleEnd._chart = this.id;
								handleEnd.bandline = bandEndID;
								handleEnd.output = outputID;
								handleEnd.curve = curveID;
								handleEnd.eX = hPoints[1][0];
								handleEnd.eY = hPoints[1][1];
								addToEventList(handleEnd.mouseDown, startDrag, [dRect]);
								handleEnd._animCurrent = animEditElms;

								var bandEnd = gs.getElement("line", this._drawCall, bandEndID, "eve-chart-curve-handle-line", {x1: sPoints[1][0], y1: sPoints[1][1], x2: hPoints[1][0], y2: hPoints[1][1]}, {stroke: "#000000", strokeWidth: 1, strokeDasharray: "5, 2", opacity: 0.35});
								bandEnd._animCurrent = animEditElms;

								if (isNil(editOutput)) {
									editOutput = document.createElement("div");
								}
								editOutput.id = outputID;
								editOutput.style.position = "absolute";
								editOutput.style.zIndex = 100;
								editOutput.display = "block";
								editOutput.style.padding = "10px";
								var cDims = getElmPos(get(this._container));
								editOutput.style.left = cDims.left + "px";
								editOutput.style.top = cDims.top + "px";
								editOutput.style.overflow = "auto";
								editOutput.style.width = "250px";
								editOutput.style.height = "210px";
								editOutput.style.boxSizing = "border-box";
								editOutput.style.background = "white";
								editOutput.style.border = "10px solid #777777";
								editOutput.style.fontFamily = "inherit";
								editOutput.style.fontWeight = "bold";
								editOutput.style.fontSize = "14px";
								var output = "curve.pointOriginX = " + ((sPoints[0][0] - this._plotOrigin.x) / (this._plotWidth - this._plotOrigin.x)) + ";<br />";
								output += "curve.pointOriginY = " + (((sPoints[0][1] - this._plotHeight) - this._plotOrigin.y) / (this._plotHeight - this._plotOrigin.y)) + ";<br />";
								output += "curve.handleOriginX = " + ((hPoints[0][0] - this._plotOrigin.x) / (this._plotWidth - this._plotOrigin.x)) + ";<br />";
								output += "curve.handleOriginY = " + (((hPoints[0][1] - this._plotHeight) - this._plotOrigin.y) / (this._plotHeight - this._plotOrigin.y)) + ";<br />";
								output += "curve.pointEndX = " + ((this._plotOrigin.x - sPoints[1][0]) / (this._plotWidth - this._plotOrigin.x)) + ";<br />";
								output += "curve.pointEndY = " + (((this._plotHeight - sPoints[1][1]) - this._plotOrigin.y) / (this._plotHeight - this._plotOrigin.y)) + ";<br />";
								output += "curve.handleEndX = " + ((this._plotOrigin.x - hPoints[1][0]) / (this._plotWidth - this._plotOrigin.x)) + ";<br />";
								output += "curve.handleEndY = " + (((this._plotHeight - hPoints[1][1]) - this._plotOrigin.y) / (this._plotHeight - this._plotOrigin.y)) + ";<br />";

								editOutput.innerHTML = output;
								makeDraggable(editOutput, 10);
								//s._chart._container.appendChild(editOutput);
								document.body.appendChild(editOutput);

							}
							else{
								if (!isNil(editOutput)){
									removeNode(editOutput);
								}
							}
							sCurve._knots = knots;
							sCurve._targets = targets;
							sCurve._animCurrent = cloneEveObj(s._animCurrent);

						}

						if (s._type === "line" || s._type === "linearea") {

							//get the highlighted segments (if any):
							s.updateHighlights();
							s.updateSegments();
							var pc;

							for (pc = 0; pc < s._segments.length; pc++) {
								//set style properties to user-defined props, or else revert to parent series' properties:
								if (isNil(s._segments[pc].strokeColour)) {
									s._segments[pc]._strokeColour = s._lineColour;
								} else {
									s._segments[pc]._strokeColour = s._segments[pc].strokeColour;
								}

								if (isNil(s._segments[pc].strokeColourHover)) {
									s._segments[pc]._strokeColourHover = s._lineHoverColour;
								} else {
									s._segments[pc]._strokeColourHover = s._segments[pc].strokeColourHover;
								}

								if (isNil(s._segments[pc].strokeDash)) {
									s._segments[pc]._strokeDash = s._lineDash;
								} else {
									s._segments[pc]._strokeDash = s._segments[pc].strokeDash;
								}

								if (isNil(s._segments[pc].strokeOpacity)) {
									s._segments[pc]._strokeOpacity = s._lineOpacity;
								} else {
									s._segments[pc]._strokeOpacity = s._segments[pc].strokeOpacity;
								}

								//unless we're doing auto-styling, in which case this takes precedence:
								if (!isNil(s._highlights) && s._highlights.length > 0) {
									for (var hs = 0; hs < s._highlights.length; hs++) {
										var hi = getEveElmByID(s._highlights[hs]);
										if (hi._type === "segment" && hi._elmRef === s._segments[pc]) {
											//min values - lighter than series
											//based on series colours first:
											var hDash = "5,5";
											nodeFillHover = eve.shadeColour(s._lineColour, 0.5);
											var hColourHover = eve.shadeColour(s._lineColour, 0.25);
											var hOpacity = 1;
											//or on highlight colours if they're set:
											if (!isNil(hi._strokeColour)) {
												nodeFillHover = hi._strokeColour;
												hColourHover = eve.shadeColour(hi._strokeColour, 0.25);
											}
											if (!isNil(hi._strokeColourHover)) {
												hColourHover = hi._strokeColourHover;
											}

											//max values - darker than series
											if (hi._criteria === "max" || hi._criteria === "maxRise") {
												hDash = "0";
												nodeFillHover = eve.shadeColour(s._lineColour, -0.35);
												hColourHover = eve.shadeColour(s._lineColour, 0.5);
												if (!isNil(hi._strokeColour)) {
													hColourHover = eve.shadeColour(hi._strokeColour, -0.5);
												}
											}

											//and finally,set them to specified values if set by user:
											if (!isNil(hi.strokeColour)) {
												nodeFillHover = hi.strokeColour;
											}
											if (!isNil(hi.strokeColourHover)) {
												hColourHover = hi.strokeColourHover;
											}
											if (!isNil(hi.strokeDash)) {
												hDash = hi.strokeDash;
											}
											if (!isNil(hi.strokeOpacity)) {
												hOpacity = hi.strokeOpacity;
											}
											s._segments[pc]._strokeColour = nodeFillHover;
											s._segments[pc]._strokeColourHover = hColourHover;
											s._segments[pc]._strokeDash = hDash;
											s._segments[pc]._strokeOpacity = hOpacity;
										}
									}
								}
							}

							sLine = gs.getElement("path", this._drawCall, this.id + "_" + s._id + "_path", "eve-chart-series-line-path", null, {stroke: s._lineColour, strokeWidth: s._lineWidth, strokeDasharray: s._lineDash, strokeOpacity: s._lineOpacity, strokeLinejoin: s._lineJoin, strokeLinecap: s._lineCap, fillOpacity: 0});
							sLine.lineHover = s._lineHoverColour;
							if (!isNil(sLine.path)) {
								//sLine.oPath = sLine.path.clone();
								sLine.oPath = cloneEveObj(sLine.path);
							}
							if (s.lineDash) {
								sLine.dashArray = s.lineDash;
							}
							sLine.interpolation = s._interpolation;
							s._direction = setDirection(sPoints);

							sLine.direction = s._direction;
							sLine.path = eve.makePath(sPoints, s._interpolation, false, s._direction);

							if (!isNil(sLine.points)) {
								sLine.oPoints = sLine.points.slice();
							}
							sLine.points = sPoints;
							sLine._knots = knots;
							sLine._targets = targets;
							sLine.segments = s._segments;
							sLine.interpolation = s._interpolation;
							sLine._animCurrent = cloneEveObj(s._animCurrent);

						}
					}

					var podPoints = [];
					var podWidth = s._width;
					if (podWidth.toString().indexOf("%") !== -1){
						podWidth = ((parseFloat(podWidth) / 100.0) * this._plotWidth);
					}

					for (dc = 0; dc < drawData.length; dc++) {
					// function nodeLoop(b){

						nodeDC = getEveElmByID(s._nodes[dc]);
						//var prevStacks=0;
						var nodeKey = dc.toString();

						if (this._type === "XY") {

							if (sAxisY._type === "band" && s._type !== "funnel") {
								nodeKey = "cat_" + drawData[dc][s._domain];
							} else if (sAxisX._type === "band" && s._type !== "funnel") {
								nodeKey = "cat_" + drawData[dc][s._domain];
							}
							//idSuffix = "_" + nodeKey.replace(/\s/g, "_").split("+").join("plus");
							idSuffix = "_" + eve.replaceSpecialChars(nodeKey.toString().split("+").join("plus"));

							var nodeDuration = s._animCurrent.duration / drawData.length;
							var nodeDelay = s._animCurrent.delay + (nodeDuration * dc);

							//try each node taking a proportion of the total series time:
							nodeDuration = s._animCurrent.duration * 0.25;
							nodeDelay = s._animCurrent.delay + (((s._animCurrent.duration - nodeDuration) / drawData.length) * dc);

							var meID = this.id + "_series" + sc + "_node" + idSuffix;
							if((s._nodeType !== "none" && s._nodeType !== "") && s._nodeWidth > 0 && !strIs(s._type, "pod")){
								markerElm = gs.getElement("group", this._drawCall, meID, "eve-chart-node", null, {fillOpacity: s._fillOpacity});
								if (s._nodeGlow){
									markerElm.__glowGroup = markerElm.getElement("group", this._drawCall, meID + "_glow_group", "eve-chart-node-glow-group");
//									markerElm.glowGroup._animCurrent = new eve.elmAnim("fade in", s._animCurrent.duration, s._animCurrent.delay, s._animCurrent.ease);
//									if (this._drawCall > 1){
//										markerElm.glowGroup._animCurrent.mode = "move";
//									}
								}
								if (!isNil(nodeDC)){
									if (nodeDC._strokeOpacity > 0 && nodeDC._strokeWidth > 0) {
//										markerElm.setStyle("stroke", s._strokeColour).setStyle("stroke-opacity", s._strokeOpacity).setStyle("stroke-width", s._strokeWidth);
										markerElm.setStyle("stroke-opacity", nodeDC._strokeOpacity).setStyle("stroke-width", nodeDC._strokeWidth);
									}
								}
								else{
									if (s._strokeOpacity > 0 && s._strokeWidth > 0) {
										markerElm.setStyle("stroke-opacity", s._strokeOpacity).setStyle("stroke-width", s._strokeWidth);
									}
								}
								if (sAxisX._type === "band") {
									markerElm.band = drawData[dc][sAxisX._domain];
								} else if (sAxisY._type === "band") {
									markerElm.band = drawData[dc][sAxisY._domain];
								}
							}
							var mEndHor = {x: this._plotOrigin.x, y: nodePoint[sc][dc][1]};
							var mEndVer = {x: nodePoint[sc][dc][0], y: this._plotHeight - this._plotOrigin.y};
							if (sAxisY._origin === "top" || sAxisX._position === "top") {
								mEndVer.y = this._plotOrigin.x;
							}
							if (sAxisX._origin === "right" || sAxisY._position === "right") {
								mEndHor.x = this._plotWidth - this._plotOrigin.y;
							}

							nodeFill = s._paletteColour;
							nodeFillHover = s._hoverColour;
							if (s._type === "line" || s._type === "linearea" || s._type === "scatter" || s._type === "curve" || s._type === "bar" || s._type === "smudge" || (s._type === "shape" && s._shapeNodes) || (s._type === "funnel" && s._shapeNodes)) {
								if (!isNil(nodeDC._colour) && nodeDC._colour !== "auto") {
									nodeFill = nodeDC._colour;
									if (isNil(nodeDC._hoverColour)) {
										nodeDC._hoverColour = eve.shadeColour(nodeDC._colour, -0.25);
									}
								}
								if (!isNil(nodeDC._hoverColour) && nodeDC._hoverColour !== "auto") {
									nodeFillHover = nodeDC._hoverColour;
								}
								if (!isNil(nodeDC._strokeColour) && nodeDC._strokeColour !== "auto") {
									markerElm.setStyle("stroke", nodeDC._strokeColour).setStyle("stroke-opacity", nodeDC._strokeOpacity).setStyle("stroke-width", nodeDC._strokeWidth);
									if (isNil(nodeDC._strokeHoverColour || nodeDC._strokeHoverColour === "auto")) {
										nodeDC._strokeHoverColour = eve.shadeColour(nodeDC._strokeColour, -0.25);
									}

								}
								if (!isNil(nodeDC._hoverColour) && nodeDC._hoverColour !== "auto") {
									nodeFillHover = nodeDC._hoverColour;
								}
							}

							var flipBar;
							if (s._type === "smudge") {

								flipBar = false;

								markerElm._animDeath = new eve.elmAnim("fade and remove", s._animCurrent.duration, s._animCurrent.delay, s._animCurrent.ease);

								if (sBaseAxis._horizontal) { //BARS ARE VERTICAL

									markerElm.eX = nodePoint[sc][dc][0];

									if (nodeDimY[sc][dc] < 0) {
										nodePoint[sc][dc][1] = this._plotHeight - this._plotOrigin.y;
										flipBar = true;
									}
									sNode = markerElm.getElement("rect", this._drawCall, this.id + "_series" + sc + "_knot" + idSuffix, "eve-chart-smudge");

									sNode.eX = -s._nodeWidth / 2;
									sNode.eY = nodePoint[sc][dc][1];
									sNode.eWidth = s._nodeWidth;
									sNode.eHeight = Math.abs(nodeDimY[sc][dc]);
									if (sAxisX._position === "top") {
										sNode.eY = nodePoint[sc][dc][1] - nodeDimY[sc][dc];
									}

								} else { //BARS ARE HORIZONTAL

									markerElm.eY = nodePoint[sc][dc][1];

									if (nodeDimX[sc][dc] < 0) {
										flipBar = true;
									}
									sNode = markerElm.getElement("rect", this._drawCall, this.id + "_series" + sc + "_knot" + idSuffix, "eve-chart-smudge");
									sNode.eX = nodePoint[sc][dc][0] - nodeDimX[sc][dc];
									sNode.eY = -s._nodeWidth / 2;
									sNode.eWidth = Math.abs(nodeDimX[sc][dc]);
									sNode.eHeight = s._nodeWidth;
									if (sAxisY._position === "right") {
										sNode.eX = nodePoint[sc][dc][0];
									}
								}

								var sGrad = new eve.linearGradient(0, 0, 0, 1);
								sGrad.addStop(nodeFill, s._fringeOpacity, 0);
								sGrad.addStop(nodeFill, 1, nodeCentroid[sc][dc]);
								sGrad.addStop(nodeFill, s._fringeOpacity, 1);
								sNode.setStyle("fill", sGrad);

								// if (isNil(nodeDC._hoverColour)) {
									var hGrad = new eve.linearGradient(0, 0, 0, 1);
									hGrad.addStop(nodeFillHover, s._fringeOpacity, 0);
									hGrad.addStop(nodeFillHover, 1, nodeCentroid[sc][dc]);
									hGrad.addStop(nodeFillHover, s._fringeOpacity, 1);
									nodeDC._hoverColour = hGrad;
								// }

								if (s._strokeWidth > 0){
									var ssGrad = new eve.linearGradient(0, 0, 0, 1);
									ssGrad.addStop(nodeDC._strokeColour, s._fringeOpacity, 0);
									ssGrad.addStop(nodeDC._strokeColour, 1, nodeCentroid[sc][dc]);
									ssGrad.addStop(nodeDC._strokeColour, s._fringeOpacity, 1);
									sNode.setStyle("stroke", ssGrad);
									nodeDC._strokeColour = ssGrad;
									var hsGrad = new eve.linearGradient(0, 0, 0, 1);
									hsGrad.addStop(nodeDC._strokeHoverColour, s._fringeOpacity, 0);
									hsGrad.addStop(nodeDC._strokeHoverColour, 1, nodeCentroid[sc][dc]);
									hsGrad.addStop(nodeDC._strokeHoverColour, s._fringeOpacity, 1);
									nodeDC._strokeHoverColour = hsGrad;
								}

								if (s._redrawRequired) {
									sNode._animCurrent.mode = (sNode.age === 0 || rebuild) ? s._animBirth.mode : s._animCurrent.mode;
									sNode._animCurrent.duration = nodeDuration;
									sNode._animCurrent.delay = nodeDelay;
									sNode._animCurrent.ease = s._animCurrent.ease;

									if (this._drawCall > 1 && !rebuild) {
										markerElm._animCurrent = new eve.elmAnim(s._animCurrent.mode, nodeDuration, nodeDelay, s._animCurrent.ease);
									}

									if (flipBar) {
										if (sNode._animCurrent.mode === "growUp") {
											sNode._animCurrent.mode = "growDown";
										} else if (sNode._animCurrent.mode === "growDown") {
											sNode._animCurrent.mode = "growUp";
										} else if (sNode._animCurrent.mode === "growRight") {
											sNode._animCurrent.mode = "growLeft";
										} else if (sNode._animCurrent.mode === "growLeft") {
											sNode._animCurrent.mode = "growRight";
										}
										if (markerElm._animCurrent.mode === "growUp") {
											markerElm._animCurrent.mode = "growDown";
										} else if (markerElm._animCurrent.mode === "growDown") {
											markerElm._animCurrent.mode = "growUp";
										} else if (markerElm._animCurrent.mode === "growRight") {
											markerElm._animCurrent.mode = "growLeft";
										} else if (markerElm._animCurrent.mode === "growLeft") {
											markerElm._animCurrent.mode = "growRight";
										}
									}
								} else {
									sNode._animCurrent = cloneEveObj(s._animCurrent);
									markerElm._animCurrent = cloneEveObj(s._animCurrent);
								}
//								if (markerElm.glowGroup){
//									markerElm.glowGroup._animCurrent = cloneEveObj(sNode._animCurrent);
//								}
							}

							if (s._type === "bar") {

								flipBar = false;

								markerElm._animDeath = new eve.elmAnim("fade and remove", s._animCurrent.duration, s._animCurrent.delay, s._animCurrent.ease);

								if (sBaseAxis._horizontal) { //BARS ARE VERTICAL

									markerElm.eX = nodePoint[sc][dc][0];

									if (nodeDimY[sc][dc] < 0) {
										nodePoint[sc][dc][1] = this._plotHeight - this._plotOrigin.y;
										flipBar = true;
									}
									sNode = markerElm.getElement("rect", this._drawCall, this.id + "_series" + sc + "_knot" + idSuffix, "eve-chart-bar");

									sNode.eX = -s._nodeWidth / 2;
									sNode.eY = nodePoint[sc][dc][1];
									sNode.eWidth = s._nodeWidth;
									sNode.eHeight = Math.abs(nodeDimY[sc][dc]);
									if (sAxisX._position === "top") {
										sNode.eY = nodePoint[sc][dc][1] - nodeDimY[sc][dc];
									}

									if (s._reflect && s._reflectDepth > 0 && nodePoint[sc][dc][1] + nodeDimY[sc][dc] === this._plotHeight) {
										var sHeight = s._reflectDepth;
										if (sHeight > sNode.eHeight) {
											sHeight = sNode.eHeight;
										}
										if (sHeight > 0) {
											if (s._reflectSteps > 1) {

												for (var rs = 0; rs < s._reflectSteps; rs++) {
													var stepHeight = sHeight / s._reflectSteps;
													var oStep = s._reflectStartOpacity / s._reflectSteps;
													var startStep = 100.0 - (s._reflectStartOpacity * 100.0);
													var sOpacity = oStep / (oStep * (rs + 1 + startStep));
													sOpacity = Math.min(1, sOpacity * s._reflectStartOpacity);

													rNode = markerElm.getElement("rect", this._drawCall, this.id + "_series" + sc + "_knot_reflect_" + rs + "_" + idSuffix, "eve-chart-bar-reflect", null, {fillOpacity: sOpacity, fill: nodeFill});
													rNode.eX = sNode.eX;
													rNode.eHeight = sHeight - (stepHeight * rs);
													rNode.eY = this._plotOrigin.y + this._plotHeight + 1;
													rNode.eWidth = sNode.eWidth;
													rNode._animCurrent = new eve.elmAnim("appear");
												}

											} else {
												rNode = markerElm.getElement("rect", this._drawCall, this.id + "_series" + sc + "_knot_reflect" + idSuffix, "eve-chart-bar-reflect");
												rNode.eX = sNode.eX;
												rNode.eHeight = sHeight;
												rNode.eY = this._plotOrigin.y + this._plotHeight + 1;

												var refGradient = new eve.linearGradient(0, 0, 0, 1, null, this.id + "_series" + sc + "_reflect_gradient_" + nodeFill);
												refGradient.addStop(nodeFill, s._reflectStartOpacity, 0);
												refGradient.addStop(nodeFill, 0, 1);
												rNode.setStyle("fill", refGradient);

												rNode.eWidth = sNode.eWidth;

												if (s._redrawRequired) {
													rNode._animCurrent.mode = s._animCurrent.mode;
													if (rNode.age === 0 || rebuild) {
														rNode._animCurrent.mode = s._animBirth.mode;
													}
													rNode._animCurrent.duration = nodeDuration;
													rNode._animCurrent.delay = nodeDelay;
													rNode._animCurrent.ease = s._animCurrent.ease;

													if (!flipBar) {
														if (rNode._animCurrent.mode === "growUp") {
															rNode._animCurrent.mode = "growDown";
														} else if (rNode._animCurrent.mode === "growDown") {
															rNode._animCurrent.mode = "growUp";
														} else if (rNode._animCurrent.mode === "growRight") {
															rNode._animCurrent.mode = "growLeft";
														} else if (rNode._animCurrent.mode === "growLeft") {
															rNode._animCurrent.mode = "growRight";
														}
													}
												} else {
													sNode._animCurrent = cloneEveObj(s._animCurrent);
													markerElm._animCurrent = cloneEveObj(s._animCurrent);
												}

											}
										}

									}

								} else { //BARS ARE HORIZONTAL

									markerElm.eY = nodePoint[sc][dc][1];

									if (nodeDimX[sc][dc] < 0) {
										flipBar = true;
									}
									sNode = markerElm.getElement("rect", this._drawCall, this.id + "_series" + sc + "_knot" + idSuffix, "eve-chart-bar");
									sNode.eX = nodePoint[sc][dc][0] - nodeDimX[sc][dc];
									sNode.eY = -s._nodeWidth / 2;
									sNode.eWidth = Math.abs(nodeDimX[sc][dc]);
									sNode.eHeight = s._nodeWidth;
									if (sAxisY._position === "right") {
										sNode.eX = nodePoint[sc][dc][0];
									}
								}

								sNode.setStyle("fill", nodeFill);

								if (s._redrawRequired) {
									sNode._animCurrent.mode = (sNode.age === 0 || rebuild) ? s._animBirth.mode : s._animCurrent.mode;
									sNode._animCurrent.duration = nodeDuration;
									sNode._animCurrent.delay = nodeDelay;
									sNode._animCurrent.ease = s._animCurrent.ease;

									if (this._drawCall > 1 && !rebuild) {
										markerElm._animCurrent = new eve.elmAnim(s._animCurrent.mode, nodeDuration, nodeDelay, s._animCurrent.ease);
									}

									if (flipBar) {
										if (sNode._animCurrent.mode === "growUp") {
											sNode._animCurrent.mode = "growDown";
										} else if (sNode._animCurrent.mode === "growDown") {
											sNode._animCurrent.mode = "growUp";
										} else if (sNode._animCurrent.mode === "growRight") {
											sNode._animCurrent.mode = "growLeft";
										} else if (sNode._animCurrent.mode === "growLeft") {
											sNode._animCurrent.mode = "growRight";
										}
										if (markerElm._animCurrent.mode === "growUp") {
											markerElm._animCurrent.mode = "growDown";
										} else if (markerElm._animCurrent.mode === "growDown") {
											markerElm._animCurrent.mode = "growUp";
										} else if (markerElm._animCurrent.mode === "growRight") {
											markerElm._animCurrent.mode = "growLeft";
										} else if (markerElm._animCurrent.mode === "growLeft") {
											markerElm._animCurrent.mode = "growRight";
										}
									}
								} else {
									sNode._animCurrent = cloneEveObj(s._animCurrent);
									markerElm._animCurrent = cloneEveObj(s._animCurrent);
								}
							}

							if (s._type === "pod"){

								podPoints.push({width: podWidth - (((dc - (drawData.length / 2)) / (drawData.length / 2)) * podWidth), y: nodePoint[sc][dc][1], height: 2});
								if (dc > 0){
									podPoints[podPoints.length - 1].height = nodePoint[sc][dc - 1][1] - nodePoint[sc][dc][1];
								}
								if (dc < drawData.length / 2){
									podPoints[podPoints.length - 1].width = (dc / (drawData.length / 2)) * podWidth;
								}
								if (podPoints[podPoints.length - 1].width < 1){
									podPoints[podPoints.length - 1].width = 1;
								}

								if (dc === drawData.length - 1){
									var podPath = new eve.path(null, false);
									var midPos = {width: 0, pos: -1};
									if (s._stepped){
										podPath.addCommand("moveTo", [podPoints[0].width / 2, podPoints[0].y]);
										var pp;
										for (pp = 1; pp < podPoints.length; pp++){
											podPath.addCommand("lineTo", [podPoints[pp - 1].width / 2, podPoints[pp].y]);
											podPath.addCommand("lineTo", [podPoints[pp].width / 2, podPoints[pp].y]);
											if (midPos.width < podPoints[pp].width){
												midPos.width = podPoints[pp].width;
												midPos.pos = nodePoint[sc][pp][1];
											}
										}
										for (pp = podPoints.length - 1; pp >= 0; pp--){
											podPath.addCommand("lineTo", [-podPoints[pp].width / 2, podPoints[pp].y]);
											if (pp > 0){
												podPath.addCommand("lineTo", [-podPoints[pp - 1].width / 2, podPoints[pp].y]);
											}
										}
										podPath.addCommand("close");
									}
									else{
										podPath.addCommand("moveTo", [0, podPoints[0].y]);
										var podPointMidBottom = proportionalProps(podPoints, 0.25, ["width", "y"]);
										var podPointMidMid = proportionalProps(podPoints, 0.5, ["width", "y"]);
										var podPointMidTop = proportionalProps(podPoints, 0.75, ["width", "y"]);
										var handlesBot = eve.cubicFromPoints([0, podPoints[0].y], [podPointMidBottom.width / 2, podPointMidBottom.y], [podWidth / 2, podPointMidMid.y]);
										var handlesTop = eve.cubicFromPoints([podWidth / 2, podPointMidMid.y], [podPointMidTop.width / 2, podPointMidTop.y], [0, podPoints[podPoints.length - 1].y]);
										podPath.addCommand("cubic", handlesBot.end, handlesBot.ctrl2, handlesBot.ctrl1);
										podPath.addCommand("cubic", handlesTop.end, handlesTop.ctrl1, handlesTop.ctrl2);
										podPath.addCommand("cubic", [-handlesBot.end[0], handlesBot.end[1]], [-handlesTop.ctrl2[0], handlesTop.ctrl2[1]], [-handlesTop.ctrl1[0], handlesTop.ctrl1[1]]);
										podPath.addCommand("cubic", [0, podPoints[0].y], [-handlesBot.ctrl1[0], handlesBot.ctrl1[1]], [-handlesBot.ctrl2[0], handlesBot.ctrl2[1]]);
										midPos.pos = handlesBot.end[1];
									}

									gshape = gs.getElement("path", this._drawCall, this.id + "_series" + sc + "_shape", "eve-chart-shape", null, {fill: nodeFill});
									gshape.path = podPath;
									gshape.eX = nodePoint[sc][dc][0];

									var shOffset = s._offsetX;
									if (shOffset.toString().indexOf("%") !== -1){
										shOffset = ((parseFloat(shOffset) / 100.0) * this._plotWidth);
									}
									gshape.eX += shOffset;

									var pGrad = new eve.linearGradient(0, 0, 0, 1);
									pGrad.addStop(nodeFill, 0, 0);
									pGrad.addStop(nodeFill, 1, 1 - ((nodePoint[sc][0][1] - midPos.pos) / (nodePoint[sc][0][1] - nodePoint[sc][drawData.length - 1][1])));
									pGrad.addStop(nodeFill, 0, 1);
									gshape.setStyle("fill", pGrad);
									if (s._strokeWidth > 0){
										gshape.setStyle("stroke-width", s._strokeWidth);
										var psGrad = new eve.linearGradient(0, 0, 0, 1);
										psGrad.addStop(nodeDC._strokeColour, 0, 0);
										psGrad.addStop(nodeDC._strokeColour, 1, 1 - ((nodePoint[sc][0][1] - midPos.pos) / (nodePoint[sc][0][1] - nodePoint[sc][drawData.length - 1][1])));
										psGrad.addStop(nodeDC._strokeColour, 0, 1);
										gshape.setStyle("stroke", psGrad);
									}
									gshape._animCurrent = cloneEveObj(s._animCurrent);

									if (s._legend && s._showLegend){
										var podLgd = eve.makeText(this, this._vGroupLegend, this._drawMode, {id: this.id + "_pod_legend_" + sc, drawCall: this._drawCall, string: s._legend, className: "eve-chart-legend-text", fontSize: s._legendFontSize, fontFamily: s._legendFontFamily, fontColour: s._legendFontColour, textAlign: "centre", verticalAlign: "top", x: gshape.eX + gs.eX, y: gs.eY + this._plotHeight + s._legendOffsetY});
										podLgd._animCurrent = cloneEveObj(s._animCurrent);
										if (this._drawCall === 1){
											podLgd._animCurrent.mode = "fade in";
										}
									}

								}

							}

							if (s._type === "shape" || s._type === "funnel") {
								var pX, pY;
								if (sAxisX._type === "band") {
									//find the band:
									var bIndex = 0, bi;
									for (bi = 0; bi < sAxisX._bands.length; bi++) {
										if (sAxisX._bands[bi]._label === s._fdata[dc][sAxisX._domain]) {
											bIndex = bi;
											break;
										}
									}
									pX = ((this._plotWidth / sAxisX._bands.length) * bIndex) + (bandWidthX / 2);
									if (sAxisX._origin === "right") {
										pX = this._plotWidth - pX;
									}
								} else {
									pX = this._plotWidth * ((drawData[dc][sAxisX._domain] - sAxisX._rangeMin) / sAxisX._range);
									if (sAxisX._origin === "right") {
										pX = this._plotWidth - (this._plotWidth * ((drawData[dc][sAxisX._domain] - sAxisX._rangeMin) / sAxisX._range));
									}
								}
								pY = nodePoint[sc][dc][1];

								sPoints.push([pX, pY]);
							}

							if ((s._type === "scatter" || s._type === "line" || s._type === "linearea" || s._type === "curve" || (s._type === "shape" && s._shapeNodes) || (s._type === "funnel" && s._shapeNodes)) && !isNil(markerElm)) {
								//change this if the nodeType is not "circle":
								var knotAge = (this._drawCall === 1 || rebuild) ? 0 : this._drawCall;
								var nTarget;
								sNode = markerElm.getElement("circle", knotAge, this.id + "_series" + sc + "_knot" + idSuffix, "eve-chart-knot", {r: nodeDC._width / 2}, {fill: nodeFill, fillOpacity: nodeDC._fillOpacity});

								if (nodeDC._strokeOpacity > 0 && nodeDC._strokeWidth > 0) {
									markerElm.setStyle("stroke", nodeDC._strokeColour).setStyle("stroke-opacity", nodeDC._strokeOpacity).setStyle("stroke", nodeDC._strokeColour).setStyle("stroke-width", nodeDC._strokeWidth);
								}
								if ((s._type === "shape" || s._type === "funnel") && s._shapeNodes){
									//sNode.setStyle("strokeOpacity", 0);
									//markerElm.setStyle("opacity", 0);
								}
								sNode.eX = nodePoint[sc][dc][0];
								sNode.eY = nodePoint[sc][dc][1];

								if (nodeDC._glow){
									var gOpacities = eve.fakeGradientAlphas(0, nodeDC._glowOpacity, nodeDC._glowSteps);
									for (var gst = 0; gst < nodeDC._glowSteps; gst++){
										var gNode = markerElm.__glowGroup.getElement("circle", knotAge, this.id + "_series" + sc + "_knot" + idSuffix + "_glow_" + gst, "eve-chart-knot-glow", {r: nodeDC._width / 2});
										gNode.setStyle("fill", nodeDC._glowColour)
											.setStyle("stroke", nodeDC._glowColour)
											.setStyle("stroke-width", gst * (nodeDC._glowSize / nodeDC._glowSteps))
											.setStyle("opacity", gOpacities[gst]);
										gNode.eX = sNode.eX + nodeDC._glowOffsetX;
										gNode.eY = sNode.eY + nodeDC._glowOffsetY;
										if (this._drawCall === 1 && this._drawMode !== "PDF" && !rejigOnly){
											markerElm.__glowGroup.setStyle("opacity", 0);
										}
									}
								}

								if (s._showMeasureLines && this._drawMode !== "PDF") {
									nTarget = gTargets.getElement("circle", this._drawCall, this.id + "_series" + sc + "_target" + idSuffix, "eve-chart-target", {r: nodeDC._width / 2}, {fillOpacity: 0, shapeRendering: "geometricPrecision", opacity: 0, pointerEvents: "none"});
									if (nodeDC._strokeOpacity > 0 && nodeDC._strokeWidth > 0) {
										nTarget.setStyle("stroke", nodeDC._strokeColour).setStyle("stroke-opacity", nodeDC._strokeOpacity).setStyle("stroke-width", 1);
									}
									nTarget.rad = nodeDC._width / 2;
									nTarget.radHover = nodeDC._width;
									nTarget.eX = nodePoint[sc][dc][0];
									nTarget.eY = nodePoint[sc][dc][1];
								}

								if (nodeDC._visible === false || s._editable) {
									sNode.setStyle("visibility", "hidden");
									if (s._showMeasureLines) {
										nTarget.setStyle("visibility", "hidden");
									}
								}

								if (knotAge > 0 && !((s._type === "shape" || s._type === "funnel") && s._shapeNodes)){
									sNode.setStyle("opacity", 1);
								}
								else {
									// if ((s._type === "shape" && s._shapeNodes) || (s._type === "funnel" && s._shapeNodes) || s._type === "scatter"){
									//
									// }
									// else{
									if (s._type !== "scatter" && s._animCurrent.duration > 0) {
										sNode.setStyle("opacity", 0);
									}
								}
								//added this condition to prevent new knots appearing in their final position when we change data points on a line series:
								if ((s._type === "line" || s._type === "linearea") && s._animCurrent.duration > 0 && sNode.age === 0 && this._drawCall > 1) {
									sNode.setStyle("opacity", 0);
								}
								knots[dc] = sNode;
								targets[dc] = nTarget;

								if (s._redrawRequired) {
									if (((s._type === "shape" || s._type === "funnel") && s._shapeNodes) || (s._type === "scatter")) {
									// if (s._type === "scatter") {
										sNode._animCurrent = new eve.elmAnim(s._animCurrent.mode, nodeDuration, nodeDelay, s._animCurrent.ease);
									}
								} else {
									sNode._animCurrent = cloneEveObj(s._animCurrent);
									if (this._drawCall > 1 && !rebuild) {
										sNode._animCurrent.mode = "move";
									}
									markerElm._animCurrent = new eve.elmAnim(s._animCurrent.mode, nodeDuration, nodeDelay, s._animCurrent.ease);
								}
								markerElm._animDeath = new eve.elmAnim("fade and remove", nodeDuration, nodeDelay, "linear");
							}

							if (!isNil(sNode) && !strIs(s._type, "pod")) {
								if (s._type === "smudge"){
									nodeFill = sGrad;
									nodeFillHover = hGrad;
								}
								sNode.nodeProps = {
									fill: nodeFill,
									fillHover: nodeFillHover,
									fillOpacity: nodeDC._fillOpacity,
									fillHoverOpacity: nodeDC._fillHoverOpacity,
									stroke: nodeDC._strokeColour,
									strokeHover: nodeDC._strokeHoverColour,
									strokeOpacity: nodeDC._strokeOpacity,
									strokeHoverOpacity: nodeDC._strokeHoverOpacity
								};
								fixNodeProps(sNode);
								addToEventList(sNode.mouseOver, nodeOver, [getEveElmByID(s._chart), s, s._showMeasureLines, s._showTooltips, s._editable]);
								addToEventList(sNode.mouseLeave, nodeLeave, [getEveElmByID(s._chart), s, s._showMeasureLines, s._showTooltips, s._editable]);
//								addToEventList(sNode.mouseOver, nodeHover, [getEveElmByID(s._chart), s, s._showMeasureLines, s._showTooltips, s._editable]);
								if (!isNil(markerElm.__glowGroup)){
									sNode._glow = markerElm.__glowGroup;
								}
							}

							mEndHor.y = nodePoint[sc][dc][1];
							mEndVer.x = nodePoint[sc][dc][0];
							if (s._type === "smudge"){
								// mEndHor.y += nodeDimY[sc][dc] - (nodeDimY[sc][dc] * nodeCentroid[sc][dc]);
								mEndHor.y = 100;
							}

							if (!isNil(nodeDC) && s._showMeasureLines && !isNil(markerElm) && this._drawMode !== "PDF") {

								var gm = this._vGroupMeasures.getElement("group", this._drawCall, this.id + "_series" + sc + "_measures", "eve-chart-measures");
								gm.eX = this._marginLeft + this._paddingLeft;
								gm.eY = this._marginTop + this._paddingTop;

								if (isDefault(s._measureLineColour)) {
									s._measureLineColour = eve.shadeColour(nodeDC._strokeColour, -0.25);
								}

								var mLineHor = gm.getElement("line", this._drawCall, this.id + "_series" + sc + "_measureLineHor" + idSuffix, "eve-chart-measure-hor", {x1: nodePoint[sc][dc][0], y1: nodePoint[sc][dc][1], x2: nodePoint[sc][dc][0], y2: nodePoint[sc][dc][1], vectorEffect: "non-scaling-stroke"}, {strokeDasharray: "2,2", stroke: s._measureLineColour, strokeWidth: s._measureLineWidth, opacity: s._measureLineOpacity, pointerEvents: "none"});
								if (s._type === "smudge"){
									mLineHor.setAttribute("y1", nodePoint[sc][dc][1] + (nodeDimY[sc][dc] * nodeCentroid[sc][dc]));
									mLineHor.setAttribute("y2", nodePoint[sc][dc][1] + (nodeDimY[sc][dc] * nodeCentroid[sc][dc]));
								}
								mLineHor.endX = mEndHor.x;
								mLineHor.endY = mEndHor.y;
								markerElm.hLine = mLineHor.id;

								var mLineVer = gm.getElement("line", this._drawCall, this.id + "_series" + sc + "_measureLineVer" + idSuffix, "eve-chart-measure-ver", {x1: nodePoint[sc][dc][0], y1: nodePoint[sc][dc][1], x2: nodePoint[sc][dc][0], y2: nodePoint[sc][dc][1], vectorEffect: "non-scaling-stroke"}, {strokeDasharray: "2, 2", stroke: s._measureLineColour, strokeWidth: s._measureLineWidth, opacity: s._measureLineOpacity, pointerEvents: "none"});
								if (s._type === "smudge"){
									mLineVer.setAttribute("y1", nodePoint[sc][dc][1] + (nodeDimY[sc][dc] * nodeCentroid[sc][dc]));
									mLineVer.setAttribute("y2", nodePoint[sc][dc][1] + (nodeDimY[sc][dc] * nodeCentroid[sc][dc]));
								}
								mLineVer.endX = mEndVer.x;
								mLineVer.endY = mEndVer.y;
								markerElm.vLine = mLineVer.id;

								if (s._type === "bar") {
									if (sBaseAxis._horizontal) {
										mLineVer.setStyle("display", "none");
									} else {
										mLineHor.setStyle("display", "none");
									}
								}
							}
						} // if (this._type === "XY")

						//TOOLTIPS:

						if (!isNil(nodeDC) && s._showTooltips && this._drawMode !== "PDF" && !strIs(s._type, "pod") && (s._type === "sector" || !isNil(markerElm))) {

							if (s._type === "sector") {
								markerElm = gs.wedges[this._drawCall][dc].sector;
								markerElm.tooltipID = this.id + "_series" + sc + "_tooltip_" + eve.replaceSpecialChars(drawData[dc][s._labelProp]);
							} else {
								markerElm.tooltipID = this.id + "_series" + sc + "_tooltip_" + idSuffix;
							}

							var tooltip = this._vGroupToolTips.getElement("group", this._drawCall, markerElm.tooltipID, "eve-chart-tooltip", null, {opacity: 0, display: "none", pointerEvents: "none"});
							if (s._type === "sector") {
								tooltip.eX = markerElm.getCentroid()[0] + markerElm.centre[0];
								tooltip.eY = markerElm.centre[1] - markerElm.getCentroid()[1];
							} else {
								tooltip.eX = nodePoint[sc][dc][0];
								tooltip.eY = nodePoint[sc][dc][1];
							}

							if (this._drawCall > 1) {
								tooltip._animCurrent = new eve.elmAnim("move", s._animCurrent.delay, s._animCurrent.delay, "easeOut");
							}

							var bgID = idSuffix;
							if (s._type === "sector") {
								bgID = eve.replaceSpecialChars(drawData[dc][s._labelProp]);
							}
							var ttBG = tooltip.getElement("rect", this._drawCall, this.id + "_series" + sc + "_tooltip_bg_" + bgID, "eve-chart-tooltip-rect", null, {fill: s._tooltipBackgroundColour, fillOpacity: s._tooltipBackgroundOpacity, stroke: s._tooltipBorderColour, strokeWidth: s._tooltipBorderWidth, shapeRendering: "crispEdges"});

							var ttString = "";
							if (s._legend !== "none" && s._legend !== "") {
								if (s._legend === "auto") {
									s._legend = "Series " + (sc + 1);
								}
								ttString += "<b>" + s._legend + "</b><br>";
							}
							if (!isNil(nodeDC._tooltipTitle) && nodeDC._tooltipTitle !== ""){
								ttString += "<b>" + nodeDC._tooltipTitle + "</b><br>";
							}

							if (this._type === "XY") {
								if (sAxisX._type === "band" || sAxisY._type === "band") {
									if (!isNil(s._domain)) {
										ttString += s._domain.toProperCase() + ": " + drawData[dc][s._domain] + "<br>";
									}
									if (sAxisY._type === "band") {
										ttString += s._measureProp.toProperCase() + ": " + eve.formatNumber(nodeDC._data[s._measureProp], sAxisX._tickTextFormat) + "<br>";
									} else {
										if (eve.isObject(nodeDC._data[s._measureProp])){
											for (var datakey in nodeDC._data[s._measureProp]) {
												if (Object.prototype.hasOwnProperty.call(nodeDC._data[s._measureProp], datakey)) {
													ttString += datakey + ": " + eve.formatNumber(nodeDC._data[s._measureProp][datakey], sAxisY._tickTextFormat) + "<br>";
												}
											}
										}
										else{
											ttString += s._measureProp.toProperCase() + ": " + eve.formatNumber(nodeDC._data[s._measureProp], sAxisY._tickTextFormat) + "<br>";
										}
									}
								} else {
									if (sAxisY._domain === s._measureProp) {
										ttString += sAxisX._domain.toProperCase() + ": " + eve.formatNumber(drawData[dc][sAxisX._domain], sAxisX._tickTextFormat) + "<br>";
										ttString += sAxisY._domain.toProperCase() + ": " + eve.formatNumber(drawData[dc][sAxisY._domain], sAxisY._tickTextFormat) + "<br>";
									} else {
										ttString += sAxisY._domain.toProperCase() + ": " + eve.formatNumber(drawData[dc][sAxisY._domain], sAxisY._tickTextFormat) + "<br>";
										ttString += sAxisX._domain.toProperCase() + ": " + eve.formatNumber(drawData[dc][sAxisX._domain], sAxisX._tickTextFormat) + "<br>";
									}
								}
							} else if (s._type === "sector") {
								ttString = drawData[dc][s._labelProp] + "<br>" + drawData[dc][s._measureProp];
								if (s.tooltipNumberFormat){
									ttString = drawData[dc][s._labelProp] + "<br>" + eve.formatNumber(drawData[dc][s._measureProp], s.tooltipNumberFormat);
								}
							}

							var ttID = idSuffix;
							if (s._type === "sector") {
								ttID = eve.replaceSpecialChars(drawData[dc][s._labelProp]);
							}

							var ttTextElm = eve.makeText(this, tooltip, this._drawMode, {id: this.id + "_series" + sc + "_tooltip_text_" + ttID, drawCall: this._drawCall, string: ttString, className: "eve-chart-tooltip-text", fontSize: s._tooltipFontSize, fontFamily: s._tooltipFontFamily, fontColour: s._tooltipFontColour});
							var ttWidth = ttTextElm.width + (s._tooltipPadding * 2);
							var ttHeight = ttTextElm.height + (s._tooltipPadding * 2);

							ttTextElm.eX = s._tooltipPadding;
							ttTextElm.eY = s._tooltipPadding;
							//nudge the text up a bit to compensate for inaccuracies in measuring its height:
							ttTextElm.eY -= s._tooltipFontSize * 0.2;

							ttBG.eWidth = ttWidth;
							ttBG.eHeight = ttHeight;

							if (this._type === "XY") {
								//x-axis origin on the left, y-axis origin at the bottom - try and put the tooltip just above the nodePoint:
								var ttx = nodePoint[sc][dc][0] - (ttWidth / 2);
								var tty = nodePoint[sc][dc][1] - (ttHeight + 10);

								if (tty - ttHeight < -(this._marginTop + this._paddingTop)) {
									//if it goes off the top of the chart, move it to the right of the node instead:
									tty = 10;
									ttx = nodePoint[sc][dc][0] + (s._nodeWidth / 2) + 10;
									//if it then goes off the right of the chart, move it to the left:
									if (ttx + ttWidth > this._plotWidth + this._marginRight + this._paddingRight) {
										ttx = nodePoint[sc][dc][0] - (ttWidth + (s._nodeWidth / 2) + 10);
									}
								}
								//if it goes off the left of the chart, move it to the right of the node:
								if (ttx < -(this._marginLeft + this._paddingLeft)) {
									tty = nodePoint[sc][dc][1] - (ttHeight / 2);
									ttx = nodePoint[sc][dc][0] + (s._nodeWidth / 2) + 10;
								}
								//if it goes off the right of the chart, move it to the left:
								if (ttx + ttWidth > this._plotWidth + this._marginRight + this._paddingRight) {
									tty = nodePoint[sc][dc][1] - (ttHeight / 2);
									ttx = nodePoint[sc][dc][0] - (ttWidth + (s._nodeWidth / 2) + 10);
								}

								if (s._measureProp === sAxisY._domain && sAxisY._origin === "top") {
									//y-axis origin at the top - try and put the tooltip just below the nodePoint:
									ttx = nodePoint[sc][dc][0] - (ttWidth / 2);
									tty = nodePoint[sc][dc][1] + 10;
									if (tty + ttHeight > this._plotHeight + this._marginBottom + this._paddingBottom) {
										//if it goes off the bottom of the chart, move it to the right of the node instead:
										tty = this._plotHeight + this._marginBottom + this._paddingBottom - (10 + ttHeight);
										ttx = nodePoint[sc][dc][0] + (s._nodeWidth / 2) + 10;
										//if it then goes off the right of the chart, move it to the left:
										if (ttx + ttWidth > this._plotWidth + this._marginRight + this._paddingRight) {
											ttx = nodePoint[sc][dc][0] - (ttWidth + (s._nodeWidth / 2) + 10);
										}
									}
									//if it goes off the left of the chart, move it to the right of the node:
									if (ttx < -(this._marginLeft + this._paddingLeft)) {
										tty = nodePoint[sc][dc][1] - (ttHeight / 2);
										ttx = nodePoint[sc][dc][0] + (s._nodeWidth / 2) + 10;
									}
									//if it goes off the right of the chart, move it to the left:
									if (ttx + ttWidth > this._plotWidth + this._marginRight + this._paddingRight) {
										tty = nodePoint[sc][dc][1] - (ttHeight / 2);
										ttx = nodePoint[sc][dc][0] - (ttWidth + (s._nodeWidth / 2) + 10);
									}
								}

								if (s._measureProp === sAxisX._domain) {
									if (sAxisX._origin === "left") {
										//x-axis origin at the left - try and put the tooltip to the right of the nodePoint:
										ttx = nodePoint[sc][dc][0] + 10;
										tty = nodePoint[sc][dc][1] - (ttHeight / 2);
										//if it goes off the right of the chart, move it above the node:
										if (ttx + ttWidth > this._plotWidth + this._marginRight + this._paddingRight) {
											ttx = this._plotWidth + this._marginRight + this._paddingRight - (ttWidth + 10);
											tty = nodePoint[sc][dc][1] - ((s._nodeWidth / 2) + ttHeight + 10);
											//if it then goes off the top of the chart, move it below the nodePoint:
											if (tty < -(this._marginTop + this._paddingTop)) {
												ttx = nodePoint[sc][dc][0] - (ttWidth / 2);
												tty = nodePoint[sc][dc][1] + 10;
											}
										}
									} else {
										//x-axis origin at the right - try and put the tooltip to the left of the nodePoint:
										ttx = nodePoint[sc][dc][0] - (ttWidth + 10);
										tty = nodePoint[sc][dc][1] - (ttHeight / 2);
										//if it goes off the left of the chart, move it above the node:
										if (ttx < 0) {
											ttx = nodePoint[sc][dc][0] - (ttWidth / 2);
											if (ttx < 0) {
												ttx = 10;
											}
											tty = nodePoint[sc][dc][1] - (ttHeight + (s._nodeWidth / 2) + 10);
											//if it then goes off the top of the chart, move it below the nodePoint:
											if (tty < -(this._marginTop + this._paddingTop)) {
												ttx = nodePoint[sc][dc][0] - (ttWidth / 2);
												tty = nodePoint[sc][dc][1] + 10 + (s._nodeWidth / 2);
											}
										}
									}
								}

								if (s._type === "scatter" || s._type === "line" || s._type === "linearea" || s._type === "curve") {
									if (sBaseAxis._position === "bottom") {
										tty -= nodeDC._height;
									} else if (sBaseAxis._position === "top") {
										tty += nodeDC._height;
									} else if (sBaseAxis._position === "left") {
										ttx += nodeDC._width;
									} else if (sBaseAxis._position === "top") {
										ttx -= nodeDC._width;
									}
								}

								tooltip.eX = ttx;
								tooltip.eY = tty;
							} else {
								tooltip.eX -= ttWidth / 2;
								tooltip.eY -= ttHeight / 2;
							}
						}

					}

					if (s._type === "shape" || s._type === "funnel") {
						if (s._redrawRequired) {

							if (!isNil(gshape.path)) {
								gshape.oPath = cloneEveObj(gshape.path);
							}
							gshape.path = eve.makePath(sPoints, s._interpolation);
							if (s._type === "funnel") {
								gshape.pathTop = eve.makePath(sPoints.slice(0, sPoints.length / 2), s._interpolation);
								gshape.pathBottom = eve.makePath(sPoints.slice(sPoints.length / 2).reverse(), s._interpolation);
							}

							if (!isNil(gshape.points)) {
								gshape.oPoints = gshape.points.slice();
							}
							gshape.points = sPoints;
							gshape.closed = true;
							gshape._animCurrent = s._animCurrent;
						} else {
							gshape._animCurrent = cloneEveObj(s._animCurrent);
						}
					}

					if ((s._type === "line" || s._type === "linearea") && !isNil(sLine)) {
						sLine._knots = knots;
						sLine._targets = targets;
					}
					animTime += s._animCurrent.duration;

				}
				else {
					this._series.remove(s);
					s = undefined;
				}

			}

			buildLegend(this);

			function buildLegend(c){

				var sc;
				var s;
				var showLegends = false;
				var legendTextSize = {
					width: 0,
					height: 0
				};
				var swatchSize = {
					width: 0,
					height: 0
				};
				s = getEveElmByID(c._series[0]);
				var largestFontSize = 10;

				var lSwatchMargin, lMargin, legendSize;

				if (!isNil(c.showLegend) && c.showLegend === false) {
					showLegends = false;
				} else {
					if (getType(c._type) === "sector") {
						if (s._showLegend && s._legend !== "") {
							showLegends = true;
						}
					} else {
						for (sc = 0; sc < c._series.length; sc++) {
							var ss = getEveElmByID(c._series[sc]);
							if (ss._showLegend && ss._legend !== "" && !strIs(ss._type, "pod")) {
								showLegends = true;
							}
						}
					}
				}

				if (showLegends) {
					var lSize;
					if (getType(c._type) === "sector") {
						if (s._showLegend && s._legend !== "") {
							largestFontSize = s._legendFontSize;
							for (nc = 0; nc < s._nodes.length; nc++) {
								var n = getEveElmByID(s._nodes[nc]);
								n._label = n._data[s._labelProp];
								if (s._legendIgnoreBreaks) {
									n._label = n._label.split("<br>").join(" ").split("<BR>").join(" ").split("<br/>").join(" ").split("<BR/>").join(" ").split("<br />").join(" ").split("<BR />").join(" ");
								}
								// lSize = textSize(n._label, s._legendFontFamily, s._legendFontSize, null, null, this._drawMode, this._artboard);
								lSize = textSize(n._label, s._legendFontFamily, s._legendFontSize, null, null, 0, c._drawMode, c._artboard);
								if (legendTextSize.width < lSize.width) {
									legendTextSize.width = lSize.width;
								}
								if (legendTextSize.height < lSize.height) {
									legendTextSize.height = lSize.height;
								}
								if (swatchSize.width < s._legendSwatchWidth) {
									swatchSize.width = s._legendSwatchWidth;
								}
								if (swatchSize.height < s._legendSwatchHeight) {
									swatchSize.height = s._legendSwatchHeight;
								}
							}
						}
					} else {
						for (sc = 0; sc < c._series.length; sc++) {
							if (sc === 0) {
								largestFontSize = s._legendFontSize;
							} else {
								if (largestFontSize < s._legendFontSize) {
									largestFontSize = s._legendFontSize;
								}
							}
							// lSize = textSize(c._series[i]._legend, s._legendFontFamily, s._legendFontSize, null, null, c._drawMode, c._artboard);
							lSize = textSize(getEveElmByID(c._series[sc])._legend, s._legendFontFamily, s._legendFontSize, null, null, 0, c._drawMode, c._artboard);
							if (legendTextSize.width < lSize.width) {
								legendTextSize.width = lSize.width;
							}
							if (legendTextSize.height < lSize.height) {
								legendTextSize.height = lSize.height;
							}
							if (swatchSize.width < s._legendSwatchWidth) {
								swatchSize.width = s._legendSwatchWidth;
							}
							if (swatchSize.height < s._legendSwatchHeight) {
								swatchSize.height = s._legendSwatchHeight;
							}
						}
					}

					var lbg, lHeight = swatchSize.height, legendX, legendY;
//					var legendPadding = largestFontSize / 2;
					lSwatchMargin = largestFontSize / 2;
					lMargin = largestFontSize * 0.5;

					if (s.legendPadding){
						lMargin = parseFloat(s.legendPadding);
					}

					//background rect:
					if (c.legendBackgroundColour){
						c._legendBackgroundColour = c.legendBackgroundColour;
					}
					else if(c._legendBackgroundColour === "auto"){
						c._legendBackgroundColour = c._backgroundColour;
					}
					lbg = c._vGroupLegend.getElement("rect", c._drawCall, c.id + "_legend_BG", "eve-chart-legend-bg", null, {fill: c._legendBackgroundColour, opacity: c._legendBackgroundOpacity, shapeRendering: "crispEdges"});

					if (legendTextSize.height > lHeight) {
						lHeight = legendTextSize.height;
					}
					legendSize = {width: lSwatchMargin + swatchSize.width + lSwatchMargin + legendTextSize.width, height: lHeight + lSwatchMargin};

					var lbgWidth = (legendSize.width * c._series.length) + (lMargin * (c._series.length + 1));
					if (getType(c._type) === "sector") {
						lbgWidth = (legendSize.width * s._nodes.length) + (lMargin * (s._nodes.length + 1));
					}
					var lbgHeight = legendSize.height + (lMargin * 2);
					if (c._legendStacked) {
						lbgWidth = legendSize.width + (lMargin * 2);
						lbgHeight = (legendSize.height * c._series.length) + (lMargin * (c._series.length + 1));
						if (getType(c._type) === "sector") {
							lbgHeight = (legendSize.height * s._nodes.length) + (lMargin * (s._nodes.length + 1));
						}
					}
					lbg.eWidth = lbgWidth;
					lbg.eHeight = lbgHeight;

					if (c._drawCall === 1 || rebuild) {
						lbg._animCurrent = new eve.elmAnim("fade in");
					} else {
						lbg._animCurrent = new eve.elmAnim("morph");
					}
					lbg._animCurrent.delay = animTime;
					lbg._animCurrent.duration = duration * c._animInRatios.legend;
					lbg._animCurrent.ease = "easeOut";

//					legendPadding = 5;
					legendX = c._marginLeft;
					legendY = c._marginTop + (parseFloat(c._titleFontSize) * 2);
					if (!isNil(c._xAxis) && getEveElmByID(c._xAxis)._position === "top") {
						legendY = (c._marginTop + c._paddingTop + c._plotHeight) - lbgHeight;
					}
					if (!isNil(c._yAxis) && getEveElmByID(c._yAxis)._position === "left") {
						legendX = c._width - (c._marginRight + c._paddingRight + lbgWidth);
					}

					if (c._legendPos){
						if (c._legendPos === "topLeft"){
							legendX = c._marginLeft;
							if (!isNil(c._yAxis) && getEveElmByID(c._yAxis)._position === "left") {
								legendX = c._marginLeft + c._paddingLeft;
							}
							legendY = c._marginTop + (parseFloat(c._titleFontSize) * 2);
						}
						else if (c._legendPos === "bottomCentre"){
							legendX = c._marginLeft + c._plotWidth / 2;
							legendY = c._marginTop + c._plotHeight;
						}
					}

					c._vGroupLegend.eX = legendX + parseFloat(c._legendOffsetX);
					c._vGroupLegend.eY = legendY + parseFloat(c._legendOffsetY);

					if (c._drawCall === 1 || rebuild) {
						c._vGroupLegend._animCurrent = new eve.elmAnim("fade in");
					} else {
						c._vGroupLegend._animCurrent = new eve.elmAnim("morph");
					}
					c._vGroupLegend._animCurrent.delay = animTime;
					c._vGroupLegend._animCurrent.duration = duration * c._animInRatios.legend;
					c._vGroupLegend._animCurrent.ease = "easeOut";

				}

				var lCaption = [];

				if (showLegends) {
					if (getType(c._type) === "sector") {
						s = getEveElmByID(c._series[0]);
						for (var nd = 0, nl = s._nodes.length; nd < nl; nd++) {
							nodeDC = getEveElmByID(s._nodes[nd]);
							var swatchColour = nodeDC._colour;
							if (s.legendSwatchColour){
								swatchColour = s.legendSwatchColour;
							}
							lCaption.push({series: s._id, label: nodeDC._label, offsetX: s._legendOffsetX, offsetY: s._legendOffsetY, width: s._legendSwatchWidth, height: s._legendSwatchHeight, opacity: s._legendOpacity, swatchType: s._legendSwatchType, swatchColour: swatchColour, swatchFillOpacity: nodeDC._fillOpacity, swatchStrokeColour: nodeDC._strokeColour, swatchStrokeWidth: nodeDC._strokeWidth, swatchStrokeOpacity: nodeDC._strokeOpacity, swatchFillHover: nodeDC._hoverColour});
						}
					}
					else {
						for (var sn = 0, sel = c._series.length; sn < sel; sn++) {
							s = getEveElmByID(c._series[sn]);
							if (s._showLegend && s._legend !== "none" && s._legend !== "") {
								lCaption.push({series: c._series[sn], label: s._legend, offsetX: s._legendOffsetX, offsetY: s._legendOffsetY, width: s._legendSwatchWidth, height: s._legendSwatchHeight, opacity: s._legendOpacity, swatchType: s._legendSwatchType, swatchColour: s._paletteColour, swatchFillOpacity: s._fillOpacity, swatchStrokeColour: s._strokeColour, swatchStrokeWidth: s._strokeWidth, swatchStrokeOpacity: s._strokeOpacity, swatchFillHover: s._hoverColour});
								if (s._type !== "bar" && s._type !== "shape" && s._type !== "funnel") {
									lCaption[lCaption.length - 1].swatchColour = s._lineColour;
								}
								else{
									lCaption[lCaption.length - 1].swatchColour = s._colour;
								}
									if (s.legendSwatchColour){
										lCaption[lCaption.length - 1].swatchColour = s.legendSwatchColour;
									}
									if (s.legendSwatchFillOpacity){
										lCaption[lCaption.length - 1].swatchFillOpacity = s.legendSwatchFillOpacity;
									}
									lCaption[lCaption.length - 1].swatchFillHover = s._lineHoverColour;
								//}
							}
						}
					}
				}

				var ln, ll;
				for (ln = 0, ll = lCaption.length; ln < ll; ln++) {

					var lLeft = 0;
					var lTop = (legendSize.height / 2);
					if (c._legendStacked) {
						lTop += ((legendSize.height + lMargin) * ln);
					} else {
						lLeft = ((legendSize.width + lMargin) * ln);
					}

					var lgdSuffix = "_" + ln;
					if (getType(c._type) === "sector") {
						lgdSuffix = "_" + eve.replaceSpecialChars(lCaption[ln].label);
					}
					var lgdID = c.id + "_legend" + lgdSuffix;

					var glg = c._vGroupLegend.getElement("group", c._drawCall, lgdID, "eve-chart-legend", null, {opacity: s._legendOpacity});
					glg.eX = lMargin + lLeft + lCaption[ln].offsetX;
					glg.eY = lMargin + lTop + lCaption[ln].offsetY;

					if (c._drawCall === 1 || glg.age === 0 || rebuild) {
						glg._animCurrent = new eve.elmAnim("fade in");
					} else {
						glg._animCurrent = new eve.elmAnim("morph");
					}
					glg._animCurrent.delay = animTime + (((duration * c._animInRatios.legend) / lCaption.length) * (ln + 1));
					glg._animCurrent.duration = ((duration * c._animInRatios.legend) / lCaption.length) * (ln + 1);
					glg._animCurrent.ease = "easeOut";
					glg._animDeath = new eve.elmAnim("fade and remove", glg._animCurrent.duration, glg._animCurrent.delay, glg._animCurrent.ease);

					if (lCaption[ln].swatchType === "rectangle") {
						var lSwatch = glg.getElement("rect", c._drawCall, c.id + "_legend_swatch" + lgdSuffix, "eve-chart-legend-swatch", {width: lCaption[ln].width, height: lCaption[ln].height}, {fill: lCaption[ln].swatchColour, fillOpacity: lCaption[ln].swatchFillOpacity});
						if (lCaption[ln].swatchStrokeOpacity > 0 && lCaption[ln].swatchStrokeWidth > 0) {
							lSwatch.setStyle("stroke", lCaption[ln].swatchStrokeColour).setStyle("stroke-opacity", lCaption[ln].swatchStrokeOpacity).setStyle("stroke-width", lCaption[ln].swatchStrokeWidth);
						}
						lSwatch.fillhover = lCaption[ln].swatchFillHover;
						lSwatch.eY = -(lCaption[ln].height / 2);
						lSwatch.eWidth = lCaption[ln].width;
						lSwatch.eHeight = lCaption[ln].height;

						if (c._drawCall > 1 && lSwatch.age > 0 && !rebuild) {
							lSwatch._animCurrent.delay = animTime + (((duration * c._animInRatios.legend) / lCaption.length) * (ln + 1));
							lSwatch._animCurrent.duration = ((duration * c._animInRatios.legend) / lCaption.length) * (ln + 1);
							lSwatch._animCurrent.ease = "easeOut";
							lSwatch._animCurrent.mode = "morph";
						} else {
							lSwatch._animCurrent.mode = "";
						}
						lSwatch._animDeath = new eve.elmAnim("parent", lSwatch._animCurrent.duration, lSwatch._animCurrent.delay, lSwatch._animCurrent.ease);

					}

					var lLabel = eve.makeText(c, glg, c._drawMode, {id: c.id + "_legend" + lgdSuffix + "_label", drawCall: c._drawCall, string: lCaption[ln].label, className: "eve-chart-legend-text", fontSize: getEveElmByID(lCaption[ln].series)._legendFontSize, fontFamily: getEveElmByID(lCaption[ln].series)._legendFontFamily, fontColour: getEveElmByID(lCaption[ln].series)._legendFontColour, verticalAlign: "middle", x: lCaption[ln].width + lSwatchMargin});

					if (c._animInRatios.legend === 0){
						lLabel._animCurrent = new eve.elmAnim();
					}
					else{
						lLabel._animDeath = new eve.elmAnim("parent", lLabel._animCurrent.duration, lLabel._animCurrent.delay, lLabel._animCurrent.ease);
						if (c._drawCall > 1 && lLabel.age > 0 && !rebuild) {
							lLabel._animCurrent.delay = animTime + (((duration * c._animInRatios.legend) / lCaption.length) * (ln + 1));
							lLabel._animCurrent.duration = ((duration * c._animInRatios.legend) / lCaption.length) * (ln + 1);
							lLabel._animCurrent.ease = "easeOut";
							lLabel._animCurrent.mode = "fadeDownChangeFadeUp";
							if (s._legendChangeLegendAnim === "flipText") {
								lLabel._animCurrent.mode = "flipText";
							}
						} else {
							lLabel._animCurrent.mode = "";
						}
					}
				}
				animTime += c._vGroupLegend._animCurrent.duration;

				buildLabels(c);
			}

			function buildLabels(c){
				for (var ln = 0, ll = c._labels.length; ln < ll; ln++) {
				// function buildLabels(ln, c){

					if (c._labels.length > 0){

						var lbl = getEveElmByID(c._labels[ln]);
						var lblLink = getEveElmByID(lbl._link);
						if (!isNil(lblLink)){
							var lblLinkSeries = getEveElmByID(lblLink._series);
						}
						var labelGroup;

						var xFromVal = isNil(lbl.x);
						var yFromVal = isNil(lbl.y);

						eve.setObjProps(lbl);

						if (!isNil(lbl.xVal) && !isNil(c._xAxis)){
							lbl._x = getEveElmByID(c._xAxis).getVal(lbl.xVal, true);
						}
						else{
							if (!isNil(lbl.x) && lbl.x.toString().indexOf("%") !== -1){
								lbl._x = ((parseFloat(lbl.x) / 100.0) * c._plotWidth);
							}
						}
						if (!isNil(lbl.yVal) && !isNil(c._yAxis)) {
							lbl._y = getEveElmByID(c._yAxis).getVal(lbl.yVal, true);
						}
						else{
							if (!isNil(lbl.y) && lbl.y.toString().indexOf("%") !== -1){
								lbl._y = ((parseFloat(lbl.y) / 100.0) * c._plotHeight);
							}
						}

						if (!isNil(lbl.width) && lbl.width.toString().indexOf("%") !== -1){
							lbl._width = (parseFloat(lbl.width) / 100.0) * c._plotWidth;
						}
						if (!isNil(lbl.height) && lbl.height.toString().indexOf("%") !== -1){
							lbl._height = (parseFloat(lbl.height) / 100.0) * c._plotHeight;
						}
//						if (!isNil(lbl._m_x) && lbl._m_x.toString().indexOf("%") !== -1){
//							lbl._x = ((parseFloat(lbl._m_x) / 100.0) * c._plotWidth);
//						}
//						if (!isNil(lbl._m_y) && lbl._m_y.toString().indexOf("%") !== -1){
//							lbl._y = ((parseFloat(lbl._m_y) / 100.0) * c._plotHeight);
//						}
//						if (!isNil(lbl._m_width) && lbl._m_width.toString().indexOf("%") !== -1){
//							lbl._width = (parseFloat(lbl._m_width) / 100.0) * c._plotWidth;
//						}
//						if (!isNil(lbl._m_height) && lbl._m_height.toString().indexOf("%") !== -1){
//							lbl._height = (parseFloat(lbl._m_height) / 100.0) * c._plotHeight;
//						}

						if (!isNil(lblLink) && lblLink instanceof eve.highlight) {
							if (lbl._animCurrent.mode === "fadeDownChangeFadeUp" && c._drawCall > 1 && !rebuild) {
								lbl._animCurrent.delay = lblLinkSeries._animCurrent.delay;
								lbl._animCurrent.duration = lblLinkSeries._animCurrent.duration;
							}
							if (c._drawCall <= 1 || rebuild) {
								lbl._animCurrent.delay = lblLinkSeries._animCurrent.delay + lblLinkSeries._animCurrent.duration;
								lbl._animCurrent.duration = lblLinkSeries._animCurrent.duration / 4;
							}
						}

						if (lblLink instanceof eve.axis && xFromVal && !isNil(lbl.axisVal) && lblLink._horizontal) {
							lbl._x = lblLink.getVal(lbl.axisVal, true);
						}
						if (lblLink instanceof eve.axis && yFromVal && !isNil(lbl.axisVal) && !lblLink._horizontal) {
							lbl._y = lblLink.getVal(lbl.axisVal, true);
						}

						lbl.eX = lbl._x;
						lbl.eY = c._plotHeight - lbl._y;

						if (yFromVal) {
							lbl.eY = lbl._y;
						}

						var parentIsLabel = false;
						var parentGroup = c._vGroupOverlays;
						if (lbl._draggable) {
							parentGroup = c._vGroupDraggables;
						}
						for (var pl = 0, pll = c._labels.length; pl < pll; pl++) {
							var sibLbl = getEveElmByID(c._labels[pl]);
							if (sibLbl !== lbl) {
								for (var lc = 0; lc < sibLbl._children.length; lc++) {
									if (sibLbl._children[lc] === lbl) {
										parentGroup = sibLbl._eveBpElm;
										parentIsLabel = true;
										break;
									}
								}
							}
						}

						var labelGroupID = c.id + "_label_group_" + (ln + 1);
						labelGroup = parentGroup.getElement("g", c._drawCall, labelGroupID, "eve-chart-series-label-group");
						labelGroup._draggable = lbl._draggable;

						labelGroup._animCurrent = cloneEveObj(lbl._animCurrent);
						labelGroup._innerText = lbl._text;
						labelGroup._xAxisStart = c._marginLeft + c._paddingLeft;
						labelGroup._yAxisStart = c._marginTop + c._paddingTop + c._plotHeight;
						var cAxisX = getEveElmByID(c._xAxis);
						var cAxisY = getEveElmByID(c._yAxis);
						if (!isNil(cAxisX)){
							labelGroup._xAxisRatio = (cAxisX._rangeMax - cAxisX._rangeMin) / c._plotWidth;
							labelGroup._xAxisMin = cAxisX._rangeMin;
							labelGroup._xAxisMax = cAxisX._rangeMax;
							labelGroup._xAxisOrigin = cAxisX._origin;
						}
						if (!isNil(cAxisY)){
							labelGroup._yAxisRatio = (cAxisY._rangeMax - cAxisY._rangeMin) / c._plotHeight;
							labelGroup._yAxisMin = cAxisY._rangeMin;
							labelGroup._yAxisMax = cAxisY._rangeMax;
							labelGroup._yAxisOrigin = cAxisY._origin;
						}
						labelGroup._xChartOffset = c._marginLeft + c._paddingLeft;
						labelGroup._yChartOffset = c._marginTop + c._paddingTop;
						lbl._domElmID = labelGroupID;
						lbl._eveBpElm = labelGroup;

						if (rejigOnly){
							lbl._dragRect.xMin = lbl._dragRect.xMax = lbl._dragRect.yMin = lbl._dragRect.yMax = null;
						}

						if (lblLink instanceof eve.axis && lbl._draggable) {
							if (lbl._rangeStart) {
								lblLink._rangeLabelStart = labelGroupID;
							}
							if (lbl._rangeEnd) {
								lblLink._rangeLabelEnd = labelGroupID;
							}
							if (lbl._highlightBlock !== null){
								lbl._highlight = c._vGroupBlocks.getElement("rect", c._drawCall, c.id + "_label" + ln + "_highlight", "eve-chart-label-highlight", null, {fill: lbl._highlightBlockColour, opacity: lbl._highlightBlockOpacity});
								lbl._highlight._animCurrent = cloneEveObj(lbl._animCurrent);
								if (c._drawCall > 1){
									lbl._highlight._animCurrent.mode = null;
								}
								if (lbl._highlightBlock === "col"){
									lbl._highlight.eWidth = c._plotWidth * lblLink._bands[0]._end;
									lbl._highlight.eHeight = c._plotHeight;
								}
							}
						}

						//create a group now for the leaders and intNodes so that they get drawn UNDERNEATH everything else:
						var leadersGroup;
						if (lbl._leaders.length > 0) {
							leadersGroup = labelGroup.getElement("g", c._drawCall, c.id + "_leaders_" + (ln + 1), "eve-chart-series-leaders-group");
						}

						var intNodesGroup;
						if (!isNil(lbl._intNodes.length) && lbl._intNodes.length > 0) {
							intNodesGroup = labelGroup.getElement("g", c._drawCall, c.id + "_intNodes_" + (ln + 1), "eve-chart-series-int-nodes-group");
						}

						if (c._drawMode !== "PDF"){
							var labelGroupElm = get(labelGroupID, labelGroup._container);

							if (c._drawCall > 1 && lbl._draggable && !isNil(labelGroupElm) && !rebuild) {

								if (labelGroup._linkedAxis._horizontal) {
									if (!isNil(lbl._axisVal)) {
										lbl.eX = labelGroup._linkedAxis.getVal(lbl._axisVal, true);
									} else if (!isNil(labelGroupElm._dragPosX)) {
										lbl.eX = labelGroupElm._dragPosX;
									}
								} else {
									if (!isNil(lbl._axisVal)) {
										lbl.eY = labelGroup._linkedAxis.getVal(lbl._axisVal, true);
									} else if (!isNil(labelGroupElm._dragPosY)) {
										lbl.eY = labelGroupElm._dragPosY;
									}
								}
							}
						}

						//the makeText function will use the value of the label location to update any evaluated text.
						//therefore, if it's draggable and snaps to axis elements, we need to update its position now:

						if (lblLink instanceof eve.axis) {
							labelGroup._linkedAxis = lblLink;

							if (!isNil(lbl._dragSnap)) {

								var snapPos = 0;
								var minPos, maxPos;
								var plotDim = c._plotWidth;
								if (!lblLink._horizontal) {
									plotDim = c._plotHeight;
								}

								//bands:
								var snapCount = lblLink._bands.length;
								var snapDim = plotDim / snapCount;
								var snapStart = ((lblLink._bands[0]._end - lblLink._bands[0]._start) * plotDim) / 2;
								//ticks:
								if (strIs(lbl._dragSnap, "ticks")) {
									snapCount++;
									snapStart = 0;
								}
								//subticks:
								else if (strIs(lbl._dragSnap, "subticks")) {
									snapCount = (lblLink._useSubTicks * lblLink._bands.length) + 1;
									snapDim = plotDim / (snapCount - 1);
									snapStart = 0;
								}
								labelGroup._snaps = [];
								snapPos = snapStart;

								minPos = 0;
								maxPos = 0;
								if (lblLink._horizontal) {
									if (!isNil(lbl._dragRect.xMin)){
										if (lbl._dragRect.xMin.toString().indexOf("%") !== -1){
											minPos = ((parseFloat(lbl._dragRect.xMin) / 100.0) * c._plotWidth);
										}
										else{
											minPos = lbl._dragRect.xMin;
										}
									}
									else if (!isNil(lbl._dragRect.xMinVal)){
										minPos = lblLink.getScreenPos(lbl._dragRect.xMinVal);
									}
									if (!isNil(lbl._dragRect.xMax)){
										if (lbl._dragRect.xMax.toString().indexOf("%") !== -1){
											maxPos = ((parseFloat(lbl._dragRect.xMax) / 100.0) * c._plotWidth);
										}
										else{
											maxPos = lbl._dragRect.xMax;
										}
									}
									else if (!isNil(lbl._dragRect.xMaxVal)){
										maxPos = lblLink.getScreenPos(lbl._dragRect.xMaxVal);
									}
									else{
										maxPos = c._plotWidth;
									}
								} else {
									if (!isNil(lbl._dragRect.yMin)){
										if (lbl._dragRect.yMin.toString().indexOf("%") !== -1){
											//minPos = (100 - (parseFloat(lbl._dragRect.yMin) / 100.0)) * c._plotHeight;
											minPos = (parseFloat(lbl._dragRect.yMin) / 100.0) * c._plotHeight;
										}
										else{
											//minPos = c._plotHeight - lbl._dragRect.yMin;
											minPos = lbl._dragRect.yMin;
										}
									}
									else if (!isNil(lbl._dragRect.yMinVal)){
										minPos = lblLink.getScreenPos(lbl._dragRect.yMinVal);
									}
									if (!isNil(lbl._dragRect.yMax)){
										if (lbl._dragRect.yMax.toString().indexOf("%") !== -1){
											maxPos = (parseFloat(lbl._dragRect.yMax) / 100.0) * c._plotHeight;
										}
										else{
											maxPos = lbl._dragRect.yMax;
										}
									}
									else if (!isNil(lbl._dragRect.yMaxVal)){
										maxPos = lblLink.getScreenPos(lbl._dragRect.yMaxVal);
									}
									else{
										maxPos = c._plotHeight;
									}
								}

								for (var ds = 0; ds <= snapCount; ds++) {
									if (snapPos >= minPos && snapPos <= maxPos) {
										labelGroup._snaps.push(eve.setDecimal(snapPos, 1));
									}
									snapPos += snapDim;
								}

								if (lblLink._horizontal) {
									lbl.eX = labelGroup._snaps.closest(lbl.eX);
								} else {
									lbl.eY = labelGroup._snaps.closest(lbl.eY);
								}
							}
							if (lblLink._horizontal) {
								lbl.axisVal = lbl._axisVal = lblLink.getVal(lbl.eX);
							} else {
								lbl.axisVal = lbl._axisVal = lblLink.getVal(lbl.eY);
							}

						}
						var lblBG;
						var lblGlow;
						var lblGrab;
						if (!isNil(lbl._boxStyle) && lbl._boxStyle !== "none" && lbl._boxStyle !== "") {
							if (lbl._glow){
								lblGlow = labelGroup.getElement("group", c._drawCall, c.id + "_label" + ln + "_label_glow_group", "eve-chart-label-glow-group");
							}
							lblBG = labelGroup.getElement("path", c._drawCall, c.id + "_label" + ln + "_label_shape", "eve-chart-label");
						}

						if (!isNil(lbl._icon)){
							var lblIcon = labelGroup.getElement("group", c._drawCall, c.id + "_label" + ln + "_label_icon", "eve-chart-label-icon", {fill: lbl._iconFillColour});
							var icoElm;
							var iPath = eve.getItemByID(icoShapes, lbl._icon);
							if (!isNil(iPath)){
								icoElm = lblIcon.getElement("path", c._drawCall, c.id + "_label" + ln + "_label_icon_elm");
								icoElm.path = eve.svgPathToEvePath(iPath.path);
								icoElm.eScale = lbl._iconScale;
								icoElm.eX = lbl._iconOffsetX;
								icoElm.eY = lbl._iconOffsetY;
							}
						}

						var textElm = eve.makeText(c, labelGroup, c._drawMode, {id: c.id + "_label_text_" + (ln + 1), drawCall: c._drawCall, holderObj: lbl, string: lbl._text, className: "eve-chart-label-text", fontSize: lbl._fontSize, fontFamily: lbl._fontFamily, fontColour: lbl._fontColour, fontWeight: lbl._fontWeight, fontStyle: lbl._fontStyle, textAlign: lbl._textAlign, leading: lbl._leading});

						if (lbl._draggable) {
							textElm.setStyle("pointerEvents", "none");
							textElm.setStyle("userSelect", "none");
						}
						if (!isNil(lbl._boxStyle) && lbl._boxStyle !== "none" && lbl._boxStyle !== "") {
							lblGrab = labelGroup.getElement("path", c._drawCall, c.id + "_label" + ln + "_label_grab", "eve-chart-grab");
						}

						labelGroup._textElm = lbl._textElm = textElm.id;

						var lblWidth = 0;
						if (lbl._width === "auto") {
							lblWidth = textElm.width + lbl._padding * 2;
						} else {
							lblWidth = parseFloat(lbl._width);
						}
						var lblHeight = 0;
						if (lbl._height === "auto") {
							lblHeight = textElm.height + lbl._padding * 2;
						} else {
							lblHeight = parseFloat(lbl._height);
						}

						if (lbl._boxStyle === "circle") {
							if (lblWidth > lblHeight) {
								lblHeight = lblWidth;
							} else {
								lblWidth = lblHeight;
							}
						}

						var lblCornerRad = lbl._cornerRadius;
						//parseInt to avoid blurry outlines:
						lblWidth = parseInt(lblWidth);
						lblHeight = parseInt(lblHeight);
						lblCornerRad = parseInt(lblCornerRad);

						labelGroup._lblWidth = lblWidth;
						labelGroup._lblHeight = lblHeight;

						var lRad = null;
						var lPath = null;
						var hLength = null;
						var pBase;

						//pointerEnd, topLeft, topCentre, topRight, middleLeft, middleCentre, middleRight, bottomLeft, bottomCentre, bottomRight
						var lblOrigin = {hor: 0, ver: 0};
						var lblMargin = {left: 0, top: 0, right: 0, bottom: 0};
						if (lbl._pointerLeft) {
							lblMargin.left = lbl._pointerLength;
						}
						if (lbl._pointerTop) {
							lblMargin.top = lbl._pointerLength;
						}
						if (lbl._pointerRight) {
							lblMargin.right = lbl._pointerLength;
						}
						if (lbl._pointerBottom) {
							lblMargin.bottom = lbl._pointerLength;
						}
						if (lbl._origin.toString().toLowerCase().indexOf("middle") === 0) {
							lblOrigin.ver = lblHeight / 2;
						} else if (lbl._origin.toString().toLowerCase().indexOf("bottom") === 0) {
							lblOrigin.ver = lblHeight;
						} else if (lbl._origin.toString() === "pointerEnd") {
							if (lbl._pointerLeft) {
								lblOrigin.ver = (lblHeight / 2) + lbl._pointerOffsetLeft;
							} else if (lbl._pointerRight) {
								lblOrigin.ver = (lblHeight / 2) + lbl._pointerOffsetRight;
							} else if (lbl._pointerTop) {
								lblOrigin.ver = -lbl._pointerLength;
							} else if (lbl._pointerBottom) {
								lblOrigin.ver = lbl._pointerLength + lblHeight;
							} else {
								//pointerEnd is the default origin, but in this case there is no pointer - default to top:
								lblOrigin.ver = 0;
							}
						}
						if (lbl._origin.toString().toLowerCase().indexOf("centre") !== -1) {
							lblOrigin.hor = lblWidth / 2;
						} else if (lbl._origin.toString().toLowerCase().indexOf("right") !== -1) {
							lblOrigin.hor = lblWidth;
						} else if (lbl._origin.toString() === "pointerEnd") {
							if (lbl._pointerLeft) {
								lblOrigin.hor = -lbl._pointerLength;
							} else if (lbl._pointerRight) {
								lblOrigin.hor = lblWidth + lbl._pointerLength;
							} else if (lbl._pointerTop) {
								lblOrigin.hor = (lblWidth / 2) + lbl._pointerOffsetTop;
							} else if (lbl._pointerBottom) {
								lblOrigin.hor = (lblWidth / 2) + lbl._pointerOffsetBottom;
							} else {
								//pointerEnd is the default origin, but in this case there is no pointer - default to textAlign:
								lblOrigin.hor = 0;
								if (strIs(lbl._textAlign, "center") || strIs(lbl._textAlign, "centre")) {
									lblOrigin.hor = lblWidth / 2;
								} else if (strIs(lbl._textAlign, "right")) {
									lblOrigin.hor = lblWidth;
								}
							}
						}

						if (lbl._boxStyle !== "none") {

							//pointer dims:
							var pLength = 0;
							var pWidthVer = 0;
							var pWidthHor = 0;
							var pOffsetTop = 0;
							var pOffsetBottom = 0;
							var pOffsetLeft = 0;
							var pOffsetRight = 0;

							if (lbl._pointerBottom || lbl._pointerTop || lbl._pointerLeft || lbl._pointerRight) {
								pLength = lbl._pointerLength;
								pWidthVer = lbl._pointerWidth;
								pWidthHor = lbl._pointerWidth;
								pOffsetTop = lbl._pointerOffsetTop;
								pOffsetBottom = lbl._pointerOffsetBottom;
								pOffsetLeft = lbl._pointerOffsetLeft;
								pOffsetRight = lbl._pointerOffsetRight;

								if (pWidthVer > lblHeight) {
									pWidthVer = lblHeight;
									if (lbl._boxStyle === "rectangle" && lblCornerRad > 0) {
										if (pWidthVer > lblHeight - (2 * lblCornerRad)) {
											pWidthVer = lblHeight - (2 * lblCornerRad);
										}
									}
								}
								if (pWidthHor > lblWidth) {
									pWidthHor = lblWidth;
									if (lbl._boxStyle === "rectangle" && lblCornerRad > 0) {
										if (pWidthHor > lblWidth - (2 * lblCornerRad)) {
											pWidthHor = lblWidth - (2 * lblCornerRad);
										}
									}
								}
								if (lbl._boxStyle === "rectangle") {
									//make sure the pointer does not go off the edge of a rectangle:
									if ((pOffsetTop + (pWidthHor / 2)) > (lblWidth / 2) - lblCornerRad) {
										pOffsetTop = (lblWidth / 2) - (lblCornerRad + (pWidthHor / 2));
									}
									if (pOffsetTop - (pWidthHor / 2) < -((lblWidth / 2) - lblCornerRad)) {
										pOffsetTop = -((lblWidth / 2) - (lblCornerRad + (pWidthHor / 2)));
									}
									if ((pOffsetBottom + (pWidthHor / 2)) > (lblWidth / 2) - lblCornerRad) {
										pOffsetBottom = (lblWidth / 2) - (lblCornerRad + (pWidthHor / 2));
									}
									if (pOffsetBottom - (pWidthHor / 2) < -((lblWidth / 2) - lblCornerRad)) {
										pOffsetBottom = -((lblWidth / 2) - (lblCornerRad + (pWidthHor / 2)));
									}
									if ((pOffsetLeft + (pWidthVer / 2)) > (lblHeight / 2) - lblCornerRad) {
										pOffsetLeft = (lblHeight / 2) - (lblCornerRad + (pWidthVer / 2));
									}
									if (pOffsetLeft - (pWidthVer / 2) < -((lblHeight / 2) - lblCornerRad)) {
										pOffsetLeft = -((lblHeight / 2) - (lblCornerRad + (pWidthVer / 2)));
									}
									if ((pOffsetRight + (pWidthVer / 2)) > (lblHeight / 2) - lblCornerRad) {
										pOffsetRight = (lblHeight / 2) - (lblCornerRad + (pWidthVer / 2));
									}
									if (pOffsetRight - (pWidthVer / 2) < -((lblHeight / 2) - lblCornerRad)) {
										pOffsetRight = -((lblHeight / 2) - (lblCornerRad + (pWidthVer / 2)));
									}
								}
							}

							lPath = eve.addPath(null, true);

							if (lbl._boxStyle === "rectangle") {

								hLength = lblCornerRad * HANDLE_FACTOR;
								//top edge:
								lPath.addCommand("moveTo", [lblCornerRad, 0]);
								if (lbl._pointerTop) {
									lPath.addCommand("lineTo", [((lblWidth - pWidthHor) / 2) + pOffsetTop, 0]);
									lPath.addCommand("lineTo", [(lblWidth / 2) + pOffsetTop, -pLength]);
									lPath.addCommand("lineTo", [(lblWidth / 2) + (pWidthHor / 2) + pOffsetTop, 0]);
								}
								lPath.addCommand("lineTo", [lblWidth - lblCornerRad, 0]);
								//top right-hand corner:
								if (lblCornerRad > 0) {
									lPath.addCommand("cubic", [lblWidth, lblCornerRad], [lblWidth - lblCornerRad + hLength, 0], [lblWidth, lblCornerRad - hLength]);
								}
								//right-hand edge:
								if (lbl._pointerRight) {
									lPath.addCommand("lineTo", [lblWidth, ((lblHeight - pWidthVer) / 2) + pOffsetRight]);
									lPath.addCommand("lineTo", [lblWidth + pLength, (lblHeight / 2) + pOffsetRight]);
									lPath.addCommand("lineTo", [lblWidth, (lblHeight / 2) + (pWidthVer / 2) + pOffsetRight]);
								}
								lPath.addCommand("lineTo", [lblWidth, lblHeight - lblCornerRad]);
								//bottom right-hand corner:
								if (lblCornerRad > 0) {
									lPath.addCommand("cubic", [lblWidth - lblCornerRad, lblHeight], [lblWidth, lblHeight - lblCornerRad + hLength], [lblWidth - lblCornerRad + hLength, lblHeight]);
								}
								//bottom edge:
								if (lbl._pointerBottom) {
									lPath.addCommand("lineTo", [(lblWidth / 2) + (pWidthHor / 2) + pOffsetBottom, lblHeight]);
									lPath.addCommand("lineTo", [(lblWidth / 2) + pOffsetBottom, lblHeight + pLength]);
									lPath.addCommand("lineTo", [((lblWidth - pWidthHor) / 2) + pOffsetBottom, lblHeight]);
								}
								lPath.addCommand("lineTo", [lblCornerRad, lblHeight]);
								//bottom left-hand corner:
								if (lblCornerRad > 0) {
									lPath.addCommand("cubic", [0, lblHeight - lblCornerRad], [lblCornerRad - hLength, lblHeight], [0, lblHeight - lblCornerRad + hLength]);
								}
								//left edge:
								if (lbl._pointerLeft) {
									lPath.addCommand("lineTo", [0, (lblHeight / 2) + (pWidthVer / 2) + pOffsetLeft]);
									lPath.addCommand("lineTo", [-pLength, (lblHeight / 2) + pOffsetLeft]);
									lPath.addCommand("lineTo", [0, ((lblHeight - pWidthVer) / 2) + pOffsetLeft]);
								}
								//top left-hand corner:
								if (lblCornerRad > 0) {
									lPath.addCommand("lineTo", [0, lblCornerRad]);
									lPath.addCommand("cubic", [lblCornerRad, 0], [0, lblCornerRad - hLength], [lblCornerRad - hLength, 0]);
								}
							} else if (lbl._boxStyle === "circle") {

								lRad = (lblWidth / 2);
								var cCentre = {x: lblWidth / 2, y: lblHeight / 2};

								if (lbl._pointerTangential) {
									var theta = Math.asin(lRad / (lRad + pLength));
									pBase = Math.sin(theta) * lRad;
									var distTangent = Math.cos(theta) * lRad;

									hLength = ((lRad * 4) / 3) * Math.tan(Math.PI / (2 * (360 / eve.toDegrees(theta))));
									var beta = Math.atan(hLength / lRad);
									var alpha = theta - beta;
									var hHyp = hLength / Math.sin(beta);
									var handleOpp = Math.sin(alpha) * hHyp;
									var handleAdj = Math.cos(alpha) * hHyp;

									var quadHandle = lRad * HANDLE_FACTOR;

									if (lbl._pointerTop) {
										lPath.addCommand("moveTo", [cCentre.x, cCentre.y - (lRad + pLength)]);
										lPath.addCommand("lineTo", [cCentre.x + distTangent, cCentre.y - pBase]);
										lPath.addCommand("cubic", [cCentre.x + lRad, cCentre.y], [cCentre.x + handleAdj, cCentre.y - handleOpp], [cCentre.x + lRad, cCentre.y - hLength]);
										lPath.addCommand("cubic", [cCentre.x, cCentre.y + lRad], [cCentre.x + lRad, cCentre.y + quadHandle], [cCentre.x + quadHandle, cCentre.x + lRad]);
										lPath.addCommand("cubic", [cCentre.x - lRad, cCentre.y], [cCentre.x - quadHandle, cCentre.y + lRad], [cCentre.x - lRad, cCentre.x + quadHandle]);
										lPath.addCommand("cubic", [cCentre.x - distTangent, cCentre.y - pBase], [cCentre.x - lRad, cCentre.y - hLength], [cCentre.x - handleAdj, cCentre.x - handleOpp]);
									}
									if (lbl._pointerRight) {
										lPath.addCommand("moveTo", [cCentre.x + lRad + pLength, cCentre.y]);
										lPath.addCommand("lineTo", [cCentre.x + pBase, cCentre.y + distTangent]);
										lPath.addCommand("cubic", [cCentre.x, cCentre.y + lRad], [cCentre.x + handleOpp, cCentre.y + handleAdj], [cCentre.x + hLength, cCentre.y + lRad]);
										lPath.addCommand("cubic", [cCentre.x - lRad, cCentre.y], [cCentre.x - quadHandle, cCentre.y + lRad], [cCentre.x - lRad, cCentre.y + quadHandle]);
										lPath.addCommand("cubic", [cCentre.x, cCentre.y - lRad], [cCentre.x - lRad, cCentre.y - quadHandle], [cCentre.x - quadHandle, cCentre.y - lRad]);
										lPath.addCommand("cubic", [cCentre.x + pBase, cCentre.y - distTangent], [cCentre.x + hLength, cCentre.y - lRad], [cCentre.x + handleOpp, cCentre.y - handleAdj]);
									}
									if (lbl._pointerBottom) {
										lPath.addCommand("moveTo", [cCentre.x, cCentre.y + lRad + pLength]);
										lPath.addCommand("lineTo", [cCentre.x - distTangent, cCentre.y + pBase]);
										lPath.addCommand("cubic", [cCentre.x - lRad, cCentre.y], [cCentre.x - handleAdj, cCentre.y + handleOpp], [cCentre.x - lRad, cCentre.y + hLength]);
										lPath.addCommand("cubic", [cCentre.x, cCentre.y - lRad], [cCentre.x - lRad, cCentre.y - quadHandle], [cCentre.x - quadHandle, cCentre.x - lRad]);
										lPath.addCommand("cubic", [cCentre.x + lRad, cCentre.y], [cCentre.x + quadHandle, cCentre.y - lRad], [cCentre.x + lRad, cCentre.x - quadHandle]);
										lPath.addCommand("cubic", [cCentre.x + distTangent, cCentre.y + pBase], [cCentre.x + lRad, cCentre.y + hLength], [cCentre.x + handleAdj, cCentre.x + handleOpp]);
									}
									if (lbl._pointerLeft) {
										lPath.addCommand("moveTo", [cCentre.x - (lRad + pLength), cCentre.y]);
										lPath.addCommand("lineTo", [cCentre.x - pBase, cCentre.y - distTangent]);
										lPath.addCommand("cubic", [cCentre.x, cCentre.y - lRad], [cCentre.x - handleOpp, cCentre.y - handleAdj], [cCentre.x - hLength, cCentre.y - lRad]);
										lPath.addCommand("cubic", [cCentre.x + lRad, cCentre.y], [cCentre.x + quadHandle, cCentre.y - lRad], [cCentre.x + lRad, cCentre.y - quadHandle]);
										lPath.addCommand("cubic", [cCentre.x, cCentre.y + lRad], [cCentre.x + lRad, cCentre.y + quadHandle], [cCentre.x + quadHandle, cCentre.y + lRad]);
										lPath.addCommand("cubic", [cCentre.x - pBase, cCentre.y + distTangent], [cCentre.x - hLength, cCentre.y + lRad], [cCentre.x - handleOpp, cCentre.y + handleAdj]);
									}

								} else {

									//set up the points on the circumference - either side of pointer "bases":
									var pTopStart = {x: cCentre.x, y: cCentre.y - lRad};
									var pTopEnd = {x: cCentre.x, y: cCentre.y - lRad};
									var pRightStart = {x: cCentre.x + lRad, y: cCentre.y};
									var pRightEnd = {x: cCentre.x + lRad, y: cCentre.y};
									var pBottomStart = {x: cCentre.x, y: cCentre.y + lRad};
									var pBottomEnd = {x: cCentre.x, y: cCentre.y + lRad};
									var pLeftStart = {x: cCentre.x - lRad, y: cCentre.y};
									var pLeftEnd = {x: cCentre.x - lRad, y: cCentre.y};
									//angles of quadrants:
									var quadTheta = {tr: 90, br: 90, bl: 90, tl: 90};
									var quadAlpha = {tr: 0, br: 0, bl: 0, tl: 0};
									var quadBeta = {tr: 0, br: 0, bl: 0, tl: 0};

									if (lbl._pointerTop) {
										pTopStart.x = cCentre.x - (pWidthHor / 2);
										pTopEnd.x = cCentre.x + (pWidthHor / 2);
										//find the face of the sector formed on the perimeter of the circle by the invisible "edge" of our pointer
										// pBase=adjacent (line from centre to edge). Use Pythagoras:
										pBase = Math.sqrt((lRad * lRad) - ((pWidthHor / 2) * (pWidthHor / 2)));
										pTopStart.y = cCentre.y - pBase;
										pTopEnd.y = pTopStart.y;
										//adjust the angles of the quadrants:
										quadAlpha.tr = Math.asin((pWidthHor / 2) / lRad);
										quadBeta.tl = quadAlpha.tr;
										quadTheta.tr -= quadBeta.tl;
										quadTheta.tl -= quadAlpha.tr;
									}
									if (lbl._pointerRight) {
										pRightStart.y = cCentre.y - (pWidthVer / 2);
										pRightEnd.y = cCentre.y + (pWidthVer / 2);
										//find the face of the sector formed on the perimeter of the circle by the invisible "edge" of our pointer
										// pBase=adjacent (line from centre to edge). Use Pythagoras:
										pBase = Math.sqrt((lRad * lRad) - ((pWidthVer / 2) * (pWidthVer / 2)));
										pRightStart.x = cCentre.x + pBase;
										pRightEnd.x = pRightStart.x;
										//adjust the angles of the quadrants:
										quadAlpha.br = Math.asin((pWidthVer / 2) / lRad);
										quadBeta.tr = quadAlpha.br;
										quadTheta.tr -= quadBeta.tr;
										quadTheta.br -= quadAlpha.br;
									}
									if (lbl._pointerBottom) {
										pBottomStart.x = cCentre.x + pWidthHor / 2;
										pBottomEnd.x = cCentre.x - pWidthHor / 2;
										//find the face of the sector formed on the perimeter of the circle by the invisible "edge" of our pointer
										// pBase=adjacent (line from centre to edge). Use Pythagoras:
										pBase = Math.sqrt((lRad * lRad) - ((pWidthHor / 2) * (pWidthHor / 2)));
										pBottomStart.y = cCentre.y + pBase;
										pBottomEnd.y = pBottomStart.y;
										//adjust the angles of the quadrants:
										quadAlpha.bl = Math.asin((pWidthHor / 2) / lRad);
										quadBeta.br = quadAlpha.bl;
										quadTheta.br -= quadBeta.br;
										quadTheta.bl -= quadAlpha.bl;
									}
									if (lbl._pointerLeft) {
										pLeftStart.y = cCentre.y + (pWidthVer / 2);
										pLeftEnd.y = cCentre.y - (pWidthVer / 2);
										//find the face of the sector formed on the perimeter of the circle by the invisible "edge" of our pointer
										// pBase=adjacent (line from centre to edge). Use Pythagoras:
										pBase = Math.sqrt((lRad * lRad) - ((pWidthVer / 2) * (pWidthVer / 2)));
										pLeftStart.x = cCentre.x - pBase;
										pLeftEnd.x = pLeftStart.x;
										//adjust the angles of the quadrants:
										quadAlpha.tl = Math.asin((pWidthVer / 2) / lRad);
										quadBeta.bl = quadAlpha.tl;
										quadTheta.bl -= quadBeta.bl;
										quadTheta.tl -= quadAlpha.tl;
									}

									lRad = lblWidth / 2;
	//								var hOppStart;
	//								var hAdjStart;
	//								var hOppEnd;
	//								var hAdjEnd;
									//first quadrant - from 12 o'clock to 3 o'clock
									lPath.addCommand("moveTo", [pTopEnd.x, pTopEnd.y]);

									hLength = ((4 * lRad) / 3) * Math.tan(Math.PI / (2 * (360 / quadTheta.tr)));
	//								hOppStart = Math.sin(quadAlpha.tr) * hLength;
	//								hAdjStart = Math.cos(quadAlpha.tr) * hLength;
	//								hOppEnd = Math.sin(quadBeta.tr) * hLength;
	//								hAdjEnd = Math.cos(quadBeta.tr) * hLength;
									lPath.addCommand("arc", [pRightStart.x, pRightStart.y], null, null, null, [lRad, lRad], null, [cCentre.x, cCentre.y], 0, 1);

									if (lbl._pointerRight) {
										lPath.addCommand("lineTo", [cCentre.x + pBase + pLength, cCentre.y]);
										lPath.addCommand("lineTo", [pRightEnd.x, pRightEnd.y]);
									}

									//second quadrant - 3 o'clock to 6 o'clock
									hLength = ((4 * lRad) / 3) * Math.tan(Math.PI / (2 * (360 / quadTheta.br)));
	//								hOppStart = Math.sin(quadAlpha.br) * hLength;
	//								hAdjStart = Math.cos(quadAlpha.br) * hLength;
	//								hOppEnd = Math.sin(quadBeta.br) * hLength;
	//								hAdjEnd = Math.cos(quadBeta.br) * hLength;
									lPath.addCommand("arc", [pBottomStart.x, pBottomStart.y], null, null, null, [lRad, lRad], null, [cCentre.x, cCentre.y], 0, 1);

									if (lbl._pointerBottom) {
										lPath.addCommand("lineTo", [cCentre.x, cCentre.y + pLength + pBase]);
										lPath.addCommand("lineTo", [pBottomEnd.x, pBottomEnd.y]);
									}

									//third quadrant - 6 o'clock to 9 o'clock
									hLength = ((4 * lRad) / 3) * Math.tan(Math.PI / (2 * (360 / quadTheta.bl)));
	//								hOppStart = Math.sin(quadAlpha.bl) * hLength;
	//								hAdjStart = Math.cos(quadAlpha.bl) * hLength;
	//								hOppEnd = Math.sin(quadBeta.bl) * hLength;
	//								hAdjEnd = Math.cos(quadBeta.bl) * hLength;
									lPath.addCommand("arc", [pLeftStart.x, pLeftStart.y], null, null, null, [lRad, lRad], null, [cCentre.x, cCentre.y], 0, 1);

									if (lbl._pointerLeft) {
										lPath.addCommand("lineTo", [cCentre.x - (pBase + pLength), cCentre.y]);
										lPath.addCommand("lineTo", [pLeftEnd.x, pLeftEnd.y]);
									}

									//fourth quadrant - 9 o'clock to 12 o'clock
									hLength = ((4 * lRad) / 3) * Math.tan(Math.PI / (2 * (360 / quadTheta.tl)));
	//								hOppStart = Math.sin(quadAlpha.tl) * hLength;
	//								hAdjStart = Math.cos(quadAlpha.tl) * hLength;
	//								hOppEnd = Math.sin(quadBeta.tl) * hLength;
	//								hAdjEnd = Math.cos(quadBeta.tl) * hLength;
									lPath.addCommand("arc", [pTopStart.x, pTopStart.y], null, null, null, [lRad, lRad], null, [cCentre.x, cCentre.y], 0, 1);

									if (lbl._pointerTop) {
										lPath.addCommand("lineTo", [cCentre.x, cCentre.y - (pBase + pLength)]);
									}
								}
							}

							lblBG.setStyle("fill", lbl._backgroundColour).setStyle("stroke", lbl._borderColour).setStyle("stroke-width", lbl._borderWidth).setStyle("shape-rendering", "geometricPrecision");
							lblBG.path = lPath;
							lblBG.oPath = cloneEveObj(lblBG.path);
							lblBG.eX = 0.5;
							lblBG.eY = 0.5;

							if (lbl._glow){
								var sPath = cloneEveObj(lblBG.path);
								var sOpacities = eve.fakeGradientAlphas(0, lbl._glowOpacity, lbl._glowSteps);
								for (var ss = 0; ss < lbl._glowSteps; ss++){
									var shadShape = lblGlow.getElement("path", c._drawCall, c.id + "_label" + ln + "_label_glow_" + ss, "eve-chart-label-glow");
									shadShape.setStyle("fill", lbl._glowColour)
										.setStyle("stroke", lbl._glowColour)
										.setStyle("stroke-width", ss * (lbl._glowSize / lbl._glowSteps))
										.setStyle("opacity", sOpacities[ss]);
									shadShape.path = sPath;
									shadShape.oPath = sPath;
									shadShape.eX = lblBG.eX + lbl._glowOffsetX;
									shadShape.eY = lblBG.eY + lbl._glowOffsetY;
								}
							}

							if (lbl._draggable) {
								labelGroup.setStyle("cursor", "move");
								if (lblLink && lblLink instanceof eve.axis) {
									if (lblLink._horizontal) {
										labelGroup.setStyle("cursor", "ew-resize");
									} else {
										labelGroup.setStyle("cursor", "ns-resize");
									}
								}
							}

							if (lbl._boxStyle === "rectangle" && lblCornerRad === 0 && pLength === 0) {
								lblBG.setStyle("shape-rendering", "crispEdges");
							}

							lblBG._animCurrent = cloneEveObj(lbl._animCurrent);
							if (lbl._age > 0 && lbl._redrawRequired) {
								if (isNil(lblBG._animCurrent.mode)) {
									lblBG._animCurrent.mode = "morph";
								}
							}
						}

						if (!isNil(lblBG)){
							lblGrab.path = cloneEveObj(lblBG.path);
							lblGrab.oPath = cloneEveObj(lblBG.path);
							lblGrab.setStyle("fill", "red").setStyle("fillOpacity", 0);
						}

						textElm.eX = lblWidth / 2;
						if (strIs(lbl._textAlign, "left")) {
							textElm.eX = lbl._padding;
						}
						if (strIs(lbl._textAlign, "right")) {
							textElm.eX = lblWidth - lbl._padding;
						}

						if (lbl._textOffsetX) {
							textElm.eX += lbl._textOffsetX;
						}

						textElm.eY = (lblHeight - textElm.height) / 2;
						if (lbl._textOffsetY) {
							textElm.eY += lbl._textOffsetY;
						}

						if (textElm.nudgeTop){
							textElm.eY -= textElm.nudgeTop;
						}

						if (lbl._draggable) {
							textElm.setStyle("cursor", "move");
						}
						//labelGroup.nodes.push(textElm);

						textElm._animCurrent = cloneEveObj(lbl._animCurrent);
						if (lbl._age > 0 && lbl._redrawRequired) {
							if (isNil(textElm._animCurrent.mode) || textElm._animCurrent.mode === "morph") {
								textElm._animCurrent.mode = "flipText";
							}
						}

						if (!isNil(lblLink)) {
							if (lblLink instanceof eve.highlight || lblLink instanceof eve.seriesNode || lblLink instanceof eve.seriesLineSegment) {
								//find the locaton on the x-axis:
								var seriesOffset = 5;
								var axX = getEveElmByID(lblLinkSeries._axisX);
								var axY = getEveElmByID(lblLinkSeries._axisY);

								var segStartX, segEndX, segStartY, segEndY;

								if (!isNil(lblLink._elmRef)) {
									segStartX = axX.getVal(lblLink._elmRef._dataStart[axX._domain], true);
									segEndX = axX.getVal(lblLink._elmRef._dataEnd[axX._domain], true);
									segStartY = axY.getVal(lblLink._elmRef._dataStart[axY._domain], true);
									segEndY = axY.getVal(lblLink._elmRef._dataEnd[axY._domain], true);
								} else {
									segStartX = axX.getVal(lblLink._data[axX._domain], true);
									segEndX = axX.getVal(lblLink._data[axX._domain], true);
									segStartY = axY.getVal(lblLink._data[axY._domain], true);
									segEndY = axY.getVal(lblLink._data[axY._domain], true);
								}
								var posLeft, posRight, posBelow, posAbove;

								if (lblLinkSeries._direction === "up" || lblLinkSeries._direction === "down") {

									lbl.eY = c._plotHeight - (segStartY - ((segStartY - segEndY) / 2));
									//position left of the segment:
									posLeft = segStartX - seriesOffset;
									if (segEndX < segStartX) {
										posLeft = segEndX - seriesOffset;
									}
									//position right of the segment:
									posRight = seriesOffset + segStartX;
									if (segEndX > segStartX) {
										posRight = segEndX + seriesOffset;
									}

									if (lblLink._criteria === "maxRise" || lblLink._criteria === "max") {
										//put it below the segment:
										lbl.eX = posLeft;
									} else {
										//put it above the segment:
										lbl.eX = posRight;
									}

								} else {

									lbl.eX = segStartX + ((segEndX - segStartX) / 2);
									//position below the segment:
									posBelow = segStartY + seriesOffset;
									if (segEndY > segStartY) {
										posBelow = segEndY + seriesOffset;
									}
									//position above the segment:
									posAbove = segStartY - seriesOffset;
									if (segEndY < segStartY) {
										posAbove = segEndY - seriesOffset;
									}

									if (lblLink._criteria === "maxRise" || lblLink._criteria === "max") {
										//put it below the segment:
										lbl.eY = posBelow;
									} else {
										//put it above the segment:
										lbl.eY = posAbove;
									}

								}
								//make sure the label doesn't go off the edge of the chart:
								var lblGroupHeight = lblHeight + lblMargin.top + lblMargin.bottom;
								var lblGroupWidth = lblWidth + lblMargin.left + lblMargin.right;

								//nudge it back onto the chart:
								if (lbl.eX - lblOrigin.hor < -(c._paddingLeft + c._marginLeft)) {
									lbl.eX = c._paddingLeft + c._marginLeft + lblOrigin.hor;
								}
								if (lbl.eX + lblGroupWidth - lblOrigin.hor > c._plotWidth + c._paddingRight + c._marginRight) {
									lbl.eX = (c._plotWidth + c._paddingRight + c._marginRight) - lblGroupWidth + lblOrigin.hor;
								}

								if (lbl.eY - lblOrigin.ver < -(c._paddingTop + c._marginTop)) {
									lbl.eY = posBelow + (lblGroupHeight + seriesOffset + lblMargin.top + lblMargin.bottom);
								}
								if (lbl.eY - lblOrigin.ver + lblGroupHeight > c._plotHeight + c._paddingBottom + c._marginBottom) {
									lbl.eY = posAbove - (lblGroupHeight + seriesOffset + lblMargin.top + lblMargin.bottom);
								}
								if (!isNil(lbl.offsetX)) {
									lbl.eX += lbl.offsetX;
								}
								if (!isNil(lbl.offsetY)) {
									lbl.eY += lbl.offsetY;
								}
							}
						}

						//TODO: this prevents draggable labels from jumping around after resizing. It's horrible, though - can we do it better?
						//TODO: I've commented it out for the Centsai debt consolidator chart - fix it all
						//can we simply recalculate the posn from its axisVal?
						// var lblElm = get(labelGroup.id, labelGroup._container);
						// if (lbl._draggable && !isNil(lblElm) && !isNil(lblElm._dragPosX) && !isNil(lblElm._dragPosY) && lblLink && lblLink instanceof eve.axis && lblLink._horizontal) {
						// 	labelGroup.eX = lblElm._dragPosX;
						// 	labelGroup.eY = parseInt(lbl.eY - lblOrigin.ver);
						// }
						// else if (lbl._draggable && !isNil(lblElm) && !isNil(lblElm._dragPosX) && !isNil(lblElm._dragPosY) && lblLink && lblLink instanceof eve.axis && !lblLink._horizontal) {
						// 	labelGroup.eY = lblElm._dragPosY;
						// 	labelGroup.eX = parseInt(lbl.eX - lblOrigin.hor);
						// }
						// else if (lbl._draggable && !isNil(lblElm) && !isNil(lblElm._dragPosX) && !isNil(lblElm._dragPosY)) {
						// 	labelGroup.eX = lblElm._dragPosX;
						// 	labelGroup.eY = lblElm._dragPosY;
						// }

						if (!isNil(lbl._axisVal) && !isNil(lblLink) && lblLink instanceof eve.axis) {
							if (lblLink._horizontal){
								// labelGroup.eX = (c._plotWidth / lblLink._range) * lbl._axisVal;
								lbl.eX = lblLink.getVal(lbl._axisVal, true);
							}
						}

						// else {
							//round off to avoid blurry edges:
							labelGroup.eX = parseInt(lbl.eX - lblOrigin.hor);
							labelGroup.eY = parseInt(lbl.eY - lblOrigin.ver);
						//}

						labelGroup.oX = labelGroup.eX;
						labelGroup.oY = labelGroup.eY;
						labelGroup._offsetX = lblOrigin.hor;
						labelGroup._offsetY = lblOrigin.ver;
						labelGroup._labelObj = lbl;

						if (parentIsLabel) {
							labelGroup.eX += parentGroup._lblWidth / 2;
						}

						if (!isNil(lbl._highlight) && lbl._highlightBlock === "col"){
							lbl._highlight.eX = lbl.eX - ((c._plotWidth * lblLink._bands[0]._end) / 2);
						}

						//leaders:
						for (var ld = 0; ld < lbl._leaders.length; ld++) {

							var ldr = getEveElmByID(lbl._leaders[ld]);
							eve.setObjProps(ldr);

							var ldPath = eve.addPath(null, false);

							var fromX = lblOrigin.hor;
							var fromY = lblOrigin.ver;

							var toX = -labelGroup.eX;
							var toY = c._plotHeight - labelGroup.eY;

							if (!isNil(lblLink) && lblLink instanceof eve.axis) {
								if (lblLink._position === "left") {
									toX = c._plotWidth - labelGroup.eX;
								} else if (lblLink._position === "right") {
									//Y-axis:
									toX = -labelGroup.eX;
								} else {
									//X-axis:
									toY = c._plotHeight - labelGroup.eY;
								}
							}

							var lsX, lsY, leX, leY;
							if (!isNil(ldr._startX)) {
								lsX = ldr._startX;
								if (lsX.toString().indexOf("%") !== -1){
									fromX = ((parseFloat(lsX) / 100.0) * c._plotWidth) - labelGroup.eX;
								}
								else{
									fromX = lsX - labelGroup.eX;
								}
							}
							if (!isNil(ldr._startY)) {
								lsY = ldr._startY;
								if (lsY.toString().indexOf("%") !== -1){
									fromY = ((100 - (parseFloat(lsY) / 100.0)) * c._plotHeight) - labelGroup.eY;
								}
								else{
									fromY = lsY - labelGroup.eY;
								}
							}
							if (!isNil(ldr._endX)) {
								leX = ldr._endX;
								if (leX.toString().indexOf("%") !== -1){
									toX = ((parseFloat(leX) / 100.0) * c._plotWidth) - labelGroup.eX;
								}
								else{
									toX = leX - labelGroup.eX;
								}
							}
							if (!isNil(ldr._endY)) {
								leY = ldr._endY;
								if (lsY.toString().indexOf("%") !== -1){
									toY = ((100 - (parseFloat(leY) / 100.0)) * c._plotHeight) - labelGroup.eY;
								}
								else{
									toY = leY - labelGroup.eY;
								}
							}
							if (ldr._horizontal) {
								toY = fromY;
							} else if (ldr._vertical) {
								toX = fromX;
							}

							//calc new points based on offsets:
							var ldWidth = fromX - toX;
							var ldHeight = toY - fromY;
							//use pythagoras to get the length:
							var ldLength = Math.sqrt((ldWidth * ldWidth) + (ldHeight * ldHeight));
							//get the angle:
							var ldAngle = Math.asin(ldWidth / ldLength);
							//and then get the amounts to remove from each coordinate:
							if (!isNil(ldr._offset.start)) {
								var fromOffsetX = Math.sin(ldAngle) * ldr._offset.start;
								var fromOffsetY = Math.cos(ldAngle) * ldr._offset.start;
								fromX -= fromOffsetX;
								fromY += fromOffsetY;
							}
							if (!isNil(ldr._offset.end)) {
								var toOffsetX = Math.sin(ldAngle) * ldr._offset.end;
								var toOffsetY = Math.cos(ldAngle) * ldr._offset.end;
								toX += toOffsetX;
								toY -= toOffsetY;
							}

							ldPath.addCommand("moveTo", [fromX, fromY]);
							ldPath.addCommand("lineTo", [toX, toY]);

							if (lbl._draggable && (ldr._vertical || ldr._horizontal)) {
								if (ldr._strokeOpacity > 0 && ldr._strokeWidth > 0) {
									var leaderGrab = leadersGroup.getElement("rect", c._drawCall, c.id + "_label" + ln + "_label_leader_grab" + ld, "eve-chart-label-leader-grab", null, {fill: "white", opacity: 0});
									if (ldr._vertical) {
										leaderGrab.eWidth = 20;
										leaderGrab.eHeight = Math.abs(toY - fromY);
										leaderGrab.eX = fromX - 10;
										leaderGrab.eY = fromY;
									} else {
										leaderGrab.eY = fromY - 10;
										leaderGrab.eX = fromX;
										leaderGrab.eHeight = 20;
										leaderGrab.eWidth = Math.abs(toX - fromX);
									}
								}
							}

							var leaderLine = leadersGroup.getElement("path", c._drawCall, c.id + "_label" + ln + "_label_leader" + ld, "eve-chart-label-leader");
							if (ldr._strokeOpacity > 0 && ldr._strokeWidth > 0) {
								leaderLine.setStyle("stroke", ldr._strokeColour).setStyle("stroke-width", ldr._strokeWidth).setStyle("opacity", ldr._strokeOpacity).setStyle("stroke-dasharray", ldr._strokeDash);
								if (ldr._arrowhead) {
									if (!ldr._arrowheadWidth) {
										ldr._arrowheadWidth = 8;
									}
									if (!ldr._arrowheadHeight) {
										ldr._arrowheadHeight = 14;
									}
									var aOffset = ldr._arrowOffset || 0;
									var arrPath = eve.addPath(null, false);
									arrPath.addCommand("moveTo", [toX, toY + ldr._arrowheadHeight + aOffset]);
									arrPath.addCommand("lineTo", [toX - (ldr._arrowheadWidth / 2), toY + aOffset]);
									arrPath.addCommand("lineTo", [toX + (ldr._arrowheadWidth / 2), toY + aOffset]);
									arrPath.addCommand("close");

									var leaderArrow = leadersGroup.getElement("path", c._drawCall, c.id + "_label" + ln + "_label_leader_arrow" + ld, "eve-chart-label-leader-arrow", null, {fill: ldr._strokeColour, opacity: ldr._strokeOpacity});

									if (ldr._arrowheadType === "reverseArrow") {
										arrPath = eve.addPath(null, false);
										arrPath.addCommand("moveTo", [toX, toY + aOffset]);
										arrPath.addCommand("lineTo", [toX + (ldr._arrowheadWidth / 2), toY + aOffset + ldr._arrowheadHeight]);
										arrPath.addCommand("lineTo", [toX - (ldr._arrowheadWidth / 2), toY + aOffset + ldr._arrowheadHeight]);
										arrPath.addCommand("close");
									}
									leaderArrow.path = arrPath;
								}
							}
							leaderLine.path = ldPath;
							if (c._drawCall > 1){
								leaderLine._animCurrent = {mode: "morph", duration: lbl._animCurrent.duration, delay: lbl._animCurrent.delay, ease: "easeOut"};
							}
						}

						//intersection nodes:

						if (!isNil(lbl._intNodes.length) && lbl._intNodes.length > 0) {
							labelGroup._intNodes = [];
						}

						for (var nn = 0; nn < lbl._intNodes.length; nn++) {

							var inod = getEveElmByID(lbl._intNodes[nn]);
							var iNodeID = c.id + "_label" + ln + "_int_node" + nn;
							inod.domID = iNodeID;
//							eve.setAllProps(inod, true, true, true);

							var iNode = intNodesGroup.getElement("circle", c._drawCall, iNodeID, "eve-chart-label-int-node", {r: inod._width / 2}, {fill: inod._colour, fillOpacity: inod._fillOpacity});
							if (inod._strokeOpacity > 0 && inod._strokeWidth > 0) {
								iNode.setStyle("stroke", inod._strokeColour).setStyle("stroke-opacity", inod._strokeOpacity).setStyle("stroke-width", inod._strokeWidth);
							}
							iNode.eX = labelGroup._offsetX;
							iNode.eY = labelGroup._offsetY;
							iNode._referenceObj = lbl._intNodes[nn];

							if (c._drawCall > 1 && !rebuild) {
								if (isNil(get(iNodeID, c._container))) {
									iNode._animCurrent = new eve.elmAnim("fade in", 0.25, animTime, "easeIn");
								} else {
									iNode._animCurrent = new eve.elmAnim("move", 0.25, animTime, "easeIn");
									if (!isNil(inod._series)) {
										iNode._animCurrent.delay = getEveElmByID(inod._series)._animCurrent.delay;
										iNode._animCurrent.duration = getEveElmByID(inod._series)._animCurrent.duration;
										iNode._animCurrent.ease = getEveElmByID(inod._series)._animCurrent.ease;
									}
								}
							}

							if (!isNil(inod._series)) {
								iNode._seriesObj = c.id + "_" + inod._series + "_path";
								if (getEveElmByID(inod._series)._type === "funnel" || getEveElmByID(inod._series)._type === "shape") {
									iNode._seriesObj = c.id + "_" + inod._series + "_shape";
								}
								iNode._seriesObjType = getEveElmByID(inod._series)._type;
								if (!isNil(lblLink) && lblLink instanceof eve.axis) {
									if (!lblLink._horizontal) {
										//Y-axis - only node's X position will change:
										iNode.eX = 50;
									} else {
										//X-axis - only node's Y position will change:
										iNode.eY = 50;
									}
								}
							}
							labelGroup._intNodes.push(lbl._intNodes[nn]);

						}

						if (lbl._draggable) {

							var dRangeMin = 0;
							var dRangeMax = 1;

							//if the label is linked to an axis, and the user hasn't specified a drag restriction, restrict it to the axis orientation:
							if (lblLink instanceof eve.axis) {
								if (lblLink._horizontal){
									// if (isNil(lbl._dragRect.yMin) && isNil(lbl._dragRect.yMinVal)){
									if (isNil(lbl._dragRect.yMinVal)){
										//lbl._dragRect.yMin = labelGroup.eY + lblOrigin.ver;
										//lbl._dragRect.yMin = 0;
										lbl._dragRect.yMin = c._plotHeight - (labelGroup.eY + lblOrigin.ver);
									}
									// if (isNil(lbl._dragRect.yMax) && isNil(lbl._dragRect.yMaxVal)){
									if (isNil(lbl._dragRect.yMaxVal)){
										lbl._dragRect.yMax = lbl._dragRect.yMin;
									}
								}
								else if (!lblLink._horizontal){
									// if (isNil(lbl._dragRect.xMin) && isNil(lbl._dragRect.xMinVal)){
									if (isNil(lbl._dragRect.xMinVal)){
										lbl._dragRect.xMin = labelGroup.eX + lblOrigin.hor;
									}
									// if (isNil(lbl._dragRect.xMax) && isNil(lbl._dragRect.xMaxVal)){
									if (isNil(lbl._dragRect.xMaxVal)){
										lbl._dragRect.xMax = labelGroup.eX + lblOrigin.hor;
									}
								}
								dRangeMin = lblLink._rangeMin;
								dRangeMax = lblLink._rangeMax;
							}

							if (isNil(lbl._dragRect.xMin) && isNil(lbl._dragRect.xMinVal)) {
								lbl._dragRect.xMin = 0;
							}
							if (isNil(lbl._dragRect.xMax) && isNil(lbl._dragRect.xMaxVal)) {
								lbl._dragRect.xMax = c._plotWidth;
							}
							if (isNil(lbl._dragRect.yMin) && isNil(lbl._dragRect.yMinVal)) {
								lbl._dragRect.yMin = 0;
							}
							if (isNil(lbl._dragRect.yMax) && isNil(lbl._dragRect.yMaxVal)) {
								lbl._dragRect.yMax = c._plotHeight;
							}
							if (lbl.dragMinY && lblLink instanceof eve.axis) {
								lbl._yAxisRatio = (lblLink._rangeMax - lblLink._rangeMin) / c._plotHeight;
								lbl._dragRect.yMin = lbl.dragMinY / lbl._yAxisRatio;
								//eve.setDecimal(elm._xAxisRatio * (pos.x - elm._xAxisStart), 3);
							}

							addToEventList(labelGroup.mouseDown, startDrag, [lbl._dragRect, dRangeMin, dRangeMax, lbl._onDrag, lbl._onDragComplete]);
						}
					}
				}
				postBuild(c, duration, rejigOnly);
			}
		};

		this.cancelDraw = function(){
			clearTimeout(this.plotTimeout);
			for (var t = 0; t < eve.tweens.length; t++) {
				if (eve.tweens[t].owner === this) {
					eve.tweens[t].cancelled = true;
				}
			}
			this._ready = true;
		};

		this.draw = function(duration, delay, rebuild, endFunction, endFunctionParams, buildDoneFunction, buildDoneFunctionParams, rejigOnly) {
			if (this._drawMode !== "PDF"){
				if (!this._ready){
					this.cancelDraw();
				}
				// I commented this out because it broke the change animtion on bar.change.php - no idea why I added it!
//				else{
//					if (!isNil(get(this.id, this._container))) {
//						removeNode(get(this.id, this._container));
//					}
//				}
			}
			//delay=delay || 0.1; //if delay is not specified, wait a moment to allow fonts to load
			delay = parseFloat(delay) || 0;
			this.startTime = Date.now();
			this.prevTimeGone = 0;
			this.timeGone = 0;
			//console.log(this.startTime);
			var c = this;

			c.endFunction = endFunction;
			c.endFunctionParams = endFunctionParams;
			c.buildDoneFunction = buildDoneFunction;
			c.buildDoneFunctionParams = buildDoneFunctionParams;

			if (this._drawMode !== "PDF" && rebuild) {
				c._drawCall = 0;
				//var oldSVG=get(c._id+"_svg");
				var oldSVG = get(c._id, c._container);
				if (!isNil(oldSVG)) {
					oldSVG.id = oldSVG.id + "_old";
					if (delay > 0) {
						fadeRemove(oldSVG, delay * 0.75);
					} else {
						removeNode(oldSVG);
					}
				}
			}

			if (this._drawMode !== "PDF" && delay > 0) {
				this.plotTimeout = setTimeout(function() {
					c.plot(duration, rebuild, rejigOnly);
				}, delay * 1000.0);
			} else {
				c.plot(duration, rebuild, rejigOnly);
			}
		};

		this.plot = function(duration, rebuild, rejigOnly){

			if (this._drawMode !== "PDF" && this._ready) {
				for (var t = 0; t < eve.tweens.length; t++) {
					if (eve.tweens[t].owner === this) {
						eve.tweens[t].cancelled = true;
					}
				}
			}

			if (this._drawMode !== "PDF"){
				this._ready = false;
			}
			if (!rejigOnly){
				this._drawCall++;
			}
			duration = duration || 0;
			this.build(duration, rebuild, rejigOnly);

		};

	};

	var postBuild = function(c, duration, rejigOnly){
		if (c._drawMode === "SVG" && c._drawCall === 1) {
			c._artboard = eve.getSVG(c._container, c._id, "eve-chart", c._width, c._height, c._width, c._height);
			var elmInArray = false;
			for (var r = 0; r < resizeElms.length; r++){
				if (resizeElms[r].id === c._id){
					elmInArray = true;
					break;
				}
			}
			if (!elmInArray){
				resizeElms.push(c);
			}
		}

		var showResult = function(){
			if (!isNil(c.buildDoneFunction)) {
				if (!isNil(c.buildDoneFunctionParams)) {
					c.buildDoneFunction.apply(c, c.buildDoneFunctionParams);
				} else {
					c.buildDoneFunction();
				}
			}
			if (duration > 0) {
				updateElms(c._blueprintGroup);
				setAnims(c._blueprintGroup, c._drawMode, c._drawCall, c, cleanNodes, [c._blueprintGroup, true, null, endDraw]);
			} else {
				if (c._drawMode !== "PDF"){
					setProps(c._blueprintGroup, c._drawMode, c._drawCall, c);
					updateElms(c._blueprintGroup, cleanNodes, [c._blueprintGroup, true, null, endDraw]);
				}
				else{
					// fix glitch:
					c._container.text("");
				}
			}
		};

		var endDraw = function(){

			var ac, sc, lc, pc;
			var a, s, l;

//			c.oldAxes = [];
			for (ac = 0; ac < c._axes.length; ac++) {
//				c.oldAxes[ac] = getEveElmByID(c._axes[ac]).cloneRef()._id;
				a = getEveElmByID(c._axes[ac]);
				a._oldProps = {};
				for (pc = 0; pc < axisPropsRedraw.length; pc++){
					a._oldProps[axisPropsRedraw[pc]] = a["_" + axisPropsRedraw[pc]];
				}
			}
//			c.oldSeries = [];
			for (sc = 0; sc < c._series.length; sc++) {
				s = getEveElmByID(c._series[sc]);
				s._oldProps = {};
				for (pc = 0; pc < seriesPropsRedraw.length; pc++){
					if (seriesPropsRedraw[pc] === "data"){
						s._oldProps[seriesPropsRedraw[pc]] = s["_" + seriesPropsRedraw[pc]].clone(true);
					}
					else{
						s._oldProps[seriesPropsRedraw[pc]] = s["_" + seriesPropsRedraw[pc]];
					}
				}
//				c.oldSeries[sc] = getEveElmByID(c._series[sc]).cloneRef()._id;
			}
//			c.oldLabels = [];
			for (lc = 0; lc < c._labels.length; lc++) {
//				c.oldLabels[lc] = getEveElmByID(c._labels[lc]).cloneRef()._id;
				l = getEveElmByID(c._labels[lc]);
				l._oldProps = {};
				for (pc = 0; pc < labelPropsRedraw.length; pc++){
					l._oldProps[labelPropsRedraw[pc]] = l["_" + labelPropsRedraw[pc]];
				}
			}

			//var c = this;
			setTimeout(function() {
				setReady(c);
				if (c._showExport) {
					var btnWidth = 120;
					var btnHeight = 36;
					var btnMargin = 20;
					var btnID = c._id + "_button_export";
					var btnExport = get(btnID, c._container);
					if (!btnExport) {
						btnExport = document.createElementNS(svgns, "g");
						btnExport.id = btnID;
						btnExport.style.cursor = "pointer";
						var btnExportRect = document.createElementNS(svgns, "rect");
						btnExportRect.setAttribute("width", btnWidth + "px");
						btnExportRect.setAttribute("height", btnHeight + "px");
						btnExportRect.setAttribute("fill", "#262829");
						btnExportRect.setAttribute("rx", "4px");
						btnExportRect.setAttribute("ry", "4px");
						btnExport.appendChild(btnExportRect);
						var btnCaption = document.createElementNS(svgns, "text");
						btnCaption.setAttribute("x", (btnWidth / 2) + "px");
						btnCaption.setAttribute("y", (btnHeight / 2) + "px");
						btnCaption.setAttribute("width", btnWidth + "px");
						btnCaption.setAttribute("height", btnHeight + "px");
						btnCaption.style.fill = "#FFFFFF";
						btnCaption.style.fontSize = "14px";
						btnCaption.style.fontFamily = "sans-serif";
						btnCaption.setAttribute("text-anchor", "middle");
						btnCaption.setAttribute("dominant-baseline", "central");
						var textNode = document.createTextNode("Export SVG...");
						btnCaption.appendChild(textNode);
						btnExport.appendChild(btnCaption);
						c._artboard.appendChild(btnExport);
						btnExport.onclick = function() {
							eve.exportSVG(c._artboard, c._id, c._exportDashes);
						};
					}
					eve.setSvgTransform(btnExport, "translate", [c._width - (btnWidth + btnMargin), btnMargin]);
				}
				if (c._drawMode !== "PDF" && c._artboard.style.visibility === "hidden"){
					c._artboard.style.visibility = "visible";
				}
				if (!isNil(c.endFunction)) {
					if (!isNil(c.endFunctionParams)) {
						c.endFunction.apply(this, c.endFunctionParams);
					} else {
						c.endFunction();
					}
				}

			}, duration * 1000.0);
		};

		drawElms(c._blueprintGroup, c._artboard, duration, c._drawMode, c._drawCall, c._vDefs, c._container, c, rejigOnly, cleanNodes, [c._blueprintGroup, false, false, showResult]);

	};

	var setProps = function(node, drawMode, drawCall, owner) {

		var defs;
		if (drawMode === "SVG") {
			defs = eve.getSvgDefs(owner._artboard);
		}

		for (var i = 0, nn = node.nodes.length; i < nn; i++) {
			var n = node.nodes[i];
			var elm = get(n.id, owner._container);
			if (!isNil(elm)) {
				if (strIs(elm.tagName, "tspan")) {
					elm.textContent = n.text;
					if (!isNil(elm.eX)) {
						elm.setAttribute("x", elm.eX);
					}
					if (!isNil(elm.eY)) {
						elm.setAttribute("y", elm.eY);
					}
				} else if (strIs(elm.tagName, "circle")) {
					elm.setAttribute("cx", elm.eX || 0);
					elm.setAttribute("cy", elm.eY || 0);
				} else {
					eve.setSvgTransform(elm, "translate", [elm.eX || 0, elm.eY || 0]);
				}
				if (!isNil(elm.eRot)) {
					eve.setSvgTransform(elm, "rotate", elm.eRot);
				}
				if (n.type === "path") {
					elm.setAttribute("d", elm.path.toSVG());
				} else if (n.type === "sector") {
					elm.setAttribute("d", elm.sector.toSVG());
				}

				if (n.type === "path" || n.type === "line") {
					if (!isNil(elm.dashArray)) {
						if (parseFloat(elm.dashArray) === elm.getTotalLength()){
							elm.style.strokeDasharray = "";
							elm.style.strokeDashoffset = "";
						}
						else{
							elm.style.strokeDasharray = elm.dashArray + elm.getTotalLength();
							elm.style.strokeDashoffset = -elm.getTotalLength();
						}
					}
				}

				elm.setAttribute("width", elm.eWidth);
				elm.setAttribute("height", elm.eHeight);

				if (!isNil(elm.eGradient)) {
					var gradAdded = false;
					for (var c = 0; c < defs.childNodes.length; c++) {
						if (defs.childNodes[c].id === elm.eGradient.id) {
							gradAdded = true;
							break;
						}
					}
					if (!gradAdded) {
						defs.appendChild(elm.eGradient);
					}
					elm.style.fill = "url(#" + elm.eGradient.id + ")";
				}
			}

			if (n.nodes.length > 0) {
				setProps(n, drawMode, drawCall, owner);
			}
			if (!n.immortal) {
				n.age = drawCall;
			}
		}
	};

	var setAnims = function(node, drawMode, drawCall, owner, endFunction, endFunctionParams) {
		var defs;
		if (drawMode === "SVG") {
			defs = eve.getSvgDefs(owner._artboard);
		}
		for (var i = 0, nn = node.nodes.length; i < nn; i++) {
			var n = node.nodes[i];
			var anim = n._animCurrent;

			if (!isNil(anim) && !isNil(anim.mode) && anim.mode !== "" && anim.mode !== "none") {

				anim.path = anim.path || {type: "linear"};

				if (drawMode === "SVG") {
					var elm = get(n.id, owner._container);
					if (!isNil(elm)) {

//						var toStroke, fromStroke;
						var toOpacity, toFill, fromFill, kc = null;
						var toX, toY, toWidth, toHeight, fromX, fromY, fromWidth, fromHeight = 0;

						if (!isNil(elm.eGradient)) {
							var gradAdded = false;
							for (var c = 0; c < eve.getSvgDefs(elm.ownerSVGElement).childNodes.length; c++) {
								if (defs.childNodes[c].id === elm.eGradient.id) {
									gradAdded = true;
									break;
								}
							}
							if (!gradAdded) {
								defs.appendChild(elm.eGradient);
							}
						}

						if (anim.mode === "" || anim.mode === "none" || isNil(anim.mode)) {
							elm.setAttribute("width", elm.eWidth);
							elm.setAttribute("height", elm.eHeight);
						} else {
							if (anim.mode === "appear") {
								elm.style.opacity = 0;
								elm.setAttribute("width", elm.eWidth);
								elm.setAttribute("height", elm.eHeight);
								if (!isNil(elm.eGradient)) {
									elm.style.fill = "url(#" + elm.eGradient.id + ")";
								}
								if (anim.endOpacity !== 0 && anim.endOpacity !== "0") {
									toOpacity = 1;
									if (!isNil(anim.endOpacity)) {
										toOpacity = anim.endOpacity;
									}
									appear(elm, anim.delay, toOpacity);
								}
							}

							if (anim.mode === "fade and remove") {
								fadeRemove(elm, anim.duration, anim.delay, owner);
							}

							if (anim.mode === "disappear") {
								elm.style.opacity = 0;
								disappear(elm, anim.delay);
							}

							if (anim.mode === "fade in") {
								if (n.eWidth) {
									elm.setAttribute("width", n.eWidth);
								}
								if (n.eHeight) {
									elm.setAttribute("height", n.eHeight);
								}
								toOpacity = elm.style.opacity || 1;
								elm.style.opacity = 0;
								if (!isNil(anim.endOpacity)) {
									toOpacity = anim.endOpacity;
								}
								eve.tweenFromTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {opacity: 0}, {opacity: toOpacity}, owner);
							}

							//if (anim.mode==="draw"){
							if (anim.mode.toString().indexOf("draw") !== -1) {
								if (n.type === "path" || n.type === "line") {
									var delayAdd = 0;
									if (elm.exists) {
										if (!isNil(elm.oPath)) {
											elm.setAttribute("d", elm.oPath.toSVG());
										}
										delayAdd = anim.duration / 4;
										eve.tweenTo(elm, anim.duration / 4, anim.delay, anim.ease, anim.path, {"opacity": 0, "onComplete": setPath, "onCompleteParams": [elm]}, owner);
									}
									if (isNil(elm.dashArray)) {
										elm.style.strokeDasharray = elm.getTotalLength();
										elm.style.strokeDashoffset = -elm.getTotalLength();
										eve.tweenFromTo(elm, anim.duration - delayAdd, anim.delay + delayAdd, anim.ease, anim.path, {drawSVG: "0%", opacity: 1}, {drawSVG: "100%", opacity: 1}, owner);
									} else {
										elm.style.strokeDasharray = elm.dashArray + elm.getTotalLength();
										elm.style.strokeDashoffset = -elm.getTotalLength();
										eve.tweenFromTo(elm, anim.duration - delayAdd, anim.delay + delayAdd, anim.ease, anim.path, {drawSvgDashed: "0%", opacity: 1}, {drawSvgDashed: "100%", opacity: 1}, owner);
									}
								}
								if (!isNil(n._knots)) {
									for (var k = 0; k < n._knots.length; k++) {
										var knot = get(n._knots[k].id, owner._container);
										if (elm.exists) {
											knot.shown = false;
											knot.setAttribute("x", fromX);
											knot.setAttribute("y", fromY);
											eve.tweenTo(knot, anim.duration / 4, anim.delay, anim.ease, anim.path, {opacity: 0}, owner);
										} else {
											knot.style.opacity = 0;
										}
									}
								}
							}

							if (anim.mode === "move") {
								fromX = elm.eX || 0;
								fromY = elm.eY || 0;
								var fromRot = elm.eRot || [0, 0, 0];

								if (!isNil(elm.oX)) {
									fromX = elm.oX;
								}
								if (!isNil(elm.oY)) {
									fromY = elm.oY;
								}
								if (!isNil(elm.oRot)) {
									fromRot = elm.oRot;
								}

								toX = elm.eX || 0;
								toY = elm.eY || 0;
								var toRot = elm.eRot || [0, 0, 0];

								elm.eX = fromX;
								elm.eY = fromY;
								elm.eRot = fromRot;

								if (n.type === "line") {
									elm.setAttribute("x1", elm.oX1);
									elm.setAttribute("y1", elm.oY1);
									elm.setAttribute("x2", elm.oX2);
									elm.setAttribute("y2", elm.oY2);
									eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {x1: elm.eX1, y1: elm.eY1, x2: elm.eX2, y2: elm.eY2}, owner);
								} else {
									if (n.type === "tspan") {
										elm.setAttribute("x", fromX);
										elm.setAttribute("y", fromY);
									} else if (n.type === "circle") {
										elm.setAttribute("cx", fromX);
										elm.setAttribute("cy", fromY);
									} else {
										eve.setSvgTransform(elm, "translate", [fromX, fromY]);
										eve.setSvgTransform(elm, "rotate", fromRot);
									}
									eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {x: toX, y: toY, rotation: toRot}, owner);
								}
							}

							if (anim.mode === "flipText") {

								fromX = elm.eX || 0;
								fromY = elm.eY || 0;

								if (!isNil(elm.oX)) {
									fromX = elm.oX;
								}
								if (!isNil(elm.oY)) {
									fromY = elm.oY;
								}

								toX = elm.eX || 0;
								toY = elm.eY || 0;

								if (!isNil(elm.newX)) {
									toX = elm.newX;
								}
								if (!isNil(elm.newY)) {
									toY = elm.newY;
								}

								elm.eX = fromX;
								elm.eY = fromY;

								eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {x: toX, y: toY}, owner);

								for (var t = 0; t < elm.childNodes.length; t++) {
									var cText = elm.childNodes[t];
									eve.flipElmText(cText, anim.duration, anim.delay);
									if (cText.newX && cText.newY) {
										eve.tweenTo(cText, anim.duration, anim.delay, anim.ease, anim.path, {x: cText.newX, y: cText.newY}, owner);
									}
								}

							}

							if (anim.mode === "fadeAndFlipText") {

								fromX = elm.eX || 0;
								fromY = elm.eY || 0;

								if (!isNil(elm.oX)) {
									fromX = elm.oX;
								}
								if (!isNil(elm.oY)) {
									fromY = elm.oY;
								}

								toX = elm.eX || 0;
								toY = elm.eY || 0;

								elm.eX = fromX;
								elm.eY = fromY;

								toOpacity = elm.style.opacity || 1;
								elm.style.opacity = 0;
								if (!isNil(anim.endOpacity)) {
									toOpacity = anim.endOpacity;
								}

								eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {opacity: toOpacity, x: toX, y: toY}, owner);

								for (var ec = 0; ec < elm.childNodes.length; ec++) {
									var chText = elm.childNodes[ec];
									// chText.childNodes[0].oldText = chText.textContent;
									// chText.childNodes[0].newText = n.toText[ec];
									chText.oldText = n.fromText[ec];
									chText.newText = chText.textContent;
									eve.flipElmText(chText, anim.duration, anim.delay);
								}

							}

							if (anim.mode === "fadeDownChangeFadeUp") {

								fromX = elm.eX || 0;
								fromY = elm.eY || 0;

								if (!isNil(elm.oX)) {
									fromX = elm.oX;
								}
								if (!isNil(elm.oY)) {
									fromY = elm.oY;
								}

								toX = elm.eX || 0;
								toY = elm.eY || 0;

								if (!isNil(elm.newX)) {
									toX = elm.newX;
								}
								if (!isNil(elm.newY)) {
									toY = elm.newY;
								}

								elm.eX = fromX;
								elm.eY = fromY;

								if (n.type === "path" || n.type === "shape") {
									if (!isNil(elm.oPath)) {
										elm.setAttribute("d", elm.oPath.toSVG());
									}
								}

								if (n.type === "text") {
									eve.tweenTo(elm, anim.duration / 3, anim.delay, anim.ease, anim.path, {"opacity": 0, "onComplete": setText, "onCompleteParams": [elm]}, owner);
								} else {
									eve.tweenTo(elm, anim.duration / 3, anim.delay, anim.ease, anim.path, {"opacity": 0}, owner);
								}
								if (n.type === "tspan") {
									eve.tweenTo(elm, 0, (anim.duration / 3) + anim.delay, "easeInOut", null, {"x": toX, "y": toY}, owner);
								} else if (n.type === "circle") {
									eve.tweenTo(elm, 0, (anim.duration / 3) + anim.delay, "easeInOut", null, {"cx": toX, "cy": toY}, owner);
								} else {
									eve.tweenTo(elm, 0, (anim.duration / 3) + anim.delay, "easeInOut", null, {"x": toX, "y": toY}, owner);
									if (n.type === "path" || n.type === "shape") {
										eve.tweenTo(elm, 0, (anim.duration / 3) + anim.delay, "easeInOut", null, {"path": elm.path.toSVG()}, owner);
									}
								}
								toOpacity = n.getStyle("opacity");
								if (isNil(toOpacity)){
									toOpacity = 1;
								}
								eve.tweenFromTo(elm, anim.duration / 3, anim.delay + (2 * (anim.duration / 3)), "easeInOut", anim.path, {opacity: 0}, {"opacity": toOpacity}, owner);

							}

							if (anim.mode === "rotate") {
								// elm.oRot = n.oRot = 0;
								// n.eRot = 0;
								// elm.eRot = 0;
								eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {rotation: n.eRot}, owner);
							}

							if (anim.mode === "rotateAndFadeIn") {
								toOpacity = elm.style.opacity || 1;
								elm.style.opacity = 0;
								if (!isNil(anim.endOpacity)) {
									toOpacity = anim.endOpacity;
								}
								elm.oRot = n.oRot || [0, 0, 0];
								if (!isNil(elm.sector)) {
									n.eRot = [0, 0, 0];
									elm.eRot = [0, 0, 0];
									if (!isNil(elm.oSector)) {
										elm.setAttribute("d", elm.oSector.toSVG());
									} else {
										elm.setAttribute("d", n.sector.toSVG());
									}
									eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {sector: n.sector, opacity: toOpacity}, owner);
								}
								else{
									eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {rotation: n.eRot, opacity: toOpacity}, owner);
								}
							}

							if (anim.mode === "rotateAndGrow") {
								fromFill = elm.oFill || elm.eFill;
//								fromStroke = elm.oStroke || elm.eStroke;
								toFill = elm.eFill;
//								toStroke = elm.eStroke;
								elm.oRot = n.oRot;
								n.eRot = [0, 0, 0];
								elm.eRot = [0, 0, 0];
								if (!isNil(elm.oSector)) {
									elm.setAttribute("d", elm.oSector.toSVG());
								} else {
									elm.setAttribute("d", n.sector.toSVG());
								}
								eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {sector: n.sector}, owner);
							}

							if (anim.mode === "morph" || anim.mode === "growWedge" || anim.mode === "rotateCollapse") {

								fromX = elm.eX || 0;
								fromY = elm.eY || 0;
								fromWidth = elm.eWidth || 0;
								fromHeight = elm.eHeight || 0;
								fromFill = elm.oFill || elm.eFill;
//								fromStroke = elm.oStroke || elm.eStroke;

								if (!isNil(elm.oX)) {
									fromX = elm.oX;
								}
								if (!isNil(elm.oY)) {
									fromY = elm.oY;
								}
								if (!isNil(elm.oWidth)) {
									fromWidth = elm.oWidth;
								}
								if (!isNil(elm.oHeight)) {
									fromHeight = elm.oHeight;
								}

								toX = elm.eX || 0;
								toY = elm.eY || 0;
								toWidth = elm.eWidth || 0;
								toHeight = elm.eHeight || 0;
								toFill = elm.eFill;
//								toStroke = elm.eStroke;

								if (n.type === "path" || n.type === "shape") {
									if (!isNil(elm.oPath)) {
										elm.setAttribute("d", elm.oPath.toSVG());
									} else {
										elm.setAttribute("d", n.path.toSVG());
									}

									// if (anim.mode === "morph") {
										elm.style.stroke = n.getStyle("stroke", true);
									// }

									elm.fPath = n.fPath;

									if (!isNil(n._knots)) {
										// new knots appear in their final position, which looks weird.
										// instead, find the props of the last of the existing knots and move all the new ones to that location:
										var oCX = null;
										var oCY = null;
										for (kc = 0; kc < n._knots.length; kc++) {
											if (!isNil(getOldProp(n, "points")) && !isNil(getOldProp(n, "points")[kc])) {
												oCX = getOldProp(n, "points")[kc][0];
												oCY = getOldProp(n, "points")[kc][1];
											}
										}
										for (kc = 0; kc < n._knots.length; kc++) {
											if (!isNil(getOldProp(n, "points")) && !isNil(getOldProp(n, "points")[kc])) {
												get(n._knots[kc].id, owner._container).setAttribute("cx", getOldProp(n, "points")[kc][0]);
												get(n._knots[kc].id, owner._container).setAttribute("cy", getOldProp(n, "points")[kc][1]);
											} else {
												if (!isNil(oCX)){
													get(n._knots[kc].id, owner._container).setAttribute("cx", oCX);
													get(n._knots[kc].id, owner._container).setAttribute("cy", oCY);
												}
												get(n._knots[kc].id, owner._container).style.opacity = 0;
												eve.tweenTo(get(n._knots[kc].id, owner._container), anim.duration, anim.delay, anim.ease, anim.path, {opacity: 1}, owner);
											}
										}
									}
									if (!isNil(elm.oldDashArray) && !isNil(elm.oldStrokeGradient)) {
									// if (!isNil(elm.oldDashArray)) {
										elm.style.strokeDasharray = elm.oldDashArray;
										elm.style.stroke = "url(#" + elm.oldStrokeGradient.id + ")";
										eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {path: n.path.toSVG(), strokeDasharray: elm.dashArray, stroke: elm.strokeGradient}, owner);
									} else {
										eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {start: anim.start, end: anim.end, path: n.path.toSVG()}, owner);
									}
								} else if (n.type === "sector") {

									if (!isNil(elm.oSector)) {
										elm.setAttribute("d", elm.oSector.toSVG());
									} else {
										elm.setAttribute("d", n.sector.toSVG());
									}
									if (anim.mode === "rotateCollapse") {
										elm.bpElm = n;
										eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {sector: n.sector, onComplete: disappear, onCompleteParams: [elm, 0]}, owner);
									} else {
										eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {sector: n.sector, fill: toFill}, owner);
									}

								} else {
									eve.setSvgTransform(elm, "translate", [fromX, fromY]);
									elm.setAttribute("width", fromWidth);
									elm.setAttribute("height", fromHeight);
									elm.style.fill = fromFill;
									eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {fill: toFill, x: toX, y: toY, width: toWidth, height: toHeight}, owner);
								}
							}

							if (anim.mode === "growUp") {
								fromHeight = 0;
								fromWidth = elm.eWidth;
								fromY = elm.eHeight + elm.eY;
								fromX = elm.eX || 0;

								if (!isNil(elm.oHeight)) {
									fromHeight = elm.oHeight;
								}
								if (!isNil(elm.oY)) {
									fromY = elm.oY;
								}
								if (!isNil(elm.oWidth)) {
									fromWidth = elm.oWidth;
								}

								toWidth = elm.eWidth;
								toHeight = elm.eHeight;
								toX = elm.eX || 0;
								toY = elm.eY;

								elm.eX = fromX;
								elm.eY = fromY;
								elm.setAttribute("height", fromHeight);
								elm.setAttribute("width", fromWidth);

								eve.setSvgTransform(elm, "translate", [fromX, fromY]);
								eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {x: toX, y: toY, width: toWidth, height: toHeight}, owner);
							}
							if (anim.mode === "growDown") {
								fromHeight = 0;
								fromWidth = elm.eWidth;
								fromY = elm.eY;
								fromX = elm.eX || 0;

								if (!isNil(elm.oHeight)) {
									fromHeight = elm.oHeight;
								}
								if (!isNil(elm.oY)) {
									fromY = elm.oY;
								}
								if (!isNil(elm.oWidth)) {
									fromWidth = elm.oWidth;
								}

								toWidth = elm.eWidth;
								toHeight = elm.eHeight;
								toX = elm.eX || 0;
								toY = elm.eY;

								elm.eX = fromX;
								elm.eY = fromY;
								elm.setAttribute("height", fromHeight);
								elm.setAttribute("width", fromWidth);

								eve.setSvgTransform(elm, "translate", [fromX, fromY]);
								eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {x: toX, y: toY, width: toWidth, height: toHeight}, owner);
							}
							if (anim.mode === "growLeft") {
								fromHeight = elm.eHeight;
								fromWidth = 0;
								fromX = elm.eWidth + elm.eX;
								fromY = elm.eY || 0;

								if (!isNil(elm.oHeight)) {
									fromHeight = elm.oHeight;
								}
								if (!isNil(elm.oX)) {
									fromX = elm.oX;
								}
								if (!isNil(elm.oWidth)) {
									fromWidth = elm.oWidth;
								}

								toWidth = elm.eWidth;
								toHeight = elm.eHeight;
								toX = elm.eX;
								toY = elm.eY || 0;

								elm.eX = fromX;
								elm.eY = fromY;
								elm.setAttribute("height", fromHeight);
								elm.setAttribute("width", fromWidth);

								eve.setSvgTransform(elm, "translate", [fromX, fromY]);
								eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {x: toX, y: toY, width: toWidth, height: toHeight}, owner);
							}
							if (anim.mode.toString().indexOf("reveal") === 0) {
								var clipID = n.id + "_clip";
								var clipRectID = n.id + "_clip_rect";
								var clip = get(clipID, owner._container);
								if (!clip) {
									clip = document.createElementNS(svgns, "clipPath");
									clip.id = clipID;
									//eve.getSvgDefs(elm.ownerSVGElement).appendChild(clip);
									defs.appendChild(clip);
								}
								var clipRect = get(clipRectID, owner._container);
								if (!clipRect) {
									clipRect = document.createElementNS(svgns, "rect");
									clipRect.id = clipRectID;
									clip.appendChild(clipRect);
								}
								elm.setAttribute("clip-path", "url(#" + clipID + ")");
								if (anim.mode === "revealRight") {
									clipRect.setAttribute("x", 0);
									clipRect.setAttribute("y", 0);
									clipRect.setAttribute("height", owner._plotHeight);
									clipRect.setAttribute("width", 0);
									eve.tweenTo(clipRect, anim.duration, anim.delay, anim.ease, anim.path, {width: owner._plotWidth, onComplete: removeClipPath, onCompleteParams: [elm]}, owner);
								} else if (anim.mode === "revealLeft") {
									clipRect.setAttribute("x", owner._plotWidth);
									clipRect.setAttribute("y", 0);
									clipRect.setAttribute("height", owner._plotHeight);
									clipRect.setAttribute("width", 0);
									eve.tweenTo(clipRect, anim.duration, anim.delay, anim.ease, anim.path, {x: 0, width: owner._plotWidth, onComplete: removeClipPath, onCompleteParams: [elm]}, owner);
								} else if (anim.mode === "revealDown") {
									clipRect.setAttribute("x", -500);
									clipRect.setAttribute("y", 0);
									clipRect.setAttribute("height", 0);
									clipRect.setAttribute("width", owner._plotWidth);
									eve.tweenTo(clipRect, anim.duration, anim.delay, anim.ease, anim.path, {height: owner._plotHeight, onComplete: removeClipPath, onCompleteParams: [elm]}, owner);
								} else if (anim.mode === "revealUp") {
									clipRect.setAttribute("x", -500);
									clipRect.setAttribute("y", owner._plotHeight);
									clipRect.setAttribute("height", owner._plotHeight);
									clipRect.setAttribute("width", owner._plotWidth * 2);
									eve.tweenTo(clipRect, anim.duration, anim.delay, anim.ease, anim.path, {y: -owner._plotHeight, onComplete: removeClipPath, onCompleteParams: [elm]}, owner);
								}
							}

							if (anim.mode === "growRight") {
								fromHeight = elm.eHeight;
								fromWidth = 0;
								fromY = elm.eY || 0;
								fromX = elm.eX;

								if (!isNil(elm.oWidth)) {
									fromWidth = elm.oWidth;
								}
								if (!isNil(elm.oHeight)) {
									fromHeight = elm.oHeight;
								}
								if (!isNil(elm.oX)) {
									fromX = elm.oX;
								}
								if (!isNil(elm.oY)) {
									fromY = elm.oY;
								}

								toWidth = elm.eWidth;
								toHeight = elm.eHeight;
								toX = elm.eX || 0;
								toY = elm.eY;

								elm.eX = fromX;
								elm.eY = fromY;
								elm.setAttribute("height", fromHeight);
								elm.setAttribute("width", fromWidth);

								eve.setSvgTransform(elm, "translate", [fromX, fromY]);
								eve.tweenTo(elm, anim.duration, anim.delay, anim.ease, anim.path, {x: toX, y: toY, width: toWidth, height: toHeight}, owner);
							}
						}
					}
				}
			}
			if (n.nodes.length > 0) {
				setAnims(n, drawMode, drawCall, owner);
			}

			if (!n.immortal) {
				n.age = drawCall;
			}
		}
		if (!isNil(endFunction)) {
			setTimeout(function(){
				if (!isNil(endFunctionParams)) {
					endFunction.apply(this, endFunctionParams);
				} else {
					endFunction();
				}
			}, 0);
		}
	};

	var cleanNodes = function(node, afterAnim, deleteIt, endFunction, endFunctionParams) {
		node.nodes.forEach(function(n) {
			if (!isNil(n)) {
				if (n.deleteMe || (n.deleteMeAfterAnim && afterAnim) || deleteIt) {
					if (n.nodes.length > 0) {
						cleanNodes(n, afterAnim, true);
					}
					node.removeNode(n.id);
				} else {
					if (n.nodes.length > 0) {
						cleanNodes(n, afterAnim);
					}
				}
			}
		});
		if (!isNil(endFunction)) {
			// setTimeout(function(){
				if (!isNil(endFunctionParams)) {
					endFunction.apply(this, endFunctionParams);
				} else {
					endFunction();
				}
			// }, 0);
		}
	};



	var updateElms = function(node, endFunction, endFunctionParams) {
		//update on-screen elements - can only be done after DOM elements have been created for some updates:
		for (var i = 0, nn = node.nodes.length; i < nn; i++) {
			var n = node.nodes[i];
			var elm = get(n.id, n.domElm);
			if (!isNil(elm)) {
				if (elm._linkedAxis && elm.axisVal) {
					if (elm._linkedAxis._horizontal) {
						eve.setSvgTransform(elm, "translate", elm._linkedAxis.getVal(elm.axisVal, true) - elm._offsetX);
					} else {
						eve.setSvgTransform(elm, "translate", [null, elm._linkedAxis.getVal(elm.axisVal, true) - elm._offsetY]);
					}
				}
				if (!isNil(elm._intNodes) || !isNil(elm._children)) {
					updateChildren(elm);
				}
			}
			if (n.nodes.length > 0) {
				updateElms(n);
			}
		}
		if (!isNil(endFunction)) {
			setTimeout(function(){
				if (!isNil(endFunctionParams)) {
					endFunction.apply(this, endFunctionParams);
				} else {
					endFunction();
				}
			}, 0);
		}
	};

	var getTextWidthPDF = function(n, c){
		n._container.save();
		var t, tWidth = [], fSpan = null;
		for (t = 0; t < n.nodes.length; t++){
			if (!isNil(n.nodes[t].eX)){
				if (fSpan !== null){
					fSpan.lineWidth = tWidth[tWidth.length - 1];
				}
				fSpan = n.nodes[t];
				tWidth.push(0);
			}
			var tAttrs = getCumTextAttrs(n.nodes[t]);
			if (c.fontNormal){
				n._container.font(c.fontNormal);
			}
			if (tAttrs.weight && tAttrs.weight === "bold" && c.fontBold){
				n._container.font(c.fontBold);
			}
			if (!isNil(tAttrs.family) && tAttrs.family !== "inherit" && tAttrs.family !== "auto"){
				n._container.font(tAttrs.family);
			}
			n._container.fontSize(tAttrs.size);
			tWidth[tWidth.length - 1] += n._container.widthOfString(n.nodes[t].text);
		}
		if (fSpan !== null){
			fSpan.lineWidth = tWidth[tWidth.length - 1];
		}
		var ret = 0;
		for (t = 0; t < tWidth.length; t++){
			if (ret < tWidth[t]){
				ret = tWidth[t];
			}
		}
		n._container.restore();
		return ret;
	};

	var drawElms = function(node, parent, duration, drawMode, drawCall, defs, owner, c, rejigOnly, endFunction, endFunctionParams) {
		for (var i = 0; i < node.nodes.length; i++) {

		// function drawElmLoop(){
			var n = node.nodes[i];
			var elmType;

			if (drawMode === "SVG") {
				elmType = n.type;
				if (elmType === "group") {
					elmType = "g";
				}
				if (elmType === "sector") {
					elmType = "path";
				}

				var newElm = get(n.id, owner);

				if (isNil(newElm)) {
					newElm = document.createElementNS(svgns, elmType);
					newElm.id = n.id;
					newElm._age = n.age;
					newElm.exists = false;
				} else {
					if (n.age < drawCall && n.age !== 0) {
						if (!isNil(n._animDeath) && !isNil(n._animDeath.mode)) {
							//put sector animate away code here
							n.deleteMeAfterAnim = true;
							n._animCurrent = n._animDeath;
						} else {
							if (!n.getProp(n, "deleteMe") && !n.getProp(n, "deleteMeAfterAnim")) {
								n.deleteMe = true;
								n.domElm = null;
								fadeRemove(newElm, n._animCurrent.duration, n._animCurrent.delay);
							} else {
								n.deleteParent = true;
							}
						}
					} else {
						newElm.exists = true;
						newElm._age = n.age;
					}
				}

				if (isNil(newElm.mouseOver) && n.mouseOver.length > 0) {
					newElm.mouseOver = [];
					for (var moe = 0; moe < n.mouseOver.length; moe++) {
						if (!n.mouseOver[moe].added) {
							newElm.mouseOver[moe] = n.mouseOver[moe];
							n.mouseOver[moe].added = true;
						}
					}
					eve.addEventHandler(newElm, "mouseover", function(evt) {
						for (var mo = 0; mo < evt.currentTarget.mouseOver.length; mo++) {
							evt.currentTarget.mouseOver[mo].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseOver[mo].params));
						}
					});
					eve.addEventHandler(newElm, "touchstart", function(evt) {
						evt.preventDefault();
						for (var ts = 0; ts < evt.currentTarget.mouseOver.length; ts++) {
							evt.currentTarget.mouseOver[ts].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseOver[ts].params));
						}
					});
				}
				if (isNil(newElm.mouseLeave) && n.mouseLeave.length > 0) {
					newElm.mouseLeave = [];
					for (var mle = 0; mle < n.mouseLeave.length; mle++) {
						if (!n.mouseLeave[mle].added) {
							newElm.mouseLeave[mle] = n.mouseLeave[mle];
							n.mouseLeave[mle].added = true;
						}
					}
					eve.addEventHandler(newElm, "mouseleave", function(evt) {
						for (var ml = 0; ml < evt.currentTarget.mouseLeave.length; ml++) {
							evt.currentTarget.mouseLeave[ml].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseLeave[ml].params));
						}
					});
				}
				if (isNil(newElm.mouseDown) && n.mouseDown.length > 0) {
					newElm.mouseDown = [];
					for (var mde = 0; mde < n.mouseDown.length; mde++) {
						if (!n.mouseDown[mde].added) {
							newElm.mouseDown[mde] = n.mouseDown[mde];
							n.mouseDown[mde].added = true;
						}
					}
					eve.addEventHandler(newElm, "mousedown", function(evt) {
						for (var md = 0; md < evt.currentTarget.mouseDown.length; md++) {
							evt.currentTarget.mouseDown[md].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseDown[md].params));
						}
					});
					eve.addEventHandler(newElm, "touchstart", function(evt) {
						evt.preventDefault();
						for (var tsd = 0; tsd < evt.currentTarget.mouseDown.length; tsd++) {
							evt.currentTarget.mouseDown[tsd].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseDown[tsd].params));
						}
					});
				}
				if (isNil(newElm.mouseUp) && n.mouseUp.length > 0) {
					newElm.mouseUp = [];
					for (var mue = 0; mue < n.mouseUp.length; mue++) {
						if (!n.mouseUp[mue].added) {
							newElm.mouseUp[mue] = n.mouseUp[mue];
							n.mouseUp[mue].added = true;
						}
					}
					eve.addEventHandler(newElm, "mouseup", function(evt) {
						for (var mu = 0; mu < evt.currentTarget.mouseUp.length; mu++) {
							evt.currentTarget.mouseUp[mu].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseUp[mu].params));
						}
					});
					eve.addEventHandler(newElm, "touchend", function(evt) {
						evt.preventDefault();
						for (var te = 0; te < evt.currentTarget.mouseUp.length; te++) {
							evt.currentTarget.mouseUp[te].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseUp[te].params));
						}
					});
				}
				if (isNil(newElm.mouseMove) && n.mouseMove.length > 0) {
					newElm.mouseMove = [];
					for (var mme = 0; mme < n.mouseMove.length; mme++) {
						if (!n.mouseMove[mme].added) {
							newElm.mouseMove[mme] = n.mouseMove[mme];
							n.mouseMove[mme].added = true;
						}
					}
					eve.addEventHandler(newElm, "mousemove", function(evt) {
						for (var mm = 0; mm < evt.currentTarget.mouseMove.length; mm++) {
							evt.currentTarget.mouseMove[mm].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseMove[mm].params));
						}
					});
					eve.addEventHandler(newElm, "touchmove", function(evt) {
						evt.preventDefault();
						for (var tm = 0; tm < evt.currentTarget.mouseMove.length; tm++) {
							evt.currentTarget.mouseMove[tm].functionName.apply(evt.currentTarget, [evt, evt.currentTarget].concat(evt.currentTarget.mouseMove[tm].params));
						}
					});
				}

				if (!n._draggable) {
					newElm.oX = newElm.eX;
					newElm.oY = newElm.eY;
					newElm.oWidth = newElm.eWidth;
					newElm.oHeight = newElm.eHeight;
					// newElm.oRot = newElm.eRot || [0, 0, 0];
					newElm.oRot = newElm.eRot;
					newElm.oScale = newElm.eScale;
					newElm.oFill = newElm.eFill;
					newElm.oStroke = newElm.eStroke;
				} else {
					newElm._plotHeight = n._plotHeight;
					newElm._plotWidth = n._plotWidth;
					newElm._plotOriginX = n._plotOriginX;
					newElm._plotOriginY = n._plotOriginY;
				}

				if (strIs(newElm.tagName, "line")) {
					n.oX1 = newElm.oX1 = newElm.getAttribute("x1");
					n.oY1 = newElm.oY1 = newElm.getAttribute("y1");
					n.oX2 = newElm.oX2 = newElm.getAttribute("x2");
					n.oY2 = newElm.oY2 = newElm.getAttribute("y2");
					newElm.eX1 = n.getAttribute("x1");
					newElm.eY1 = n.getAttribute("y1");
					newElm.eX2 = n.getAttribute("x2");
					newElm.eY2 = n.getAttribute("y2");
				}

				if (!n.deleteMe && !n.deleteParent) {

					var defsElm = get(defs.id, owner._container);

					newElm.eX = n.eX; // || 0;
					newElm.eY = n.eY; // || 0;
					newElm.eWidth = n.eWidth || 0;
					newElm.eHeight = n.eHeight || 0;
					newElm.eRot = n.eRot || [0, 0, 0];
					newElm.eScale = n.eScale || 1;
					newElm.eGradient = n.eGradient;

					if (n._animCurrent.mode === "" || n._animCurrent.mode === "none" || isNil(n._animCurrent.mode)) {
						if (!isNil(newElm.eWidth)) {
							newElm.setAttribute("width", newElm.eWidth);
						}
						if (!isNil(newElm.eHeight)) {
							newElm.setAttribute("height", newElm.eHeight);
						}
					}

					newElm._eveElm = n;
					if (!isNil(n.oldStrokeGradient)) {
						newElm.oldStrokeGradient = n.oldStrokeGradient;
					}
					if (!isNil(n.strokeGradient)) {
						newElm.strokeGradient = n.strokeGradient;
					}
					if (!isNil(n.dashArray)) {
						newElm.dashArray = n.dashArray;
					}
					if (!isNil(n.dStyle)) {
						newElm.dStyle = n.dStyle;
					}
					if (!isNil(n._innerText)) {
						newElm._innerText = n._innerText;
					}
					if (!isNil(n._buildOptions)) {
						newElm._buildOptions = n._buildOptions;
					}
					if (!isNil(n._textElm)) {
						newElm._textElm = n._textElm;
					}
					if (!isNil(n._offsetX)) {
						newElm._offsetX = n._offsetX;
					}
					if (!isNil(n._offsetY)) {
						newElm._offsetY = n._offsetY;
					}
					if (!isNil(n._linkedAxis)) {
						newElm._linkedAxis = n._linkedAxis;
					}
					if (!isNil(n._seriesObj)) {
						newElm._seriesObj = n._seriesObj;
					}
					if (!isNil(n._seriesObjType)) {
						newElm._seriesObjType = n._seriesObjType;
					}
					if (!isNil(n._labelObj)) {
						newElm._labelObj = n._labelObj;
						if (!isNil(n._labelObj._highlight)) {
							newElm._highlight = n._labelObj._highlight;
						}
					}
					if (n.nodeProps){
						newElm.nodeProps = n.nodeProps;
					}
					if (!isNil(n.interpolation)) {
						newElm.interpolation = n.interpolation;
					}
					if (!isNil(n.direction)) {
						newElm.direction = n.direction;
					}
					if (!isNil(n._intNodes)) {
						newElm._intNodes = n._intNodes;
					}
					if (!isNil(n.clockwise)) {
						newElm.clockwise = n.clockwise;
					}
					if (!isNil(n.series)) {
						newElm.series = n.series;
					}
					if (!isNil(newElm.path)) {
						newElm.oPath = newElm.path;
					} else if (!isNil(n.oPath)) {
						newElm.oPath = n.oPath;
					}
					//if (!isNil(n.oPath)){newElm.oPath=n.oPath;}
					if (!isNil(newElm.points)) {
						newElm.oPoints = newElm.points;
					}
					if (!isNil(n.path)) {
						newElm.path = n.path;
					}
					if (!isNil(n.pathTop)) {
						newElm.pathTop = n.pathTop;
					}
					if (!isNil(n.pathBottom)) {
						newElm.pathBottom = n.pathBottom;
					}
					if (!isNil(n.points)) {
						newElm.points = n.points;
					}

					if (!isNil(newElm.sector)) {
						newElm.oSector = newElm.sector;
					}
					if (!isNil(n.oSector)) {
						newElm.oSector = n.oSector;
					}
					if (!isNil(n.sector)) {
						newElm.sector = n.sector;
					}
					if (!isNil(n._hoverOffset)) {
						newElm._hoverOffset = n._hoverOffset;
					}

					if (!isNil(n.hLine)) {
						newElm.hLine = n.hLine;
					}
					if (!isNil(n.vLine)) {
						newElm.vLine = n.vLine;
					}
					if (!isNil(n.tooltipID)) {
						newElm.tooltipID = n.tooltipID;
					}
					if (!isNil(n.endX)) {
						newElm.endX = n.endX;
					}
					if (!isNil(n.endY)) {
						newElm.endY = n.endY;
					}

					if (!isNil(n._rotateCentreX)) {
						newElm._rotateCentreX = n._rotateCentreX;
					}
					if (!isNil(n._rotateCentreY)) {
						newElm._rotateCentreY = n._rotateCentreY;
					}
					if (!isNil(n._referenceObj)) {
						newElm._referenceObj = n._referenceObj;
					}
					if (!isNil(n._xAxisStart)) {
						newElm._xAxisStart = n._xAxisStart;
					}
					if (!isNil(n._xAxisRatio)) {
						newElm._xAxisRatio = n._xAxisRatio;
					}
					if (!isNil(n._xAxisMin)) {
						newElm._xAxisMin = n._xAxisMin;
					}
					if (!isNil(n._yAxisStart)) {
						newElm._yAxisStart = n._yAxisStart;
					}
					if (!isNil(n._xAxisMax)) {
						newElm._xAxisMax = n._xAxisMax;
					}
					if (!isNil(n._xAxisOrigin)) {
						newElm._xAxisOrigin = n._xAxisOrigin;
					}
					if (!isNil(n._yAxisRatio)) {
						newElm._yAxisRatio = n._yAxisRatio;
					}
					if (!isNil(n._yAxisMin)) {
						newElm._yAxisMin = n._yAxisMin;
					}
					if (!isNil(n._yAxisMax)) {
						newElm._yAxisMax = n._yAxisMax;
					}
					if (!isNil(n._yAxisOrigin)) {
						newElm._yAxisOrigin = n._yAxisOrigin;
					}
					if (!isNil(n._xChartOffset)) {
						newElm._xChartOffset = n._xChartOffset;
					}
					if (!isNil(n._yChartOffset)) {
						newElm._yChartOffset = n._yChartOffset;
					}

					if (!isNil(n.bandline)) {
						newElm.bandline = n.bandline;
					}
					if (!isNil(n.handle)) {
						newElm.handle = n.handle;
					}
					if (!isNil(n.output)) {
						newElm.output = n.output;
					}
					if (!isNil(n._plotHeight)) {
						newElm._plotHeight = n._plotHeight;
					}
					if (!isNil(n._plotWidth)) {
						newElm._plotWidth = n._plotWidth;
					}
					if (!isNil(n._plotOriginX)) {
						newElm._plotOriginX = n._plotOriginX;
					}
					if (!isNil(n._plotOriginY)) {
						newElm._plotOriginY = n._plotOriginY;
					}
					if (!isNil(n.curve)) {
						newElm.curve = n.curve;
					}

					if (!isNil(n.rad)) {
						newElm.rad = n.rad;
					}
					if (!isNil(n.radHover)) {
						newElm.radHover = n.radHover;
					}

					if (!isNil(n._snaps)) {
						newElm._snaps = n._snaps;
					}

					if (n.type === "path") {
						newElm.interpolation = n.interpolation;
						newElm.path = n.path;

						if (!isNil(n.segments) && n.segments.length > 0 && (isNil(n.getStyle("stroke-dasharray")) || n.getStyle("stroke-dasharray").indexOf(" ") === -1)) {

							var pathReversed = false;
							if ((getEveElmByID(n.segments[0]._series)._domain === getEveElmByID(getEveElmByID(n.segments[0]._series)._axisX)._domain && getEveElmByID(getEveElmByID(n.segments[0]._series)._axisX)._origin === "right") ||
								(getEveElmByID(n.segments[0]._series)._domain === getEveElmByID(getEveElmByID(n.segments[0]._series)._axisY)._domain && getEveElmByID(getEveElmByID(n.segments[0]._series)._axisY)._origin === "top")) {
								pathReversed = true;
							}

							//get length of segments:
							var cumulativeLength = 0, s, seg, segPath;
							if (!isNil(n.interpolation) && n.interpolation !== "" && n.interpolation !== "linear") {
								//we need at least 3 points to make a bezier, so do all the segments apart from the last one:
								for (s = 0; s < n.segments.length - 1; s++) {
									seg = document.createElementNS(svgns, "path");
									segPath = new eve.path(null, false);
									if (!isNil(n.path.commands[s])){
										segPath.commands.push(cloneEveObj(n.path.commands[s]));
									}
									if (!isNil(n.path.commands[s + 1])){
										segPath.commands.push(cloneEveObj(n.path.commands[s + 1]));
									}
									seg.setAttribute("d", segPath.toSVG());
									n.segments[s].segLength = seg.getTotalLength();
									cumulativeLength += n.segments[s].segLength;
								}
								//and then get the length of the last one by subtracting the cumulative length of the others from the total line:
								var segAll = document.createElementNS(svgns, "path");
								var segPathAll = n.path.toSVG();
								segAll.setAttribute("d", segPathAll);
								n.segments[n.segments.length - 1].segLength = segAll.getTotalLength() - cumulativeLength;
							} else {
								for (s = 0; s < n.segments.length; s++) {
									n.segments[s].segLength = lineLength(n.points[s], n.points[s + 1]);
									cumulativeLength += n.segments[s].segLength;
								}
							}

							//dashed lines:
							var strLineType = "";
							var continuous = 0;

							for (s = 0; s < n.segments.length; s++) {
								if (n.segments[s]._strokeDash.indexOf(",") !== -1) {
									var dashLength = parseFloat(n.segments[s]._strokeDash.split(",")[0].trim());
									var gapLength = parseFloat(n.segments[s]._strokeDash.split(",")[1].trim());
									var numDashes = n.segments[s].segLength / (gapLength + dashLength);
									numDashes = Math.floor(numDashes);
									if (eve.isOdd(numDashes)) {
										numDashes++;
									}
									var stepSize = n.segments[s].segLength / numDashes;
									var dashRatio = dashLength / (dashLength + gapLength);
									var gapRatio = gapLength / (dashLength + gapLength);

									continuous += (dashRatio * stepSize) / 2;
									strLineType += continuous.toString();
									continuous = 0;

									var strDash = ", ";
									for (var nd = 0; nd < numDashes - 1; nd++) {
										strDash += (gapRatio * stepSize).toString() + ", " + (dashRatio * stepSize).toString() + ", ";
									}
									strDash += (gapRatio * stepSize).toString() + ", ";
									continuous = (dashRatio * stepSize) / 2;
									strLineType += strDash;
								} else {
									continuous += n.segments[s].segLength;
								}
								if (s === n.segments.length - 1) {
									strLineType += continuous.toString();
								}
							}
							if (!isNil(newElm.dashArray)) {
								newElm.oldDashArray = newElm.dashArray;
							}
							newElm.dashArray = strLineType;
							if (n._animCurrent.mode === "draw" && duration > 0) {
								//to stop it appearing briefly before the tween begins:
								newElm.style.visibility = "hidden";
							}

							if (n.segments.length > 1) {
								//LINEAR GRADIENT :
								var sColour = n.segments[0]._strokeColour;
								var sOpacity = n.segments[0]._strokeOpacity;

								//create a linear gradient based on the element properties:
								var curGradient = new eve.linearGradient(0, 0, 1, 0);
								if (!isNil(n.segments[0]._series) && !isNil(n.segments[0]._series._direction)) {
									if (n.segments[0]._series._direction === "down") {
										curGradient.endX = 0;
										curGradient.endY = 1;
									}
								}

								var sPos = 0;
//								var curStops = [];
								//stop at 0 - start of gradient:
								curGradient.addStop(sColour, sOpacity, 0);
								if (pathReversed) {
									curGradient.stops[0].offset = 1;
								}
								//stop at end of segment - end of colour/opacity run:
								curGradient.addStop(sColour, sOpacity, 1 / n.segments.length);
								// if (pathReversed) {
								// 	curGradient.stops[1].offset = 1 - (1 / n.segments.length);
								// }
								if (!isNil(n.path)){
									var pathStartX = n.path.commands[0].to[0];
									var pathEndX = n.path.commands[n.path.commands.length - 1].to[0];
									var pathLengthX = Math.abs(pathEndX - pathStartX);
									curGradient.stops[1].offset = n.path.commands[1].to[0] / pathLengthX;
								}
								if (pathReversed) {
									curGradient.stops[1].offset = 1 - curGradient.stops[1].offset;
								}

//								var stopCount = 0;
								for (s = 1; s < n.segments.length; s++) {
									if (!isNil(n.path.commands[s])){
										sPos = n.path.commands[s].to[0] / pathLengthX;
									}
									if (pathReversed) {
										sPos = 1 - (s / n.segments.length);
									}
									if (n.segments[s]._strokeColour !== sColour || n.segments[s]._strokeOpacity !== sOpacity) {
//										stopCount++;
										//move the offset of the current stop to this point:
										curGradient.stops[curGradient.stops.length - 1].offset = sPos; // - (parseFloat(n.getStyle("strokeWidth")) / 2) / pathLengthX);
										//change the ongoing properties to the new ones:
										sColour = n.segments[s]._strokeColour;
										if (sColour === "auto"){
											sColour = n.segments[s]._series._strokeColour;
										}
										sOpacity = n.segments[s]._strokeOpacity;
										//and create a new start stop:
										curGradient.addStop(sColour, sOpacity, sPos);
										//and a new end stop:
										curGradient.addStop(sColour, sOpacity, sPos);
									}
								}
								//finally, move the last end stop:
								curGradient.stops[curGradient.stops.length - 1].offset = 1;
								if (pathReversed) {
									curGradient.stops[curGradient.stops.length - 1].offset = 0;
									curGradient.stops.reverse();
								}
								defsElm.appendChild(curGradient.toSVGGradient());

								if (!isNil(n.strokeGradient)){
									n.oldStrokeGradient = n.strokeGradient;
								}
								else{
									n.oldStrokeGradient = curGradient;
								}
								n.setStyle("stroke", "url(#" + n.oldStrokeGradient.id + ")");
								newElm.style.stroke = "url(#" + n.oldStrokeGradient.id + ")";
								n.strokeGradient = newElm.strokeGradient = curGradient;
								newElm.oldStrokeGradient = n.oldStrokeGradient;
							}
						}

						if (!newElm.exists) {
							newElm.setAttribute("d", n.path.toSVG());
							newElm.setAttribute("fill-rule", "evenodd");
						}
					}

					if (n.type === "sector") {
						newElm.setAttribute("d", n.sector.toSVG());
						newElm.setAttribute("fill-rule", "evenodd");
					}

					var eGrad;
					if (n.getStyle("fill") instanceof eve.parallelHatch){
						var eHatch = n.getStyle("fill");
						if (isNil(eHatch.id) || eHatch.id === ""){
							hatchNum++;
							eHatch.id = "eve_hatch_" + hatchNum;
						}
						var hat = eHatch.toSVGPattern();
						defsElm.appendChild(hat);
						newElm.eHatch = hat;
						if (!newElm.exists){
							newElm.style["fill"] = "url(#" + eHatch.id + ")";
						}
					}
					else if (n.getStyle("fill") instanceof eve.linearGradient){
						eGrad = n.getStyle("fill");
						if (isNil(eGrad.id) || eGrad.id === ""){
							gradNum++;
							eGrad.id = "eve_gradient_" + gradNum;
						}
						var fgrad = eGrad.toSVGGradient();
						defsElm.appendChild(fgrad);
						//n.setStyle("fill", "url(#" + grad.id + ")");
						newElm.eGradient = fgrad;
						if (!newElm.exists){
							newElm.style["fill"] = "url(#" + eGrad.id + ")";
						}
					}

					if (n.getStyle("stroke") instanceof eve.linearGradient){
						eGrad = n.getStyle("stroke");
						if (isNil(eGrad.id) || eGrad.id === ""){
							gradNum++;
							eGrad.id = "eve_gradient_" + gradNum;
						}
						var sgrad = eGrad.toSVGGradient();
						defsElm.appendChild(sgrad);
						//n.setStyle("fill", "url(#" + grad.id + ")");
						newElm.eGradient = sgrad;
						if (!newElm.exists){
							newElm.style["stroke"] = "url(#" + eGrad.id + ")";
						}
					}

					if (eve.isArray(n.className)) {
						newElm.setAttribute("class", n.className.join(" "));
					} else {
						if (n.className && n.className !== "") {
							newElm.setAttribute("class", n.className);
						}
					}

					var moveX = null;
					var moveY = null;
					var scale = 1;
					var rotate = 0;
					if (!isNil(n.eX)) {
						moveX = parseFloat(n.eX);
					} else {
						if (n.type !== "tspan") {
							n.eX = newElm.eX = 0;
						}
					}
					if (!isNil(n.eY)) {
						moveY = parseFloat(n.eY);
					} else {
						if (n.type !== "tspan") {
							n.eY = newElm.eY = 0;
						}
					}
					if (!isNil(n.eRot)) {
						rotate = n.eRot;
					}
					if (!isNil(n.eScale)) {
						scale = n.eScale;
					}
					if (!isNil(newElm.oX)) {
						moveX = parseFloat(newElm.oX);
					}
					if (!isNil(newElm.oY)) {
						moveY = parseFloat(newElm.oY);
					}
					if (!isNil(newElm.oRot)) {
						//rotate = parseFloat(newElm.oRot);
						rotate = newElm.oRot;
					}

					if (elmType !== "circle") {
						if (n.type === "tspan") {
							if (newElm.exists && !isNil(n.parent) && (n.parent._animCurrent.mode === "fadeDownChangeFadeUp" || n.parent._animCurrent.mode === "flipText")) {
								newElm.newX = n.eX;
								newElm.newY = n.eY;
								newElm.newRot = n.eRot;
							} else {
								if (!isNil(moveX)) {
									newElm.setAttribute("x", moveX.toFixed(6));
								}
								if (!isNil(moveY)) {
									newElm.setAttribute("y", moveY.toFixed(6));
								}
							}
						} else {
							// if (!isNil(n.outlineBox)) {
							// 	get(n.outlineBox.id, n._container).setAttribute("x", (moveX + n.outlineBox.offsetX).toFixed(6));
							// 	get(n.outlineBox.id, n._container).setAttribute("y", (moveY + n.outlineBox.offsetY).toFixed(6));
							// }
							if (!strIs(newElm.tagName, "defs")) {
								if (!(newElm.exists && (n._animCurrent.mode === "fadeDownChangeFadeUp" || n._animCurrent.mode === "flipText"))) {
									eve.setSvgTransform(newElm, "translate", [moveX, moveY]);
									if (eve.isArray(rotate)) {
										eve.setSvgTransform(newElm, "rotate", rotate);
									} else {
										eve.setSvgTransform(newElm, "rotate", [rotate]);
									}
									if (scale !== 1){
										eve.setSvgTransform(newElm, "scale", scale);
									}
								}
							}
						}
					} else {
						if (!isNil(moveX)){
							newElm.setAttribute("cx", moveX.toFixed(6));
						}
						if (!isNil(moveY)){
							newElm.setAttribute("cy", moveY.toFixed(6));
						}
					}

					//sometimes resizing a chart or updating a label changes it from multi-line to single line
					//when this happens, the later tspans don't get removed, so this removes all the tspans before new ones are created:
					if (rejigOnly){
						if (n.type === "text" && newElm.exists) {
							var tspans = newElm.getElementsByTagName("tspan");
							while (tspans.length > 0) {
								removeNode(tspans[0]);
							}
						}
					}

					var textNode;
					if (n.type === "text" && !isNil(n.text) && n.text !== "") {
						if (!newElm.exists) {
							textNode = document.createTextNode(n.text);
							newElm.appendChild(textNode);
						} else {
							newElm.firstChild.nodeValue = n.text;
						}
					}
					if (n.type === "tspan" && !isNil(n.text) && n.text !== "") {
						// if (newElm.exists && newElm.firstChild) {
						if (newElm.exists && !isNil(newElm.firstChild) && strIs(newElm.firstChild.nodeName, "#text")) {
							if (!isNil(n.parent) && n.parent._animCurrent.mode === "flipText") {
								//if we're going to animate the text, we need to keep it as it is - we'll add the new text to a var:
								// newElm.firstChild.oldText = newElm.firstChild.nodeValue;
								// newElm.firstChild.newText = n.text;
								newElm.oldText = newElm.firstChild.nodeValue;
								newElm.newText = n.text;
							} else if (!isNil(n.parent) && n.parent._animCurrent.mode === "fadeDownChangeFadeUp") {
								//if we're going to animate the parent down and then back up, we need to keep the text as it is - we'll add the new text to a var:
								// newElm.firstChild.newText = n.text;
								newElm.newText = n.text;
							} else {
								if (newElm.firstChild.nodeValue !== n.text) {
									newElm.firstChild.nodeValue = n.text;
								}
							}
						} else {
							textNode = document.createTextNode(n.text);
							newElm.appendChild(textNode);
							// newElm.firstChild.newText = n.text;
							newElm.newText = n.text;
						}
					}
					if (!newElm.exists) {
						parent.appendChild(newElm);
					}

					passProps(n, newElm);
					for (var an = 0, al = n.attributes.length; an < al; an++) {
						//text is not an attribute, it's a value
						var a = n.attributes[an];
						if (a.name !== "text") {
							newElm.setAttribute(a.name, a.value);
						}
					}

					for (var sn = 0, sl = n.styles.length; sn < sl; sn++) {
						var st = n.styles[sn];
						if (st.name) {
							if (st.name === "fill") {
								newElm.eFill = st.value;
							}
							if (st.name === "stroke") {
								newElm.eStroke = st.value;
							}
							if (st.name === "cursor") {
								//this is necessary because this attribute gets added with no value if we change the cursor - which stops the element from showing up!
								// newElm.setAttribute("systemLanguage", "en");
								//newElm.style.cursor = "url(data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" fill=\"red\" height=\"48\" viewBox=\"0 0 24 24\" width=\"48\"><path d=\"M0 0h24v24H0z\" fill=\"none\"/><path d=\"M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z\"/></svg>";
							}
							if (st.name === "transform") {
								newElm.style.webkitTransform = newElm.style.MozTransform = newElm.style.msTransform = newElm.style.OTransform = newElm.style.transform = st.value;
							} else {
								newElm.style[eve.camelToDashed(st.name)] = st.value;
							}
						}
					}
				}

			}
			else if (drawMode === "PDF") {

				n._container.save();
				n.pdfDrawn = false;

				elmType = n.type;

				var dims = getCumDims(n);
				var attrs = getCumAttrs(n);
				var pFill = attrs.fill;
				if (pFill[0].isPDFLinearGradient){
					pFill[0] = transposePDFGrad(pFill[0], n, dims);
				}
				var pStroke = attrs.stroke;
				if (pStroke[0].isPDFLinearGradient){
					pStroke[0] = transposePDFGrad(pStroke[0], n, dims);
				}

				var pStrokeWidth = attrs.strokeWidth;

				pFill[1] *= dims.opacity;
				pFill[1] *= dims.fillOpacity;
				pStroke[1] *= dims.opacity;
				pStroke[1] *= dims.strokeOpacity;

				n._container.lineWidth(pStrokeWidth);

				if (pFill[0] instanceof eve.parallelHatch && elmType !== "group"){
					var hX = 0, hY = 0, hWidth = n._container.page.width, hHeight = n._container.page.height;
					var hPad = 10;
					if (elmType === "rect" && !isNil(n.eWidth) && !isNil(n.eHeight)) {
						hWidth = n.eWidth + (hPad * 2);
						hHeight = n.eHeight + (hPad * 2);
					}
					hX = dims.x - hPad;
					hY = dims.y - hPad;
					n._container.save();
					if (elmType === "rect" && !isNil(n.eWidth) && !isNil(n.eHeight)) {
						n._container.rect(dims.x, dims.y, n.eWidth, n.eHeight).clip();
					}
					else if (elmType === "sector") {
//						n._container.rect(dims.x, dims.y, n.eWidth, n.eHeight).clip();
						n._container.translate(dims.x, dims.y);
						n._container.path(n.sector.toSVG()).clip();
					}
					pdfHatch(n._container, hX, hY, hWidth, hHeight, pFill[0].strokeColour, pFill[0].strokeWidth, pFill[0].strokeGap, pFill[0].rotation);
					n._container.restore();
				}

				if (elmType !== "tspan") {
					n._container.rotate(dims.rot[0], {origin: [dims.x + dims.rot[1], dims.y + dims.rot[2]]});
				}

				// if (!(pFill[0] instanceof eve.parallelHatch) && (!isNil(pStroke[0]) && pStroke[1] > 0 && pStrokeWidth > 0)){
				// if (elmType === "circle" || elmType === "rect" || elmType === "sector" || elmType === "path") {
						if (elmType === "circle") {
							n._container.circle(dims.x, dims.y, n.getAttribute("r"));
						}
						if (elmType === "line") {
							n._container.polygon([dims.x + parseFloat(n.getAttribute("x1")), dims.y + parseFloat(n.getAttribute("y1"))], [dims.x + parseFloat(n.getAttribute("x2")), dims.y + parseFloat(n.getAttribute("y2"))]);
						}
						if (elmType === "rect" && !isNil(n.eWidth) && !isNil(n.eHeight)) {
							n._container.rect(dims.x, dims.y, n.eWidth, n.eHeight);
						}
						if (elmType === "sector") {
							n._container.translate(dims.x, dims.y);
							n._container.path(n.sector.toSVG());
						}
						if (elmType === "path") {

							n._container.translate(dims.x, dims.y);
							n._container.scale(dims.scale);
							if (!isNil(n.segments)){
								for (var sg = 0; sg < n.segments.length; sg++){
									var sPath = new eve.path(null, false);
									sPath.commands[0] = cloneEveObj(n.path.commands[sg]);
									sPath.commands[0].type = "moveTo";
									sPath.commands[1] = cloneEveObj(n.path.commands[sg + 1]);

									if (n.segments[sg]._strokeDash !== "0"){
										var dash = n.segments[sg]._strokeDash.split(/[\s,]+/);
										n._container.path(sPath.toSVG()).dash(parseFloat(dash[0]), {space: parseFloat(dash[1])});
									}
									else{
										n._container.undash();
										n._container.strokeOpacity(pStroke[1]);
										n._container.path(sPath.toSVG());
									}
									if (n.segments[sg]._strokeColour !== "auto"){
										n._container.strokeOpacity(pStroke[1]);
										n._container.stroke(n.segments[sg]._strokeColour).undash();
									}
									else{
										n._container.stroke(pStroke[0]).undash();
									}
	//								n._container.undash();

								}
								n.pdfDrawn = true;
							}
							else{
								var pDash = n.getStyle("stroke-dasharray");
								if (!isNil(pDash) && pDash !== "0"){
									pDash = pDash.split(/[\s,]+/);
									n._container.path(n.path.toSVG()).dash(parseFloat(pDash[0]), {space: parseFloat(pDash[1])});
								}
								else{
									n._container.path(n.path.toSVG());
								}
							}
						}
					// }
					if (elmType === "text"){
						n.pdfTextWidth = getTextWidthPDF(n, c);
					}
					if (elmType === "tspan" && n.text !== "") {
						// n._container.translate(dims.x, dims.y);

						var tAttrs = null;
						var firstSpan = false;
						var continueTextAfter = false;
						if (!isNil(n.eX) && !isNil(n.eY)){
							firstSpan = true;
						}
						var pn, nIndex = -1;
						for (pn = 0; pn < n.parent.nodes.length; pn++){
							if (n.parent.nodes[pn] === n){
								nIndex = pn;
								break;
							}
						}

						tAttrs = getCumTextAttrs(n);
						if (c.fontNormal){
							n._container.font(c.fontNormal);
						}
						if (tAttrs.weight && tAttrs.weight === "bold" && c.fontBold){
							n._container.font(c.fontBold);
						}
						if (!isNil(tAttrs.family) && tAttrs.family !== "inherit"){
							n._container.font(tAttrs.family);
						}
						n._container.fontSize(tAttrs.size);

						if (firstSpan){
							n._container.x = dims.x;
							var tOptions = {width: n.lineWidth * 1.01};
							// n._container.y = dims.y;
							if (tAttrs.anchor === "end"){
								// tOptions.align = "right";
								n._container.x = dims.x - n.lineWidth;
							}
							else if(tAttrs.anchor === "middle"){
								// tOptions.align = "center";
								n._container.x = dims.x - (n.lineWidth / 2);
							}

							// n._container.y = dims.y - n._container.heightOfString(n.text, {width: tOptions.width + 10});
							var fontBaselineOffset = (n._container._font.ascender / 1000 * tAttrs.size);
							// fontBaselineOffset = 0;
							n._container.y = dims.y - fontBaselineOffset;
						}
						if (eve.fixAngleDegs(dims.rot[0]) === 270){
							// n._container.y = dims.y + fontBaselineOffset;
							// n._container.x += fontBaselineOffset;

							// if (n.id === "txt90_tspan1"){
							// 	n._container.x = 100;
							// }
							// if (n.id === "txt90_tspan2"){
							// 	n._container.x = 200;
							// }

						}
						if (eve.fixAngleDegs(dims.rot[0]) === 180){
							// n._container.y = dims.y + fontBaselineOffset;
						}
						if (eve.fixAngleDegs(dims.rot[0]) === 90){
							// n._container.y = dims.y;
							// n._container.x -= fontBaselineOffset;
						}

						// a tspan MUST have a <text> element as its parent
						var txtDims = getCumDims(n.parent);
						if (txtDims.rot[0] !== 0){
							n._container.rotate(txtDims.rot[0], {origin: [txtDims.x + txtDims.rot[1], txtDims.y + txtDims.rot[2]]});
						}

						if (!isNil(n.parent.nodes[nIndex + 1])){
							if (n.parent.nodes[nIndex + 1].eX === null){
								tOptions.continued = true;
								continueTextAfter = true;
								// tOptions.width = null;
							}
							else{
								tOptions.continued = false;
							}
						}
						else{
							tOptions.continued = false;
						}

						n._container.fillColor(tAttrs.fill).text(n.text, tOptions); //.fill(tAttrs.fill);
					}

					if (!n.pdfDrawn && n.type !== "group"){
						n._container.fillOpacity(pFill[1]);
						if (!isNil(pStroke[0]) && pStroke[1] > 0 && pStrokeWidth > 0){
							n._container.strokeOpacity(pStroke[1]);
							if (!isNil(pFill[0])){
								if (pFill[0] instanceof eve.parallelHatch){
									// n._container.fillAndStroke(pFill[0] + ", " + pStroke[0]);
									n._container.fillOpacity(0);
									n._container.fillAndStroke("white", pStroke[0]);
								}
								else{
									n._container.fillOpacity(pFill[1]);
									n._container.fillAndStroke(pFill[0], pStroke[0]);
								}
							}
							else{
								n._container.stroke(pStroke[0]);
							}
						}
						else if (!isNil(pFill[0])){
							if(pFill[0] instanceof eve.parallelHatch){
								n._container.fillOpacity(0);
								n._container.fill("white");
							}
							else{
								n._container.fillOpacity(pFill[1]);
								n._container.fill(pFill[0]);
							}
						}
					}
//					n._container.undash();
				// }


				if (!continueTextAfter){
					n._container.restore();
					n._container.save();
				}

			}

			if (n.nodes.length > 0) {
				// setTimeout(function(){
					drawElms(n, newElm, duration, drawMode, drawCall, defs, owner, c, rejigOnly);
				// }, 0);
			}

		}
		if (!isNil(endFunction)) {
			// setTimeout(function(){
				if (!isNil(endFunctionParams)) {
					endFunction.apply(this, endFunctionParams);
				} else {
					endFunction();
				}
			// }, 0);
		}
	};

	var tweakSvgForExport = function(elm, includeDashes) {
		var maxDashes = 6;
		var styleInline = true;
		var numAttrs = ["x", "y", "dx", "dy", "rx", "ry", "cx", "cy", "x1", "x2", "y1", "y2"];
		//overly complex dasharrays break Illustrator. Remove them:
		if (!isNil(elm.style)) {
			var elmCSS = window.getComputedStyle(elm);
			if (includeDashes !== true) {
				if (!isNil(elm.style.strokeDasharray) && elm.style.strokeDasharray !== "") {
					var numDashes = elm.style.strokeDasharray.split(" ").length;
					if (numDashes > maxDashes) {
						elm.style.strokeDasharray = null;
					}
				}
			}
			if (strIs(elm.tagName, "text")) {
				elm.style.fontFamily = elmCSS.fontFamily;
			}

			for (var a = 0; a < numAttrs.length; a++) {
				if (elm.getAttribute(numAttrs[a])) {
					elm.setAttribute(numAttrs[a], eve.setDecimal(parseFloat(elm.getAttribute(numAttrs[a])), 3));
				}
			}

			if (styleInline) {
				var fillOpacity;
				var fill = elmCSS.fill;
				if (fill.indexOf("rgba") === 0) {
					var rgba = fill.replace("rgba", "").replace("rgb", "").replace("(", "").replace(")", "").replace(/ /g, "");
					fillOpacity = parseFloat(rgba.split(",")[3]);
					if (fillOpacity === 0) {
						elm.style.fill = "none";
					} else {
						elm.style.fillOpacity = fillOpacity;
					}
				} else {
					if (!strIs(elm.tagName, "g")) {
						elm.style.fill = elmCSS.fill;
					}
				}

				if (!strIs(elm.tagName, "g")) {
					elm.style.fillOpacity = elmCSS.fillOpacity;
					elm.style.stroke = elmCSS.stroke;
					elm.style.strokeWidth = elmCSS.strokeWidth;
					elm.style.fontFamily = elmCSS.fontFamily;
					elm.style.fontSize = elmCSS.fontSize;
					elm.style.fontSize = elmCSS.fontSize;
					elm.style.opacity = elmCSS.opacity;
				}
			}
		}
		if (!isNil(elm.childNodes) && elm.childNodes.length > 0) {
			for (var n = 0; n < elm.childNodes.length; n++) {
				tweakSvgForExport(elm.childNodes[n], includeDashes);
			}
		}
	};

	var pdfHatch = function(doc, left, top, width, height, colour, strokeWidth, gap, rotation){

		var ang = eve.fixAngleDegs(rotation);
		if (ang > 90 && ang < 270){
			ang -= 180;
		}
		ang = eve.fixAngleDegs(ang);
		if (ang === 360){
			ang = 0;
		}

		if (ang === 90 || ang === 270){
			var xAt = left - (gap / 2);
			while(xAt < left + width){
				xAt += gap;
				doc.moveTo(xAt, top);
				doc.lineTo(xAt, top + height).lineWidth(strokeWidth).strokeColor(colour).stroke();
			}
		}
		else{
			var r = eve.toRadians(ang);
			var ver = Math.tan(r) * width;
			var hyp = gap / Math.cos(r);
			var drawHeight = height + ver;
			var yAt = top;
			if (ang > 0){
				yAt -= ver;
			}
			while(yAt < top + drawHeight){
				yAt += hyp;
				doc.moveTo(left, yAt);
				doc.lineTo(left + width, yAt + ver).lineWidth(strokeWidth).strokeColor(colour).stroke();
			}
		}

	};

	var transposePDFGrad = function(grad, elm, dims){
		var elmX = 0, elmY = 0, elmWidth = 1, elmHeight = 1;
		if (elm.type === "rect"){
			elmX = dims.x;
			elmY = dims.y;
			elmWidth = elm.eWidth;
			elmHeight = elm.eHeight;
		}
		else if(elm.type === "path"){
			var pSize = elm.path.getSize();
//			elmX = dims.x + pSize.left;
//			elmY = dims.y + pSize.top;
			// before drawing a path, we translate the document:
			elmX = pSize.left;
			elmY = pSize.top;
			elmWidth = pSize.width;
			elmHeight = pSize.height;
		}
		grad.x1 = elmX + (grad.x1 * elmWidth);
		grad.x2 = elmX + (grad.x2 * elmWidth);
		grad.y1 = elmY + (grad.y1 * elmHeight);
		grad.y2 = elmY + (grad.y2 * elmHeight);
		return grad;
	};

	var pdfColour = function(colour, doc){
		var clr = colour;
		if (eve.isArray(colour)){
			clr = colour[0];
		}
		if (clr === "transparent" || clr === "none"){
			return ["#FFFFFF", 0];
		}
		else if (clr === "auto"){
			return ["#FFFFFF", 0];
		}
		else if (clr instanceof eve.parallelHatch){
			return [clr, 1];
		}
		else if (clr instanceof eve.linearGradient){
			var lGrad = doc.linearGradient(clr.startX, clr.startY, clr.endX, clr.endY);
			lGrad.isPDFLinearGradient = true;
			for (var s = 0; s < clr.stops.length; s++){
				lGrad.stop(clr.stops[s].offset, clr.stops[s]. colour, clr.stops[s].opacity);
			}
			return [lGrad, 1];
		}
		else {
			var alpha = 1;
			if (clr.indexOf("rgba(") === 0){
				alpha = parseFloat(clr.split(",")[3].trim());
			}
			return [eve.colourToHex(clr), alpha];
		}
	};

	var getCumTextAttrs = function(n){
		var tFill = n.getStyle("fill");
		var tSize = parseFloat(n.getStyle("font-size"));
		var tWeight = n.getStyle("font-weight");
		var tFamily = n.getStyle("font-family");
		var tBaseline = n.getAttribute("alignmentBaseline");
		var tAnchor = n.getAttribute("textAnchor");

		var r = {fill: tFill, size: tSize, weight: tWeight, family: tFamily, baseline: tBaseline, anchor: tAnchor};

		if (n.type === "tspan"){
			tFill = n.parent.getStyle("fill");
			tSize = parseFloat(n.parent.getStyle("font-size"));
			tFamily = n.parent.getStyle("font-family");
			if (isNil(tWeight)){
				tWeight = n.parent.getStyle("font-weight");
			}
			tBaseline = n.parent.getAttribute("alignmentBaseline");
			tAnchor = n.parent.getAttribute("textAnchor");
			if (isNil(r.fill) && !isNil(tFill)){
				r.fill = tFill;
			}
			if ((isNil(r.size) || isNaN(r.size)) && !isNil(tSize)){
				r.size = tSize;
			}
			if (isNil(r.family) && !isNil(tFamily)){
				r.family = tFamily;
			}
			if (isNil(r.weight) && !isNil(tWeight)){
				r.weight = tWeight;
			}
			if (isNil(r.baseline) && !isNil(tBaseline)){
				r.baseline = tBaseline;
			}
			if (isNil(r.anchor) && !isNil(tAnchor)){
				r.anchor = tAnchor;
			}
		}
		return r;
	};

	var getCumAttrs = function(nod){
		var n = nod;
		var r = {fillColour: null, fillOpacity: null, stroke: null, strokeWidth: null};

		while(!isNil(n)){
			if (r.fillColour === null){
				if (n.getStyle("fill")){
					r.fillColour = pdfColour(n.getStyle("fill"), n._container)[0];
					if (n.getStyle("fill") === "transparent"){
						r.fillOpacity = 0;
					}
				}
				else if (n.getAttribute("fill")){
					r.fillColour = pdfColour(n.getAttribute("fill"), n._container)[0];
					if (n.getAttribute("fill") === "transparent"){
						r.fillOpacity = 0;
					}
				}
			}
			if (r.fillOpacity === null){
				if (n.getStyle("fillOpacity")){
					r.fillOpacity = n.getStyle("fillOpacity");
				}
				else if (n.getStyle("fill-opacity")){
					r.fillOpacity = n.getStyle("fill-opacity");
				}
				else if (n.getAttribute("fillOpacity")){
					r.fillOpacity = n.getAttribute("fillOpacity");
				}
				else if (n.getAttribute("fill-opacity")){
					r.fillOpacity = n.getAttribute("fill-opacity");
				}
			}
			if (r.stroke === null){
				if (n.getStyle("stroke")){
					r.stroke = pdfColour(n.getStyle("stroke"), n._container);
//					if (n.getStyle("strokeOpacity")){
//						r.stroke[1] = n.getStyle("strokeOpacity");
//					}
				}
				else if (n.getAttribute("stroke")){
					r.stroke = pdfColour(n.getAttribute("stroke"), n._container);
				}
			}
			if (r.strokeWidth === null){
				if (n.getStyle("strokeWidth")){
					r.strokeWidth = n.getStyle("strokeWidth");
				}
				else if (n.getStyle("stroke-width")){
					r.strokeWidth = n.getStyle("stroke-width");
				}
				else if (n.getAttribute("strokeWidth")){
					r.strokeWidth = n.getAttribute("strokeWidth");
				}
				else if (n.getAttribute("stroke-width")){
					r.strokeWidth = n.getAttribute("stroke-width");
				}
			}
			n = n.parent;
		}
		if (r.fillColour === null){
			if (r.fillOpacity === null){
				r.fill = ["#FFFFFF", 1];
			}
			else{
				r.fill = ["#FFFFFF", r.fillOpacity];
			}
		}
		else{
			if (r.fillOpacity === null){
				r.fill = [r.fillColour, 1];
			}
			else{
				r.fill = [r.fillColour, r.fillOpacity];
			}
		}
		if (r.stroke === null){
			r.stroke = ["#FFFFFF", 0];
		}
		if (r.strokeWidth === null){
			r.strokeWidth = 0;
		}
		return r;
	}

	var getCumDims = function(nod){
		var n = nod;
		var r = {x: 0, y: 0, rot: [0, 0, 0], opacity: 1, fillOpacity: 1, strokeOpacity: 1, scale: 1};
		while(!isNil(n)){
			if (!isNil(n.eX)){
				r.x += n.eX;
			}
			if (!isNil(n.eY)){
				r.y += n.eY;
			}
			if (!isNil(n.eScale)){
				r.scale *= n.eScale;
			}
			if (!isNil(n.eRot)){
				if (!eve.isArray(n.eRot)) {
					r.rot[0] += n.eRot;
				}
				else{
					r.rot[0] += (n.eRot[0] || 0);
					r.rot[1] += (n.eRot[1] || 0);
					r.rot[2] += (n.eRot[2] || 0);
				}
			}
			if (!isNil(n.getStyle("opacity"))){
				r.opacity *= n.getStyle("opacity");
			}
			else if (!isNil(n.getAttribute("opacity"))){
				r.opacity *= n.getAttribute("opacity");
			}

			if (!isNil(n.getStyle("strokeOpacity"))){
				r.strokeOpacity *= n.getStyle("strokeOpacity");
			}
			else if (!isNil(n.getAttribute("strokeOpacity"))){
				r.strokeOpacity *= n.getAttribute("strokeOpacity");
			}

			if (!isNil(n.getStyle("fillOpacity"))){
				r.fillOpacity *= n.getStyle("fillOpacity");
			}
			else if (!isNil(n.getAttribute("fillOpacity"))){
				r.fillOpacity *= n.getAttribute("fillOpacity");
			}

			n = n.parent;
		}
		return r;
	};

	var prettifyHTML = function(node, hLevel) {
		var level = hLevel || 0;
		var indentBefore = new Array(level++ + 1).join('  '),
			indentAfter = new Array(level - 1).join('  '),
			textNode;
		if (!isNil(node.children)) {
			for (var i = 0; i < node.children.length; i++) {
				textNode = document.createTextNode('\n' + indentBefore);
				node.insertBefore(textNode, node.children[i]);
				prettifyHTML(node.children[i], level);
				if (node.lastElementChild === node.children[i]) {
					textNode = document.createTextNode('\n' + indentAfter);
					node.appendChild(textNode);
				}
			}
		}
		return node;
	};

	eve.exportSVG = function(eSVG, id, includeDashes, returnAsString) {
		//alert(eSVG);
		var strSVG = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		strSVG += "<!-- Generator: Eve JS Library version 1.0  -->\n";
		strSVG += "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
		strSVG += "<svg version=\"1.1\" id=\"eveChart\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"" + eSVG.x.baseVal.value + "px\" y=\"" + eSVG.y.baseVal.value + "px\" viewBox=\"" + eSVG.viewBox.baseVal.x + " " + eSVG.viewBox.baseVal.y + " " + eSVG.viewBox.baseVal.width + " " + eSVG.viewBox.baseVal.height + "\" style=\"enable-background:new " + eSVG.viewBox.baseVal.x + " " + eSVG.viewBox.baseVal.y + " " + eSVG.viewBox.baseVal.width + " " + eSVG.viewBox.baseVal.height + ";\" xml:space=\"preserve\">\n";
		var svgCopy = eSVG.cloneNode(true);

		svgCopy.style.visibility = "hidden";
		//lose the export button:
		for (var n = 0; n < svgCopy.childNodes.length; n++) {
			if (svgCopy.childNodes[n].id.indexOf("button_export") !== -1) {
				removeNode(svgCopy.childNodes[n]);
			}
		}

		eSVG.parentNode.appendChild(svgCopy);

		tweakSvgForExport(svgCopy, includeDashes);

		var svgBody = svgCopy.innerHTML.replace(/&quot;/g, "'");
		strSVG += svgBody + "\n";
		strSVG += "</svg>";

		var fDiv = document.createElement('div');
		fDiv.innerHTML = strSVG.trim();
		strSVG = prettifyHTML(fDiv).innerHTML;

		removeNode(svgCopy);
		//remove whitespace between tspan tags - these cause extra empty text elements in Illustrator:
		strSVG = strSVG.replace(/\>[\s]+<tspan/g, "><tspan").replace(/\/tspan>[\s]+</g, "/tspan><");

		if (returnAsString) {
			return strSVG;
		}

		var svgFileName = id + ".svg";
		var a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";
		var json = strSVG,
			blob = new Blob([json], {
				type: "image/svg+xml;charset=utf-8"
			}),
			url = window.URL.createObjectURL(blob);
		a.href = url;
		a.download = svgFileName;
		a.click();
		window.URL.revokeObjectURL(url);
		removeNode(a);

	};

	eve.getSVG = function(parent, id, className, width, height) {
		if (isNil(parent)) {
			parent = get("body");
		}
		var svg = get(id, parent);
		if (!svg) {
			for (var pc = 0; pc < parent.childNodes.length; pc++) {
				if (parent.childNodes[pc].id === id){
					return parent.childNodes[pc];
				}
			}
			svg = document.createElementNS(svgns, "svg");
			svg.id = id;
			svg.setAttribute("class", className);
			svg.setAttribute("viewBox", "0 0 " + width + " " + height);
			svg.setAttribute("preserveAspectRatio", "none");
//			svg.style.width =  width + "px";
//			svg.style.height = height + "px";
			svg.style["-webkit-touch-callout"] = svg.style["-webkit-user-select"] = svg.style["-khtml-user-select"] = svg.style["-moz-user-select"] = svg.style["-ms-user-select"] = svg.style["user-select"] = "none";
			parent.appendChild(svg);
		}
		return svg;
	};

	return eve;
}));
